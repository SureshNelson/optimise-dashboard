//<!-- SideMenu -->
window.onload = function () {
    //$(function () {
    //    $('.tree li:has(ul)').addClass('parent_li');
    //    $('.tree li.parent_li > a').on('click', function (e) {
    //        var children = $(this).parent('li.parent_li').find(' > ul > li');
    //        if (children.is(":visible")) {
    //            children.hide('fast');
    //            $(this).find(' > i').addClass('glyphicon-plus-sign').removeClass('glyphicon-minus-sign');
    //        } else {
    //            children.show('fast');
    //            $(this).find(' > i').addClass('glyphicon-minus-sign').removeClass('glyphicon-plus-sign');
    //        }
    //        e.stopPropagation();
    //    });
    //});
};
$(document).ready(function () {
    /*
     *  Simple image gallery. Uses default settings
     */
    $('.fancybox').fancybox();
    /*
     *  Button helper. Disable animations, hide close button, change title type and content
     */
    $('.fancybox-buttons').fancybox({
        openEffect: 'fade',
        closeEffect: 'fade',

        prevEffect: 'elastic',
        nextEffect: 'elastic',

        closeBtn: false,

        helpers: {
            title: {
                type: 'inside'
            },
            buttons: {}
        },

        afterLoad: function () {
            this.title = 'No. ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
        },
        afterClose: function () {
            $(".pannelDiv").css("display", "block!important");
            $('#DAvailability').hide();
        }
    });

    // <!-- Menu Toggle Script -->
    $("#menu-toggle").click(function (e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
        $("#showPanel1").toggleClass("showPanel");
        $("#sideMenuBtn").toggleClass("showPanel");
        if ($.session.get('Availability') == 0)
            $("#showPanel2").toggleClass("showPanel");
        else
            $("#DTimeline").toggleClass("showPanel");

        $("#changeclass1").toggleClass("width3");
        $("#changeclass2").toggleClass("width3");
        $("#changeclass3").toggleClass("width3");

        $("#changeclass4").toggleClass("width4");
        $("#DivRanking").toggleClass("width4");
        $('#DAvailability').toggleClass("width4");

    });
    //<!-- Tooltip and popover Script -->
    $(".icon24").tooltip({
        placement: 'left',
        title: ''
    });
    $(function () { $('.popover-show').popover('show'); });
    $(function () { $('.popover-hide').popover('hide'); });
    $(function () { $(".target").popover({ html: true }); });

    $("#popup1Trigger").popover({
        html: true,
        content: function () {
            return $('#popup1').html();
        }
    });
    $("#popup2Trigger").popover({
        html: true,
        content: function () {
            return $('#popup2').html();
        }
    });
    $("#popup3Trigger").popover({
        html: true,
        content: function () {
            return $('#popup3').html();
        }
    });
});
