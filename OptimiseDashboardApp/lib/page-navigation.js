var ServiceURL = "https://test-optimise.marstongroup.co.uk:26443/OptimiseAPI/";
//var ServiceURL = "http://localhost:8068/";
var RedirectUrl = "http://localhost:8063/Admin/ITadmin.html";
var HrUrl = "http://localhost:8063/Admin/HRView.html";
var DataType = 'json';

$(document).ready(function () {
    $('#officerId,#password').keyup(function (e) {
        if (e.keyCode == 13)
            $('#btnLogin').trigger('click');
    });

    $('#btnLogin').click(function () {
        if ($('#officerId').val() == '') {
            // alert("Invalid OfficerId.");
            $('#lblUNPWDWarningInfo').html('Whoops! Invalid OfficerId.');
            $(".errormsg").show();
            return;
        }

        if ($('#password').val() == '') {
            //alert("Invalid Password.");
            $('#lblUNPWDWarningInfo').html('Whoops! Invalid Password.');
            $(".errormsg").show();
            return;
        }
        $('#imgLoginProcessing').show();
        GetToken();
    });
});

function GetToken() {
    $.ajax({
        type: 'POST',
        url: ServiceURL + 'token',
        data: {
            "grant_type": "password",
            "username": $('#officerId').val(),
            "password": $('#password').val()
        },
        dataType: DataType,
        success: function (data, textStatus, xhr) {
            //$.session.set('Token', data.access_token);
            //$.session.set('TokenType', data.token_type);
            //$.session.set('Token', data.access_token);
            $.session.set('TokenAuthorization', data.token_type + ' ' + data.access_token);
            $.session.set('OfficerID', $.trim($('#officerId').val()));
            $.session.set('Password', $.trim($('#password').val()));
            Loginnew();
        },
        error: function (xhr, textStatus, errorThrown) {
            //    alert('Error');
        }
    });
}

function Loginnew() {
    $.ajax({
        url: ServiceURL + 'api/v1/officers/' + $.session.get('OfficerID') + '/Login',
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        type: 'POST',
        dataType: DataType,
        success: function (data) {
            if (!(data == '' || data == null || data == '[]')) {
                $.session.set('OfficerName', data.OfficerName);
                $.session.set('CompanyID', data.CompanyID);
                $.session.set('tgVal', data.TargetNo);
                $.session.set('aref', data.AutoRefresh);
                $.session.set('RoleType', data.RoleType);
                $.session.set('OfficerRole', data.Officerrole);

                if (data.RoleType == 6) {
                    $.session.set('bc', 1);
                    $.session.set('IsVedio', 1);
                    window.top.location.href = HrUrl;// "HRView.html";
                }
                else if (data.RoleType == 5) {
                    $.session.set('bc', 1);
                    $.session.set('IsVedio', 1);
                    window.top.location.href = RedirectUrl;
                }
                else if (data.RoleType == 1) {
                    $.session.set('IsVedio', 1);
                    if (data.CompanyID == 1)
                        window.top.location = "Dashboard_MS.html";
                    else
                        window.top.location = "Dashboard_HC.html";
                }
                else if (data.RoleType == 7) {
                    if (data.CompanyID == 1)
                        window.top.location = "Dashboard_ES.html";
                    else
                        window.top.location = "Dashboard_HC.html";
                }
                else {
                    $.session.set('bc', 1);
                    $.session.set('IsVedio', 1);
                    if (data.CompanyID == 1)
                        window.top.location = "Dashboard_ES.html";// "Dashboard_MS.html";
                    else
                        window.top.location = "Dashboard_HC.html";
                }
            }
            else {
                $('#lblUNPWDWarningInfo').html("Officer ID and Password didnot match.");
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            // alert('Error');
        }
    });
}