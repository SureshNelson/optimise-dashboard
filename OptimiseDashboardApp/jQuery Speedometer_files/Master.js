﻿$j = jQuery;

$j(function () {

    // Attributions setup.
    $j(".greatestNation").dialog({
        autoOpen: false,
        width: "800px",
        title: "What's the greatest nation in the world?",
        minHeight: "300px",
        modal: true,
        open: function (e, target) {
            $j(".ui-widget-overlay").click(function () {
                $j(".attributions").dialog("close");
            });
        }
    });
    $j(".greatestNationLink").click(function (e) {
        e.preventDefault();
        $j(".greatestNation").dialog("open");
    });




    // Attributions setup.
    $j(".attributions").dialog({
        autoOpen: false,
        width: "600px",
        title: "Attributions",
        minHeight: "300px",
        modal: true,
        open: function (e, target) {
            $j(".ui-widget-overlay").click(function () {
                $j(".attributions").dialog("close");
            });
        }
    });
    $j(".attributionsLink").click(function (e) {
        e.preventDefault();
        $j(".attributions").dialog("open");
    });



//    // bigPanel centering.
//    $j(".bigPanelSet").each(function () {

//        var bigPanelSet = $j(this);
//        var bigPanels = bigPanelSet.find(".bigPanel");

//        var currentOffset = 0;
//        var currentRow = 0;
//        var row;


//        bigPanels.each(function (i) {
//            var panel = $j(this);
//            var offset = panel.offset().top;

//            // Create new panel row.
//            if (offset > currentOffset) {
//                currentRow++;
//                currentOffset = offset;

//                row = $j("<div></div>")
//                    .addClass("row" + currentRow);
//                row.appendTo(bigPanelSet);
//            }

//            panel.data("row", currentRow);

//        });

//        $j("<div class='clear'></div>").appendTo(bigPanelSet);

//        bigPanels.each(function () {
//            var panel = $j(this);
//            panel.appendTo(
//                bigPanelSet.find(
//                    ".row" + panel.data("row")
//            ));
//        });

//    });

});