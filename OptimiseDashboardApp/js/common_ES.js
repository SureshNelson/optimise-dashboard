$(document).ready(function () {
    /*
     *  Simple image gallery. Uses default settings
     */
    $('.fancybox').fancybox();
    /*
     *  Button helper. Disable animations, hide close button, change title type and content
     */
    $('.fancybox-buttons').fancybox({
        openEffect: 'fade',
        closeEffect: 'fade',
        prevEffect: 'elastic',
        nextEffect: 'elastic',
        closeBtn: false,
        helpers: {
            title: {
                type: 'inside'
            },
            buttons: {}
        },
        afterLoad: function () {
            this.title = 'No. ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
        },
        afterClose: function () {
            $(".pannelDiv").css("display", "block!important");
        }
    });

    // <!-- Menu Toggle Script -->
    $("#menu-toggle").click(function (e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
        $("#showPanel").toggleClass("showPanel");
        $("#sideMenuBtn").toggleClass("showPanel");

    });
    //<!-- Tooltip and popover Script -->
    $(".icon24").tooltip({
        placement: 'left',
        title: ''
    });
    //$(function () { $('.popover-show').popover('show'); });
    //$(function () { $('.popover-hide').popover('hide'); });
    //$(function () { $(".target").popover({ html: true }); });

    $("#popup2Trigger").popover({
        html: true,
        content: function () {
            return $('#popup2').html();
        }
    });
    $("#popup3Trigger").popover({
        html: true,
        content: function () {
            return $('#popup3').html();
        }
    });
});
