﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace OptimiseDashboardApp
{
    public static class JSONHelper
    {
        public static string FromDataTable(DataTable dt, bool IsforMap = false)
        {
            string rowDelimiter = "";

            StringBuilder result = new StringBuilder("[");
            foreach (DataRow row in dt.Rows)
            {
                result.Append(rowDelimiter);
                if (IsforMap)
                    result.Append(FromDataRow(row, row["color"].ToString()));
                else
                    result.Append(FromDataRow(row));

                rowDelimiter = ",";
            }
            result.Append("]");

            return result.ToString();
        }

        public static string FromDataRow(DataRow row, string Color = "_blank")
        {
            DataColumnCollection cols = row.Table.Columns;
            string colDelimiter = "";

            StringBuilder result = new StringBuilder("{");
            for (int i = 0; i < cols.Count; i++)
            { // use index rather than foreach, so we can use the index for both the row and cols collection
                result.Append(colDelimiter).Append("\"")
                      .Append(cols[i].ColumnName).Append("\":")
                      .Append(JSONValueFromDataRowObject(row[i], cols[i].DataType));

                colDelimiter = ",";
            }

            //TODO : call for icon img
            // icon: {image: "images/gmap_pin_orange.png", iconsize: [26, 46], iconanchor: [12,46] }
            if (!string.IsNullOrEmpty(Color) && Color != "_blank")
            {
                switch (Color)
                {

                    case "blue":
                        result.Append(colDelimiter).Append("\"icon\": { \"image\": \"Images/MapIcons/blue.png\", \"iconsize\": [26, 46], \"iconanchor\": [12,46] }");
                        break;
                    case "gray":
                        result.Append(colDelimiter).Append("\"icon\": { \"image\": \"Images/MapIcons/gray.png\", \"iconsize\": [26, 46], \"iconanchor\": [12,46] }");
                        break;
                    case "green":
                        result.Append(colDelimiter).Append("\"icon\": { \"image\": \"Images/MapIcons/green.png\", \"iconsize\": [26, 46], \"iconanchor\": [12,46] }");
                        break;
                    case "pink":
                        result.Append(colDelimiter).Append("\"icon\": { \"image\": \"Images/MapIcons/pink.png\", \"iconsize\": [26, 46], \"iconanchor\": [12,46] }");
                        break;
                    case "red":
                        result.Append(colDelimiter).Append("\"icon\": { \"image\": \"Images/MapIcons/red.png\", \"iconsize\": [26, 46], \"iconanchor\": [12,46] }");
                        break;
                    case "white":
                        result.Append(colDelimiter).Append("\"icon\": { \"image\": \"Images/MapIcons/white.png\", \"iconsize\": [26, 46], \"iconanchor\": [12,46] }");
                        break;
                    case "yellow":
                        result.Append(colDelimiter).Append("\"icon\": { \"image\": \"Images/MapIcons/yellow.png\", \"iconsize\": [26, 46], \"iconanchor\": [12,46] }");
                        break;

                }
            }


            result.Append("}");
            return result.ToString();
        }

        // possible types:
        // http://msdn.microsoft.com/en-us/library/system.data.datacolumn.datatype(VS.80).aspx
        private static Type[] numeric = new Type[] {typeof(byte), typeof(decimal), typeof(double), 
                                     typeof(Int16), typeof(Int32), typeof(SByte), typeof(Single),
                                     typeof(UInt16), typeof(UInt32), typeof(UInt64)};

        // I don't want to rebuild this value for every date cell in the table
        private static long EpochTicks = new DateTime(1970, 1, 1).Ticks;

        private static string JSONValueFromDataRowObject(object value, Type DataType)
        {
            // null
            if (value == DBNull.Value) return "null";

            // numeric
            if (Array.IndexOf(numeric, DataType) > -1)
                return value.ToString(); // TODO: eventually want to use a stricter format

            // boolean
            if (DataType == typeof(bool))
                return ((bool)value) ? "true" : "false";

            // date -- see http://weblogs.asp.net/bleroy/archive/2008/01/18/dates-and-json.aspx
            if (DataType == typeof(DateTime))
                return "\"\\/Date(" + new TimeSpan(((DateTime)value).ToUniversalTime().Ticks - EpochTicks).TotalMilliseconds.ToString() + ")\\/\"";

            // TODO: add Timespan support
            // TODO: add Byte[] support

            //TODO: this would be _much_ faster with a state machine
            // string/char  
            //return "\"" + value.ToString().Replace(@"\", @"\\").Replace("\n", "\\n").Replace("\"", @"\""") + "\"";
            return "\"" + value.ToString().Replace(@"\", @"\\").Replace("\r", "\\r").Replace("\r\n", "~break~").Replace("\n", "\\n").Replace("\"", @"\""").Replace("	", "") + "\"";
        }
    }

}