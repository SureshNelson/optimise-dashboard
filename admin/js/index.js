var selectedGroupID;
var selectedManagerID;
var returntype;
var mID;
var OfficerType;
var OfficerRole;
var Certificate;
var Role;
//var date;
//var TodayDate;
$(document).ready(function () {
    mID = getCookie("OfficerID");
    if (mID == 112 || mID == 9999) {
        $('#imgManagerAdd').show();
    }
    else {
        $('#imgManagerAdd').hide();
    }
    GetManagers();
    GetCompany();
    GetRoles(1);
    GetOfficerrole();
    $('#imgManagerAdd').click(function () {
        $(':text').val('');
        $('#lblErrMsg,#lblErrMsg1').html('');
        $('.id').show();
        $('.onlyforofficer').hide();
        $('#lblPwd').html('Password');
        $('#lblID').html('Manager id');
        $('#lblName').html('Manager name');
        $('#selRole1,#SelValidatedActions1').val(0).attr('selected', 'selected');
        GetRoles(0, 1, 1); //param - 1 : key, 2: Type, 3: Add or update
        AlertMessage("Add Manager", 1);
    });
    $('#imgGroupAdd').click(function () {
        $(':text').val('');
        $('#lblErrMsg,#lblErrMsg1').html('');
        $('.id').hide();
        $('.onlyforofficer').hide();
        $('#lblName').html('Group name');
        AlertMessage("Add Group", 2);
    });
    $('#imgOfficerAdd').click(function () {
        $(':text').val('');
        $('#lblErrMsg,#lblErrMsg1').html('');
        $('.id').show();
        $('.onlyforofficer').show();
        $('#lblPwd').html('Password');
        $('#lblID').html('Officer id');
        $('#lblName').html('Officer name');
        $('#SelOfficerrole,#selOffType,#selCompany,#selRole,#SelTeamleader,#SelCertificated,#SelValidatedActions').val(0).attr('selected', 'selected');
        GetRoles(0, 3, 1); //param - 1 : key, 2: Type, 3: Add or update                
        AlertMessage("Add Officer", 3);
    });
    $('.OfficerList').hide();
    GetOfficerListforGridView();
    $('#btnSearchOfficer').click(function () {
        GetOfficerListforGridView();
    });
    $('#lblOffList').click(function () {
        if ($.trim($('#lblOffList').html()) == "Officer List") {
            $('.OfficerList').show();
            $('.TeamAdmin').hide();
            $('#lblOffList').html('Team Admin');
        }
        else {
            $('.OfficerList').hide();
            $('.TeamAdmin').show();
            $('#lblOffList').html('Officer List');
        }
    });
});


function GetManagers() {
    $.ajax({
        type: "GET",
        url: serviceUrl + "/GetManagerDetailsAdmin?ManagerID=" + mID, // Location of the service
        contentType: "application/javascript", // content type sent to server
        dataType: "jsonp", //Expected data format from server       
        processdata: false, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
        },
        complete: function () {

        },
        success: function (msg) {//On Successfull service call  
            var result = JSON.parse(msg);

            $('#ULManagerList').empty();

            $.each(result, function (key, value) {

                $('#ULManagerList').append($('<li class="right clearfix">')
                    .append($('<span class="chat-img pull-right">').append($('<img src="img/edit.png" style="cursor:pointer" alt="edit" class="edit">').attr('id', 'ManagerEdit' + key).attr('onclick', 'GetRoles(' + key + ',' + 1 + ',2)')))
                    .append($('<div class="chat-body clearfix">').append($('<label>').attr('style', 'display:none;').attr('id', 'lblMgrID' + key).attr('Mgrid', $.trim(value.OfficerID)))
                                                                .append($('<label>').attr('style', 'display:none;').attr('id', 'lblMgrPwd' + key).attr('MgrPwd', $.trim(value.Password)))
                                                                .append($('<label>').attr('style', 'display:none;').attr('id', 'lblMgrRoleID' + key).attr('MgrRoleID', $.trim(value.RoleType)))
                                                                .append($('<label>').attr('style', 'display:none;').attr('id', 'lblMgrValidatedActions' + key).attr('MgrValidatedActions', $.trim(value.IsValidatedActions)))
                    .append($('<p style="cursor:pointer;">').attr('id', 'PMid' + key).attr('Mid', $.trim(value.id)).html(value.OfficerName + ' - ' + value.OfficerID)))
                    )

                if (key == 0) {
                    $('#ULManagerList').find('div').attr('id', 'SelectedManager');
                    $('#ULManagerList').find('div').css('background-color', '#D8A5D2');;
                    selectedManagerID = $('#PMid' + key).attr('Mid');
                    GetManagersGroups($('#ULManagerList').find('p').attr('Mid'));
                }

                $('#PMid' + key).click(function () {

                    $('#SelectedManager').removeAttr('style');
                    $('#SelectedManager').removeAttr('id');
                    $(this).parent().attr('id', 'SelectedManager');
                    $(this).parent().css('background-color', '#D8A5D2');

                    GetManagersGroups($(this).attr('Mid'));
                    selectedManagerID = $(this).attr('Mid');

                });
            });
        },
        error: function () {

        }
    });
}

function GetManagersGroups(Mid) {

    $.ajax({
        type: "GET",
        url: serviceUrl + "/GetGroupForManagerAdmin?ManagerID=" + Mid, // Location of the service
        contentType: "application/javascript", // content type sent to server
        dataType: "jsonp", //Expected data format from server       
        processdata: false, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
        },
        complete: function () {

        },
        success: function (msg) {//On Successfull service call  
            var result = JSON.parse(msg);
            if ($.trim(result) == '')
                $('#ULOfficersList').empty();
            $('#ULGroupList').empty();
            $.each(result, function (key, value) {
                //alert(value.GroupName);
                $('#ULGroupList').append($('<li class="right clearfix">')
                   .append($('<span class="chat-img pull-right">').append($('<img src="img/edit.png" style="cursor:pointer" alt="edit" class="edit">')
                        .attr('id', 'GroupEdit' + key).attr('onclick', 'UpdateUserandGroupName(' + key + ',' + 2 + ')')))
                   .append($('<div class="chat-body clearfix">').append($('<p style="cursor:pointer;">')
                        .attr('id', 'PGid' + key).attr('Gid', $.trim(value.GroupID)).html(value.GroupName)))
                   )
                if (key == 0) {
                    $('#ULGroupList').find('div').attr('id', 'SelectedGroup');
                    $('#ULGroupList').find('div').css('background-color', '#D8A5D2');
                    selectedGroupID = $('#PGid' + key).attr('Gid');
                    GetOfficers($('#ULGroupList').find('p').attr('Gid'));
                }
                $('#PGid' + key).mouseover(function () {

                    $('#SelectedGroup').removeAttr('style');
                    $('#SelectedGroup').removeAttr('id');
                    $(this).parent().attr('id', 'SelectedGroup');
                    $(this).parent().css('background-color', '#D8A5D2');
                    selectedGroupID = $(this).attr('Gid');
                    GetOfficers($(this).attr('Gid'));
                });
            });

            $("#ULGroupList p").draggable({
                appendTo: "body",
                helper: "clone"
            });

            $("#ULManagerList li").droppable({
                activeClass: "ui-state-default",
                hoverClass: "ui-state-hover",
                accept: "#ULGroupList p",
                drop: function (event, ui) {
                    UpdateGrouptoNewManager($(this).find('p').attr('mid'), ui.draggable.attr('gid'));
                }
            })

        },
        error: function () {

        }
    });
}

function GetOfficers(Gid) {

    $.ajax({
        type: "GET",
        url: serviceUrl + "/GetOfficerForGroupAdmin?GroupID=" + Gid, // Location of the service
        contentType: "application/javascript", // content type sent to server
        dataType: "jsonp", //Expected data format from server       
        processdata: false, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
        },
        complete: function () {

        },
        success: function (msg) {//On Successfull service call  
            var result = JSON.parse(msg);
            $('#ULOfficersList').empty();
            $.each(result, function (key, value) {
                //alert(value.GroupName);

                $('#ULOfficersList').append($('<li class="right clearfix">')
                   .append($('<span class="chat-img pull-right">').append($('<img src="img/edit.png" style="cursor:pointer" alt="edit" class="edit">').attr('id', 'OfficerEdit' + key).attr('onclick', 'GetRoles(' + key + ',' + 3 + ',2)')))
                   .append($('<div class="chat-body clearfix">')
                            .append($('<label>').attr('style', 'display:none;').attr('id', 'lblOffPwd' + key).attr('OffPwd', $.trim(value.Password)))
                            .append($('<label>').attr('style', 'display:none;').attr('id', 'lblOffEmpType' + key).attr('OffEmpType', $.trim(value.EmpType)))
                            .append($('<label>').attr('style', 'display:none;').attr('id', 'lblOffComID' + key).attr('OffComID', $.trim(value.CompanyID)))
                            .append($('<label>').attr('style', 'display:none;').attr('id', 'lblOffNote' + key).attr('OffNote', $.trim(value.Note)))
                            .append($('<label>').attr('style', 'display:none;').attr('id', 'lblOffMail' + key).attr('OffMail', $.trim(value.Email)))
                            .append($('<label>').attr('style', 'display:none;').attr('id', 'lblOffMob' + key).attr('OffMob', $.trim(value.Mobile)))
                            .append($('<label>').attr('style', 'display:none;').attr('id', 'lblOffRoleID' + key).attr('OffRoleID', $.trim(value.RoleType)))
                            .append($('<label>').attr('style', 'display:none;').attr('id', 'lblOffTeamcode' + key).attr('OffTeamcode', $.trim(value.Teamcode)))
                            .append($('<label>').attr('style', 'display:none;').attr('id', 'lblOfficerrole' + key).attr('Officerrole', $.trim(value.Officerrole)))
                            .append($('<label>').attr('style', 'display:none;').attr('id', 'lblOffCertificated' + key).attr('OffCertificated', $.trim(value.Certificated)))
                            .append($('<label>').attr('style', 'display:none;').attr('id', 'lblOffPostcode' + key).attr('OffPostcode', $.trim(value.Postcode)))
                            .append($('<label>').attr('style', 'display:none;').attr('id', 'lblOffTeamleader' + key).attr('OffTeamleader', $.trim(value.Teamleader)))
                            .append($('<label>').attr('style', 'display:none;').attr('id', 'lblOffValidatedActions' + key).attr('OffValidatedActions', $.trim(value.IsValidatedActions)))
                            .append($('<p style="cursor:pointer;">').attr('id', 'POid' + key).attr('Oid', $.trim(value.OfficerID)).html(value.OfficerName + ' - ' + value.OfficerID)))
                       )
                if (key == 0) {
                    $('#ULOfficersList').find('div').attr('id', 'SelectedOfficer');
                    $('#ULOfficersList').find('div').css('background-color', '#D8A5D2');
                }
                $('#POid' + key).mouseover(function () {
                    $('#SelectedOfficer').removeAttr('style');
                    $('#SelectedOfficer').removeAttr('id');
                    $(this).parent().attr('id', 'SelectedOfficer');
                    $(this).parent().css('background-color', '#D8A5D2');
                    //selectedGroupID = $(this).attr('Oid');
                    //GetOfficers($(this).attr('Oid'));
                });
            });

            try {
                $("#ULOfficersList div").draggable({
                    appendTo: "body",
                    cursor: 'move',
                    revert: "invalid",
                    helper: "clone"
                });

                $("#ULGroupList li").droppable({
                    activeClass: "ui-state-default",
                    hoverClass: "ui-state-hover",
                    accept: "#ULOfficersList div",
                    tolerance: "touch",
                    drop: function (event, ui) {
                        // ui.draggable.attr('Oid')
                        UpdateOfficertoNewGroup($(this).find('p').attr('Gid'), ui.draggable.text().substr(ui.draggable.text().indexOf('-') + 1));
                    }
                })

                $("#ULManagerList li").droppable({
                    activeClass: "ui-state-default",
                    hoverClass: "ui-state-hover",
                    accept: "#ULOfficersList div",
                    tolerance: "touch",
                    drop: function (event, ui) {
                        UpdateOfficertoNewManager($(this).find('p').attr('mid'), ui.draggable.text().substr(ui.draggable.text().indexOf('-') + 1));
                    }
                })

            } catch (e) {
                alert(e.message);
            }

        },
        error: function () {

        }
    });


}

function UpdateOfficertoNewManager(Mid, Oid) {

    $.ajax({
        type: "GET",
        url: serviceUrl + "/UpdateOfficerinManager?ManagerID=" + Mid + "&OfficerID=" + Oid, // Location of the service
        contentType: "application/javascript", // content type sent to server
        dataType: "jsonp", //Expected data format from server       
        processdata: false, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
        },
        complete: function () {

        },
        success: function (msg) {//On Successfull service call  
            GetOfficers(selectedGroupID);
        },
        error: function () {

        }
    });

}

function UpdateOfficertoNewGroup(Gid, Oid) {

    $.ajax({
        type: "GET",
        url: serviceUrl + "/UpdateOfficerinGroup?GroupID=" + Gid + "&OfficerID=" + Oid, // Location of the service
        contentType: "application/javascript", // content type sent to server
        dataType: "jsonp", //Expected data format from server       
        processdata: false, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
        },
        complete: function () {

        },
        success: function (msg) {//On Successfull service call  
            GetOfficers(selectedGroupID);
        },
        error: function () {

        }
    });

}

function UpdateGrouptoNewManager(Mid, Gid) {
    $.ajax({
        type: "GET",
        url: serviceUrl + "/UpdateGroupInManager?ManagerID=" + Mid + "&GroupID=" + Gid, // Location of the service
        contentType: "application/javascript", // content type sent to server
        dataType: "jsonp", //Expected data format from server       
        processdata: false, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (msg) {//On Successfull service call  
            GetManagersGroups(selectedManagerID);
        },
        error: function () {
        }
    });
}

function UpdateUserandGroupName(key, Type) {
    var Title, separate;

    $('#lblErrMsg1').html('');
    if (Type == 1) {
        separate = $('#PMid' + key).html().split('-');
        Title = "Update Manager Details";
        $('#txtOffName').val($.trim(separate[0]));
        $('#txtResetPwd').val($('#lblMgrPwd' + key).attr('MgrPwd'));
        $('#lblOffName').html('Manager Name');
        $('#lblMgrRoleID' + key).attr('MgrRoleID') == '' ? $('#selRole1').val(0).attr('selected', 'selected') : $('#selRole1').val($('#lblMgrRoleID' + key).attr('MgrRoleID')).attr('selected', 'selected');
        $('#lblMgrValidatedActions' + key).attr('MgrValidatedActions') == '' ? $('#SelValidatedActions1').val(0).attr('selected', 'selected') : ($('#lblMgrValidatedActions' + key).attr('MGrValidatedActions') == 'true' ? $('#SelValidatedActions1').val(1).attr('selected', 'selected') : $('#SelValidatedActions1').val(2).attr('selected', 'selected'));
        $('.pwd').show();
        $('.onlyforofficers').hide();
    }
    else if (Type == 2) {
        Title = 'Update Group Details';
        $('#txtOffName').val($('#PGid' + key).html());
        $('#lblOffName').html('Group Name');
        $('.pwd,.onlyforofficers').hide();
    }
    else {
        Title = 'Update Officer Details';
        separate = $('#POid' + key).html().split('-');
        $('#txtOffName').val($.trim(separate[0]));
        $('#txtResetPwd').val($('#lblOffPwd' + key).attr('OffPwd'));
        $('#lblOffName').html('Officer Name');
        $('.pwd,.onlyforofficers').show();

        $('#lblOffEmpType' + key).attr('OffEmpType') == '' ? $('#selOfficerType1').val(0).attr('selected', 'selected') : $('#selOfficerType1').val($('#lblOffEmpType' + key).attr('OffEmpType')).attr('selected', 'selected');
        $('#lblOffComID' + key).attr('OffComID') == '' ? $('#selCompany1').val(0).attr('selected', 'selected') : $('#selCompany1').val($('#lblOffComID' + key).attr('OffComID')).attr('selected', 'selected');
        $('#lblOffRoleID' + key).attr('OffRoleID') == '' ? $('#selRole1').val(0).attr('selected', 'selected') : $('#selRole1').val($('#lblOffRoleID' + key).attr('OffRoleID')).attr('selected', 'selected');
        $('#txtNote1').val($('#lblOffNote' + key).attr('OffNote'));
        $('#txtEmail1').val($('#lblOffMail' + key).attr('OffMail'));
        $('#txtMobile1').val($('#lblOffMob' + key).attr('OffMob'));

        $('#txtTeamcode1').val($('#lblOffTeamcode' + key).attr('OffTeamcode'));
        $('#lblOfficerrole' + key).attr('Officerrole') == '' ? $('#SelOfficerrole1').val(0).attr('selected', 'selected') : $('#SelOfficerrole1').val($('#lblOfficerrole' + key).attr('Officerrole')).attr('selected', 'selected');
        $('#lblOffCertificated' + key).attr('OffCertificated') == '' ? $('#SelCertificated1').val(0).attr('selected', 'selected') : ($('#lblOffCertificated' + key).attr('OffCertificated') == 'true' ? $('#SelCertificated1').val(1).attr('selected', 'selected') : $('#SelCertificated1').val(2).attr('selected', 'selected'));
        $('#txtPostcode1').val($('#lblOffPostcode' + key).attr('OffPostcode'));
        $('#lblOffTeamleader' + key).attr('OffTeamleader') == '' ? $('#SelTeamleader1').val(0).attr('selected', 'selected') : ($('#lblOffTeamleader' + key).attr('OffTeamleader') == 'true' ? $('#SelTeamleader1').val(1).attr('selected', 'selected') : $('#SelTeamleader1').val(2).attr('selected', 'selected'));
        $('#lblOffValidatedActions' + key).attr('OffValidatedActions') == '' ? $('#SelValidatedActions1').val(0).attr('selected', 'selected') : ($('#lblOffValidatedActions' + key).attr('OffValidatedActions') == 'true' ? $('#SelValidatedActions1').val(1).attr('selected', 'selected') : $('#SelValidatedActions1').val(2).attr('selected', 'selected'));
    }
    if (Type == 3) {

        $('#DAlertDialog').dialog({
            modal: true,
            resizable: false,
            width: "47%",
            "title": Title,
            buttons: {
                "Submit": function () {
                    if (UpdateUserandGroupName1(key, Type))
                        $(this).dialog("close");
                },
                "Block user": function () {
                    BlockUser(key);
                    $(this).dialog("close");
                }
            }
        });
    }
    else {
        var Width;
        if (Type == 1)
            Width = "47%";
        else
            Width = "27%";
        $('#DAlertDialog').dialog({
            modal: true,
            resizable: false,
            width: Width,
            "title": Title,
            buttons: {
                "Submit": function () {
                    if (UpdateUserandGroupName1(key, Type))
                        $(this).dialog("close");
                }
            }
        });
    }
    $('#btnResetPwd').click(function () {
        ResetPassword(key, Type);
    });
}

function UpdateUserandGroupName1(key, Type) {
    var LabelID, Attrib, pass = 0;
    if (Type == 1) {
        LabelID = 'lblMgrID';
        Attrib = 'Mgrid'
        if (($.trim($('#txtOffName').val()) == '') || ($('#selRole1').val() == 0) || ($('#SelValidatedActions1').val() == 0)) {
            $('#lblErrMsg1').html('Manager Name, Role, Accepted actions is compulsory.');
            pass = 1;
        }
    }
    else if (Type == 2) {
        LabelID = 'PGid';
        Attrib = 'Gid';
        if ($.trim($('#txtOffName').val()) == '') {
            $('#lblErrMsg1').html('Group name is compulsory');
            pass = 1;
        }
    }
    else {
        LabelID = 'POid';
        Attrib = 'Oid';
        if (($.trim($('#txtOffName').val()) == '') || ($('#selCompany1').val() == 0) || ($('#selOfficerType1').val() == 0) || ($('#selRole1').val() == 0) || ($('#SelOfficerrole1').val() == 0) || ($('#SelCertificated1').val() == 0) || ($('#SelTeamleader1').val() == 0) || ($('#SelValidatedActions1').val() == 0)) {
            $('#lblErrMsg1').html('Officer Name, OfficerType, Company, Role, Officer role, Certificated, Team leader and Accepted actions is compulsory.');
            pass = 1;
        }
    }
    if (pass == 0) {
        $.ajax({
            type: "GET",
            url: serviceUrl + "/UpdateUserandGroupName?Type=" + Type + "&ModifiedName=" + $('#txtOffName').val() + "&Id=" + $('#' + LabelID + key).attr(Attrib) +
                "&OfficerType=" + $('#selOfficerType1').val() + "&CompanyID=" + $('#selCompany1').val() + "&Note=" + $('#txtNote1').val() + "&Email=" +
                $('#txtEmail1').val() + "&Mobile=" + $('#txtMobile1').val() + "&Role=" + $('#selRole1').val() + "&Teamcode=" + $('#txtTeamcode1').val() +
                "&Postcode=" + $('#txtPostcode1').val() + "&Officerrole=" + $('#SelOfficerrole1').val() + "&Certificated=" + ($('#SelCertificated1').val() == 1 ? 1 : 0) +
                "&Teamleader=" + ($('#SelTeamleader1').val() == 1 ? 1 : 0) + "&Validatedactions=" + ($('#SelValidatedActions1').val() == 1 ? 1 : 0), // Location of the service
            contentType: "application/javascript", // content type sent to server
            dataType: "jsonp", //Expected data format from server       
            processdata: false, //True or False      
            async: true,
            timeout: 20000,
            beforeSend: function () {
            },
            complete: function () {
            },
            success: function (msg) {//On Successfull service call  
                if (Type == 1) {
                    GetManagers();
                }
                else if (Type == 2) {
                    GetManagersGroups(selectedManagerID);
                }
                else {
                    GetOfficers(selectedGroupID);
                }
            },
            error: function () {
            }
        });
        return true;
    }
    if (pass == 1)
        return false;
}

function BlockUser(key) {
    $('#bOfficerName').html($('#txtOffName').val());
    $('#DConfirmDialog').attr('title', 'Confirmation of blocking officer');
    $('#DConfirmDialog').dialog({
        modal: true,
        resizable: false,
        width: "30%",
        buttons: {
            "Yes": function () {
                $.ajax({
                    type: "GET",
                    url: serviceUrl + "/UpdateUserandGroupName?Type=" + 4 + "&ModifiedName=" + $('#txtOffName').val() + "&Id=" + $('#lblOfficerID' + key).html(), // Location of the service
                    contentType: "application/javascript", // content type sent to server
                    dataType: "jsonp", //Expected data format from server       
                    processdata: false, //True or False      
                    async: true,
                    timeout: 20000,
                    beforeSend: function () {
                    },
                    complete: function () {
                    },
                    success: function (msg) {//On Successfull service call  
                        GetManagers();
                    },
                    error: function () {
                    }
                });
                $(this).dialog("destroy").remove();
            },
            "No": function () {
                $(this).dialog("destroy").remove();
            }
        }
    });

}

function InsertManagerGroupOfficer(Type) {
    var pass = 0, oid, opwd;
    if (Type == 1) {
        if (($.trim($('#txtName').val()) == '') || ($.trim($('#txtID').val()) == '') || ($.trim($('#txtPwd').val()) == '') || ($('#selRole').val() == 0) || ($('#SelValidatedActions').val() == 0)) {
            $('#lblErrMsg').html('Manager Name, ID, Password, Role, Accepted actions is compulsory.');
            pass = 1;
        }
    }
    else if (Type == 3) {
        if (($.trim($('#txtName').val()) == '') || ($.trim($('#txtID').val()) == '') || ($.trim($('#txtPwd').val()) == '') || ($('#selCompany').val() == 0) || ($('#selOfficerType').val() == 0) || ($('#selRole').val() == 0) || ($('#SelTeamleader').val() == 0) || ($('#SelCertificated').val() == 0) || ($('#SelOfficerrole').val() == 0) || ($('#SelValidatedActions').val() == 0)) {
            $('#lblErrMsg').html('Officer Name, ID, Password, OfficerType, Company, Role, Team leader, Certificated, Officer role, Accepted actions is compulsory.');
            pass = 1;
        }
    }
    else {
        if ($.trim($('#txtName').val()) == '') {
            $('#lblErrMsg').html('Group name is compulsory');
            pass = 1;
        }
        $('#txtID,#txtPwd').val(0);
    }
    if (pass == 0) {
        $.ajax({
            type: "GET",
            url: serviceUrl + "/InsertManagerGroupOfficer?Name=" + $.trim($('#txtName').val()) + "&ManagerID=" + selectedManagerID + "&OfficerID=" + $.trim($('#txtID').val()) +
                                        "&OfficerPwd=" + $.trim($('#txtPwd').val()) + "&GroupID=" + selectedGroupID + "&Type=" + Type + "&OfficerType=" + $('#selOffType').val() +
                                        "&Company=" + $('#selCompany').val() + "&Note=" + $.trim($('#txtNote').val()) + "&Email=" + $.trim($('#txtEmail').val()) +
                                        "&Mobile=" + $.trim($('#txtMobile').val()) + "&Role=" + $('#selRole').val() + "&Teamcode=" + $.trim($('#txtTeamcode').val()) + "&Officerrole=" + $('#SelOfficerrole').val() +
                                        "&Certificated=" + ($('#SelCertificated').val() == 1 ? 1 : 0) + "&Postcode=" + $.trim($('#txtPostcode').val()) + "&Teamleader=" + ($('#SelTeamleader').val() == 1 ? 1 : 0) + "&Validatedactions=" + ($('#SelValidatedActions').val() == 1 ? 1 : 0), // Location of the service
            contentType: "application/javascript", // content type sent to server
            dataType: "jsonp", //Expected data format from server       
            processdata: false, //True or False      
            async: true,
            timeout: 20000,
            beforeSend: function () {
            },
            complete: function () {
            },
            success: function (msg) {//On Successfull service call  

                var result = JSON.parse(msg);
                $.each(result, function (key, value) {
                    if (value.Result == 'Success') {
                        if (Type == 1) {
                            GetManagers();
                            $('#DAddDialog').dialog("close");
                        }
                        else if (Type == 2) {
                            GetManagersGroups(selectedManagerID);
                            $('#DAddDialog').dialog("close");
                        }
                        else {
                            GetOfficers(selectedGroupID);
                            $('#DAddDialog').dialog("close");
                        }
                    }
                    else if (value.Result == 'AlreadyExists') {
                        if (Type == 1) {
                            $('#lblErrMsg').html('Manager ID already exists.');
                            returntype = false;
                        }
                        else if (Type == 2) {
                            $('#lblErrMsg').html('Group name already exists.');
                            returntype = false;
                        }
                        else {
                            $('#lblErrMsg').html('Officer ID already exists.');
                            returntype = false;
                        }
                    }

                });
            },
            error: function () {
                alert('error');
            }
        });
    }
}

function ResetPassword(key, Type) {
    $('#DResetDialog').dialog({
        modal: true,
        resizeable: false,
        width: "25%",
        "title": "Confirm",
        buttons: {
            "Yes": function () {
                var ofid;
                if (Type == 1)
                    ofid = $('#lblMgrID' + key).attr('Mgrid');
                else
                    ofid = $('#POid' + key).attr('Oid');
                $.ajax({
                    type: "GET",
                    url: serviceUrl + "/UpdatePassword?OfficerID=" + ofid, // Location of the service
                    contentType: "application/javascript", // content type sent to server
                    dataType: "jsonp", //Expected data format from server       
                    processdata: false, //True or False      
                    async: true,
                    timeout: 20000,
                    beforeSend: function () {
                    },
                    complete: function () {
                    },
                    success: function (msg) {//On Successfull service call  
                        var result = JSON.parse(msg);
                        $.each(result, function (key, value) {
                            if (value.Password != '') {
                                $('#txtResetPwd').val(value.Password);
                            }
                        });
                    },
                    error: function () {
                        alert('error');
                    }
                });
                $(this).dialog("close");
            },
            "No": function () {
                $(this).dialog("close");
            }
        }
    });


}

function AlertMessage(Title, Type) {
    var width;
    if (Type == 2)
        width = "25%";
    else
        width = "43%";
    $('#DAddDialog').dialog({
        modal: true,
        resizable: false,
        width: width,
        "title": Title,
        buttons: {
            "Add": function () {
                InsertManagerGroupOfficer(Type);
            }
        }
    })
}

function GetCompany() {
    $('#selCompany,#selCompany').empty();
    $.ajax({
        type: "GET",
        url: serviceUrl + "/GetCompany", // Location of the service
        contentType: "application/javascript", // content type sent to server
        dataType: "jsonp", //Expected data format from server       
        processdata: false, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (msg) {//On Successfull service call  

            var result = JSON.parse(msg);
            $.each(result, function (key, value) {
                if (key == 0)
                    $('#selCompany,#selCompany1').append($('<option>').attr('value', 0).html('Select'));
                $('#selCompany,#selCompany1').append($('<option>').attr('value', $.trim(value.CompanyID)).html(value.CompanyName));
            });
        },
        error: function () {
            alert('error');
        }
    });
}

function GetRoles(key, Type, Check) {
    $('#selRole,#selRole1').empty();
    $.ajax({
        type: "GET",
        url: serviceUrl + "/GetRoles", // Location of the service
        contentType: "application/javascript", // content type sent to server
        dataType: "jsonp", //Expected data format from server       
        processdata: false, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (msg) {//On Successfull service call  

            var result = JSON.parse(msg);
            if (Type == 1) {
                $.each(result, function (key, value) {
                    if (key == 0)
                        $('#selRole,#selRole1').append($('<option>').attr('value', 0).html('Select'));
                    if (value.RoleType == 'DepartmentHead' || value.RoleType == 'Manager')
                        $('#selRole,#selRole1').append($('<option>').attr('value', $.trim(value.RoleID)).html(value.RoleType));
                });
            }
            else {
                $.each(result, function (key, value) {
                    if (key == 0)
                        $('#selRole,#selRole1').append($('<option>').attr('value', 0).html('Select'));
                    if (value.RoleType != 'DepartmentHead' && value.RoleType != 'Manager')
                        $('#selRole,#selRole1').append($('<option>').attr('value', $.trim(value.RoleID)).html(value.RoleType));
                });
            }
            if (Check == 2)
                UpdateUserandGroupName(key, Type);
        },
        error: function () {
            alert('error');
        }
    });
}

function GetOfficerrole() {
    $('#SelOfficerrole,#SelOfficerrole1').empty();
    $.ajax({
        type: "GET",
        url: serviceUrl + "/GetOfficerRole", // Location of the service
        contentType: "application/javascript", // content type sent to server
        dataType: "jsonp", //Expected data format from server       
        processdata: false, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (msg) {//On Successfull service call  

            var result = JSON.parse(msg);
            $.each(result, function (key, value) {
                if (key == 0)
                    $('#SelOfficerrole,#SelOfficerrole1').append($('<option>').attr('value', 0).html('Select'));
                $('#SelOfficerrole,#SelOfficerrole1').append($('<option>').attr('value', $.trim(value.OfficerRoleID)).html(value.OfficerRole));
            });

        },
        error: function () {
            alert('error');
        }
    });
}

function GetOfficerListforGridView() {
    $.ajax({
        type: "GET",
        url: serviceUrl + "/GetOfficerListforGridView?OfficerID=" + ($.trim($('#txtOfficerID').val()) == '' ? 0 : $.trim($('#txtOfficerID').val())) + "&OfficerName=" + $.trim($('#txtOfficerName').val()) + "&Type=" + $('#SelType').val() + "&ManagerID=" + mID, // Location of the service
        contentType: "application/javascript", // content type sent to server
        dataType: "jsonp", //Expected data format from server       
        processdata: false, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (msg) {//On Successfull service call  
            $('#tbodyGridView').empty();
            //$('#tblGridView').dataTable().fnClearTable();
            var result = JSON.parse(msg);
            var cls;
            if (result != '') {
                $('#tblGridView').append($('<thead>').append($('<tr class="gridrow-head">').append($('<td>').html('Manager Name')).append($('<td>').html('Group'))
                            .append($('<td>').html('Officer ID')).append($('<td>').html('Officer Name')).append($('<td>').html('Email')).append($('<td>').html('Mobile'))
                            .append($('<td>').html('Type')).append($('<td>').html('Team code')).append($('<td>').html('Officer role')).append($('<td>').html('Certificated'))
                            .append($('<td>').html('Post code')).append($('<td>').html('Team leader')))).append($('<tbody>').attr('id', 'tbodyGridView'))
                $.each(result, function (key, value) {
                    //key % 2 == 1 ? cls = "alt-row" : cls = "";
                    $('#tbodyGridView').append($('<tr >').append($('<td>').html(value.ManagerName)).append($('<td>').html(value.GroupName)).append($('<td>').html(value.OfficerID)).append($('<td>').html(value.OfficerName)).append($('<td>').html(value.Email))
                    .append($('<td>').html(value.Mobile)).append($('<td>').html(value.EmpType)).append($('<td>').html(value.Teamcode)).append($('<td>').html(value.OfficerRole))
                        .append($('<td>').html(value.Certificated)).append($('<td>').html(value.Postcode)).append($('<td>').html(value.Teamleader)))
                });
                $('#tblGridView').dataTable({
                    "sScrollY": "1000px",
                    "bPaginate": false,
                    "bDestroy": true
                    // "aLengthMenu": [[10, 25, 50, 75, 100, -1], [10, 25, 50, 75, 100, "All"]],
                    // "iDisplayLength": 25
                });
                //$('#tblGridView_filter')

            }
            else {
                $('#tblGridView').append($('<thead>').append($('<tr class="gridrow-head">').append($('<td>').html('Manager Name')).append($('<td>').html('Group'))
                            .append($('<td>').html('Officer ID')).append($('<td>').html('Officer Name')).append($('<td>').html('Email')).append($('<td>').html('Mobile'))
                            .append($('<td>').html('Type')).append($('<td>').html('Team code')).append($('<td>').html('Officer role')).append($('<td>').html('Certificated'))
                            .append($('<td>').html('Post code')).append($('<td>').html('Team leader'))))
                    .append($('<tbody>').attr('id', 'tbodyGridView')).append($('<tr>').append($('<td colspan="12" align="center">').html('No officers available to display.')));
            }
        },
        error: function () {
            alert('error');
        }
    });
}