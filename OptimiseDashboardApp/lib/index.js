﻿//==========================================================================================================================================
/* File Name: index.js */
/* File Created: January 21, 2014 */
/* Created By  : R.Santhakumar */
//==========================================================================================================================================

$(document).ready(function () {
    date = new Date();
    TodayDate = $.datepicker.formatDate('yy/mm/dd', date) + ' ' + date.getHours() + ':' + date.getMinutes();
    TodayDateOnly = $.datepicker.formatDate('dd/mm/yy', date);

    $('#txtFromDate,#txtToDate,#txtFromDateCA,#txtToDateCA,#txtFromDateLocation,#txtToDateLocation').val(TodayDateOnly);
    $('#txtFromDateRanking,#txtToDateRanking,#txtTLineFromDate,#txtTLineToDate').val(TodayDateOnly);

    //==========================================================================================================================================
    //TODO : Datepicker 
    var cDate = new Date();
    $('#txtFromDate,#txtToDate,#txtFromDateCA,#txtToDateCA,#txtFromDateRanking,#txtToDateRanking').datetimepicker({
        timepicker: false,
        format: 'd/m/Y'
    }).datepicker("setDate", TodayDate);

    $('#txtFromDateLocation,#txtToDateLocation,#txtTLineFromDate,#txtTLineToDate').datetimepicker({
        timepicker: false,
        format: 'd/m/Y'
    }).datepicker("setDate", TodayDate);


    //******************************************************************************
    try {
        if ($.session.get('OfficerID') != '' && $.session.get('OfficerID') != undefined) {
            $.session.set('RankingType', 'Paid');
            $.session.set('ManagerID', OfficerID);
            $.session.set('Map', '');
            $('#lblLoginOfficer').html($.session.get('OfficerName'));
            $.connection.hub.qs = { "Authorization": $.session.get('Token') };
            counter = $.connection.dashboardHub;
            $.connection.hub.start({ waitForPageLoad: false });
            OfficerID = $.session.get('OfficerID');

            $.connection.hub.start()
                .done(function () {
                    $.loader({
                        className: "blue-with-image",
                        content: ' '
                    });
                    counter.server.getmanagertree(OfficerID);
                    counter.server.getofficerstree();
                    setInterval(function () {
                        if (CaseActionURL != "") {
                            GetActionsHub(CaseActionURL);
                        }
                    }, 60000);
                })
                .fail(function () {
                    alert('fail');
                });

            $('#AHeartBeat').click(function () {
                HeartBeat(1);
            });

            $('#AHeartBeatEnlarge').click(function () {
                HeartBeat(2);
            });

            $('#ALocationwithHB').click(function () {
                LocationwithHB(1);
            });

            $('#ALocationwithHBEnlarge').click(function () {
                LocationwithHB(2);
            });

            $('#APageEnlarge').click(function () {
                MapExpandView();
            });

            $('#txtRefreshTime').keydown(function (event) {
                if (event.shiftKey == true) {
                    $('#txtRefreshTime').attr('title', 'Enter numeric values');
                    return false;
                }
                if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190 || ((event.keyCode == 65 || event.keyCode == 67 || event.keyCode == 86) && event.ctrlKey == true)) {
                    return true;
                }
                else {
                    $('#txtRefreshTime').attr('title', 'Enter numeric values');
                    return false;
                }
            });

            $('#btnTodayWMADate').click(function () {
                $('#txtFromDate,#txtToDate').val(TodayDateOnly);
                $('#btnWMADateOk').trigger('click');
                $('#AWMADate').attr('data-original-title', 'Today');
            });

            $('#btnCAToday').click(function () {
                $('#txtFromDateCA,#txtToDateCA').val(TodayDateOnly);
                $('#btnCADateOk').trigger('click');
                $('#ACADate').attr('data-original-title', 'Today');
            });

            $('#btnRAToday').click(function () {
                $('#txtFromDateRanking,#txtToDateRanking').val(TodayDateOnly);
                $('#btnRankingDateOK').trigger('click');
                $('#ARankingDate').attr('data-original-title', 'Today');
            });

            $('#btnLocationToday').click(function () {
                $('#txtFromDateLocation').val(TodayDateOnly);
                $('#btnLocationDateOk').trigger('click');
                $('#ALocationDate').attr('data-original-title', 'Today');
            });

            $('#ALastLocation').click(function () {
                MapView();
                //   clearInterval(RefreshIntervalValue);
            });

            $('#btnTLineSubmit').click(function () {
                GetTimelineResult($.trim($.session.get('TimelineOfficer')));
            });

            $('#ACloseLocation').click(function () {
                RefreshIntervalValue = setInterval(function () {
                    if ($('#lblSelectedOfficerID').html() != '0') {
                        FillWMA('0', $('#txtFromDate').val(), $('#txtFromDate').val(), $('#lblSelectedOfficerID').html());
                        FillCaseActions('0', $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), $('#lblSelectedOfficerID').html());
                        FillCaseDetailsAction('0', $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), $('#lblSelectedOfficerID').html());
                        GetStatistics($('#lblSelectedOfficerID').html());
                    }
                    else if ($('#lblSelectedGroupID').html() != '0') {
                        FillWMA('0', $('#txtFromDate').val(), $('#txtFromDate').val(), '0', $('#lblSelectedGroupID').html());
                        FillCaseActions('0', $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), '0', $('#lblSelectedGroupID').html());
                        FillCaseDetailsAction('0', $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), '0', $('#lblSelectedGroupID').html());
                        FillRankings('0', $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Paid');
                        //FillRankings('0', $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Returned', $('#lblSelectedGroupID').html());
                        //FillRankings('0', $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Part Paid', $('#lblSelectedGroupID').html());
                        FillRankingsMore('0', $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Paid', $('#lblSelectedGroupID').html());
                        //FillRankingsMore('0', $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Returned', $('#lblSelectedGroupID').html());
                        //FillRankingsMore('0', $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Part Paid', $('#lblSelectedGroupID').html());
                    }
                    else if ($('#lblSelectedManagerID').html() != '0') {
                        FillWMA($('#lblSelectedManagerID').html(), $('#txtFromDate').val(), $('#txtFromDate').val(), '0', '0');
                        FillCaseActions($('#lblSelectedManagerID').html(), $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), '0', '0');
                        FillCaseDetailsAction($('#lblSelectedManagerID').html(), $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), '0', '0');
                        FillRankings($('#lblSelectedManagerID').html(), $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Paid');
                        //FillRankings($('#lblSelectedManagerID').html(), $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Returned', '0');
                        //FillRankings($('#lblSelectedManagerID').html(), $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Part Paid', '0');
                        FillRankingsMore($('#lblSelectedManagerID').html(), $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Paid', '0');
                        //FillRankingsMore($('#lblSelectedManagerID').html(), $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Returned', '0');
                        //FillRankingsMore($('#lblSelectedManagerID').html(), $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Part Paid', '0');
                        GetStatistics($('#lblSelectedManagerID').html());
                    }
                    else {
                        FillWMA(OfficerID, $('#txtFromDate').val(), $('#txtFromDate').val());
                        FillCaseActions(OfficerID, $('#txtFromDateCA').val(), $('#txtFromDateCA').val());
                        FillCaseDetailsAction(OfficerID, $('#txtFromDateCA').val(), $('#txtFromDateCA').val());
                        FillRankings(OfficerID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Paid');
                        //FillRankings(OfficerID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Returned');
                        //FillRankings(OfficerID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Part Paid');
                        FillRankingsMore(OfficerID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Paid');
                        //FillRankingsMore(OfficerID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Returned');
                        //FillRankingsMore(OfficerID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Part Paid');
                        GetStatistics(OfficerID);
                    }

                }, $('#txtRefreshTime').val() * 1000 * 60);
            });

            $('#txtareacontent').val('');

            //: Default checkbox is checked or not
            if ($("#chkIsPriority").is(':checked')) {
                max = 200;
                $('.word_count span').text(max - $.trim($('#txtareacontent').val()).length + ' characters left');
            }
            else {
                max = 900;
                $('.word_count span').text(max - $.trim($('#txtareacontent').val()).length + ' characters left');
            }

            // Checkbox change function
            $("#chkIsPriority").change(function () {
                if ($("#chkIsPriority").is(':checked')) {
                    max = 200;
                    $('.word_count span').text(max - $.trim($('#txtareacontent').val()).length + ' characters left');
                }
                else {
                    max = 900;
                    $('.word_count span').text(max - $.trim($('#txtareacontent').val()).length + ' characters left');
                }
            });

            $("#txtareacontent").bind('paste', function (e) {
                if ($("#chkIsPriority").is(':checked')) {
                    max = 200;
                    $('#txtareacontent').attr('maxlength', '200');
                    setTimeout(function () {
                        if ($('#txtareacontent').val().length >= max) {
                            $('.word_count span').text(' Character limit exceeded').attr('style', 'font-weight: bold');
                        }
                        else {
                            $('.word_count span').text(max - $('#txtareacontent').val().length + ' characters left');
                        }
                    }, 100);
                }
                else {
                    max = 900;
                    $('#txtareacontent').attr('maxlength', '900');
                    setTimeout(function () {
                        if ($('#txtareacontent').val().length >= max) {
                            $('.word_count span').text(' Character limit exceeded').attr('style', 'font-weight: bold');
                        }
                        else {
                            $('.word_count span').text(max - $('#txtareacontent').val().length + ' characters left');
                        }
                    }, 100);
                }
            });

            // Textarea keydown function
            $('#txtareacontent').keydown(function () {
                var len = $.trim($('#txtareacontent').val()).length;// $(this).val().length;
                if ($("#chkIsPriority").is(':checked')) {
                    max = 200;
                    $('.word_count span').text(max - len + ' characters left');
                }
                else {
                    max = 900;
                    $('.word_count span').text(max - len + ' characters left');
                }
                if (len >= max) {
                    $(this).val($(this).val().substring(0, len - 1));
                    $('.word_count span').text(' Reached the limit');
                }
                else {
                    var ch = max - len;
                    $('.word_count span').text(ch + ' characters left');
                }

                if ($.trim($('#txtareacontent').val()).length > 0) {
                    $('#btnSendNotification').removeAttr('disabled');
                }
                else {
                    $('#btnSendNotification').attr('disabled', true);
                }
            });

            $('#btnSendNotification').click(function () {
                var array_of_checked_values = $("#selOfficer").multiselect("getChecked").map(function () {
                    return this.value;
                }).get();
                var notes = $("#txtareacontent").val().replace(/[^a-z0-9,£:'`&.\s]/gi, '').replace(/[_\s]/g, ' ');
                var OIDs = array_of_checked_values.toString().split(',');
                if (!(OIDs == '' || OIDs == null)) {
                    if ($("#chkIsPriority").is(':checked')) {
                        max = 200;
                        if ($.trim($('#txtareacontent').val()).length >= max) {
                            $('.word_count span').text(' Character limit exceeded').attr('style', 'font-weight: bold');
                        }
                        else {
                            $.each(OIDs, function (key, value) {
                                UpdateNotificationAlert($.session.get('OfficerID'), notes, value, $('#chkIsPriority:checked').val() != undefined ? 1 : 0);
                            });
                            $('#txtareacontent').val('');
                            $("#chkIsPriority").prop("checked", false);
                        }
                    }
                    else {
                        max = 900;
                        if ($.trim($('#txtareacontent').val()).length >= max) {
                            $('.word_count span').text(' Character limit exceeded').attr('style', 'font-weight: bold');
                        }
                        else {
                            $.each(OIDs, function (key, value) {
                                UpdateNotificationAlert($.session.get('OfficerID'), notes, value, $('#chkIsPriority:checked').val() != undefined ? 1 : 0);
                            });
                            $('#txtareacontent').val('');
                            $("#chkIsPriority").prop("checked", false);
                        }
                    }
                }
                else {
                    $('.word_count').hide();
                    $('#lblMsgReport').html('Please select officer(s)').show().fadeOut(4000);
                    $('#lblMsgReport').css('color', 'red');
                    $('.word_count').fadeIn(4000);
                }
            });

            //==========================================================================================================================================

            // Target tab click
            $('#popup1Trigger').click(function () {
                $('#' + $('#popup1Trigger').attr('aria-describedby')).attr('class', 'popover fade bottom in');
                $('#' + $('#popup2Trigger').attr('aria-describedby')).attr('class', 'popover fade bottom');
                $('#' + $('#popup3Trigger').attr('aria-describedby')).attr('class', 'popover fade bottom');

                CntTab1 = 0;
                Tab = 1;
            });

            //Quick Menu tab click
            $('#popup2Trigger').click(function () {
                $('#' + $('#popup2Trigger').attr('aria-describedby')).attr('class', 'popover fade bottom in');
                $('#' + $('#popup1Trigger').attr('aria-describedby')).attr('class', 'popover fade bottom');
                $('#' + $('#popup3Trigger').attr('aria-describedby')).attr('class', 'popover fade bottom');
                if ($.session.get('FlashNews') == undefined || $.session.get('FlashNews') == 'Off') {
                    $('#DMarquee').hide();
                    $('#btnFlashOff').hide();
                    $('#btnFlashOn').show();
                }
                else {
                    $('#btnFlashOn').hide();
                    $('#DMarquee').show();
                    $('#btnFlashOff').show();
                }

                CntTab2 = 0;
                Tab = 2;
                $('#txtRefreshTime').val($.session.get('aref'));
            });

            //Logout tab click
            $('#popup3Trigger').click(function () {
                $('#' + $('#popup3Trigger').attr('aria-describedby')).attr('class', 'popover fade bottom in');
                $('#' + $('#popup1Trigger').attr('aria-describedby')).attr('class', 'popover fade bottom');
                $('#' + $('#popup2Trigger').attr('aria-describedby')).attr('class', 'popover fade bottom');

                CntTab3 = 0;
                Tab = 3;
            });

            $('#btnBack').click(function () {
                $('#lblSearchType').html('');
                showhidetabs('tblbodySearchType');
                $('#btnBack').hide();
                if ($.session.get('TreeLevel') == 0 || $.session.get('TreeLevel') == 1)
                    $('#btnWMAOfficerBack,#tblOfficer').show();
                else
                    $('#btnWMAOfficerBack').hide();
            });

            $('#btnWMAOfficerBack').click(function () {
                $('#lblSearchType,#lblOfficerDisplay').html('');
                showhidetabs('tblWMAOfficers');
                $('#btnWMAOfficerBack').hide();
                $('#lblWMAManager').show();
                if ($.session.get('TreeLevel') == 0)
                    $('#btnWMAADMBack').show();
                else
                    $('#btnWMAADMBack').hide();
            });

            $('#btnWMAADMBack').click(function () {
                $('#lblSearchType,#lblOfficerDisplay,#lblWMAADM').html('');
                showhidetabs('tblWMAADM');
                $('#tblWMAOfficers,#btnWMAADMBack').hide();
            });

            $('#btnStatsBack').click(function () {
                $('#tblStatsManagerCount,#tblStatsOfficerCount,#btnStatsADMBack,#btnStatsBack').hide();
                $('#tblStatsADMCount').show();
                $('#lblStatsMgrName').html('').hide();
                if ($.session.get('TreeLevel') == 0 || $.session.get('TreeLevel') == 2)
                    $('#btnStatsADMBack').show();
            });

            $('#btnStatsADMBack').click(function () {
                $('#tblStatsADMCount,#tblStatsOfficerCount,#btnStatsADMBack').hide();
                $('#tblStatsManagerCount').show();
            });

            $('#btnCaseActionBack').click(function () {
                caseactionshowhidetabs();
                $('#HCaseActionNavigation,#btnCaseActionBack').hide();
                $('#divCaseActionsOfficerCount,#btnCaseActionADM').show();
                $('#lblCAOfficerName').html('');
                if ($.session.get('TreeLevel') == 2) {
                    $('#btnCaseActionADM').hide();
                }
            });

            $('#btnDeviationMapBack').click(function () {
                caseactionshowhidetabs();
                $('#HCaseActionNavigation,#btnDeviationMapBack').hide();
                $('#deviatedformanager').show();
                if ($.session.get('TreeLevel') == 0 || $.session.get('TreeLevel') == 1)
                    $('#btnCaseActionBack').hide();
                else
                    $('#btnCaseActionBack').show();

                $('#lblCAOfficerName').html('');
            });

            $('#btnCaseActionManager').click(function () {
                caseactionshowhidetabs();
                $('#HCaseActionNavigation,#divCaseActionsOfficerCount,#btnCaseActionManager,#divCaseActionsADMCount').hide();
                $('#divCaseActionsManagerCount').show();
                $('#lblCAManagerName').html('');
            });

            $('#btnCaseActionADM').click(function () {
                caseactionshowhidetabs();
                $('#HCaseActionNavigation,#divCaseActionsOfficerCount,#btnCaseActionADM,#divCaseActionsManagerCount').hide();
                $('#divCaseActionsADMCount,#btnCaseActionManager').show();
                $('#lblCAADMName').html('');
                if ($.session.get('TreeLevel') == 1) {
                    $('#btnCaseActionManager').hide();
                }
            });

            $('#btnRankADMBack').click(function () {
                $('#DRankingOfficer,#DRankingCase,#btnRankADMBack').hide();
                $('#DRankingADM').show();
                $('#lblRankADMName').html('');
            });

            $('#btnRankOfficerBack').click(function () {
                if ($.session.get('TreeLevel') == 1) {
                    $('#DRankingCase,#btnRankOfficerBack,#btnRankADMBack').hide();
                    $('#DRankingOfficer').show();
                    $('#lblRankOfficerName').html('');
                }
                else {
                    $('#DRankingCase,#btnRankOfficerBack').hide();
                    $('#DRankingOfficer,#btnRankADMBack').show();
                    $('#lblRankOfficerName').html('');
                }
            });

            // TODO : Rank more
            //==========================================================================================================================================
            // paid more click
            $('#ARankingPaidMore').click(function () {
                $('#DRMPaidManager,#DRMReturnManager,#DRMPPManager').hide();
                $('#DRMPaidADM,#DRMReturnADM,#DRMPPADM').hide();
                $('#DRMPaidOfficer,#DRMReturnOfficer,#DRMPPOfficer').hide();
                $('#DRMPaidCase,#DRMReturnCase,#DRMPPCase').hide();
                $('#btnRankMoreADM,#btnRankMoreOfficer,#btnRankMoreCase,#btnRankMoreCaseNotes,#DRankMoreNotes').hide();

                $('#lblRankMore').html('Rank-Paid');
                switch ($.session.get('TreeLevel')) {
                    case '0':
                        $('#lblRankMoreManager,#lblRankMoreADM,#lblRankMoreOfficer').html('');
                        $('#DRMPaidManager').show();
                        break;
                    case '1':
                        $('#lblRankMoreADM,#lblRankMoreOfficer').html('');
                        $('#DRMPaidADM').show();
                        break;
                    case '2':
                        $('#DRMPaidOfficer').show();
                        break;
                }
            });

            // return more click
            $('#ARankingReturnedMore').click(function () {
                $('#DRMPaidManager,#DRMReturnManager,#DRMPPManager').hide();
                $('#DRMPaidADM,#DRMReturnADM,#DRMPPADM').hide();
                $('#DRMPaidOfficer,#DRMReturnOfficer,#DRMPPOfficer').hide();
                $('#DRMPaidCase,#DRMReturnCase,#DRMPPCase').hide();
                $('#btnRankMoreADM,#btnRankMoreOfficer,#btnRankMoreCase,#btnRankMoreCaseNotes,#DRankMoreNotes').hide();

                $('#lblRankMore').html('Rank-Returned');
                switch ($.session.get('TreeLevel')) {
                    case '0':
                        $('#lblRankMoreManager,#lblRankMoreADM,#lblRankMoreOfficer').html('');
                        $('#DRMReturnManager').show();
                        break;
                    case '1':
                        $('#lblRankMoreADM,#lblRankMoreOfficer').html('');
                        $('#DRMReturnADM').show();
                        break;
                    case '2':
                        $('#DRMReturnOfficer').show();
                        break;
                }
            });

            // part paid more click
            $('#ARankingPartPaidMore').click(function () {
                $('#DRMPaidManager,#DRMReturnManager,#DRMPPManager').hide();
                $('#DRMPaidADM,#DRMReturnADM,#DRMPPADM').hide();
                $('#DRMPaidOfficer,#DRMReturnOfficer,#DRMPPOfficer').hide();
                $('#DRMPaidCase,#DRMReturnCase,#DRMPPCase').hide();
                $('#btnRankMoreADM,#btnRankMoreOfficer,#btnRankMoreCase,#btnRankMoreCaseNotes,#DRankMoreNotes').hide();

                $('#lblRankMore').html('Rank-Part Paid');
                switch ($.session.get('TreeLevel')) {
                    case '0':
                        $('#lblRankMoreManager,#lblRankMoreADM,#lblRankMoreOfficer').html('');
                        $('#DRMPPManager').show();
                        break;
                    case '1':
                        $('#lblRankMoreADM,#lblRankMoreOfficer').html('');
                        $('#DRMPPADM').show();
                        break;
                    case '2':
                        $('#DRMPPOfficer').show();
                        break;
                }
            });

            //==========================================================================================================================================

            $('#btnRankMoreADM').click(function () {
                $('#btnRankMoreADM,#DRMPaidADM,#DRMReturnADM,#DRMPPADM').hide();
                $('#lblRankMoreManager,#lblRankMoreOfficer').html('');
                switch ($.session.get('RankingType')) {
                    case 'Paid':
                        $('#DRMPaidManager').show();
                        break;
                    case 'Returned':
                        $('#DRMReturnManager').show();
                        break;
                    case 'Part Paid':
                        $('#DRMPPManager').show();
                        break;
                }
            });

            $('#btnRankMoreOfficer').click(function () {
                $('#btnRankMoreOfficer,#DRMPaidOfficer,#DRMReturnOfficer,#DRMPPOfficer').hide();
                $('#lblRankMoreADM,#lblRankMoreOfficer').html('');

                switch ($.session.get('RankingType')) {
                    case 'Paid':
                        $('#DRMPaidADM').show();
                        break;
                    case 'Returned':
                        $('#DRMReturnADM').show();
                        break;
                    case 'Part Paid':
                        $('#DRMPPADM').show();
                        break;
                }

                if ($.session.get('TreeLevel') == 0)
                    $('#btnRankMoreADM').show();
                else
                    $('#btnRankMoreADM').hide();
            });

            $('#btnRankMoreCase').click(function () {
                $('#btnRankMoreCase,#DRMPaidCase,#DRMReturnCase,#DRMPPCase').hide();

                switch ($.session.get('RankingType')) {
                    case 'Paid':
                        if ($.session.get('TreeLevel') == 0 || $.session.get('TreeLevel') == 1) {
                            $('#DRMPaidOfficer,#btnRankMoreOfficer').show();//
                            $('#DRMPaidCase').hide();
                        }
                        else
                            $('#DRMPaidOfficer').show();
                        break;
                    case 'Returned':
                        if ($.session.get('TreeLevel') == 0 || $.session.get('TreeLevel') == 1) {
                            $('#DRMReturnOfficer,#btnRankMoreOfficer').show();//
                            $('#DRMReturnCase').hide();
                        }
                        else
                            $('#DRMReturnOfficer').show();
                        break;
                    case 'Part Paid':
                        if ($.session.get('TreeLevel') == 0 || $.session.get('TreeLevel') == 1) {
                            $('#DRMPPOfficer,#btnRankMoreOfficer').show();//
                            $('#DRMPPCase').hide();
                        }
                        else
                            $('#DRMPPOfficer').show();
                        break;
                }
                $('#lblRankMoreOfficer').html('');

            });

            // WMA More
            $('#AMoreWMA').click(function () {
                $('#lblWMAMoreOfficerName,#lblWMAMoreOfficerCaseName').html('');//#lblWMAMoreManagerName,
                $('#DWMAMoreManager,#DWMAMoreADM,#DWMAMoreOfficer,#DWMAMoreOfficerCaseCount,#DWMAMoreOfficerCase').hide();
                $('#btnWMAMoreManager,#btnWMAMoreADM,#btnWMAMoreOfficer,#btnWMAMoreOfficerCase').hide();
                switch ($.session.get('TreeLevel')) {
                    case '0':
                        $('#DWMAMoreManager').show();
                        break;
                    case '1':
                        $('#DWMAMoreADM').show();
                        break;
                    case '2':
                    case '3':
                        $('#DWMAMoreOfficer').show();
                        break;
                }
            });

            $('#btnWMAMoreManager').click(function () {
                $('#lblWMAMoreManagerName').html('');
                $('#btnWMAMoreManager,#DWMAMoreADM,#DWMAMoreOfficer').hide();
                $('#DWMAMoreManager').show();
            });

            $('#btnWMAMoreADM').click(function () {
                $('#lblWMAMoreADMName').html('');
                $('#btnWMAMoreADM,#DWMAMoreOfficer').hide();
                $('#DWMAMoreADM').show();
                if ($.session.get('TreeLevel') == 0)
                    $('#btnWMAMoreManager').show();
                else
                    $('#btnWMAMoreManager').hide();
            });

            $('#btnWMAMoreOfficer').click(function () {
                $('#lblWMAMoreOfficerName').html('');
                $('#DWMAMoreOfficerCaseCount,#btnWMAMoreOfficer,#btnWMAMoreManager').hide();
                if ($.session.get('TreeLevel') == 0 || $.session.get('TreeLevel') == 1)
                    $('#DWMAMoreOfficer,#btnWMAMoreADM').show();
                else
                    $('#DWMAMoreOfficer').show();
            });

            $('#btnWMAMoreOfficerCase').click(function () {
                $('#lblWMAMoreOfficerCaseName').html('');
                $('#DWMAMoreOfficerCase,#btnWMAMoreOfficerCase,#btnWMAMoreOfficer').hide();
                if ($.session.get('TreeLevel') == 0 || $.session.get('TreeLevel') == 1 || $.session.get('TreeLevel') == 2 || $.session.get('TreeLevel') == 3)
                    $('#DWMAMoreOfficerCaseCount,#btnWMAMoreOfficer').show();
                else
                    $('#DWMAMoreOfficerCaseCount').show();
            });

            $('#btnWMADateOk').click(function () {
                $('#AWMADate').attr('data-original-title', $('#txtFromDate').val());
                FillWMA($.session.get('ManagerID'), $('#txtFromDate').val(), $('#txtFromDate').val());
                $('#AWMACloseDate').trigger('click');
            });

            $('#btnCADateOk').click(function () {
                $('#ACADate').attr('data-original-title', $('#txtFromDateCA').val());
                FillCaseActions($.session.get('ManagerID'), $('#txtFromDateCA').val(), $('#txtFromDateCA').val());
                FillCaseDetailsAction($.session.get('ManagerID'), $('#txtFromDateCA').val(), $('#txtFromDateCA').val());
                $('#btnCACloseDate').trigger('click');
            });

            $('#btnLocationDateOk').click(function () {
                FillLastKnownLocation($.session.get('ManagerID'), $('#txtFromDateLocation').val(), $('#txtFromDateLocation').val());
                $('#ALocationDate').attr('data-original-title', $('#txtFromDateLocation').val());
                $('#btnLocationDateClose').trigger('click');
            });

            $('#btnRankingDateOK').click(function () {
                FillRankings($.session.get('ManagerID'), $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Paid');
                FillRankingsMore($.session.get('ManagerID'), $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Paid');
                $('#ARankingDate').attr('data-original-title', $('#txtFromDateRanking').val());
                $('#ARAClose').trigger('click');
            });

            $('#ACATab').click(function () {
                $('#ACADate').show();
                $('#AWMADate').hide();
            });

            $('#AWMATab').click(function () {
                $('#ACADate').hide();
                $('#AWMADate').show();
            });

            $('#AReturned').click(function () {
                $.session.set('RankingType', 'Returned');
            });

            $('#APaid').click(function () {
                $.session.set('RankingType', 'Paid');
            });

            $('#APartPaid').click(function () {
                $.session.set('RankingType', 'Part Paid');
            });

            $('#imgRefreshLocation').click(function () {
                if ($('#lblSelectedOfficerID').html() != '0') {
                    FillLastKnownLocation('0', $('#txtFromDateLocation').val(), $('#txtFromDateLocation').val(), $('#lblSelectedOfficerID').html());
                }
                else if ($('#lblSelectedGroupID').html() != '0') {
                    FillLastKnownLocation('0', $('#txtFromDateLocation').val(), $('#txtFromDateLocation').val(), '0', $('#lblSelectedGroupID').html());
                }
                else if ($('#lblSelectedManagerID').html() != '0') {
                    FillLastKnownLocation($('#lblSelectedManagerID').html(), $('#txtFromDateLocation').val(), $('#txtFromDateLocation').val(), '0', '0');
                }
                else {
                    FillLastKnownLocation(OfficerID, $('#txtFromDateLocation').val(), $('#txtFromDateLocation').val());
                }
            });

            $('.tree-toggle').click(function () {
                $(this).parent().children('ul.tree').toggle(200);
            });

            $('#lblTeamAdmin').click(function () {
                window.location.href = RedirectUrl;
            });

            $('#AMoreWorkingHours').click(function () {
                GetWorkingHoursMore($.session.get('OfficerID'), 0);
            });

            $('#btnWorkingHoursBack').click(function () {
                $('#DivWorkingHoursOfficersList,#btnWorkingHoursBack').hide();
                $('#DivWorkingHoursManagersList').show();
            });

            //==========================================================================================================================================

            $('#txtSearchOfficer').keyup(function () {
                //***********************************
                var isAtleastOneMatch = false;
                var isManagerMatch = false;
                $('#ulOfficers').find('li').css('display', 'block');
                if ($('#txtSearchOfficer').val().length > 0) {
                    $('#ulOfficers').find('li').css('display', 'none');

                    $('#ulOfficers li.Officer').each(function (key, value) {
                        if ($(this).text().toLowerCase().indexOf($('#txtSearchOfficer').val().toLowerCase()) >= 0) {
                            $(this).css('display', 'block');
                            isAtleastOneMatch = true;
                            //0- Zone;1 - Mgr; 2 - ADM; 3- Officer
                            try {
                                $adm = $(this).parent().parent();
                                $adm.css('display', 'block');
                                $adm.find('a').find('i').attr('class', 'glyphicon glyphicon-minus-sign');

                                $mgr = $adm.parent().parent();
                                $mgr.css('display', 'block');
                                $mgr.find('a').find('i').attr('class', 'glyphicon glyphicon-minus-sign');

                                $zone = $mgr.parent().parent();
                                $zone.css('display', 'block');
                            } catch (e) {

                            }
                        }
                    });

                    $('#ulOfficers li.ADM').each(function (key, value) {
                        if ($(this).text().toLowerCase().indexOf($('#txtSearchOfficer').val().toLowerCase()) >= 0) {
                            $(this).css('display', 'block');
                            isAtleastOneMatch = true;
                            try {
                                $mgr = $(this).parent().parent();
                                $mgr.css('display', 'block');
                                $mgr.find('a').find('i').attr('class', 'glyphicon glyphicon-minus-sign');

                                $zone = $mgr.parent().parent();
                                $zone.css('display', 'block');
                            } catch (e) {

                            }
                        }
                    });

                    $('#ulOfficers li.Manager').each(function (key, value) {
                        if ($(this).text().toLowerCase().indexOf($('#txtSearchOfficer').val().toLowerCase()) >= 0) {
                            isAtleastOneMatch = true;
                            $(this).css('display', 'block');
                            try {
                                $zone = $mgr.parent().parent();
                                $zone.css('display', 'block');
                            } catch (e) {

                            }
                        }
                    });
                }
                else {
                    isAtleastOneMatch = true;
                }

                if (!isAtleastOneMatch) {
                    $('#lblSearchOfficerInfo').html('No Match found');
                }
                else {
                    $('#lblSearchOfficerInfo').html('');
                    if ($('#txtSearchOfficer').val().length == 0) {
                        $('#ulOfficers li.Officer').each(function (key, value) {
                            $adm = $(this).parent().parent();
                            $adm.css('display', 'none');
                            $adm.find('a').find('i').attr('class', 'glyphicon glyphicon-plus-sign');

                            $mgr = $adm.parent().parent();
                            $mgr.find('a').find('i').attr('class', 'glyphicon glyphicon-plus-sign');
                        });
                    }
                }
            });

            GetFlashNews();

            $('#selCompanyList').change(function () {
                LoginDetails4SuperUser($('#selCompanyList').val(), $.session.get('OfficerName'));
            });

            //// Vedio
            //if ($.session.get('IsVedio') == 1) {
            //    //introvideo();
            //}
            ////CompanyID Check
            //if ($.session.get('CompanyID') == 1)
            //    $('#lblClientSearch,#DClient,#lblPendingAction,#lblCaseActionSearch,#IDeviated').show();
            //else
            //    $('#lblClientSearch,#DClient,#lblPendingAction,#lblCaseActionSearch,#IDeviated').hide();

            ////TODO: ClientID check
            //if ($.session.get('ClientId') == undefined || $.session.get('ClientId') == 0)
            //    $('#DClient').hide();
            //else
            //    $('#DClient').show();

            $('#pClientName').html($.session.get('ClientName'));

            // getting month target for manager when login
            if ($.session.get('tgVal') != '') {
                $('#lblMonthTotalTarget').html($.session.get('tgVal'));
                $('#txtMonthTotalTarget').html($.session.get('tgVal'));
            }

            // TODO: Flash news
            if ($.session.get('FlashNews') == undefined || $.session.get('FlashNews') == 'Off') {
                $('#DMarquee,#btnFlashOff').hide();
                $('#btnFlashOn').show();
            }
            else {
                $('#btnFlashOn').hide();
                $('#DMarquee,##btnFlashOff').show();
            }

            ////Super user role
            //if ($.session.get('RoleType') == 7) {
            //    $('#ACompanyList').show();
            //    GetCompany();
            //}
            //else {
            //    $('#ACompanyList').hide();
            //}
            //Refresh time
            if ($.session.get('aref') != undefined) {
                $('#txtRefreshTime').val($.session.get('aref'));
            }

            //=======================================================================================================================
            //TODO: Officer Tree

            counter.client.getmanagertree = function (message) {
                var SelectedOfficerID = 0;
                var multiselect = true;

                message = $.parseJSON(message);
                if (!(message == "[]" || message == null || message == undefined || message == '[{"message":"NoRecords"}]')) {

                    var TreeString = $("#ulOfficers");
                    $.each(message, function (key, value) {
                        if (key == 0) {
                            $.session.set('TreeLevel', value.TreeLevel);
                            if ($.session.get('TreeLevel') == 0 || $.session.get('TreeLevel') == 1)
                                $('#lblTeamAdmin').show();
                            else
                                $('#lblTeamAdmin').hide();
                        }

                        switch (value.TreeLevel) {
                            case '0':
                                if ($('#ZoneManager' + value.ZoneManagerID).length == 0) {
                                    if (multiselect) {
                                        $('#pListName').html('Zone Manager list');
                                        $('#tbodyOfficerList').append($('<tr>')
                                           .append($('<td>').append($('<a class="targetNew">').attr('id', 'AOfficerTarget')
                                               .attr('data-toggle', 'modal').html(value.ZoneManagerName + ' ' + value.ZoneManagerID))
                                               .append($('<label>').attr('id', 'lblOfficerID' + key).html(value.ZoneManagerID).hide()))
                                           .append($('<td>').append($('<input>').attr('type', 'text').attr('id', 'txtOfficerTarget' + key).attr('style', 'color:black'))))

                                        $('#txtOfficerTarget' + key).keydown(function (event) {
                                            if (event.shiftKey == true) {
                                                $('#txtOfficerTarget' + key).attr('title', 'Enter numeric values');
                                                return false;
                                            }
                                            if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190 || ((event.keyCode == 65 || event.keyCode == 67 || event.keyCode == 86) && event.ctrlKey == true)) {
                                                return true;
                                            }
                                            else {
                                                $('#txtOfficerTarget' + key).attr('title', 'Enter numeric values');
                                                return false;
                                            }
                                        });

                                        $('#txtOfficerTarget' + key).keyup(function (event) {
                                            var TTarget = 0;
                                            $('#tbodyOfficerList tr').each(function (key, value) {
                                                if ($(this).find('input').val() > 0) {
                                                    TTarget += parseInt($(this).find('input').val());
                                                }
                                            });
                                            $('#lblTotalTarget').html(TTarget);

                                            if ($('#lblTotalTarget').html() != $('#txtMonthTotalTarget').val()) {
                                                $('#btnSubmitOfficerTotal').attr('disabled', 'disabled');
                                                $('#lblInfo').html('Total Target should match.');
                                                $("#lblInfo").show().delay(3000).fadeOut();
                                            }
                                            else {
                                                $('#btnSubmitOfficerTotal').removeAttr('disabled');
                                            }
                                        });
                                    }
                                    $("#ulOfficers").append('<li class="Zone"><a href="#" id="ZoneManager' + value.ZoneManagerID + '">' +
                                                           '<i class="glyphicon glyphicon-plus-sign"></i> ' + value.ZoneManagerName + '</a>' +
                                                           '<ul id="ULMan' + value.ZoneManagerID + '"></ul></li>');

                                    if (key == 0) {
                                        FillCaseActions($.session.get('OfficerID'), $('#txtFromDateCA').val(), $('#txtFromDateCA').val());
                                        FillWMA($.session.get('OfficerID'), $('#txtFromDate').val(), $('#txtFromDate').val());
                                        GetStats($.session.get('OfficerID'), 0);
                                        FillLastKnownLocation($.session.get('OfficerID'), $('#txtFromDateLocation').val(), $('#txtFromDateLocation').val());
                                        FillRankings($.session.get('OfficerID'), $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Paid');
                                        FillRankings($.session.get('OfficerID'), $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Part Paid');
                                        FillRankings($.session.get('OfficerID'), $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Returned');
                                        GetWorkingHours(value.ZoneManagerID);

                                        $.session.set('Availability', 0);
                                        ViewZoneMgr = value.ZoneManagerID;
                                        $('.lastLeaf').removeClass('lastLeaf');
                                        $('#ZoneManager' + value.ZoneManagerID).addClass('lastLeaf');
                                        $('#ZoneManager' + value.ZoneManagerID).find(' > i').addClass('glyphicon-minus-sign').removeClass('glyphicon-plus-sign');
                                        $.session.set('ManagerID', value.ZoneManagerID);
                                        $.session.set('MgrName', value.ZoneManagerName);
                                    }
                                    $('#ZoneManager' + value.ZoneManagerID).click(function () {
                                        $('.lastLeaf').removeClass('lastLeaf');
                                        $(this).addClass('lastLeaf');
                                        LinkForManager = 1;
                                        $.session.set('TreeLevel', value.TreeLevel);
                                        $.session.set('ManagerID', value.ZoneManagerID);
                                        $.session.set('MgrName', value.ZoneManagerName);
                                        $.session.set('Availability', 0);
                                        $.session.set('Map', '');
                                        pLatitude = 0;
                                        pLongitude = 0;
                                        $('#lblSelectedOfficerID,#lblSelectedGroupID').html('0');
                                        $('#lblSelectedManagerID').html(value.ZoneManagerID);
                                        $('#lblCAOfficerName').html('');
                                        //TODO:  Manager 0-Zone 1-Manager 2-ADM 3-Officer
                                        //GetCaseActionsManagerCount(1, value.ZoneManagerID, 0, value.ZoneManagerName)
                                        FillCaseActions(value.ZoneManagerID, $('#txtFromDateCA').val(), $('#txtFromDateCA').val());
                                        FillWMA(value.ZoneManagerID, $('#txtFromDate').val(), $('#txtFromDate').val());
                                        GetStats(value.ZoneManagerID, 0);
                                        FillLastKnownLocation(value.ZoneManagerID, $('#txtFromDateLocation').val(), $('#txtFromDateLocation').val());
                                        FillRankings(value.ZoneManagerID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Paid');
                                        FillRankings(value.ZoneManagerID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Part Paid');
                                        FillRankings(value.ZoneManagerID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Returned');
                                        GetWorkingHours(value.ZoneManagerID);

                                        $('#AShowAll,#AShowEnlarge,#DTimeline,#DAvailability,#tblWMASearchType').hide();
                                        $('#DivRanking,#showPanel2,#imgRefreshLocation,#tblWMA').show();
                                        $('#AHeartBeat,#AHeartBeatEnlarge,#ALocationwithHB,#ALocationwithHBEnlarge').hide();
                                        $('#pMap').html('Last known location');
                                    });
                                }
                            case '1':
                                if ($('#Manager' + value.ManagerID).length == 0) {
                                    if (multiselect) {
                                        $('#pListName').html('Manager list');
                                        $('#tbodyOfficerList').append($('<tr>')
                                           .append($('<td>').append($('<a class="targetNew">').attr('id', 'AOfficerTarget')
                                               .attr('data-toggle', 'modal').html(value.ManagerName + ' ' + value.ManagerID))
                                               .append($('<label>').attr('id', 'lblOfficerID' + key).html(value.ManagerID).hide()))
                                           .append($('<td>').append($('<input>').attr('type', 'text').attr('id', 'txtOfficerTarget' + key).attr('style', 'color:black'))))

                                        $('#txtOfficerTarget' + key).keydown(function (event) {
                                            if (event.shiftKey == true) {
                                                $('#txtOfficerTarget' + key).attr('title', 'Enter numeric values');
                                                return false;
                                            }
                                            if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190 || ((event.keyCode == 65 || event.keyCode == 67 || event.keyCode == 86) && event.ctrlKey == true)) {
                                                return true;
                                            }
                                            else {
                                                $('#txtOfficerTarget' + key).attr('title', 'Enter numeric values');
                                                return false;
                                            }
                                        });

                                        $('#txtOfficerTarget' + key).keyup(function (event) {
                                            var TTarget = 0;
                                            $('#tbodyOfficerList tr').each(function (key, value) {
                                                if ($(this).find('input').val() > 0) {
                                                    TTarget += parseInt($(this).find('input').val());
                                                }
                                            });
                                            $('#lblTotalTarget').html(TTarget);

                                            if ($('#lblTotalTarget').html() != $('#txtMonthTotalTarget').val()) {
                                                $('#btnSubmitOfficerTotal').attr('disabled', 'disabled');
                                                $('#lblInfo').html('Total Target should match.');
                                                $("#lblInfo").show().delay(3000).fadeOut();
                                            }
                                            else {
                                                $('#btnSubmitOfficerTotal').removeAttr('disabled');
                                            }
                                        });
                                    }
                                    if (value.TreeLevel == 1) {
                                        TreeString.append('<li class="Manager"><a href="#" id="Manager' + value.ManagerID + '">' +
                                                   '<i class="glyphicon glyphicon-plus-sign"></i> ' + value.ManagerName + '</a>' +
                                                   '<ul id="ULADM' + value.ManagerID + '"></ul></li>');
                                    }
                                    else {
                                        $("#ULMan" + value.ZoneManagerID).append('<li class="Manager"><a href="#" id="Manager' + value.ManagerID + '">' +
                                                       '<i class="glyphicon glyphicon-plus-sign"></i> ' + value.ManagerName + '</a>' +
                                                       '<ul id="ULADM' + value.ManagerID + '"></ul></li>');
                                    }
                                    if (ViewZoneMgr == value.ZoneManagerID) {
                                        $("#ULMan" + value.ZoneManagerID).find(' > li').attr('style', 'display:block');
                                    }
                                    $('#Manager' + value.ManagerID).click(function () {
                                        $('.lastLeaf').removeClass('lastLeaf');
                                        $(this).addClass('lastLeaf');
                                        LinkForManager = 1;
                                        $.session.set('ManagerID', value.ManagerID);
                                        $.session.set('MgrName', value.ManagerName);
                                        $.session.set('Availability', 0);
                                        $.session.set('Map', '');
                                        $.session.set('TreeLevel', 1);
                                        pLatitude = 0;
                                        pLongitude = 0;
                                        $('#lblWMAManager').html(value.ManagerName);
                                        $('#lblWMAMoreManagerName,#lblRankMgrName,#lblRankMoreManager').html(' - ' + value.ManagerName);
                                        $('#lblSelectedOfficerID,#lblSelectedGroupID').html('0');
                                        $('#lblSelectedManagerID').html(value.ManagerID);
                                        $('#lblCAOfficerName').html('');

                                        FillCaseActions(value.ManagerID, $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), '0', '0');
                                        FillWMAADM(value.ManagerID, $('#txtFromDate').val(), $('#txtFromDate').val());
                                        GetStats(value.ManagerID, 0);
                                        FillLastKnownLocation(value.ManagerID, $('#txtFromDateLocation').val(), $('#txtFromDateLocation').val(), '0', '0');
                                        FillRankingsForADM(value.ManagerID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Paid');
                                        FillRankingsForADM(value.ManagerID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Part Paid');
                                        FillRankingsForADM(value.ManagerID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Returned');

                                        $('#AShowAll,#AShowEnlarge,#tblWMASearchType,#DTimeline,#DAvailability').hide();
                                        $('#DivRanking,#showPanel2,#imgRefreshLocation,#tblWMA').show();
                                        $('#AHeartBeat,#AHeartBeatEnlarge,#ALocationwithHB,#ALocationwithHBEnlarge').hide();
                                        $('#pMap').html('Last known location');
                                    });
                                }
                            case '2':
                                if ($('#ADM' + value.ADMID).length == 0) {
                                    if (value.TreeLevel == 2) {
                                        TreeString.append('<li class="ADM"><a href="#" id="ADM' + value.ADMID + '">' +
                                                   '<i class="glyphicon glyphicon-plus-sign"></i> ' + value.ADMName + '</a>' +
                                                   '<ul id="UL' + value.ADMID + '"></ul></li>');
                                    }
                                    else {
                                        $("#ULADM" + value.ManagerID).append('<li class="ADM"><a href="#" id="ADM' + value.ADMID + '">' +
                                                       '<i class="glyphicon glyphicon-plus-sign"></i> ' + value.ADMName + '</a>' +
                                                       '<ul id="UL' + value.ADMID + '"></ul></li>');
                                    }

                                    $('#ADM' + value.ADMID).click(function () {
                                        LinkForManager = 1;
                                        $('#lblSelectedOfficerID').html('0');
                                        $('#lblSelectedGroupID').html(value.ADMID);
                                        $('#lblSelectedManagerID').html('0');
                                        $('#lblCAOfficerName').html('');
                                        $('#lblWMAADM,#lblWMAMoreADMName,#lblRankADMName,#lblRankMoreADM').html(' - ' + value.ADMName);
                                        $('.lastLeaf').removeClass('lastLeaf');
                                        $(this).addClass('lastLeaf');
                                        $.session.set('ManagerID', value.ADMID);
                                        $.session.set('MgrName', value.ADMName);
                                        $.session.set('TreeLevel', 2);
                                        $.session.set('Availability', 0);
                                        $.session.set('Map', '');
                                        pLatitude = 0;
                                        pLongitude = 0;
                                        FillCaseActions(value.ADMID, $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), '0', value.ADMID);
                                        FillWMAOfficer(value.ADMID, $('#txtFromDate').val(), $('#txtFromDate').val());
                                        GetStats(value.ADMID, 0);
                                        FillLastKnownLocation(value.ADMID, $('#txtFromDateLocation').val(), $('#txtFromDateLocation').val(), '0', value.GroupID);
                                        FillRankingsForOfficer(value.ADMID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Paid');
                                        FillRankingsForOfficer(value.ADMID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Part Paid');
                                        FillRankingsForOfficer(value.ADMID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Returned');

                                        $('#AShowAll,#AShowEnlarge,#DTimeline,#DAvailability,#tblWMASearchType').hide();
                                        $('#DivRanking,#showPanel2,#imgRefreshLocation,#tblWMA').show();
                                        $('#AHeartBeat,#AHeartBeatEnlarge,#ALocationwithHB,#ALocationwithHBEnlarge').hide();
                                        $('#pMap').html('Last known location');
                                    });
                                    if (multiselect)
                                        $('#selOfficer').append($('<optgroup>').attr('label', value.ManagerName).attr('id', 'og' + value.ManagerID))
                                }
                        }

                        if (multiselect) {
                            $('#og' + value.ManagerID)
                            .append($('<option>').attr('value', $.trim(value.OfficerId)).html(value.OfficerName + ' - ' + value.OfficerId))
                            $('#lblCurrentMonth,#lblMonthTotalTarget').hide();
                            $('#selCurrentMonth,#txtMonthTotalTarget').show();
                        }
                        $('#UL' + value.ADMID)
                              .append('<li class="Officer" id="LI' + value.OfficerId + '">' +
                                      '<a style="cursor:pointer;"  id="ATreeOfficer' + value.OfficerId + '" rel="popover" data-original-title=""  class="a1" data-html="true" data-popover="true">' +
                                      '<i class="glyphicon glyphicon-circle-arrow-right"></i> ' + value.OfficerName + '<span class="pull-right">' + value.OfficerId + '</span>' +
                                      '<span style="padding-left:20px"><img src="images/circle-active.png" id="img' + value.OfficerId + '"></span></a> </li>');

                        $('#ATreeOfficer' + value.OfficerId).mouseover(function () {
                            $(function () {
                                //****************Setting up data content for the popover *******************************
                                $.ajax({
                                    url: ServiceURL + 'api/v1/officers/' + value.OfficerId + '/availability',
                                    headers: {
                                        "Authorization": $.session.get('TokenAuthorization'),
                                        'Content-Type': 'application/json'
                                    },
                                    type: 'POST',
                                    dataType: DataType,
                                    beforeSend: function () {
                                        $('#selCompanyList').empty();
                                    },
                                    success: function (data) {
                                        $.each(data, function (key, value1) {
                                            datacontent = '<table width="100%" class="display-status-table"><tr><td>' +
                                     '<div class="block" style="margin:2px!important;padding:2px!important">' +
                                     '<p class="block-heading">Availability</p><table class="table">' +
                                     '<tbody><tr><td>Week</td><td>Current Week</td><td>Visits</td><td>Next Week</td></tr>' +
                                     '<tr><td>Tue</td><td><img src="' + ((value1.Tuesday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Tuesday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Tuesday == 1) ? 'AM' : ((value1.Tuesday == 2) ? 'PM' : ((value1.Tuesday == 0) ? '' : ((value1.Tuesday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RTuesday + '</td><td><img src="' + ((value1.Tuesday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Tuesday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Tuesday1 == 1) ? 'AM' : ((value1.Tuesday1 == 2) ? 'PM' : ((value1.Tuesday1 == 0) ? '' : ((value1.Tuesday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                     '<tr><td>Wed</td><td><img src="' + ((value1.Wednesday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Wednesday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Wednesday == 1) ? 'AM' : ((value1.Wednesday == 2) ? 'PM' : ((value1.Wednesday == 0) ? '' : ((value1.Wednesday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RWednesday + '</td><td><img src="' + ((value1.Wednesday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Wednesday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Wednesday1 == 1) ? 'AM' : ((value1.Wednesday1 == 2) ? 'PM' : ((value1.Wednesday1 == 0) ? '' : ((value1.Wednesday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                     '<tr><td>Thr</td><td><img src="' + ((value1.Thursday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Thursday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Thursday == 1) ? 'AM' : ((value1.Thursday == 2) ? 'PM' : ((value1.Thursday == 0) ? '' : ((value1.Thursday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RThursday + '</td><td><img src="' + ((value1.Thursday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Thursday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Thursday1 == 1) ? 'AM' : ((value1.Thursday1 == 2) ? 'PM' : ((value1.Thursday1 == 0) ? '' : ((value1.Thursday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                     '<tr><td>Fri</td><td><img src="' + ((value1.Friday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Friday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Friday == 1) ? 'AM' : ((value1.Friday == 2) ? 'PM' : ((value1.Friday == 0) ? '' : ((value1.Friday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RFriday + '</td><td><img src="' + ((value1.Friday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Friday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Friday1 == 1) ? 'AM' : ((value1.Friday1 == 2) ? 'PM' : ((value1.Friday1 == 0) ? '' : ((value1.Friday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                    '<tr><td>Sat</td><td><img src="' + ((value1.Saturday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Saturday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Saturday == 1) ? 'AM' : ((value1.Saturday == 2) ? 'PM' : ((value1.Saturday == 0) ? '' : ((value1.Saturday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RSaturday + '</td><td><img src="' + ((value1.Saturday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Saturday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Saturday1 == 1) ? 'AM' : ((value1.Saturday1 == 2) ? 'PM' : ((value1.Saturday1 == 0) ? '' : ((value1.Saturday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                    '<tr><td>Mon</td><td><img src="' + ((value1.Monday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Monday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Monday == 1) ? 'AM' : ((value1.Monday == 2) ? 'PM' : ((value1.Monday == 0) ? '' : ((value1.Monday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RMonday + '</td><td><img src="' + ((value1.Monday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Monday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Monday1 == 1) ? 'AM' : ((value1.Monday1 == 2) ? 'PM' : ((value1.Monday1 == 0) ? '' : ((value1.Monday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                    '</tbody></table></div></td></tr></table>';

                                            datacontentforDisplay = '<table class="table table-striped table-condensed ">' +
                                     '<tbody><tr><td>Week</td><td>Current Week</td><td>Visits</td><td>Next Week</td></tr>' +
                                     '<tr><td>Tue</td><td><img src="' + ((value1.Tuesday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Tuesday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Tuesday == 1) ? 'AM' : ((value1.Tuesday == 2) ? 'PM' : ((value1.Tuesday == 0) ? '' : ((value1.Tuesday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RTuesday + '</td><td><img src="' + ((value1.Tuesday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Tuesday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Tuesday1 == 1) ? 'AM' : ((value1.Tuesday1 == 2) ? 'PM' : ((value1.Tuesday1 == 0) ? '' : ((value1.Tuesday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                     '<tr><td>Wed</td><td><img src="' + ((value1.Wednesday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Wednesday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Wednesday == 1) ? 'AM' : ((value1.Wednesday == 2) ? 'PM' : ((value1.Wednesday == 0) ? '' : ((value1.Wednesday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RWednesday + '</td><td><img src="' + ((value1.Wednesday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Wednesday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Wednesday1 == 1) ? 'AM' : ((value1.Wednesday1 == 2) ? 'PM' : ((value1.Wednesday1 == 0) ? '' : ((value1.Wednesday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                     '<tr><td>Thr</td><td><img src="' + ((value1.Thursday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Thursday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Thursday == 1) ? 'AM' : ((value1.Thursday == 2) ? 'PM' : ((value1.Thursday == 0) ? '' : ((value1.Thursday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RThursday + '</td><td><img src="' + ((value1.Thursday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Thursday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Thursday1 == 1) ? 'AM' : ((value1.Thursday1 == 2) ? 'PM' : ((value1.Thursday1 == 0) ? '' : ((value1.Thursday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                     '<tr><td>Fri</td><td><img src="' + ((value1.Friday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Friday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Friday == 1) ? 'AM' : ((value1.Friday == 2) ? 'PM' : ((value1.Friday == 0) ? '' : ((value1.Friday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RFriday + '</td><td><img src="' + ((value1.Friday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Friday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Friday1 == 1) ? 'AM' : ((value1.Friday1 == 2) ? 'PM' : ((value1.Friday1 == 0) ? '' : ((value1.Friday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                    '<tr><td>Sat</td><td><img src="' + ((value1.Saturday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Saturday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Saturday == 1) ? 'AM' : ((value1.Saturday == 2) ? 'PM' : ((value1.Saturday == 0) ? '' : ((value1.Saturday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RSaturday + '</td><td><img src="' + ((value1.Saturday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Saturday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Saturday1 == 1) ? 'AM' : ((value1.Saturday1 == 2) ? 'PM' : ((value1.Saturday1 == 0) ? '' : ((value1.Saturday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                    '<tr><td>Mon</td><td><img src="' + ((value1.Monday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Monday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Monday == 1) ? 'AM' : ((value1.Monday == 2) ? 'PM' : ((value1.Monday == 0) ? '' : ((value1.Monday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RMonday + '</td><td><img src="' + ((value1.Monday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Monday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Monday1 == 1) ? 'AM' : ((value1.Monday1 == 2) ? 'PM' : ((value1.Monday1 == 0) ? '' : ((value1.Monday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                    '</tbody></table>';

                                        });
                                    },
                                    complete: function () { },
                                    error: function (xhr, textStatus, errorThrown) {
                                    }
                                });
                                //****************Setting up data content for the popover *******************************
                            });
                            $('#ATreeOfficer' + $.trim(value.OfficerId)).attr('class', 'lastLeafMouse');
                            $('#ATreeOfficer' + $.trim(value.OfficerId)).attr('data-placement', 'right');
                            $('#ATreeOfficer' + $.trim(value.OfficerId)).attr('data-content', datacontent);
                            $('#ATreeOfficer' + $.trim(value.OfficerId)).attr('panelcontent', datacontentforDisplay);
                            $('#ATreeOfficer' + $.trim(value.OfficerId)).popover('show');
                            $('#DivAvailability').empty();
                            $('#DivAvailability').append($('#ATreeOfficer' + value.OfficerId).attr('panelcontent'));
                        });
                        $('#ATreeOfficer' + value.OfficerId).mouseout(function () {
                            $('#ATreeOfficer' + value.OfficerId).removeClass('lastLeafMouse');
                            $('#ATreeOfficer' + value.OfficerId).popover('hide');
                        });

                        if (SelectedOfficerID > 0) {
                            $('.activeListview').removeClass('activeListview');
                            $('#ATreeOfficer' + SelectedOfficerID).addClass('activeListview');
                        }
                        if (value.IsLogged != 0) {
                            $('#LI' + value.OfficerId).addClass('sidebar-nav-active');
                            $('#img' + value.OfficerId).show();
                        }
                        else {
                            $('#LI' + value.OfficerId).removeClass('sidebar-nav-active');
                            $('#img' + value.OfficerId).hide();
                        }

                        $('#ATreeOfficer' + value.OfficerId).click(function () {//.unbind()
                            LinkForManager = 2;
                            $('.lastLeaf').removeClass('lastLeaf');
                            $(this).addClass('lastLeaf');
                            $('#ATreeOfficer' + value.OfficerId).mouseout(function () {
                                $(this).addClass('lastLeaf');
                            });
                            $('#ATreeOfficer' + value.OfficerId).mouseover(function () {
                                $('.lastLeaf').removeClass('lastLeaf');
                            });
                            GetTimelineResult(value.OfficerId);

                            $('#lblSelectedOfficerID').html(value.OfficerId);
                            $('#lblSelectedGroupID,#lblSelectedManagerID').html('0');
                            $('#lblOfficerDisplay').html(value.OfficerName);

                            pLatitude = 0;
                            pLongitude = 0;
                            OffID = $(this).attr('id').substr(12);

                            $.session.set('Map', '', 1);
                            $.session.set('ManagerID', value.OfficerId);
                            $.session.set('MgrName', value.OfficerName);
                            $.session.set('TreeLevel', 3);
                            $.session.set('Availability', 1);
                            $.session.set('TimelineOfficer', value.OfficerId);

                            FillCaseActions('0', $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), value.OfficerId);//$(this).attr('id').substr(1)
                            FillWMAOfficer(value.ADMID, $('#txtFromDate').val(), $('#txtFromDate').val());
                            GetStats(value.OfficerId, 1);
                            FillLastKnownLocation('0', $('#txtFromDateLocation').val(), $('#txtFromDateLocation').val(), value.OfficerId);
                            $('#DTimeline,#DAvailability,#tblWMA').show();
                            $('#AHeartBeat,#AHeartBeatEnlarge,#AShowAll,#AShowEnlarge,#ALocationwithHB,#ALocationwithHBEnlarge').show();
                            $('#DivRanking,#showPanel2,#imgRefreshLocation,#tblWMASearchType').hide();
                        });
                    });
                    if (multiselect) {
                        var divTarget = parseInt($('#lblMonthTotalTarget').html() / ($('#tbodyOfficerList tr').length));
                        var remTarget = parseInt($('#lblMonthTotalTarget').html() % ($('#tbodyOfficerList tr').length));

                        $('#tbodyOfficerList tr').each(function (key, value) {
                            if (key == ($('#tbodyOfficerList tr').length - 1))
                                $(this).find('input').val(divTarget + remTarget);
                            else
                                $(this).find('input').val(divTarget);
                        });

                        $('#lblTotalTarget').html($.session.get('tgVal'));

                        $('#tbodyOfficerList').append($('<tr>')
                            .append($('<td>').append($('<span>').attr('class', 'pull-right').html('Total')))
                            .append($('<td>').append($('<label>').attr('id', 'lblTotalTarget'))))
                        .append($('<tr>').append($('<td>'))
                            .append($('<td>').append($('<button>').attr('id', 'btnSubmitOfficerTotal').attr('class', 'btn-primary')
                                                                  .attr('onclick', 'SubmitOfficerTotal()').html('Submit'))))

                        $('#btnSubmitOfficerTotal').click(function () {
                            var TTargetInfo = '';
                            $('#tbodyOfficerList tr').each(function (key, value) {
                                if ($(this).find('label').html() != '' && $(this).find('label').html() != undefined && parseInt($(this).find('input').val()) > 0) {
                                    if (TTargetInfo != '') TTargetInfo += ',';
                                    TTargetInfo += $(this).find('label').html() + '|' + parseInt($(this).find('input').val());
                                }
                            });

                            // ****************************** submit functionality
                            Type = "GET";
                            var inputParams = "/UpdateOfficerTarget?TMonth=" + $('#selCurrentMonth').val() + "&TargetNo=" +
                                $('#txtMonthTotalTarget').val() + "&ManagerID=" + ManagerID + "&OfficerIDs=" + TTargetInfo;

                            Url = serviceUrl + inputParams;
                            DataType = "jsonp"; ProcessData = false;

                            $.ajax({
                                type: Type,
                                url: Url, // Location of the service
                                contentType: ContentType, // content type sent to server
                                dataType: DataType, //Expected data format from server       
                                processdata: ProcessData, //True or False      
                                async: true,
                                timeout: 20000,
                                beforeSend: function () { },
                                complete: function () { },
                                success: function (result) {//On Successfull service call  
                                    $('#lblTargetInfo').html('Target has been updated successfully.');
                                    $('#ATargetInfo').click();
                                },
                                error: function () {
                                } // When Service call fails
                            });

                        });

                    }
                    $('#AShowAll').click(function () {
                        FillAllLocation(1);
                    });
                    $('#AShowEnlarge').click(function () {
                        FillAllLocation(2);
                    });
                    $('#selOfficer').multiselect({
                        noneSelectedText: 'Select Officers',
                        selectedList: 5,
                        multiple: true
                    }).multiselectfilter();

                    $('#selOfficer').multiselect("uncheckAll");
                }
                $('.tree li:has(ul)').addClass('parent_li');
                $('.tree li.parent_li > a').on('click', function (e) {
                    var children = $(this).parent('li.parent_li').find(' > ul > li');
                    if (children.is(":visible")) {
                        children.hide('fast');
                        $(this).find(' > i').addClass('glyphicon-plus-sign').removeClass('glyphicon-minus-sign');
                    } else {
                        children.show('fast');
                        $(this).find(' > i').addClass('glyphicon-minus-sign').removeClass('glyphicon-plus-sign');
                    }
                    e.stopPropagation();
                });
                $.loader('close');
            }

            //========================================================================================================================
            //TOOD: Caseaction

            counter.client.getcaseactions = function (message) {
                message = $.parseJSON(message);
                //*****************************************************************************************************************************************
                if (!(message == "[]" || message == null || message == undefined || message == '[{"message":"NoRecords"}]')) {
                    $.each(message, function (key, value) {
                        switch (value.ActionText) {
                            case 'Paid':
                            case 'PAID':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodypaidformanager').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                        $('#tbodyRankOfficerPaid').append($('<tr style = "background:#66FF99">')
                                      .append($('<td>').html(''))
                                      .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                      .append($('<td>').html(value.ActionText))
                                      .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                      .append($('<td>').html(value.DateActioned))
                                      .append($('<td>').html("" + value.Fees))
                                      .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficerRank(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                      .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                      )
                                        $('#tbodyRMPaidCase').append($('<tr style = "background:#66FF99">')
                                    .append($('<td>').html(''))
                                    .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                    .append($('<td>').html(value.ActionText))
                                    .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                    .append($('<td>').html(value.DateActioned))
                                    .append($('<td>').html("" + value.Fees))
                                    .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionRank(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                    .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                    )

                                    }
                                    else {
                                        $('#tbodypaidformanager').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                        $('#tbodyRankOfficerPaid').append($('<tr style = "background:#FFCCFF">')
                                      .append($('<td>').html(''))
                                      .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                      .append($('<td>').html(value.ActionText))
                                      .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                      .append($('<td>').html(value.DateActioned))
                                      .append($('<td>').html("" + value.Fees))
                                      .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficerRank(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                      .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                      )

                                        $('#tbodyRMPaidCase').append($('<tr style = "background:#FFCCFF">')
                                     .append($('<td>').html(''))
                                     .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                     .append($('<td>').html(value.ActionText))
                                     .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                     .append($('<td>').html(value.DateActioned))
                                     .append($('<td>').html("" + value.Fees))
                                     .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionRank(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                     .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                     )
                                    }
                                }
                                else {
                                    $('#tbodypaidformanager').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    $('#tbodyRankOfficerPaid').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficerRank(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    $('#tbodyRMPaidCase').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionRank(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                            case 'Part Paid':
                            case 'PART PAID':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodypartpaidformanager').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 2 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                        $('#tbodyRankOfficerPartPaid').append($('<tr style = "background:#66FF99">')
                                    .append($('<td>').html(''))
                                    .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                    .append($('<td>').html(value.ActionText))
                                    .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                    .append($('<td>').html(value.DateActioned))
                                    .append($('<td>').html("" + value.Fees))
                                    .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficerRank(" + key + "," + 2 + ")'> ...More</label>" : value.Notes)))
                                    .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                    )
                                        $('#tbodyRMPPCase').append($('<tr style = "background:#66FF99">')
                                  .append($('<td>').html(''))
                                  .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                  .append($('<td>').html(value.ActionText))
                                  .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                  .append($('<td>').html(value.DateActioned))
                                  .append($('<td>').html("" + value.Fees))
                                  .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionRank(" + key + "," + 2 + ")'> ...More</label>" : value.Notes)))
                                  .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                  )
                                    }
                                    else {
                                        $('#tbodypartpaidformanager').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 2 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                        $('#tbodyRankOfficerPartPaid').append($('<tr style = "background:#FFCCFF">')
                                      .append($('<td>').html(''))
                                      .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                      .append($('<td>').html(value.ActionText))
                                      .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                      .append($('<td>').html(value.DateActioned))
                                      .append($('<td>').html("" + value.Fees))
                                      .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficerRank(" + key + "," + 2 + ")'> ...More</label>" : value.Notes)))
                                      .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                      )
                                        $('#tbodyRMPPCase').append($('<tr style = "background:#FFCCFF">')
                                     .append($('<td>').html(''))
                                     .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                     .append($('<td>').html(value.ActionText))
                                     .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                     .append($('<td>').html(value.DateActioned))
                                     .append($('<td>').html("" + value.Fees))
                                     .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionRank(" + key + "," + 2 + ")'> ...More</label>" : value.Notes)))
                                     .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                     )
                                    }
                                }
                                else {
                                    $('#tbodypartpaidformanager').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 2 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    $('#tbodyRankOfficerPartPaid').append($('<tr>')
                                      .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                      .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                      .append($('<td>').html(value.ActionText))
                                      .append($('<td>').html(value.DoorColour))
                                      .append($('<td>').html(value.HouseType))
                                      .append($('<td>').html(value.DateActioned))
                                      .append($('<td>').html("" + value.Fees))
                                      .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficerRank(" + key + "," + 2 + ")'> ...More</label>" : value.Notes)))
                                      .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                      )
                                    $('#tbodyRMPPCase').append($('<tr>')
                                     .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                     .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                     .append($('<td>').html(value.ActionText))
                                     .append($('<td>').html(value.DoorColour))
                                     .append($('<td>').html(value.HouseType))
                                     .append($('<td>').html(value.DateActioned))
                                     .append($('<td>').html("" + value.Fees))
                                     .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionRank(" + key + "," + 2 + ")'> ...More</label>" : value.Notes)))
                                     .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                     )
                                }
                                break;
                            case 'Returned':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyreturnedformanager').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 4 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                        $('#tbodyRankOfficerReturn').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficerRank(" + key + "," + 4 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                        $('#tbodyRMReturnCase').append($('<tr style = "background:#66FF99">')
                                      .append($('<td>').html(''))
                                      .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                      .append($('<td>').html(value.ActionText))
                                      .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                      .append($('<td>').html(value.DateActioned))
                                      .append($('<td>').html("" + value.Fees))
                                      .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionRank(" + key + "," + 4 + ")'> ...More</label>" : value.Notes)))
                                      .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                      )
                                    }
                                    else {
                                        $('#tbodyreturnedformanager').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 4 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                        $('#tbodyRankOfficerReturn').append($('<tr style = "background:#FFCCFF">')
                                     .append($('<td>').html(''))
                                     .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                     .append($('<td>').html(value.ActionText))
                                     .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                     .append($('<td>').html(value.DateActioned))
                                     .append($('<td>').html("" + value.Fees))
                                     .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficerRank(" + key + "," + 4 + ")'> ...More</label>" : value.Notes)))
                                     .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                     )
                                        $('#tbodyRMReturnCase').append($('<tr style = "background:#FFCCFF">')
                                        .append($('<td>').html(''))
                                        .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                        .append($('<td>').html(value.ActionText))
                                        .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                        .append($('<td>').html(value.DateActioned))
                                        .append($('<td>').html("" + value.Fees))
                                        .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionRank(" + key + "," + 4 + ")'> ...More</label>" : value.Notes)))
                                        .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                        )
                                    }
                                }
                                else {
                                    $('#tbodyreturnedformanager').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 4 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    $('#tbodyRankOfficerReturn').append($('<tr>')
                                      .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                      .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                      .append($('<td>').html(value.ActionText))
                                      .append($('<td>').html(value.DoorColour))
                                      .append($('<td>').html(value.HouseType))
                                      .append($('<td>').html(value.DateActioned))
                                      .append($('<td>').html("" + value.Fees))
                                      .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficerRank(" + key + "," + 4 + ")'> ...More</label>" : value.Notes)))
                                      .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                      )
                                    $('#tbodyRMReturnCase').append($('<tr>')
                                     .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                     .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                     .append($('<td>').html(value.ActionText))
                                     .append($('<td>').html(value.DoorColour))
                                     .append($('<td>').html(value.HouseType))
                                     .append($('<td>').html(value.DateActioned))
                                     .append($('<td>').html("" + value.Fees))
                                     .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionRank(" + key + "," + 4 + ")'> ...More</label>" : value.Notes)))
                                     .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                     )
                                }
                                break;
                            case 'Left Letter':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyleftletterformanager').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 5 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodyleftletterformanager').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 5 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodyleftletterformanager').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 5 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                            case '£ Collected':
                            case 'Revisit':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyrevisitformanager').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 3 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodyrevisitformanager').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 3 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodyrevisitformanager').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 3 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                            case 'Bailed':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodybailedformanager')
                                            .append('<tr><td>' + value.CaseNumber + '</td>' +
                                                    '<td>' + value.Officer + '</td>' +
                                                    '<td>' + value.DateActioned + '</td>' +
                                                    '<td><a href="OptimiseImage.html" target="_blank" ><img style="padding-right:10px; src="images/ExpandImage_24_24_2.png" alt="Expand" title="Click to Expand image"  onclick="ViewBailedImage(' + key + ',' + "'" + value.ImageURL + "'" + ')"/></a>' +
                                                     '<img id="imgBailedImage' + key + '" src="images/ViewImage_24_24_2.png" class="BailedUnselect"  alt="View" title="Click to view image" style="cursor:pointer;" onclick="ViewBailedImage(' + key + ',' + "'" + value.ImageURL + "'" + ')"/></td></tr>')
                                        if (Bailed == 0) {
                                            $('#imgBailedImage' + key).attr('src', 'images/ViewImage_24_24_1.png').attr('class', 'BailedSelect');
                                            ViewBailedImage(key, "'" + value.ImageURL + "'");
                                            Bailed += 1;
                                        }
                                    }
                                    else {
                                        $('#tbodybailedformanager')
                                  .append('<tr><td>' + value.CaseNumber + '</td>' +
                                                    '<td>' + value.Officer + '</td>' +
                                                    '<td>' + value.DateActioned + '</td>' +
                                                    '<td><a href="OptimiseImage.html" target="_blank" ><img style="padding-right:10px; src="images/ExpandImage_24_24_2.png" alt="Expand" title="Click to Expand image"  onclick="ViewBailedImage(' + key + ',' + "'" + value.ImageURL + "'" + ')"/></a>' +
                                                     '<img id="imgBailedImage' + key + '" src="images/ViewImage_24_24_2.png" class="BailedUnselect"  alt="View" title="Click to view image" style="cursor:pointer;" onclick="ViewBailedImage(' + key + ',' + "'" + value.ImageURL + "'" + ')"/></td></tr>')
                                        if (Bailed == 0) {
                                            $('#imgBailedImage' + key).attr('src', 'images/ViewImage_24_24_1.png').attr('class', 'BailedSelect');
                                            ViewBailedImage(key, "'" + value.ImageURL + "'");
                                            Bailed += 1;
                                        }
                                    }
                                }
                                else {
                                    $('#tbodybailedformanager')
                                   .append('<tr><td>' + value.CaseNumber + '</td>' +
                                                    '<td>' + value.Officer + '</td>' +
                                                    '<td>' + value.DateActioned + '</td>' +
                                                    '<td><a href="OptimiseImage.html" target="_blank" ><img style="padding-right:10px;" src="images/ExpandImage_24_24_2.png" alt="Expand" title="Click to Expand image"  onclick="ViewBailedImage(' + key + ',' + "'" + value.ImageURL + "'" + ')"/></a>' +
                                                     '<img id="imgBailedImage' + key + '" src="images/ViewImage_24_24_2.png" class="BailedUnselect"  alt="View" title="Click to view image" style="cursor:pointer;" onclick="ViewBailedImage(' + key + ',' + "'" + value.ImageURL + "'" + ')"/></td></tr>')
                                    if (Bailed == 0) {
                                        $('#imgBailedImage' + key).attr('src', 'images/ViewImage_24_24_1.png').attr('class', 'BailedSelect');
                                        ViewBailedImage(key, "'" + value.ImageURL + "'");
                                        Bailed += 1;
                                    }
                                }
                                break;
                            case 'Arrested':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyarrestedformanager').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 3 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodyarrestedformanager').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 3 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodyarrestedformanager').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 3 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                            case 'Surrender date agreed':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodySurrenderdateagreedformanager').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 3 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodySurrenderdateagreedformanager').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 3 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodySurrenderdateagreedformanager').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 3 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;



                            case 'TCG':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionTCG,#tbodytcgformanager').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 3 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodyActionTCG,#tbodytcgformanager').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 3 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodyActionTCG,#tbodytcgformanager').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 3 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                            case 'TCG PAID':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionTCGPAID,#tbodytcgpaidformanager').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 4 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodyActionTCGPAID,#tbodytcgpaidformanager').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 4 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodyActionTCGPAID,#tbodytcgpaidformanager').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 4 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                            case 'TCG PP':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionTCGPP,#tbodytcgppformanager').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 5 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodyActionTCGPP,#tbodytcgppformanager').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 5 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodyActionTCGPP,#tbodytcgppformanager').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 5 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                            case 'UTTC':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionUTTC,#tbodyuttcformanager').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 6 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodyActionUTTC,#tbodyuttcformanager').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 6 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodyActionUTTC,#tbodyuttcformanager').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 6 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                            case 'DROPPED':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionDROPPED,#tbodydroppedformanager').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 7 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodyActionDROPPED,#tbodydroppedformanager').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 7 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodyActionDROPPED,#tbodydroppedformanager').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 7 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                            case 'Clamped':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyClampedformanager')
                                    .append('<tr><td>' + value.CaseNumber + '</td>' +
                                                   '<td>' + value.Officer + '</td>' +
                                                   '<td>' + value.DateActioned + '</td>' +
                                                    '<td><a href="OptimiseImage.html" target="_blank" ><img style="padding-right:10px;" src="images/ExpandImage_24_24_2.png" alt="Expand" title="Click to Expand image"  onclick="ViewClampedImage(' + key + ',' + value.Officer + ',' + "'" + value.CaseNumber + "'" + ')"/></a>' +
                                                   '<img id="imgClampImage' + key + '" src="images/ViewImage_24_24_2.png" class="clampUnselect"  alt="View" title="Click to view image" style="cursor:pointer;" onclick="ViewClampedImage(' + key + ',' + value.Officer + ',' + "'" + value.CaseNumber + "'" + ')"/></td></tr>')
                                        if (Clamp == 0) {
                                            ViewClampedImage(key, value.Officer, value.CaseNumber);
                                            $('#imgClampImage' + key).attr('src', 'images/ViewImage_24_24_1.png').attr('class', 'clampSelect');
                                            Clamp += 1;
                                        }
                                    }
                                    else {
                                        $('#tbodyClampedformanager')
                                     .append('<tr><td>' + value.CaseNumber + '</td>' +
                                                    '<td>' + value.Officer + '</td>' +
                                                    '<td>' + value.DateActioned + '</td>' +
                                                     '<td><a href="OptimiseImage.html" target="_blank" ><img style="padding-right:10px;" src="images/ExpandImage_24_24_2.png" alt="Expand" title="Click to Expand image"  onclick="ViewClampedImage(' + key + ',' + value.Officer + ',' + "'" + value.CaseNumber + "'" + ')"/></a>' +
                                                    '<img id="imgClampImage' + key + '" src="images/ViewImage_24_24_2.png" class="clampUnselect"  alt="View" title="Click to view image" style="cursor:pointer;" onclick="ViewClampedImage(' + key + ',' + value.Officer + ',' + "'" + value.CaseNumber + "'" + ')"/></td></tr>')
                                        if (Clamp == 0) {
                                            ViewClampedImage(key, value.Officer, value.CaseNumber);
                                            $('#imgClampImage' + key).attr('src', 'images/ViewImage_24_24_1.png').attr('class', 'clampSelect');
                                            Clamp += 1;
                                        }
                                    }
                                }
                                else {
                                    $('#tbodyClampedformanager')
                                      .append('<tr><td>' + value.CaseNumber + '</td>' +
                                                   '<td>' + value.Officer + '</td>' +
                                                   '<td>' + value.DateActioned + '</td>' +
                                                   '<td><a href="OptimiseImage.html" target="_blank" ><img style="padding-right:10px;" src="images/ExpandImage_24_24_2.png" alt="Expand" title="Click to Expand image"  onclick="ViewClampedImage(' + key + ',' + value.Officer + ',' + "'" + value.CaseNumber + "'" + ')"/></a>' +
                                                   '<img id="imgClampImage' + key + '" src="images/ViewImage_24_24_2.png" class="clampUnselect"  alt="View" title="Click to view image" style="cursor:pointer;" onclick="ViewClampedImage(' + key + ',' + value.Officer + ',' + "'" + value.CaseNumber + "'" + ')"/></td></tr>')
                                    if (Clamp == 0) {
                                        ViewClampedImage(key, value.Officer, value.CaseNumber);
                                        $('#imgClampImage' + key).attr('src', 'images/ViewImage_24_24_1.png').attr('class', 'clampSelect');
                                        Clamp += 1;
                                    }
                                }
                                break;
                            case 'OTHER':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionOTHER,#tbodyotherformanager').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 8 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodyActionOTHER,#tbodyotherformanager').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 8 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodyActionOTHER,#tbodyotherformanager').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 8 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                        }


                    });
                }
                else {
                    alert('empty');
                }
                //*****************************************************************************************************************************************
            };

            counter.client.gethubcaseactions = function (message) {
                message = $.parseJSON(message);
                //*****************************************************************************************************************************************
                if (message != undefined) {
                    if (message != "[]" && message != null && message != '[{"message":"NoRecords"}]') {
                        $('#tblbodyCA').empty();//,
                        if ((LinkForManager == 0 || LinkForManager == 1) && ($.session.get('RoleType') == 1)) {//($.trim($('#lblLoginOfficer').html()) == 'David Burton')
                            $.each(message, function (mkey, mvalue) {
                                $('#SpanAction1').html('Case Actions');
                                $('#tblbodyCA').append($('<tr>')
                                    .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                    .append($('<td>').append($('<a id="AActionModal' + mkey + '">').attr('data-toggle', 'modal')
                                                     .attr('href', '#divCaseActionModal').attr('onclick', 'caseaction(' + mkey + ')').html(mvalue.ActionText)))
                                    .append($('<td>').html(mvalue.Case))
                                    .append($('<td>').attr('title', 'Last four week average').html(mvalue.Ave)))

                                if (mvalue.ActionText == 'Defendant contact')
                                    $('#AActionModal' + mkey).removeAttr('href').removeAttr('onclick').attr('style', 'cursor:none')//('href', '#defendantcontact')
                            });
                        }
                        else if ((LinkForManager == 0 || LinkForManager == 1) && ($.session.get('RoleType') != 1)) {
                            $.each(message, function (mkey, mvalue) {
                                $('#SpanAction1').html('Case Actions');
                                $('#tblbodyCA').append($('<tr>')
                                    .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                    .append($('<td>').append($('<a id="AActionModal' + mkey + '">').attr('data-toggle', 'modal').attr('href', '#divCaseActionModal').attr('onclick', 'caseaction(' + mkey + ')').html(mvalue.ActionText)))
                                    .append($('<td>').html(mvalue.Case))
                                    .append($('<td>').attr('title', 'Last four week average').html(mvalue.Ave)))
                                if (mvalue.ActionText == 'Defendant contact')
                                    $('#AActionModal' + mkey).attr('href', '#defendantcontact')
                            });
                        }
                        else {
                            $('#tblbodyCA').empty();
                            $('#SpanAction1').html('Case Actions');
                            $('#SpanAction2').html('Warrant Matching');
                            $.each(message, function (mkey, mvalue) {
                                $('#tblbodyCA').append($('<tr>')
                                    .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                    .append($('<td>').append($('<a id="AActionModal' + mkey + '">').attr('data-toggle', 'modal').attr('href', '#' + mvalue.ActionText.toString().toLowerCase().replace(" ", "").replace(" ", "")).attr('onclick', 'caseaction(' + mkey + ')').html(mvalue.ActionText)))
                                    .append($('<td>').html(mvalue.Case))
                                    .append($('<td>').attr('title', 'Last four week average').html(mvalue.Ave)))
                                if (mvalue.ActionText == 'Defendant contact')
                                    $('#AActionModal' + mkey).attr('href', '#defendantcontact')
                            });
                        }

                        $.each(message, function (mkey, mvalue) {
                            // alert(mvalue.GPSLatitude);
                        });
                    }
                }
                //*****************************************************************************************************************************************
            };

            counter.client.getcaseactionmanager = function (message, flag, type) {
                message = $.parseJSON(message);
                //*****************************************************************************************************************************************
                if (message != undefined) {
                    if (message != "[]" && message != null && message != '[{"message":"NoRecords"}]') {
                        if (type == 'Manager') $('#tbodyCaseActionsManagerCount').empty();
                        if (type == 'ADM') $('#tbodyCaseActionsADMCount').empty();
                        if (type == 'Officer') $('#tbodyCaseActionsOfficerCount').empty();
                        $.each(message, function (key, value) {
                            //*************************************************************
                            if (type == 'Manager') {
                                $('#tbodyCaseActionsManagerCount')
                                .append($('<tr>').append($('<td>').append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.OfficerID).hide())
                                .append($('<a>').attr('onclick', 'GetCaseActionsADMCount(' + flag + ',' + $.trim(value.OfficerID) + ',' + 0 + ',' + "'" + value.OfficerName + "'" + ')')
                                    .attr('href', '#' + $('#HCaseActionTitle').html().toString().toLowerCase().replace(" ", "")).html(value.OfficerName)))
                                .append($('<td>').html(value.CaseCnt)))
                            }
                            else if (type == "ADM") {
                                $('#tbodyCaseActionsADMCount')
                                   .append($('<tr>').append($('<td>').append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.OfficerID).hide())
                                   .append($('<a>').attr('onclick', 'GetCaseActionsOfficerCount(' + flag + ',' + $.trim(value.OfficerID) + ',' + 0 + ',' + "'" + value.OfficerName + "'" + ')')
                                   .attr('href', '#' + $('#HCaseActionTitle').html().toString().toLowerCase().replace(" ", "")).html(value.OfficerName)))
                                   .append($('<td>').html(value.CaseCnt)))
                            }
                            else if (type == "Officer") {
                                var dateAr = $('#txtFromDateCA').val().split('/');
                                var newDate = dateAr[2] + '/' + dateAr[1] + '/' + dateAr[0];

                                $('#tbodyCaseActionsOfficerCount')
                                    .append($('<tr>').append($('<td>').append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.OfficerID).hide())
                                    .append($('<a>').attr('onclick', 'clickoncaseactionmanager(' + flag + ',' + key + ',' + $.trim(value.OfficerID) + ",'" + value.OfficerName + "','" + newDate + "','" + newDate + "'" + ')')
                                        .attr('href', '#' + $('#HCaseActionTitle').html().toString().toLowerCase().replace(" ", "")).html(value.OfficerName)))
                                    .append($('<td>').html(value.CaseCnt)))
                            }

                            //*************************************************************

                        });
                    }
                    else {
                        if (type == 'Manager') $('#tbodyCaseActionsManagerCount').empty().html('No records found');
                        if (type == 'ADM') $('#tbodyCaseActionsADMCount').empty().html('No records found');
                        if (type == 'Officer') $('#tbodyCaseActionsOfficerCount').empty().html('No records found');
                    }
                }
                else {
                    if (type == 'Manager') $('#tbodyCaseActionsManagerCount').empty().html('No records found');
                    if (type == 'ADM') $('#tbodyCaseActionsADMCount').empty().html('No records found');
                    if (type == 'Officer') $('#tbodyCaseActionsOfficerCount').empty().html('No records found');
                }
            };

            //========================================================================================================================
            //TODO: WMA 
            counter.client.getwmadetail = function (message, type) {
                if (message == '[]') {
                    if (type == 'Manager') $('#tbodyCaseActionsADMCount').empty().html('No records found');
                    if (type == 'ADM') {
                        $('#AMoreWMA').hide();
                        $('#tbodyWMA').empty();
                    }
                    if (type == 'Officer') {
                        $('#AMoreWMA').hide();
                        $('#tbodyWMA').empty();
                    }
                }
                else {
                    message = $.parseJSON(message);
                    if (type == 'Manager')
                        $('#tbodyWMA,#tbodyWMAMore').empty();
                    if (type == 'ADM') {
                        switch ($.session.get('TreeLevel')) {
                            case '0':
                                $('#tbodyWMAADM,#tbodyWMAMoreADM').empty();
                                break;
                            case '1':
                                $('#tbodyWMA,#tbodyWMAMoreADM').empty();
                                break;
                        }
                    }
                    if (type == 'Officer') {
                        switch ($.session.get('TreeLevel')) {
                            case '0':
                                $('#tbodyWMAOfficers,#tbodyWMAMoreOfficer').empty();
                                break;
                            case '1':
                                $('#tbodyWMAOfficers,#tbodyWMAMoreOfficer').empty();
                                break;
                            case '2':
                                $('#tbodyWMA,#tbodyWMAMoreOfficer').empty();
                                break;
                            case '3':
                                $('#tbodyWMAMoreOfficer').empty();
                                break;
                        }
                    }
                    $.each(message, function (key, value) {
                        //*************************************************************
                        if (type == 'Manager') {
                            $('#AMoreWMA').show();
                            if (key <= 4) {
                                $('#tbodyWMA')
                                    .append('<tr><td><i class="icon24"><img src="images/Case-ico.png" alt="icon"></i></td>' +
                                    '<td><a id="AMGR' + value.OfficerID + '" onclick="GetManager(' + value.OfficerID + ',' + "'" + value.OfficerName + "'" + ',' + "'" + value.FromDate + "',0" + ')" data-toggle="modal" href="#windowTitleDialog">' + value.OfficerName + '</a></td>' +
                                    '<td>' + value.CaseCnt + '</td></tr>');
                            }
                            $('#tbodyWMAMore')
                                .append('<tr><td><a onclick="GetManager(' + value.OfficerID + ',' + "'" + value.OfficerName + "'" + ',' + "'" + value.FromDate + "',1" + ')" data-toggle="modal" href="#">' + value.OfficerName + '</a></td>' +
                                '<td>' + value.CaseCnt + '</td></tr>');
                        }
                        else if (type == "ADM") {
                            $('#AMoreWMA').show();
                            switch ($.session.get('TreeLevel')) {
                                case '0':
                                    $('#tbodyWMAADM')
                                         .append('<tr><td><a onclick="GetAdm(' + value.OfficerID + ',' + "'" + value.OfficerName + "'" + ',' + "'" + value.FromDate + "',0" + ')" data-toggle="modal" style="cursor:pointer;">' + value.OfficerName + '</a></td>' +
                                    '<td>' + value.CaseCnt + '</td></tr>');
                                    break;
                                case '1':
                                    if (key <= 4) {
                                        $('#tbodyWMA')
                                               .append('<tr><td><i class="icon24"><img src="images/Case-ico.png" alt="icon"></i></td>' +
                                               '<td><a onclick="GetAdm(' + value.OfficerID + ',' + "'" + value.OfficerName + "'" + ',' + "'" + value.FromDate + "',0" + ')" data-toggle="modal" href="#windowTitleDialog" >' + value.OfficerName + '</a></td>' +
                                               '<td>' + value.CaseCnt + '</td></tr>');
                                    }
                                    break;
                            }
                            $('#tbodyWMAMoreADM')
                                .append('<tr><td><a href="#" onclick="GetAdm(' + value.OfficerID + ',' + "'" + value.OfficerName + "'" + ',' + "'" + value.FromDate + "',1" + ')" style="cursor:pointer;">' + value.OfficerName + ' </a><td>' +
                                '<td>' + value.CaseCnt + '</td></tr>');
                        }
                        else if (type == "Officer") {
                            $('#AMoreWMA').show();
                            switch ($.session.get('TreeLevel')) {
                                case '0':
                                case '1':
                                    $('#tbodyWMAOfficers').append('<tr><td><a onclick="GetOfficer(' + value.OfficerID + ',' + "'" + value.OfficerName + "'" + ',' + "'" + value.FromDate + "',0" + ')" data-toggle="modal" style="cursor:pointer;">' + value.OfficerName + '</a></td>' +
                                       '<td>' + value.CaseCnt + '</td></tr>');
                                    break;
                                case '2':
                                    if (key <= 4) {
                                        $('#tbodyWMA')
                                           .append('<tr><td><i class="icon24"><img src="images/Case-ico.png" alt="icon"></i></td>' +
                                           '<td><a onclick="GetOfficer(' + value.OfficerID + ',' + "'" + value.OfficerName + "'" + ',' + "'" + value.FromDate + "',0" + ')" data-toggle="modal" href="#windowTitleDialog">' + value.OfficerName + '</a></td>' +
                                           '<td>' + value.CaseCnt + '</td></tr>');
                                    }
                                    break;
                            }

                            $('#tbodyWMAMoreOfficer')
                                .append('<tr><td><a href="#" onclick="GetOfficer(' + value.OfficerID + ',' + "'" + value.OfficerName + "'" + ',' + "'" + value.FromDate + "',1" + ')" style="cursor:pointer;">' + value.OfficerName + ' </a><td>' +
                                '<td>' + value.CaseCnt + '</td></tr>');
                        }
                        //*************************************************************
                    });
                }
            };

            counter.client.getwmalogdetail = function (message, type) {
                message = $.parseJSON(message);
                //*****************************************************************************************************************************************
                if (message != undefined) {
                    if (message != "[]" && message != null && message != '[{"message":"NoRecords"}]') {
                        $('#tblbodySearchType').empty();
                        $('#tbodyWMAMoreOfficerCaseCount').empty();

                        showhidetabs('tblbodySearchType');
                        $('#lblSearchType').html('');
                        $('#btnBack,#btnWMAOfficerBack,#tblWMAADM').hide();
                        $('#tblbodyWMASearchType').empty();

                        if ($.session.get('TreeLevel') == 0 || $.session.get('TreeLevel') == 1) {
                            $('#btnWMAOfficerBack').show();
                        }
                        else {
                            $('#btnWMAOfficerBack').hide();
                        }
                        $.each(message, function (key, value) {
                            $('#tblbodySearchType')
                                .append('<tr><td><a style="cursor:pointer" onclick="GetWMACase(' + value.OfficerID + ',' + "'" + value.FromDate + "'" + ',' + "'" + value.LogType + "',0" + ')">' + value.LogType + '</a></td>' +
                                '<td>' + value.Case + '</td></tr>');

                            $('#tbodyWMAMoreOfficerCaseCount')
                             .append('<tr><td><a style="cursor:pointer" onclick="GetWMACase(' + value.OfficerID + ',' + "'" + value.FromDate + "'" + ',' + "'" + value.LogType + "',1" + ')">' + value.LogType + '</a></td>' +
                             '<td>' + value.Case + '</td></tr>');
                        });
                    }
                }
                //*****************************************************************************************************************************************
            };

            counter.client.getwmacasedetail = function (message) {
                var WMACase = message.split('||')[0];
                $('#tbodysearchdetails,#tbodyWMAMoreOfficerCase').empty();
                if (!(WMACase == undefined || WMACase == '[]')) {
                    message = $.parseJSON(WMACase);
                    $.each(message, function (key, value) {
                        $('#tbodysearchdetails,#tbodyWMAMoreOfficerCase')
                            .append('<tr><td>' + value.SearchText + '</td>' +
                                '<td>' + value.TotalRecordCount + '</td>' +
                                '<td>' + value.ActionedDate + '</td></tr>');
                    });
                }
                else {
                    $('#tbodysearchdetails,#tbodyWMAMoreOfficerCase').html('No warrant matching activity found for ' + message.split('||')[1]);
                }
            };

            //========================================================================================================================
            //TODO: Stats

            counter.client.gethubstats = function (message, link) {
                message = $.parseJSON(message);
                $('#tbodyStats').empty();
                $.each(message, function (key, value) {
                    var StatFormula = '<table width="100%" class="display-status-table"><tr><td>' +
                                               '<div class="block" style="margin:2px!important;padding:2px!important">' +
                                               '<a class="close" data-dismiss="modal" href="#">X</a>' +
                                               '<p class="block-heading">Percentage calculation</p><table class="">' +
                                               '<tbody>' +
                                               '<tr><td>Formula for ' + value.Description + ' : ' + value.Formula + '</td></tr>' +
                                               '<tr><td>' + '</td></tr>' +
                                               '<tr><td>Calculation for ' + value.Description + ' : ' + value.Calculation + '</td></tr>' +
                                              '</tbody></table></div></td>' +
                                              '</td></tr></table>';

                    $('#tbodyStats').append($('<tr>')
                        .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                        .append($('<td>').append($('<a>').attr('id', 'AStat' + key).attr('style', 'cursor:pointer;')
                                            .attr('onclick', 'GetStatsOnClick(' + key + ')').html(value.Description)
                                            .attr('data-original-title', '').attr('rel', 'popover').attr('class', 'a1').attr('data-html', 'true')))
                        .append($('<td id="tdbodyStats' + key + '">').html(value.DisplayValue)))

                    $('#AStat' + key).mouseover(function () {
                        $('#AStat' + key).attr('data-content', StatFormula);
                        $('#AStat' + key).attr('data-placement', 'left');
                        $('#AStat' + key).popover('show');
                    });
                    $('#AStat' + key).mouseout(function () {
                        $('#AStat' + key).popover('hide');
                    });

                    if (link == 1) {
                        if ($.trim(value.Description) == "Case holdings") {
                            $('#tdbodyStats' + key).empty();//data-toggle="modal" href="#DivLogout"
                            $('#tdbodyStats' + key).append($('<a>').attr('data-toggle', 'modal').attr('href', '#divCaseSearchByOfficerID').attr('onclick', 'GetSatasCaseActions(' + $.session.get('ManagerID') + ')').html(value.DisplayValue));
                        }
                    }
                });
                $('.close').click(function () { $('.popover').remove(); });
            };

            counter.client.getstatscaseactions = function (message) {
                message = $.parseJSON(message);

                $('#tbodyCaseSearchByOfficerID').empty();
                var HTMLString = '';

                $.each(message, function (key, value) {
                    HTMLString += '<tr><td>' + value.casenumber + '</td><td>' + value.DebtName + '</td><td>' + value.ClientName + '</td><td>' + value.ContactFirstLine + ', ' + value.ContactAddress + ', ' + value.PostCode + '</td>' +
                            '<td>' + value.CaseStatus + '</td><td>' + value.OfficerFirstName + ' ' + value.OfficerLastName + '</td><td>' + value.Ageing + '</td><td>' + value.IssueDate + '</td><td>' + value.DateActioned + '</td></tr>';
                });
                $('#tbodyCaseSearchByOfficerID').html(HTMLString);

                $('#tblCaseSearchByOfficerID').dataTable({
                    "sScrollY": "370px",
                    "bAutoWidth": false,
                    "bPaginate": true,
                    "bDestroy": true,
                    "bSort": false,
                    "sPaginationType": "full_numbers",
                    "bLengthChange": false,
                    "bScrollCollapse": true,
                    "sInfo": true,
                    "iDisplayLength": 5
                });
            };

            //========================================================================================================================
            //TODO: Lastlocation
            counter.client.getlastlocation = function (message) {
                $('#map_canvas').show();
                markers = [];
                if (message != undefined && message != '[]' && message != '[{"Result":"NoRecords"}]') {
                    $('#lblDisplay').html('').hide();
                    result = JSON.parse(message);
                    var iconImg1;

                    center = new google.maps.LatLng(52.8849565, -1.9770329);
                    options = {
                        'zoom': 6,
                        'center': center,
                        'mapTypeId': google.maps.MapTypeId.ROADMAP
                    };
                    map = new google.maps.Map(document.getElementById("map_canvas"), options);
                    mapExpand = new google.maps.Map(document.getElementById("map_canvas_Expand"), options);

                    infowindow = new google.maps.InfoWindow();

                    $.each(result, function (key, value) {
                        switch (value.IconType) {
                            case '1': // UTTC
                                iconImg1 = "images/map-icon-yellow.png";
                                break;
                            case '2':  // Returned, DROPPED
                                iconImg1 = "images/map-icon-red.png";
                                break;
                            case '3':  // Revisit,TCG
                                iconImg1 = "images/map-icon-floresent.png";
                                break;
                            case '5':  //  Paid , Part paid , TCG PP,TCG Paid 
                                iconImg1 = "images/map-icon-orange.png";
                                break;
                            case '6':  // Login
                                iconImg1 = "images/map-icon-blue.png";
                                break;
                            case '7':  // ARR
                                iconImg1 = "images/map-icon-navyblue.png";
                                break;
                            case '8':  // DEP
                                iconImg1 = "images/map-icon-lightvio.png";
                                break;
                            case '9':  // Logout
                                iconImg1 = "images/map-icon-grey.png";
                                break;
                            default:
                                iconImg1 = "images/map-icon-white.png";
                        }
                        Latitude[key] = value.latitude;
                        Longitude[key] = value.longitude;
                        if (Latitude[key] != pLatMap && Longitude[key] != pLongMap) {
                            Icon[key] = iconImg1;
                            Content[key] = value.html;
                            pContentkey = key;
                        }
                        else
                            Content[pContentkey] = Content[pContentkey] + '<br><hr>' + value.html;
                        pLatMap = value.latitude;
                        pLongMap = value.longitude;
                    });

                    for (var i = 0; i < result.length; i++) {
                        if (Latitude[i] != pLatitude && Longitude[i] != pLongitude) {
                            latLng = new google.maps.LatLng(Latitude[i], Longitude[i]);
                            marker = new google.maps.Marker({
                                'position': latLng,
                                'map': map,
                                'icon': Icon[i]
                            });
                            addInfoWindow(marker, Content[i]);
                            markers.push(marker);

                            markerExpand = new google.maps.Marker({
                                'position': latLng,
                                'map': mapExpand,
                                'icon': Icon[i]
                            });
                            addInfoWindow(markerExpand, Content[i]);
                            markersExpand.push(markerExpand);
                        }
                        pLatitude = Latitude[i];
                        pLongitude = Longitude[i];
                    }
                }
                else {
                    $('#lblDisplay').html('No activity recorded for today!').show();
                    center = new google.maps.LatLng('52.8849565', '-1.9770329');
                    options = {
                        'zoom': 6,
                        'center': center,
                        'mapTypeId': google.maps.MapTypeId.ROADMAP
                    };
                    map = new google.maps.Map(document.getElementById("map_canvas"), options);
                }
            };

            counter.client.getheartbeat = function (message) {
                $('#lblDisplay').html('').hide();
                markers = [];
                if (message != undefined && message != '[]' && message != '[{"Result":"NoRecords"}]') {
                    result = JSON.parse(message);
                    center = new google.maps.LatLng(51.699467, 0.109348);
                    options = {
                        'zoom': 5,
                        'center': center
                        //  'mapTypeId': google.maps.MapTypeId.ROADMAP
                    };

                    map = new google.maps.Map(document.getElementById("DHeartBeatMap"), options);
                    infowindow = new google.maps.InfoWindow();

                    $.each(result, function (key, value) {
                        latLng = new google.maps.LatLng(value.latitude, value.longitude);
                        marker = new google.maps.Marker({
                            'position': latLng,
                            'map': map,
                            'icon': "images/map-icon-pink.png"
                        });

                        markers.push(marker);

                        google.maps.event.addListener(markers[key], 'click', function (e) {
                            infowindow.setContent(value.html);
                            infowindow.open(map, this);
                        });
                    });
                }
                else {
                    $('#lblDisplay').html('No activity recorded for today!').show();
                    center = new google.maps.LatLng(52.8849565, -1.9770329);
                    options = {
                        'zoom': 5,
                        'center': center,
                        'mapTypeId': google.maps.MapTypeId.ROADMAP
                    };
                    map = new google.maps.Map(document.getElementById("DHeartBeatMap"), options);
                }
            };

            //========================================================================================================================
            //TODO: Ranking
            counter.client.getrankingmanager = function (message, flag, type) {
                message = $.parseJSON(message);
                //*****************************************************************************************************************************************
                if (message != undefined) {
                    if (message != "[]" && message != null && message != '[{"message":"NoRecords"}]') {
                        if (type == 'Manager') {
                            switch (flag) {
                                case '1':
                                    $('#tblbodyPaid,#tbodyRMPaidManager').empty();
                                    break;
                                case '2':
                                    $('#tblbodyReturned,#tbodyRMReturnManager').empty();
                                    break;
                                case '3':
                                    $('#tblbodyPP,#tbodyRMPPManager').empty();
                                    break;
                            }
                        }
                        if (type == 'ADM') {
                            switch (flag) {
                                case '1':
                                    if ($.session.get('TreeLevel') == '0')
                                        $('#tbodyADMRank,#tbodyRMPaidADM').empty();
                                    else
                                        $('#tbodyADMRank,#tblbodyPaid,#tbodyRMPaidADM').empty();
                                    break;
                                case '4':
                                    if ($.session.get('TreeLevel') == '0')
                                        $('#tbodyADMRank,#tbodyRMReturnADM').empty();
                                    else
                                        $('#tbodyADMRank,#tblbodyReturned,#tbodyRMReturnADM').empty();

                                    break;
                                case '2':
                                    if ($.session.get('TreeLevel') == '0')
                                        $('#tbodyADMRank,#tbodyRMPPADM').empty();
                                    else
                                        $('#tbodyADMRank,#tblbodyPP,#tbodyRMPPADM').empty();
                                    break;
                            }
                        }
                        if (type == 'Officer') {
                            switch (flag) {
                                case '1':
                                    if ($.session.get('TreeLevel') == '2')
                                        $('#tbodyOfficerRank,#tblbodyPaid,#tbodyRMPaidOfficer').empty();
                                    else
                                        $('#tbodyOfficerRank,#tbodyRMPaidOfficer').empty();
                                    break;
                                case '4':
                                    if ($.session.get('TreeLevel') == '2')
                                        $('#tbodyOfficerRank,#tblbodyReturned,#tbodyRMReturnOfficer').empty();
                                    else
                                        $('#tbodyOfficerRank,#tbodyRMReturnOfficer').empty();
                                    break;
                                case '2':
                                    if ($.session.get('TreeLevel') == '2')
                                        $('#tbodyOfficerRank,#tblbodyPP,#tbodyRMPPOfficer').empty();
                                    else
                                        $('#tbodyOfficerRank,#tbodyRMPPOfficer').empty();
                                    break;
                            }
                        }

                        $.each(message, function (key, value) {
                            //*************************************************************
                            if (type == 'Manager') {
                                switch (value.ActionText) {
                                    case 'Paid':
                                        if (key <= 4) {
                                            // $('#ARankingPaidMore').show();
                                            $('#tblbodyPaid').append($('<tr>')
                                                        .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                                        .append($('<td>').append($('<a>').attr('data-toggle', 'modal').attr('href', '#divOfficerRank').attr('onclick', 'FillRankingSelection(1,' + value.OfficerID + ',1,"' + value.OfficerName + '","' + value.ActionText + '")').html(value.OfficerName)))
                                                        .append($('<td>').append($('<label>').html(value.CaseCnt))))
                                        }
                                        $('#tbodyRMPaidManager')
                                            .append($('<tr>')
                                                .append($('<td>').append($('<a>').attr('data-toggle', 'modal').attr('href', '#').attr('onclick', 'FillRankingSelection(1,' + value.OfficerID + ',5,"' + value.OfficerName + '","' + value.ActionText + '")').html(value.OfficerName)))
                                                .append($('<td>').append($('<label>').html(value.CaseCnt))))

                                        break;
                                    case 'Returned':
                                        if (key <= 4) {
                                            //   $('#ARankingReturnedMore').show();
                                            $('#tblbodyReturned').append($('<tr>')
                                                        .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                                        .append($('<td>').append($('<a>').attr('data-toggle', 'modal').attr('href', '#divOfficerRank').attr('onclick', 'FillRankingSelection(2,' + value.OfficerID + ',1,"' + value.OfficerName + '","' + value.ActionText + '")').html(value.OfficerName)))
                                                        .append($('<td>').append($('<label>').html(value.CaseCnt))))
                                        }
                                        $('#tbodyRMReturnManager')
                                           .append($('<tr>')
                                               .append($('<td>').append($('<a>').attr('data-toggle', 'modal').attr('href', '#').attr('onclick', 'FillRankingSelection(2,' + value.OfficerID + ',5,"' + value.OfficerName + '","' + value.ActionText + '")').html(value.OfficerName)))
                                               .append($('<td>').append($('<label>').html(value.CaseCnt))))
                                        break;
                                    case 'Part Paid':
                                        if (key <= 4) {
                                            //   $('#ARankingPartPaidMore').show();
                                            $('#tblbodyPP').append($('<tr>')
                                                      .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                                      .append($('<td>').append($('<a>').attr('data-toggle', 'modal').attr('href', '#divOfficerRank').attr('onclick', 'FillRankingSelection(3,' + value.OfficerID + ',1,"' + value.OfficerName + '","' + value.ActionText + '")').html(value.OfficerName)))
                                                      .append($('<td>').append($('<label>').html(value.CaseCnt))))
                                        }
                                        $('#tbodyRMPPManager')
                                            .append($('<tr>')
                                                .append($('<td>').append($('<a>').attr('data-toggle', 'modal').attr('href', '#').attr('onclick', 'FillRankingSelection(3,' + value.OfficerID + ',5,"' + value.OfficerName + '","' + value.ActionText + '")').html(value.OfficerName)))
                                                .append($('<td>').append($('<label>').html(value.CaseCnt))))
                                        break;
                                }
                            }
                            else if (type == "ADM") {
                                switch (value.ActionText) {
                                    case 'Paid':
                                        if ($.session.get('TreeLevel') == '1') {
                                            $('#tblbodyPaid').append($('<tr>')
                                                            .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                                            .append($('<td>').append($('<a>').attr('data-toggle', 'modal').attr('href', '#divOfficerRank').attr('onclick', 'FillRankingSelection(1,' + value.OfficerID + ',2,"' + value.OfficerName + '","' + value.ActionText + '")').html(value.OfficerName)))
                                                            .append($('<td>').append($('<label>').html(value.CaseCnt))))
                                        }
                                        else {
                                            $('#tbodyADMRank').append($('<tr>')
                                                 .append($('<td>').append($('<a>').attr('onclick', 'FillRankingSelection(1,' + value.OfficerID + ',2,"' + value.OfficerName + '","' + value.ActionText + '")').attr('href', '#paid').html(value.OfficerName)))
                                                 .append($('<td>').html(value.CaseCnt))
                                                 .append($('<td>').html(value.ActionText)))
                                        }
                                        $('#tbodyRMPaidADM').append($('<tr>').append($('<td>')
                                                   .append($('<a>').attr('onclick', 'FillRankingSelection(1,' + value.OfficerID + ',6,"' + value.OfficerName + '","' + value.ActionText + '")')
                                                      .attr('href', '#' + $('#HCaseActionTitle').html().toString().toLowerCase().replace(" ", "")).html(value.OfficerName)))
                                                   .append($('<td>').html(value.CaseCnt)))
                                        break;
                                    case 'Returned':
                                        if ($.session.get('TreeLevel') == '1') {
                                            $('#tblbodyReturned').append($('<tr>')
                                                            .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                                            .append($('<td>').append($('<a>').attr('data-toggle', 'modal').attr('href', '#divOfficerRank').attr('onclick', 'FillRankingSelection(2,' + value.OfficerID + ',2,"' + value.OfficerName + '","' + value.ActionText + '")').html(value.OfficerName)))
                                                            .append($('<td>').append($('<label>').html(value.CaseCnt))))
                                        }
                                        else {
                                            $('#tbodyADMRank').append($('<tr>')
                                               .append($('<td>').append($('<a>').attr('onclick', 'FillRankingSelection(2,' + value.OfficerID + ',2,"' + value.OfficerName + '","' + value.ActionText + '")').attr('href', '#paid').html(value.OfficerName)))
                                               .append($('<td>').html(value.CaseCnt))
                                               .append($('<td>').html(value.ActionText)))
                                        }

                                        $('#tbodyRMReturnADM').append($('<tr>')
                                                     .append($('<td>').append($('<a>').attr('onclick', 'FillRankingSelection(2,' + value.OfficerID + ',6,"' + value.OfficerName + '","' + value.ActionText + '")')
                                                        .attr('href', '#' + $('#HCaseActionTitle').html().toString().toLowerCase().replace(" ", "")).html(value.OfficerName)))
                                                     .append($('<td>').html(value.CaseCnt)))
                                        break;
                                    case 'Part Paid':
                                        if ($.session.get('TreeLevel') == '1') {
                                            $('#tblbodyPP').append($('<tr>')
                                                            .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                                            .append($('<td>').append($('<a>').attr('data-toggle', 'modal').attr('href', '#divOfficerRank').attr('onclick', 'FillRankingSelection(3,' + value.OfficerID + ',2,"' + value.OfficerName + '","' + value.ActionText + '")').html(value.OfficerName)))
                                                            .append($('<td>').append($('<label>').html(value.CaseCnt))))
                                        }
                                        else {
                                            $('#tbodyADMRank').append($('<tr>')
                                                  .append($('<td>').append($('<a>').attr('onclick', 'FillRankingSelection(3,' + value.OfficerID + ',2,"' + value.OfficerName + '","' + value.ActionText + '")').attr('href', '#paid').html(value.OfficerName)))
                                                  .append($('<td>').html(value.CaseCnt))
                                                  .append($('<td>').html(value.ActionText)))
                                        }
                                        $('#tbodyRMPPADM').append($('<tr>')
                                                    .append($('<td>').append($('<a>').attr('onclick', 'FillRankingSelection(3,' + value.OfficerID + ',6,"' + value.OfficerName + '","' + value.ActionText + '")')
                                                       .attr('href', '#' + $('#HCaseActionTitle').html().toString().toLowerCase().replace(" ", "")).html(value.OfficerName)))
                                                    .append($('<td>').html(value.CaseCnt)))
                                        break;
                                }
                            }
                            else if (type == "Officer") {
                                switch (value.ActionText) {
                                    case 'Paid':
                                        if ($.session.get('TreeLevel') == '2') {
                                            if (key <= 4) {
                                                $('#tblbodyPaid').append($('<tr>')
                                                          .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                                          .append($('<td>').append($('<a>').attr('data-toggle', 'modal').attr('href', '#divOfficerRank')
                                                    .attr('onclick', 'FillRankingCase(' + flag + ',' + key + ',' + $.trim(value.OfficerID) + ",'" + value.OfficerName + "','" + value.FromDate + "',0" + ')').html(value.OfficerName)))
                                                          .append($('<td>').append($('<label>').html(value.CaseCnt))))
                                            }
                                        }
                                        else {
                                            $('#tbodyOfficerRank').append($('<tr>').append($('<td>')
                                               .append($('<a>').attr('onclick', 'FillRankingCase(' + flag + ',' + key + ',' + $.trim(value.OfficerID) + ",'" + value.OfficerName + "','" + value.FromDate + "',0" + ')')
                                                .attr('href', '#paid').html(value.OfficerName)))
                                               .append($('<td>').html(value.CaseCnt))
                                               .append($('<td>').html(value.ActionText)))
                                        }
                                        $('#tbodyRMPaidOfficer').append($('<tr>')
                                                .append($('<td>').append($('<a style="cursor:pointer;">').attr('onclick', 'FillRankingCase(' + flag + ',' + key + ',' + $.trim(value.OfficerID) + ",'" + value.OfficerName + "','" + value.FromDate + "',1" + ')')
                                                .html(value.OfficerName)))
                                                .append($('<td>').html(value.CaseCnt))
                                                .append($('<td>').html(value.ActionText)))
                                        break;
                                    case 'Returned':
                                        if ($.session.get('TreeLevel') == '2') {
                                            if (key <= 4) {
                                                $('#tblbodyReturned').append($('<tr>')
                                                          .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                                          .append($('<td>').append($('<a>').attr('data-toggle', 'modal').attr('href', '#divOfficerRank')
                                                    .attr('onclick', 'FillRankingCase(' + flag + ',' + key + ',' + $.trim(value.OfficerID) + ",'" + value.OfficerName + "','" + value.FromDate + "',0" + ')').html(value.OfficerName)))
                                                          .append($('<td>').append($('<label>').html(value.CaseCnt))))
                                            }
                                        }
                                        else {
                                            $('#tbodyOfficerRank').append($('<tr>')
                                                 .append($('<td>').append($('<a style="cursor:pointer">').attr('onclick', 'FillRankingCase(' + flag + ',' + key + ',' + $.trim(value.OfficerID) + ",'" + value.OfficerName + "','" + value.FromDate + "',0" + ')').html(value.OfficerName)))
                                                 .append($('<td>').html(value.CaseCnt))
                                                 .append($('<td>').html(value.ActionText)))
                                        }

                                        $('#tbodyRMReturnOfficer').append($('<tr>')
                                                 .append($('<td>').append($('<a style="cursor:pointer;">').attr('onclick', 'FillRankingCase(' + flag + ',' + key + ',' + $.trim(value.OfficerID) + ",'" + value.OfficerName + "','" + value.FromDate + "',1" + ')')
                                                .html(value.OfficerName)))
                                             .append($('<td>').html(value.CaseCnt))
                                             .append($('<td>').html(value.ActionText)))
                                        break;
                                    case 'Part Paid':
                                        if ($.session.get('TreeLevel') == '2') {
                                            if (key <= 4) {
                                                $('#tblbodyPP').append($('<tr>')
                                                          .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                                          .append($('<td>').append($('<a>').attr('data-toggle', 'modal').attr('href', '#divOfficerRank').attr('onclick', 'FillRankingCase(' + flag + ',' + key + ',' + $.trim(value.OfficerID) + ",'" + value.OfficerName + "','" + value.FromDate + "',0" + ')').html(value.OfficerName)))
                                                          .append($('<td>').append($('<label>').html(value.CaseCnt))))
                                            }
                                        }
                                        else {
                                            $('#tbodyOfficerRank').append($('<tr>')
                                                 .append($('<td>').append($('<a style="cursor:pointer">').attr('onclick', 'FillRankingCase(' + flag + ',' + key + ',' + $.trim(value.OfficerID) + ",'" + value.OfficerName + "','" + value.FromDate + "',0" + ')').html(value.OfficerName)))
                                                 .append($('<td>').html(value.CaseCnt))
                                                 .append($('<td>').html(value.ActionText)))
                                        }

                                        $('#tbodyRMPPOfficer').append($('<tr>')
                                            .append($('<td>').append($('<a style="cursor:pointer;">').attr('onclick', 'FillRankingCase(' + flag + ',' + key + ',' + $.trim(value.OfficerID) + ",'" + value.OfficerName + "','" + value.FromDate + "',1" + ')')
                                                .html(value.OfficerName)))
                                            .append($('<td>').html(value.CaseCnt))
                                            .append($('<td>').html(value.ActionText)))

                                        break;
                                }
                            }

                            //*************************************************************

                        });
                    }
                    else {
                        if (type == 'Manager') {
                            switch (flag) {
                                case '1':
                                    $('#tblbodyPaid,#tbodyRankMoreManager').empty().html('No rankings for paid actions.');
                                    break;
                                case '2':
                                    $('#tblbodyReturnedMore,#tblbodyReturned').empty().html('No rankings for returned actions.');
                                    break;
                                case '3':
                                    $('#tblbodyPPRanking,#tblbodyPP').empty().html('No rankings for part paid actions.');
                                    break;
                            }
                        }
                        if (type == 'ADM') $('#tbodyCaseActionsADMCount').empty().html('No records found');
                        if (type == 'Officer') $('#tbodyCaseActionsOfficerCount').empty().html('No records found');
                    }
                }
                else {
                    if (type == 'Manager') {
                        switch (flag) {
                            case '1':
                                $('#tblbodyPaid,#tbodyRankMoreManager').empty().html('No rankings for paid actions.');
                                break;
                            case '2':
                                $('#tblbodyReturnedMore,#tblbodyReturned').empty().html('No rankings for returned actions.');
                                break;
                            case '3':
                                $('#tblbodyPPRanking,#tblbodyPP').empty().html('No rankings for part paid actions.');
                                break;
                        }
                    }
                    if (type == 'ADM') $('#tbodyCaseActionsADMCount').empty().html('No records found');
                    if (type == 'Officer') $('#tbodyCaseActionsOfficerCount').empty().html('No records found');
                }
            };

        }
        else {
            window.location.href = "sign-in.html";
        }

    } catch (e) {
        alert('ready : ' + e.message);
    }

    //******************************************************************************
});

function addInfoWindow(marker, message) {
    var infoWindow = new google.maps.InfoWindow({
        content: message
    });
    google.maps.event.addListener(marker, 'click', function () {
        infoWindow.open(map, marker);
    });
}

function GetActionsHub(parameter) {
    CaseActionURL = parameter;
    try {
        counter.server.gethubcaseactions(parameter);
    } catch (e) {
        alert("GetActionsHub : " + e.message);
    }
}

function GetManager(OfficerID, OfficerName, FromDate, Flag) {
    if (Flag == 0) {
        $('#tblWMAADM,#tblWMAOfficers,#tblOfficer,#btnWMAADMBack,#btnWMAOfficerBack,#btnBack').hide();
        if ($.session.get('TreeLevel') == 0) {
            $('#lblWMAManager').html(OfficerName);
            $('#tblWMAADM').show();
            FillWMAADM(OfficerID, FromDate, FromDate);
        }
        else if ($.session.get('TreeLevel') == 1) {
            FillWMAOfficer(OfficerID, FromDate, FromDate);
        }
        else {
            FillWMADetailL1(OfficerID, FromDate, FromDate);
        }
    }
    else {
        $('#DWMAMoreADM,#btnWMAMoreManager').show();
        $('#DWMAMoreManager').hide();
        $('#lblWMAMoreManagerName').html(' - ' + OfficerName);
        if ($.session.get('TreeLevel') == 0) {
            $('#tblWMAADM').show();
            FillWMAADM(OfficerID, FromDate, FromDate);
        }
        else {
            FillWMADetailL1(OfficerID, FromDate, FromDate);
        }
    }
}

function GetAdm(OfficerID, OfficerName, FromDate, Flag) {
    if (Flag == 0) {
        $('#tblWMAOfficers').show();
        switch ($.session.get('TreeLevel')) {
            case '0':
                $('#lblWMAADM').html(' - ' + OfficerName);
                $('#btnWMAADMBack').show();
                $('#tblWMAADM').hide();
                break;
            case '1':
                $('#lblWMAADM').html(' - ' + OfficerName);
                $('#tblWMA').show();
                $('#tblWMAADM,#tblOfficer,#btnWMAADMBack').hide();
                break;
            case '2':
                break;
        }
        FillWMAOfficer(OfficerID, FromDate, FromDate);
    }
    else {
        $('#DWMAMoreOfficer,#btnWMAMoreADM').show();
        $('#DWMAMoreADM,#btnWMAMoreManager').hide();
        $('#lblWMAMoreADMName').html(' - ' + OfficerName);
        FillWMAOfficer(OfficerID, FromDate, FromDate);
    }
}

function GetOfficer(OfficerID, OfficerName, FromDate, Flag) {
    if (Flag == 0) {
        $('#tblWMAOfficers,#btnWMAADMBack').hide();
        $('#tblOfficer').show();
        $('#lblOfficerDisplay').html(' - ' + OfficerName);
        switch ($.session.get('TreeLevel')) {
            case '0':
                $('#tblbodySearchType').show();
                break;
            case '1':
                break;
            case '2':
                $('#tblWMAADM').hide();
                $('#tblbodySearchType').show();
                break;
        }
        FillWMADetailL1(OfficerID, FromDate, FromDate);
    }
    else {
        $('#DWMAMoreOfficerCaseCount,#btnWMAMoreOfficer').show();
        $('#DWMAMoreOfficer,#btnWMAMoreADM').hide();
        $('#lblWMAMoreOfficerName').html(' - ' + OfficerName);
        FillWMADetailL1(OfficerID, FromDate, FromDate);
    }
}

function GetWMACase(OfficerID, FromDate, LogType, Flag) {
    if (Flag == 0) {
        $('#lblSearchType').html(' - ' + LogType);
        FillWMADetailL2(OfficerID, FromDate, LogType);
        showhidetabs('show-search-details');
        $('#btnBack').show();
        $('#btnWMAOfficerBack').hide();
    }
    else {
        $('#lblWMAMoreOfficerCaseName').html(' - ' + LogType);
        $('#DWMAMoreOfficerCase,#btnWMAMoreOfficerCase').show();
        $('#DWMAMoreOfficerCaseCount,#btnWMAMoreOfficer').hide();
        FillWMADetailL2(OfficerID, FromDate, LogType);
    }
}

function GetStats(parameter, link) {
    try {
        counter.server.gethubstats(parameter, link);
    } catch (e) {
        // alert("GetStats : " + e.message);
    }
}

function GetSatasCaseActions(parameter) {
    try {
        counter.server.getstatscaseactions(parameter);
    } catch (e) {
        // alert("GetSatasCaseActions : " + e.message);
    }
}

//RSK
//Last location

function GetLastLocation(parameter) {
    try {
        counter.server.getlastlocation(parameter);
    }
    catch (e) {
        // alert("GetLastLocation : " + e.message);
    }
}

function GetHeartBeat(parameter) {
    try {
        counter.server.getheartbeat(parameter);
    }
    catch (e) {
        //  alert("GetHB : " + e.message);
    }
}

function GetRanking(parameter, flag, ltype) {
    try {
        counter.server.getrankingmanager(parameter, flag, ltype);
    } catch (e) {
        //    alert("GetRanking : " + e.message);
    }
}

function FillRankingSelection(Action, key, Condition, MgrName, ActionText) {
    switch (Condition) {
        case 1: //TODO: List level (Manager select)
            $('#DRankingADM').show();
            $('#lblRankMgrName').html(' - ' + MgrName);
            $('#lblRankADMName,#lblRankOfficerName').html('');
            $('#DRankingOfficer,#DRankingCase,#DRankOfficerNotes,#btnRankADMBack,#btnRankOfficerBack,#btnRankOfficerNotesBack').hide();

            FillRankingsForADM(key, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), $.session.get('RankingType'));
            break;
        case 2:// Manager select adm 
            $('#DRankingADM,#DRankingCase,#btnRankOfficerBack').hide();
            if ($.session.get('TreeLevel') == '1')
                $('#btnRankADMBack').hide();
            else
                $('#btnRankADMBack').show();

            $('#DRankingOfficer').show();
            $('#lblRankOfficerName').html('');
            $('#lblRankADMName').html(' - ' + MgrName);
            FillRankingsForOfficer(key, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), $.session.get('RankingType'));
            break;
        case 5://// Ranking More Manager list
            $('#DRMPaidManager,#DRMReturnManager,#DRMPPManager').hide();
            $('#lblRankMoreManager').html(' - ' + MgrName);
            $('#btnRankMoreADM').show();

            switch (Action) {
                case 1://Paid
                    $('#DRMPaidADM').show();
                    break;
                case 2://Returned
                    $('#DRMReturnADM').show();
                    break;
                case 3://Part Paid
                    $('#DRMPPADM').show();
                    break;
            }
            FillRankingsForADM(key, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), ActionText);
            break;
        case 6:// Ranking More ADM list
            $('#btnRankMoreADM,#DRMPaidManager,#DRMPaidADM,#DRMReturnManager,#DRMReturnADM,#DRMPPManager,#DRMPPADM').hide();
            $('#lblRankMoreADM').html(' - ' + MgrName);
            $('#btnRankMoreOfficer').show();
            switch (Action) {
                case 1://Paid
                    $('#DRMPaidOfficer').show();
                    break;
                case 2://Returned
                    $('#DRMReturnOfficer').show();
                    break;
                case 3://Part Paid
                    $('#DRMPPOfficer').show();
                    break;
            }
            FillRankingsForOfficer(key, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), ActionText);;
            break;
        case 7:
            break;
    }
}

function FillRankingCase(caseid, key, OfficerID, OfficerName, FromDate, RankType) {
    //FillRankingCase(1,0,4289,'Neil Davies','08/08/2016',0)
    if (RankType == 0) {
        caseactionshowhidetabs();
        $('#HCaseActionNavigation').show();
        $('#btnCaseActionBack').show();
        $('#divCaseActionsManagerCount,#divCaseActionsOfficerCount,#btnCaseActionManager,#btnCaseActionADM').hide();
        $('#DRankingOfficer').hide();

        FillCaseDetailsActionForManager(OfficerID, FromDate, FromDate, OfficerID, '0');
        //TODO: Rank more
        $('#DRankMorePaid,#DRankMorePaidOfficer,#btnPaidOfficerBack').hide();
        $('#DRankMoreReturned,#DRankMoreReturnedOfficer,#btnReturnedOfficerBack').hide();
        $('#DRankMorePP,#DRankMorePPOfficer,#btnPPOfficerBack').hide();

        $('#DRankingADM').hide();

        $('#lblCAOfficerName,#lblRankOfficerName,#lblRankPaidOfficer,#lblRankReturnedOfficer,#lblRankPPOfficer').html(' - ' + OfficerName);
        $('#tblRankOfficerReturn,#tblRankOfficerPartPaid,#tblRankOfficerPaid,#btnRankADMBack').hide();
        if ($.session.get('TreeLevel') == 2)
            $('#btnRankOfficerBack').hide();
        else
            $('#btnRankOfficerBack').show();
        switch (caseid) {
            case 1:
                $('#lblRankStatus').html('Ranking - Paid');
                $('#paidformanager,#DRankingCase,#tblRankOfficerPaid,#DRankMorePaidCaseList,#btnPaidCaseBack').show();
                $('#HCaseActionTitle').html('Paid');
                break;
            case 2:
                $('#lblRankStatus').html('Ranking - Part Paid');
                $('#DRankingCase,#tblRankOfficerPartPaid,#DRankMorePPCaseList,#btnPPCaseBack').show();
                break;

            case 4:
                $('#lblRankStatus').html('Ranking - Returned');
                $('#DRankingCase,#tblRankOfficerReturn,#DRankMoreReturnedCaseList,#btnReturnedCaseBack').show();
                break;
        }
    }
    else {
        $('#DRMPaidOfficer,#DRMReturnOfficer,#DRMPPOfficer').hide();
        $('#btnRankMoreOfficer').hide();

        switch ($.session.get('RankingType')) {
            case 'Paid':
                $('#DRMPaidCase').show();
                break;
            case 'Returned':
                $('#DRMReturnCase').show();
                break;
            case 'Part Paid':
                $('#DRMPPCase').show();
                break;
        }

        $('#btnRankMoreCase').show();

        $('#lblRankMoreOfficer').html(' - ' + OfficerName);
        FillCaseDetailsActionForManager(OfficerID, FromDate, FromDate, OfficerID, '0');
    }
}

function FullDescriptionOfficerRank(key, val) {
    $('#btnRankOfficerBack,#DRankingADM,#DRankingOfficer,#DRankingCase').hide();
    $('#DRankOfficerNotes').empty().html($('#tdFullDesc' + key).html()).show();
    $('#HCaseNumber').empty().html(' :  ' + $.trim($('#tdCaseNo' + key).html()) == '' ? $('#tdCaseOfficer' + key).html() : $('#tdCaseNo' + key).html() + " - " + $('#tdCaseOfficer' + key).html()).show();
    $('#btnRankOfficerNotesBack').show().click(function () {
        $('#DRankOfficerNotes,#HCaseNumber,#btnRankOfficerNotesBack').hide();
        $('#DRankingCase').show();
        if ($.session.get('TreeLevel') == '2')
            $('#btnRankOfficerBack').hide();
        else
            $('#btnRankOfficerBack').show();
    });
}

function FullDescriptionRank(key, val) {
    $('#DRankMoreNotes').show();
    $('#btnRankMoreCase').hide();
    switch ($.session.get('RankingType')) {
        case 'Paid':
            $('#DRMPaidCase').hide();
            $('#DRankMoreNotes').empty().html($('#tdFullDesc' + key).html());
            $('#btnRankMoreCaseNotes').show().click(function () {
                $('#btnRankMoreCase,#DRMPaidCase').show();
                $('#btnRankMoreCaseNotes,#DRankMoreNotes').hide();
            });
            break;
        case 'Returned':
            $('#DRMReturnCase').hide();
            $('#DRankMoreNotes').empty().html($('#tdFullDesc' + key).html());
            $('#btnRankMoreCaseNotes').show().click(function () {
                $('#btnRankMoreCase,#DRMReturnCase').show();
                $('#btnRankMoreCaseNotes#DRankMoreNotes').hide();
            });
            break;
        case 'Part Paid':
            $('#DRMPPCase').hide();
            $('#DRankMoreNotes').empty().html($('#tdFullDesc' + key).html());
            $('#btnRankMoreCaseNotes').show().click(function () {
                $('#btnRankMoreCase,#DRMPPCase').show();
                $('#btnRankMoreCaseNotes,#DRankMoreNotes').hide();
            });
            break;
    }
}

//==========================================================================================================================================
//TODO : Client search function
// Selected client case action list

function ClientSearch() {
    $('#lblClientSearch').attr('data-toggle', 'modal').attr('href', '#ClientSearch');
    $('#' + $('#popup2Trigger').attr('aria-describedby')).attr('class', 'popover fade bottom');
    GetClientList();
}

function GetClientList() {
    Type = "GET";
    var inputParams = "/GetClientList?ClientName=";
    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () { },
        complete: function () {
            if ($.session.get('ClientId') == undefined || $.session.get('ClientId') == 0) {
                $.session.set('ClientId', 0);
            }
        },
        success: function (result) {//On Successfull service call
            if (result != undefined) {
                $('#tblClientList').dataTable().fnClearTable();
                if (result != "[]" && result != null) {
                    result = JSON.parse(result);
                    $.each(result, function (key, value) {
                        $('#tbodyClientList').append('<tr style="border-bottom: 1px solid;"><td>' + value.ClientName + '</td>' +
                            '<td><img src="images/Disactive.png" class="Unselect" style="width: 24px; cursor: pointer;" title="Select client" onclick="ClientSelect(' + key + ',' + value.ClientId + ',' + "'" + value.ClientName + "'" + ')" id="imgClientview' + key + '" /></td></tr>');
                    });
                    $('#tblClientList').dataTable({
                        "sScrollY": "auto",
                        "bPaginate": true,
                        "bDestroy": true,
                        "bSort": false,
                        "sPaginationType": "full_numbers",
                        "bLengthChange": false,
                        "sPageButton": "paginate_button",
                        "sPageButtonActive": "paginate_active",
                        "sPageButtonStaticDisabled": "paginate_button",
                        "bFilter": true,
                        "bInfo": false,
                        "iDisplayLength": 5
                    });
                    if (!($.session.get('ClientKey') == undefined || $.session.get('ClientKey') == '')) {
                        $('#imgClientview' + $.session.get('ClientKey')).attr('src', 'images/Active.png').attr('class', 'Select');
                    }
                }
                else {
                    $('#tblClientList').dataTable({
                        "sScrollY": "auto",
                        "bPaginate": false,
                        "bDestroy": true,
                        "bSort": false,
                        "bLengthChange": false,
                        "bFilter": false,
                        "bInfo": false,
                        "iDisplayLength": 1
                    });
                }
            }
        },
        error: function () {
            //alert('error');
        }
    });
}

function ClientSelect(key, ClientId, Name) {
    $.session.set('ClientKey', key);
    $.session.set('ClientId', ClientId);
    $.session.set('ClientName', Name);
    $('#pClientName').html(Name);
    $('#DClient').show();

    $('.Select').removeClass('Select').addClass('Unselect').attr('src', 'images/Disactive.png');
    if ($('#imgClientview' + key).attr('src') == 'images/Disactive.png')
        $('#imgClientview' + key).attr('src', 'images/Active.png').attr('class', 'Select');
    else
        $('#imgClientview' + key).attr('src', 'images/Disactive.png').attr('class', 'Unselect');
}

function ShowAllClient() {
    $.session.set('ClientId', 0);
    $.session.set('ClientName', '');
    $.session.set('ClientKey', '');
    $('#pClientName').html('');
    $('#DClient').hide();
}

//==========================================================================================================================================
//TODO : Case action function

function FillCaseActions(ManagerID, FromDate, ToDate, OfficerID, GroupID) {
    if (OfficerID == undefined) OfficerID = 0;
    if (GroupID == undefined) GroupID = 0;

    if (FromDate == '') {
        var date = new Date();
        var TDate = $.datepicker.formatDate('dd/mm/yy', date);
        FromDate = TDate;
    }
    if (ToDate == '') {
        var date1 = new Date();
        var T1Date = $.datepicker.formatDate('dd/mm/yy', date1);
        ToDate = T1Date;
    }

    var dateAr = FromDate.split('/');
    var newDate = dateAr[2] + '/' + dateAr[1] + '/' + dateAr[0];
    FromDate = newDate;

    var dateTr = ToDate.split('/');
    var newToDate = dateTr[2] + '/' + dateTr[1] + '/' + dateTr[0];
    ToDate = newToDate;

    GetActionsHub(ManagerID.toString() + ',' + FromDate.toString() + ',' + OfficerID.toString() + ',' + GroupID.toString());
}

function FillCaseDetailsActionForManager(ManagerID, FromDate, ToDate, OfficerID, GroupID) {
    Bailed = 0;
    Clamp = 0;
    $('#tblbodyActionPaid').empty();
    $('#tbodyActionPP').empty();
    $('#tbodyActionReturned').empty();
    $('#tbodyActionLeftLetter').empty();
    $('#tbodyActionEnforcementStart').empty();
    $('#tbodyActionRevisit').empty();
    $('#tbodyActionEnforcementEnd').empty();
    //$('#tbodyActionBailed').empty();
    $('#tbodyActionArrested').empty();
    $('#tbodyActionSurrenderDateAgreed').empty();

    $('#tbodypaidformanager').empty();
    $('#tbodypartpaidformanager').empty();
    $('#tbodyreturnedformanager').empty();
    $('#tbodyleftletterformanager').empty();
    $('#tbodyrevisitformanager').empty();
    $('#tbodyClampedformanager').empty();
    $('#tbodybailedformanager').empty();
    $('#tbodyarrestedformanager').empty();
    $('#tbodySurrenderdateagreedformanager').empty();

    $('#tbodyActionTCG').empty();
    $('#tbodyActionTCGPAID').empty();
    $('#tbodyActionTCGPP').empty();
    $('#tbodyActionUTTC').empty();
    $('#tbodyActionDROPPED').empty();
    $('#tbodyActionOTHER').empty();

    $('#tbodyotherformanager').empty();
    $('#tbodydroppedformanager').empty();
    $('#tbodyuttcformanager').empty();
    $('#tbodytcgformanager').empty();
    $('#tbodytcgppformanager').empty();
    $('#tbodytcgpaidformanager').empty();

    $('#tbodyRankOfficerPaid').empty();
    $('#tbodyRankOfficerPartPaid').empty();
    $('#tbodyRankOfficerReturn').empty();

    // TODO : Rank more
    $('#tbodyRMPaidCase').empty();
    $('#tbodyRMReturnCase').empty();
    $('#tbodyRMPPCase').empty();

    if (OfficerID == undefined) OfficerID = 0;
    if (GroupID == undefined) GroupID = 0;

    var inputParams = $.trim(ManagerID) + "," + FromDate + "," + ToDate + "," + OfficerID + "," + GroupID;
    GetCaseActionsOfficer(inputParams);
}

function FullDescription(key, val) {
    $('#btnCaseActionBack,#divCaseActionsManagerCount,#divCaseActionsADMCount,#divCaseActionsOfficerCount').hide();
    if ($.session.get('CompanyID') == 1) {
        switch (val) {
            case 1:
                $('#paidformanager,#paid').hide();
                break;
            case 2:
                $('#partpaidformanager,#partpaid').hide();
                break;
            case 4:
                $('#returnedformanager,#returned,#arrestedformanager,#arrested').hide();
                break;
            case 5:
                $('#leftletterformanager,#leftletter,#bailedformanager,#bailed').hide();
                break;
            case 6:
                $('#Clampedformanager,#clamped').hide();
                break;
            case 3:
                $('#revisitformanager,#revisit,#Surrenderdateagreedformanager,#surrenderdateagreed').hide();
                break;
            case 7:
                $('#deviatedformanager,#deviated').hide();
                break;
        }
    }
    $('#divCaseActionModal').attr('class', 'modal fade in').show();
    $('#Notesformanager').empty().html($('#tdFullDesc' + key).html()).show();
    $('#HCaseNumber').empty().html(' :  ' + $.trim($('#tdCaseNo' + key).html()) == '' ? $('#tdCaseOfficer' + key).html() : $('#tdCaseNo' + key).html() + " - " + $('#tdCaseOfficer' + key).html()).show();
    $('#btnNotesBack').show().click(function () {
        if ($.session.get('CompanyID') == 1) {
            $('#paidformanager,#partpaidformanager,#returnedformanager,#leftletterformanager,#Clampedformanager,#revisitformanager,#bailedformanager,#arrestedformanager,#Surrenderdateagreedformanager,#deviated').hide();
            switch (val) {
                case 1:
                    $('#paidformanager').show();
                    //$('#paid').show();
                    break;
                case 2:
                    $('#partpaidformanager').show();
                    //$('#partpaid').show();
                    break;
                case 4:
                    if ($.session.get('OfficerRole') == 9)
                        $('#arrestedformanager').show();
                    else
                        $('#returnedformanager').show();
                    //$('#returned').show();
                    break;
                case 5:
                    if ($.session.get('OfficerRole') == 9)
                        $('#bailedformanager').show();
                    else
                        $('#leftletterformanager').show();
                    //$('#leftletter').show();
                    break;
                case 3:
                    if ($.session.get('OfficerRole') == 9)
                        $('#Surrenderdateagreedformanager').show();
                    else
                        $('#revisitformanager').show();
                    // $('#revisit').show();
                    break;
                case 6:
                    $('#Clampedformanager').show();
                    break;
                case 7:
                    $('#deviated').show();
                    break;
            }
        }
        $('#divCaseActionModal').attr('class', 'modal fade in').show();
        $('#divOfficerRank').attr('class', 'modal fade').hide();
        $('#Notesformanager,#HCaseNumber,#btnNotesBack').hide();
        $('#btnCaseActionBack').show();
    });
}

function FullDescriptionOfficer(key, val) {
    $('#btnCaseActionBack,#divCaseActionsManagerCount,#divCaseActionsADMCount,#divCaseActionsOfficerCount').hide();
    $('#btnCaseActionBack,#Notesformanager,#btnNotesBack').hide();
    if ($.session.get('CompanyID') == 1) {
        switch (val) {
            case 1:
                $('#paidformanager,#paid').hide();
                break;
            case 2:
                $('#partpaidformanager,#partpaid').hide();
                break;
            case 4:
                $('#returnedformanager,#returned,#arrestedformanager,#arrested').hide();
                break;
            case 5:
                $('#leftletterformanager,#leftletter,#Surrenderdateagreedformanager,#surrenderdateagreed').hide();
                break;
            case 3:
                $('#revisitformanager,#revisit,#bailedformanager,#bailed').hide();
                break;
            case 6:
                $('#Clampedformanager,#clamped').hide();
                break;
            case 7:
                $('#deviatedformanager,#deviated').hide();
                break;
        }
    }
    $('#divCaseActionModal').attr('class', 'modal fade in').show();
    $('#Notesformanager').empty().html($('#tdFullDesc' + key).html()).show();
    $('#HCaseNumber').empty().html(' :  ' + $.trim($('#tdCaseNo' + key).html()) == '' ? $('#tdCaseOfficer' + key).html() : $('#tdCaseNo' + key).html() + " - " + $('#tdCaseOfficer' + key).html()).show();
    $('#btnNotesBack').show().click(function () {
        if ($.session.get('CompanyID') == 1) {
            $('#paid,#partpaid,#returned,#leftletter,#revisit,#deviated,#surrenderdateagreed,#bailed,#Arrested,#clamped').hide();
            switch (val) {
                case 1:
                    if ($.session.get('TreeLevel') == 3)
                        $('#paid').show().attr('class', 'modal fade in');
                    else
                        $('#paid').show().attr('class', 'modal fade').hide();
                    break;
                case 2:
                    if ($.session.get('TreeLevel') == 3)
                        $('#partpaid').show().attr('class', 'modal fade in');
                    else
                        $('#partpaid').show().attr('class', 'modal fade').hide();
                    break;
                case 4:
                    if ($.session.get('OfficerRole') == 9) {
                        if ($.session.get('TreeLevel') == 3)
                            $('#arrested').show().attr('class', 'modal fade in');
                        else
                            $('#arrested').show().attr('class', 'modal fade').hide();
                    }
                    else {
                        if ($.session.get('TreeLevel') == 3)
                            $('#returned').show().attr('class', 'modal fade in');
                        else
                            $('#returned').show().attr('class', 'modal fade').hide();
                        break;
                    }
                case 5:
                    if ($.session.get('OfficerRole') == 9) {
                        if ($.session.get('TreeLevel') == 3)
                            $('#surrenderdateagreed').show().attr('class', 'modal fade in');
                        else
                            $('#surrenderdateagreed').show().attr('class', 'modal fade').hide();
                    }
                    else {
                        if ($.session.get('TreeLevel') == 3)
                            $('#leftletter').show().attr('class', 'modal fade in');
                        else
                            $('#leftletter').show().attr('class', 'modal fade').hide();
                    }
                    break;
                case 3:
                    if ($.session.get('OfficerRole') == 9) {
                        if ($.session.get('TreeLevel') == 3)
                            $('#bailed').show().attr('class', 'modal fade in');
                        else
                            $('#bailed').show().attr('class', 'modal fade').hide();
                    }
                    else {
                        if ($.session.get('TreeLevel') == 3)
                            $('#revisit').show().attr('class', 'modal fade in');
                        else
                            $('#revisit').show().attr('class', 'modal fade').hide();
                    }
                    break;
                case 7:
                    if ($.session.get('TreeLevel') == 3)
                        $('#deviated').show().attr('class', 'modal fade in');
                    else
                        $('#deviated').show().attr('class', 'modal fade').hide();
                    break;
            }
        }
        $('#divCaseActionModal').attr('class', 'modal fade in').hide();
        $('#Notesformanager,#HCaseNumber,#btnNotesBack').hide();
    });
}

function FillCaseActionDetailsOnClick(Action, OfficerID, OfficerName, Check) {
    $('#HCaseActionNavigation').show();
    $('#btnCaseActionBack').show();
    $('#divCaseActionsManagerCount,#divCaseActionsADMCount,#divCaseActionsOfficerCount,#btnCaseActionManager').hide();
    $('#DRankingOfficer,#DRankMorePaidOfficer,#btnPaidOfficerBack,#DRankMoreReturnedOfficer,#btnReturnedOfficerBack,#DRankMorePPOfficer,#btnPPOfficerBack,#btnRankADMBack,#btnRankOfficerBack,#DRankingADM').hide();
    $('#tblRankOfficerReturn,#tblRankOfficerPartPaid,#tblRankOfficerPaid').hide();
    $('#lblCAOfficerName,#lblRankOfficerName,#lblRankPaidOfficer,#lblRankReturnedOfficer,#lblRankPPOfficer').html(' - ' + OfficerName);
    if (Check == 0) {
        switch (Action) {
            case 1:
                $('#lblRankStatus').html('Ranking - Paid');
                $('#DRankingCase,#tblRankOfficerPaid').show();//,#btnRankOfficerBack
                if ($.session.get('TreeLevel') == 2)
                    $('#btnRankOfficerBack').hide();
                else
                    $('#btnRankOfficerBack').show();

                $('#HCaseActionTitle').html('Paid');
                FillCaseDetailsAction(0, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), OfficerID, 0);
                break;
            case 2:
                $('#lblRankStatus').html('Ranking - Returned');
                $('#DRankingCase,#tblRankOfficerReturn').show();
                if ($.session.get('TreeLevel') == 2)
                    $('#btnRankOfficerBack').hide();
                else
                    $('#btnRankOfficerBack').show();
                $('#HCaseActionTitle').html('Returned');
                FillCaseDetailsAction(0, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), OfficerID, 0);
                break;
            case 3:
                $('#lblRankStatus').html('Ranking - Part Paid');
                $('#DRankingCase,#tblRankOfficerPartPaid').show();
                if ($.session.get('TreeLevel') == 2)
                    $('#btnRankOfficerBack').hide();
                else
                    $('#btnRankOfficerBack').show();
                $('#HCaseActionTitle').html('Part Paid');
                FillCaseDetailsAction(0, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), OfficerID, 0);
                break;
            case 4:
                FillCaseDetailsAction(0, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), OfficerID, 0);
                break;
        }
    }
    else {
        switch (Action) {
            case 1:
                FillCaseDetailsAction(0, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), OfficerID, 0);
                break;
            case 2:
                FillCaseDetailsAction(0, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), OfficerID, 0);
                break;
            case 3:
                FillCaseDetailsAction(0, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), OfficerID, 0);
                break;
            case 4:
                FillCaseDetailsAction(0, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), OfficerID, 0);
                break;
        }
    }
}

function caseaction(Flag) {
    $('#btnCaseActionBack,#btnCaseActionADM,k#btnCaseActionManager,#Notesformanager,#btnNotesBack,#divCaseActionsOfficerCount,#HCaseNumber').hide();
    caseactionshowhidetabs();
    $('#divCaseActionsManagerCount').show();
    $('#lblCAZoneName,#lblCAManagerName,#lblCAADMName,#lblCAOfficerName').html('');
    Flag += 1;
    if ($.session.get('CompanyID') == 1) {
        switch (Flag) {
            case 1:
                $('#HCaseActionTitle').html('Paid');
                break;
            case 2:
                $('#HCaseActionTitle').html($.session.get('OfficerRole') == 9 ? 'Left Letter' : 'Part Paid');
                break;
            case 3:
                $('#HCaseActionTitle').html($.session.get('OfficerRole') == 9 ? 'Bailed' : 'Returned');
                break;
            case 4:
                $('#HCaseActionTitle').html($.session.get('OfficerRole') == 9 ? 'Arrested' : 'Left Letter');
                break;
            case 5:
                $('#HCaseActionTitle').html($.session.get('OfficerRole') == 9 ? 'Surrender date agreed' : 'Defendant Contact');
                break;
            case 6:
                $('#HCaseActionTitle').html($.session.get('OfficerRole') == 9 ? 'Returned' : 'Clamped');
                break;
            case 7:
                $('#HCaseActionTitle').html('Deviation');
                break;
        }
    }
    switch ($.session.get('TreeLevel')) {
        case '0':
            $('#divCaseActionsOfficerCount,#divCaseActionsADMCount').hide();
            $('#divCaseActionsManagerCount').show();
            GetCaseActionsManagerCount(Flag, $.session.get('ManagerID'), 0, $.session.get('MgrName'));
            $('#lblCAZoneName').html(' - ' + $.session.get('MgrName'));
            break;
        case '1':
            $('#divCaseActionsOfficerCount,#divCaseActionsManagerCount').hide();
            $('#divCaseActionsADMCount').show();
            GetCaseActionsADMCount(Flag, $.session.get('ManagerID'), 0, $.session.get('MgrName'));
            if ($.session.get('TreeLevel') == 1)
                $('#btnCaseActionManager').hide();
            else
                $('#lblCAManagerName').show();
            break;
        case '2':
        case '3':
            GetCaseActionsOfficerCount(Flag, $.session.get('ManagerID'), 0, $.session.get('MgrName'));
            $('#divCaseActionsOfficerCount').show();
            $('#divCaseActionsManagerCount,#divCaseActionsADMCount').hide();
            break;
    }
}

function GetCaseActionsManagerCount(Flag, MgrID, GrpID, MgrName) {
    if ($('#txtFromDateCA').val() == '') {
        var date = new Date();
        var TDate = $.datepicker.formatDate('dd/mm/yy', date);
        $('#txtFromDateCA').val(TDate);
    }
    var dateAr = $('#txtFromDateCA').val().split('/');
    var newDate = dateAr[2] + '/' + dateAr[1] + '/' + dateAr[0];

    var inputParams = $('#HCaseActionTitle').html() + "," + newDate + "," + newDate + "," + $.session.get('CompanyID') + "," + MgrID + "," + GrpID;
    GetCaseActionManager(inputParams, Flag, "Manager");
}

function GetCaseActionsADMCount(Flag, MgrID, GrpID, MgrName) {
    $('#btnCaseActionBack,#Notesformanager,#btnNotesBack,#divCaseActionsManagerCount,#divCaseActionsOfficerCount').hide();
    caseactionshowhidetabs();
    $('#divCaseActionsADMCount').show();
    if ($.session.get('TreeLevel') == 0 || $.session.get('TreeLevel') == 1)
        $('#btnCaseActionManager').show();
    else
        $('#btnCaseActionManager').show();

    $('#lblCAManagerName').html(' - ' + MgrName);

    if ($('#txtFromDateCA').val() == '') {
        var date = new Date();
        var TDate = $.datepicker.formatDate('dd/mm/yy', date);
        $('#txtFromDateCA').val(TDate);
    }
    var dateAr = $('#txtFromDateCA').val().split('/');
    var newDate = dateAr[2] + '/' + dateAr[1] + '/' + dateAr[0];

    var inputParams = $('#HCaseActionTitle').html() + "," + newDate + "," + newDate + "," + $.session.get('CompanyID') + "," + MgrID + "," + GrpID;
    GetCaseActionManager(inputParams, Flag, "ADM");
}

function GetCaseActionsOfficerCount(Flag, MgrID, GrpID, MgrName) {
    $('#btnCaseActionBack,#Notesformanager,#btnNotesBack,#divCaseActionsManagerCount,#divCaseActionsADMCount,#btnCaseActionManager').hide();
    caseactionshowhidetabs();
    $('#divCaseActionsOfficerCount').show();
    if ($.session.get('TreeLevel') == 1 || $.session.get('TreeLevel') == 0) {
        $('#btnCaseActionADM').show();
        $('#lblCAADMName').html(' - ' + MgrName);
    }
    if ($.session.get('TreeLevel') == 2)
        $('#lblCAADMName').html((MgrName != null && MgrName != undefined && MgrName != 'undefined' && MgrName != '') ? ' - ' + MgrName : '');

    if ($('#txtFromDateCA').val() == '') {
        var date = new Date();
        var TDate = $.datepicker.formatDate('dd/mm/yy', date);
        $('#txtFromDateCA').val(TDate);
    }
    var dateAr = $('#txtFromDateCA').val().split('/');
    var newDate = dateAr[2] + '/' + dateAr[1] + '/' + dateAr[0];

    var inputParams = $('#HCaseActionTitle').html() + "," + newDate + "," + newDate + "," + $.session.get('CompanyID') + "," + MgrID + "," + GrpID;
    GetCaseActionManager(inputParams, Flag, "Officer");
}

function clickoncaseactionmanager(caseid, key, OfficerID, OfficerName, FromDate, ToDate) {
    caseactionshowhidetabs();
    var Flag, ActionText;
    $('#HCaseActionNavigation').show();
    $('#btnCaseActionBack').show();
    $('#divCaseActionsManagerCount,#divCaseActionsOfficerCount,#btnCaseActionManager,#btnCaseActionADM').hide();
    $('#DRankingOfficer').hide();

    FillCaseDetailsActionForManager($('#HCaseActionManagerID' + key).html(), FromDate, ToDate, OfficerID, '0');
    //TODO: Rank more
    $('#DRankMorePaid,#DRankMorePaidOfficer,#btnPaidOfficerBack').hide();
    $('#DRankMoreReturned,#DRankMoreReturnedOfficer,#btnReturnedOfficerBack').hide();
    $('#DRankMorePP,#DRankMorePPOfficer,#btnPPOfficerBack').hide();

    $('#lblCAOfficerName,#lblRankOfficerName,#lblRankPaidOfficer,#lblRankReturnedOfficer,#lblRankPPOfficer').html(' - ' + OfficerName);
    $('#tblRankOfficerReturn,#tblRankOfficerPartPaid,#tblRankOfficerPaid,#btnRankADMBack,#btnRankOfficerBack').hide();
    if ($.session.get('CompanyID') == 1) {
        switch (caseid) {
            case 1:
                $('#lblRankStatus').html('Ranking - Paid');
                $('#paidformanager,#DRankingCase,#tblRankOfficerPaid,#DRankMorePaidCaseList,#btnPaidCaseBack').show();
                $('#HCaseActionTitle').html('Paid');
                break;
            case 2:
                $('#lblRankStatus').html('Ranking - Part Paid');
                $('#DRankingCase,#tblRankOfficerPartPaid,#DRankMorePPCaseList,#btnPPCaseBack').show();
                if ($.session.get('OfficerRole') == 9) {
                    $('#leftletterformanager').show();
                    $('#HCaseActionTitle').html('Left Letter');
                }
                else {
                    $('#partpaidformanager').show();
                    $('#HCaseActionTitle').html('Part Paid');
                }
                break;
            case 3:
                if ($.session.get('OfficerRole') == 9) {
                    $('#bailedformanager').show();
                    $('#HCaseActionTitle').html('Bailed');
                }
                else {
                    $('#returnedformanager').show();
                    $('#HCaseActionTitle').html('Returned');
                }
                break;
            case 4:
                $('#lblRankStatus').html('Ranking - Returned');
                $('#DRankingCase,#tblRankOfficerReturn,#DRankMoreReturnedCaseList,#btnReturnedCaseBack').show();
                if ($.session.get('OfficerRole') == 9) {
                    $('#arrestedformanager').show();
                    $('#HCaseActionTitle').html('Arrested');
                }
                else {
                    $('#leftletterformanager').show();
                    $('#HCaseActionTitle').html('Left Letter');
                }
                break;
            case 5:
                if ($.session.get('OfficerRole') == 9) {
                    $('#Surrenderdateagreedformanager').show();
                    $('#HCaseActionTitle').html('Surrender date agreed');
                }
                else {
                    $('#leftletterformanager').show();
                    $('#HCaseActionTitle').html('Left Letter');
                }
                break;
            case 6:
                if ($.session.get('OfficerRole') == 9) {
                    $('#HCaseActionTitle').html('Returned');
                    $('#returnedformanager').show();
                }
                else {
                    $('#Clampedformanager').show();
                    $('#HCaseActionTitle').html('Clamped');
                }
                Clamp = 0;
                break;
            case 7:
                $('#deviatedformanager').show();
                $('#HCaseActionTitle').html('Deviation');
                break;
        }
    }
}

// TODO: SignalR
function GetCaseActionsOfficer(parameter) {
    try {
        counter.server.getcaseactions(parameter);
    } catch (e) {
        //   alert("GetCaseActionsOfficer : " + e.message);
    }
}

function GetCaseActionManager(parameter, flag, ltype) {
    try {
        counter.server.getcaseactionmanager(parameter, flag, ltype);
    } catch (e) {
        //  alert("GetCaseActionManager : " + e.message);
    }
}

// TODO : Not Use
function clickoncaseactionofficer(caseid, key, OfficerID, OfficerName, FromDate, ToDate) {
    caseactionshowhidetabs();

    FillCaseDetailsActionForManager($('#HCaseActionManagerID' + key).html(), FromDate, ToDate, OfficerID, '0');
    $('#HCaseActionNavigation').show();
    $('#btnCaseActionBack').show();
    $('#divCaseActionsManagerCount,#divCaseActionsOfficerCount,#btnCaseActionManager').hide();
    $('#DRankingOfficer').hide();

    //TODO: Rank more
    $('#DRankMorePaid,#DRankMorePaidOfficer,#btnPaidOfficerBack').hide();
    $('#DRankMoreReturned,#DRankMoreReturnedOfficer,#btnReturnedOfficerBack').hide();
    $('#DRankMorePP,#DRankMorePPOfficer,#btnPPOfficerBack').hide();

    $('#lblCAOfficerName,#lblRankOfficerName,#lblRankPaidOfficer,#lblRankReturnedOfficer,#lblRankPPOfficer').html(' - ' + OfficerName);
    $('#tblRankOfficerReturn,#tblRankOfficerPartPaid,#tblRankOfficerPaid,#btnRankADMBack,#btnRankOfficerBack').hide();
    if ($.session.get('CompanyID') == 1) {
        switch (caseid) {
            case 1:
                $('#lblRankStatus').html('Ranking - Paid');
                $('#paidformanager,#DRankingCase,#tblRankOfficerPaid,#DRankMorePaidCaseList,#btnPaidOfficerBack').show();
                $('#HCaseActionTitle').html('Paid');
                break;
            case 2:
                $('#lblRankStatus').html('Ranking - Part Paid');
                $('#partpaidformanager,#DRankingCase,#tblRankOfficerPartPaid,#DRankMorePPCaseList,#btnPPOfficerBack').show();
                $('#HCaseActionTitle').html('Part Paid');
                break;
            case 3:
                if ($.session.get('OfficerRole') == 9) {
                    $('#bailedformanager').show();
                    $('#HCaseActionTitle').html('Bailed');
                }
                else {
                    $('#revisitformanager').show();
                    $('#HCaseActionTitle').html('£ Collected');
                }
                break;
            case 4:
                $('#lblRankStatus').html('Ranking - Returned');
                $('#DRankingCase,#tblRankOfficerReturn,#DRankMoreReturnedCaseList,#btnReturnedOfficerBack').show();
                if ($.session.get('OfficerRole') == 9) {
                    $('#arrestedformanager').show();
                    $('#HCaseActionTitle').html('Arrested');
                }
                else {
                    $('#returnedformanager').show();
                    $('#HCaseActionTitle').html('Returned');
                }
                break;
            case 5:
                if ($.session.get('OfficerRole') == 9) {
                    $('#Surrenderdateagreedformanager').show();
                    $('#HCaseActionTitle').html('Surrender date agreed');
                }
                else {
                    $('#leftletterformanager').show();
                    $('#HCaseActionTitle').html('Left Letter');
                }
            case 6:
                $('#Clampedformanager').show();
                $('#HCaseActionTitle').html('Clamped');
                break;
            case 7:
                $('#deviatedformanager').show();
                $('#HCaseActionTitle').html('Deviation');
                break;
        }
    }
}


//==========================================================================================================================================
//TODO : Warrent matching function

function FillWMA(ManagerID, FromDate, ToDate) {
    var inputParams = ManagerID.toString() + ',' + FromDate.toString();
    GetWMADetatil(inputParams, "Manager");
}

function FillWMAADM(ManagerID, FromDate, ToDate) {
    var inputParams = ManagerID.toString() + ',' + FromDate.toString();
    GetWMADetatil(inputParams, "ADM");
}

function FillWMAOfficer(ManagerID, FromDate, ToDate) {
    var inputParamsOfficer = ManagerID.toString() + ',' + FromDate.toString();
    GetWMADetatil(inputParamsOfficer, "Officer");
}

function FillWMADetailL1(OfficerID, FromDate, ToDate) {
    var inputParams = OfficerID.toString() + ',' + FromDate.toString();
    GetWMALogDetail(inputParams, OfficerID);
}

function FillWMADetailL2(OfficerID, FromDate, LogType) {
    var inputParams = OfficerID.toString() + ',' + FromDate.toString() + ',' + LogType;
    GetWMACaseDetail(inputParams);
}

//TODO: SignalR

function GetWMADetatil(parameter, ltype) {
    try {
        counter.server.getwmadetail(parameter, ltype);
    } catch (e) {
        // alert("GetWMADetatil : " + e.message);
    }
}

function GetWMALogDetail(parameter, ltype) {
    try {
        counter.server.getwmalogdetail(parameter, ltype);
    } catch (e) {
        //alert("GetWMALogDetail : " + e.message);
    }
}

function GetWMACaseDetail(parameter) {
    try {
        counter.server.getwmacasedetail(parameter);
    } catch (e) {
        //  alert("GetWMACaseDetail : " + e.message);
    }
}

//==========================================================================================================================================
//TODO : Working Hours function

function GetWorkingHours(OfficerID) {
    var Parameter = "";

    if ($.trim(OfficerID) != "") {
        Parameter = 'OfficerID=' + OfficerID;
    }

    if ($.trim(Flag) != "") {
        if (Parameter != "") {
            Parameter += "&";
            Parameter += 'ShowAll=' + Flag;
        }
        else
            Parameter = 'ShowAll=' + Flag;
    }

    $.ajax({
        url: ServiceURL + 'api/v1/officers/OfficersWorkingHours?' + Parameter,
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        type: 'GET',
        dataType: DataType,
        beforeSend: function () {
        },
        success: function (data) {
            if (!(data == '' || data == null || data == '[]')) {
                $('#AMoreWorkingHours').show();
                $('#tbodyWorkingHours').empty();
                $.each(data, function (key, value) {
                    if (value.CallId == '9999' || value.CallId == '99991') {
                        $('#thWorkingName').html('Manager Name');
                        $('#tbodyWorkingHours').append($('<tr>').append($('<td>').append($('<a>').attr('data-toggle', 'modal').attr('href', '#DivWorkingHoursMore')
                                                                    .attr('onclick', 'GetWorkingHoursMore(' + value.OfficerID + ',0' + ')').html(value.OfficerName)))
                                                                .append($('<td id="tdbodyWorkingHours' + key + '">').html(value.AvgHours))
                                                                .append($('<td>').html(value.PredictedHours)))
                    }
                    else {
                        $('#thWorkingName').html('Officer Name');
                        $('#tbodyWorkingHours').append($('<tr>').append($('<td>').html(value.OfficerName))
                                                           .append($('<td id="tdbodyWorkingHours' + key + '">').html(value.AvgHours))
                                                           .append($('<td>').html(value.PredictedHours)))
                    }
                });
            }
            else {
                $('#tbodyWorkingHours').empty().append('<tr><td colspan="3">No data found</td></tr>');
                $('#AMoreWorkingHours').hide();
            }
        },
        complete: function () { },
        error: function (xhr, textStatus, errorThrown) { }
    });
}

function GetWorkingHoursMore(OfficerID, Flag) {
    $('#DivWorkingHoursOfficersList').hide();
    $('#DivWorkingHoursManagersList').show();
    $('#btnWorkingHoursBack').hide();
    var Parameter = "";

    if ($.trim(OfficerID) != "") {
        Parameter = 'OfficerID=' + OfficerID;
    }

    if ($.trim(Flag) != "") {
        if (Parameter != "") {
            Parameter += "&";
            Parameter += 'ShowAll=' + Flag;
        }
        else
            Parameter = 'ShowAll=' + Flag;
    }

    $.ajax({
        url: ServiceURL + 'api/v1/officers/OfficersWorkingHours?' + Parameter,
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        type: 'GET',
        dataType: DataType,
        beforeSend: function () {
        },
        success: function (data) {
            $('#tbodyWorkingHoursManagersList').empty();
            if (!(data == '' || data == null || data == '[]')) {
                $.each(data, function (key, value) {
                    if (value.CallId == '9999' || value.CallId == '99991') {
                        $('#thWorkingName').html('Manager Name');
                        $('#tbodyWorkingHoursManagersList').append($('<tr>').append($('<td>').append($('<a>').attr('style', 'cursor:pointer;')
                                                                .attr('onclick', 'GetWorkingHoursForOfficers(' + value.OfficerId + ')').html(value.OfficerName)))
                                                           .append($('<td id="tdbodyWorkingHoursManagerList' + key + '">').html(value.AvgHours))
                                                           .append($('<td>').html(value.PredictedHours)))
                    }
                    else {
                        $('#thWorkingName').html('Officer Name');
                        $('#tbodyWorkingHoursManagersList').append($('<tr>').append($('<td>').html(value.OfficerName))
                                                       .append($('<td id="tdbodyWorkingHoursManagerList' + key + '">').html(value.AvgHours))
                                                       .append($('<td>').html(value.PredictedHours)))
                    }
                });
            }
            else {
                $('#tbodyWorkingHoursManagersList').append('<tr><td colspan="3">No data found </td></tr>');
            }
        },
        complete: function () { },
        error: function (xhr, textStatus, errorThrown) { }
    });
}

function GetWorkingHoursForOfficers(OfficerID) {
    $('#DivWorkingHoursOfficersList').show();
    $('#DivWorkingHoursManagersList').hide();
    $('#btnWorkingHoursBack').show();

    $.ajax({
        url: ServiceURL + 'api/v1/officers/OfficersWorkingHours?OfficerID=' + OfficerID + '&ShowAll=0',
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        type: 'GET',
        dataType: DataType,
        beforeSend: function () {
        },
        success: function (data) {
            $('#tbodyWorkingHoursManagersList').empty();
            if (!(data == '' || data == null || data == '[]')) {
                $('#tbodyWorkingHoursOfficersList').empty();
                $.each(data, function (key, value) {
                    $('#tbodyWorkingHoursOfficersList').append($('<tr>').append($('<td>').html(value.OfficerName))
                                                            .append($('<td id="tdbodyWorkingHoursOfficerList' + key + '">').html(value.AvgHours))
                                                            .append($('<td>').html(value.PredictedHours)))
                });
            }
            else {
                $('#tbodyWorkingHoursOfficersList').append('<tr><td colspan="3">No data found </td></tr>');
            }
        },
        complete: function () { },
        error: function (xhr, textStatus, errorThrown) { }
    });
}

//==========================================================================================================================================
//TODO : Last location function

function FillLastKnownLocation(ManagerID, FromDate, ToDate, OfficerID, GroupID, ShowAll) {
    if (ManagerID == $.session.get('OfficerID') && OfficerID == undefined) ManagerID = $.session.get('ManagerID');
    MID = ManagerID;

    if (OfficerID == undefined) {
        if ($('#lblSelectedOfficerID').html() == undefined || $.trim($('#lblSelectedOfficerID').html()) == '')
            OfficerID = 0;
        else
            OfficerID = $('#lblSelectedOfficerID').html();
    }
    OID = OfficerID;
    GID = GroupID;
    if (FromDate == '') {
        var date = new Date();
        var TDate = $.datepicker.formatDate('dd/mm/yy', date);
        FromDate = TDate;
    }

    var dateAr = FromDate.split('/');
    var newDate = dateAr[2] + '/' + dateAr[1] + '/' + dateAr[0];
    FromDate = newDate;

    if (OfficerID == undefined) OfficerID = 0;
    if (GroupID == undefined) GroupID = 0;
    if (ShowAll == undefined) ShowAll = 0;

    FD = FromDate;
    TD = FromDate;

    MapFillLastKnownLocation(MID, FD, TD, OID, GID, 0);
}

function MapFillLastKnownLocation(mID, fDate, tDate, oID, gID, ShowAll) {
    $('#DHeartBeatMap,#DLocationHBMap').hide();
    $('#map_canvas').show();
    if (gID == undefined) gID = 0;
    var inputParams = mID + ',' + fDate + ',' + $.trim(oID) + ',' + $.trim(gID);
    GetLastLocation(inputParams);
}

//==========================================================================================================================================
//TODO : Ranking function

function FillRankings(ManagerID, FromDate, ToDate, ActionText) {
    var inputParams = ActionText + "," + FromDate + "," + ToDate + "," + $.session.get('CompanyID') + "," + ManagerID;
    switch (ActionText) {
        case 'Paid':
            GetRanking(inputParams, 1, "Manager");
            break;
        case 'Returned':
            GetRanking(inputParams, 2, "Manager");
            break;
        case 'Part Paid':
            GetRanking(inputParams, 3, "Manager");
            break;
    }
}

function FillRankingsForADM(ManagerID, FromDate, ToDate, ActionText) {
    var Flag;
    switch (ActionText) {
        case 'Paid':
            $('#lblRankStatus').html('Ranking - Paid');
            $('#HCaseActionTitle').html('Paid');
            Flag = 1;
            break;
        case 'Returned':
            $('#lblRankStatus').html('Ranking - Returned');
            $('#HCaseActionTitle').html('Returned');
            Flag = 4;
            break;
        case 'Part Paid':
            $('#lblRankStatus').html('Ranking - Part Paid');
            $('#HCaseActionTitle').html('Part Paid');
            Flag = 2;
            break;
    }
    var inputParams = ActionText + "," + FromDate + "," + ToDate + "," + $.session.get('CompanyID') + "," + ManagerID;
    GetRanking(inputParams, Flag, "ADM");
}

function FillRankingsForOfficer(ManagerID, FromDate, ToDate, ActionText) {
    var Flag;
    switch (ActionText) {
        case 'Paid':
            $('#lblRankStatus').html('Ranking - Paid');
            $('#HCaseActionTitle').html('Paid');
            Flag = 1;
            break;
        case 'Returned':
            $('#lblRankStatus').html('Ranking - Returned');
            $('#HCaseActionTitle').html('Returned');
            Flag = 4;
            break;
        case 'Part Paid':
            $('#lblRankStatus').html('Ranking - Part Paid');
            $('#HCaseActionTitle').html('Part Paid');
            Flag = 2;
            break;
    }
    var inputParams = ActionText + "," + FromDate + "," + ToDate + "," + $.session.get('CompanyID') + "," + ManagerID;
    //Ranking SignalR Method
    GetRanking(inputParams, Flag, "Officer");
}

//==========================================================================================================================================
//TODO : Clamp function

function ViewClampedImage(key, OfficerID, CaseNo) {

    $.session.set('ImageType', 'C');
    $.session.set('ClampCaseNo', CaseNo);
    $.session.set('ClampOfficerID', OfficerID);
    $('#iframeClampImage').attr('src', 'OptimiseImage.html');
    $('#iframeActionClampImage').attr('src', 'OptimiseActionImage.html');

    $('.clampSelect').removeClass('clampSelect').addClass('clampUnselect').attr('src', 'images/ViewImage_24_24_2.png');

    if ($('#imgClampImage' + key).attr('src') == 'images/ViewImage_24_24_2.png')
        $('#imgClampImage' + key).attr('src', 'images/ViewImage_24_24_1.png').attr('class', 'clampSelect');
    else
        $('#imgClampImage' + key).attr('src', 'images/ViewImage_24_24_2.png').attr('class', 'clampUnselect');
}

function ViewActionClampedImage(key, OfficerID, CaseNo) {

    $.session.set('ImageType', 'C');
    $.session.set('ClampCaseNo', CaseNo);
    $.session.set('ClampOfficerID', OfficerID);
    $('#iframeActionClampImage').attr('src', 'OptimiseActionImage.html');

    $('.ActionclampSelect').removeClass('ActionclampSelect').addClass('ActionclampUnselect').attr('src', 'images/ViewImage_24_24_2.png');

    if ($('#imgActionClampImage' + key).attr('src') == 'images/ViewImage_24_24_2.png')
        $('#imgActionClampImage' + key).attr('src', 'images/ViewImage_24_24_1.png').attr('class', 'ActionclampSelect');
    else
        $('#imgActionClampImage' + key).attr('src', 'images/ViewImage_24_24_2.png').attr('class', 'ActionclampUnselect');
}

//==========================================================================================================================================
//TODO : Bail function

function ViewBailedImage(Bkey, BailedImageURL) {
    $.session.set('ImageType', 'B');
    $.session.set('BailedImageURL', BailedImageURL);
    $('#iframeBailedImage').attr('src', 'OptimiseImage.html');
    $('.BailedSelect').removeClass('BailedSelect').addClass('BailedUnselect').attr('src', 'images/ViewImage_24_24_2.png');

    if ($('#imgBailedImage' + Bkey).attr('src') == 'images/ViewImage_24_24_2.png')
        $('#imgBailedImage' + Bkey).attr('src', 'images/ViewImage_24_24_1.png').attr('class', 'BailedSelect');
    else
        $('#imgBailedImage' + Bkey).attr('src', 'images/ViewImage_24_24_2.png').attr('class', 'BailedUnselect');
}

function ViewActionBailedImage(Bkey, BailedImageURL) {
    $.session.set('ImageType', 'B');
    $.session.set('BailedImageURL', BailedImageURL);

    if ($.session.get('TreeLevel') == '3') {
        $('#iframeActionBailedImage').attr('src', 'OptimiseActionImage.html');
        $('.ActionBailedSelect').removeClass('ActionBailedSelect').addClass('ActionBailedUnselect').attr('src', 'images/ViewImage_24_24_2.png');

        if ($('#imgActionBailedImage' + Bkey).attr('src') == 'images/ViewImage_24_24_2.png')
            $('#imgActionBailedImage' + Bkey).attr('src', 'images/ViewImage_24_24_1.png').attr('class', 'ActionBailedSelect');
        else
            $('#imgActionBailedImage' + Bkey).attr('src', 'images/ViewImage_24_24_2.png').attr('class', 'ActionBailedUnselect');
    }
}

//==========================================================================================================================================
//TODO : Timeline

function GetTimelineResult(OfficerID) {
    var Fdate = $.trim($('#txtTLineFromDate').val()) == '' ? '1/1/1900' : $.trim($('#txtTLineFromDate').val());
    var dateT1 = Fdate.split('/');
    var newDateFrom = dateT1[0] + '-' + dateT1[1] + '-' + dateT1[2];
    $.ajax({
        url: ServiceURL + 'api/v1/officers/' + OfficerID + '/' + newDateFrom + '/' + newDateFrom + '/TimelineDetails',
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        type: 'GET',
        dataType: DataType,
        beforeSend: function () {
            $('#selCompanyList').empty();
        },
        success: function (data) {
            $('#cd-timeline').empty();
            if (!(data == '' || data == null || data == '[]')) {
                $.each(data, function (key, value) {
                    $('#cd-timeline').append('<div class="cd-timeline-block">' +
                                             '<div id="DTimeLinePicture' + key + '" ></div>' +
                                             '<div class="cd-timeline-content"><h2>' + value.TTitle + '</h2> <p>' + value.TDescription + '</p>' +
                                             '<span class="cd-date">' + value.TDate + '</span></div></div>');

                    switch (value.TTitle) {
                        case 'LOGIN':
                        case 'ARR':
                        case 'DEP':
                            $('#DTimeLinePicture' + key).attr('class', 'cd-timeline-img cd-picture');
                            break;
                        case 'DROPPED':
                        case 'Returned':
                        case 'UTTC':
                            $('#DTimeLinePicture' + key).attr('class', 'cd-timeline-img cd-movie');
                            break;
                        case 'OTHER':
                        case 'TCG':
                        case 'Left Letter':
                            $('#DTimeLinePicture' + key).attr('class', 'cd-timeline-img cd-location');
                            break;
                        case 'PAID':
                        case 'PART PAID':
                        case 'TCG PAID':
                        case 'TCG PP':
                        case 'Paid':
                        case 'Part Paid':
                            $('#DTimeLinePicture' + key).attr('class', 'cd-timeline-img cd-location');
                            break;
                    }
                });
            }
            else {
                $('#cd-timeline').append('<div class="cd-timeline-block">' +
                                           '<div class="cd-timeline-img cd-location" ></div>' +
                                           '<div class="cd-timeline-content"><h2>No data found...</h2></div></div>');
            }
        },
        complete: function () { },
        error: function (xhr, textStatus, errorThrown) { }
    });
}

//==========================================================================================================================================
//TODO : Notification function

function Notification() {
    $('#lblSendNotification').attr('data-toggle', 'modal').attr('href', '#Notification');
    $('#' + $('#popup2Trigger').attr('aria-describedby')).attr('class', 'popover fade bottom');
}

function UpdateNotificationAlert(ManagerID, MessageText, OfficerID, IsPriority) {
    if (OfficerID == undefined) OfficerID = 0;
    $.ajax({
        url: ServiceURL + 'api/v1/officers/' + ManagerID + '/' + MessageText.replace('&', 'ybcc') + '/' + $.trim(OfficerID) + '/' + IsPriority + '/Notification',
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        type: 'PUT',
        dataType: 'json',
        beforeSend: function () {
            $('.word_count').hide();
            $('#imgLoading').show();
            $('#btnSendNotification').hide();
        },
        success: function (data) {
        },
        complete: function () {
            if ($("#chkIsPriority").is(':checked')) {
                $('.word_count span').text('200 characters left');
            }
            else {
                $('.word_count span').text('900 characters left');
            }
            $('#lblMsgReport').html('Message sent successfully').show().fadeOut(3000);
            $('#lblMsgReport').css('color', 'green');
            $('.word_count').fadeIn(4000);
            $('#imgLoading').hide();
            $('#btnSendNotification').show();

            $('#selOfficer').multiselect({
                noneSelectedText: 'Select Officers',
                selectedList: 5,
                multiple: true
            }).multiselectfilter();

            $('#selOfficer').multiselect("uncheckAll");
        },
        error: function (xhr, textStatus, errorThrown) {
        }
    });
}

//==========================================================================================================================================
//TODO : Officer Activity status

function OfficerActivityStatusClick() {
    OfficerActivityStatusInit();
    $('#AOfficerActivityStatus').click();
}

function GetOfficerActivityStatus() {
    $.ajax({
        url: ServiceURL + 'api/v1/officers/' + tableIndex + '/' + $.session.get('OfficerID') + '/OfficerActivityStatus',
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        type: 'GET',
        dataType: 'json',
        beforeSend: function () { },
        success: function (data) {
            $('#' + $('#popup2Trigger').attr('aria-describedby')).attr('class', 'popover fade bottom');
            $('#DivOfficerStatusContent').empty();
            $('#DivOfficerStatusContent').append('<div id="tdCont"></div>');
            $('#DivOfficerStatusContent').append($('<div style="border: 1px solid #ededed; margin: -15px; margin-left:12px; width:1150px; height: 370px;display:none;" id="divMapOAS"><iframe  width="100%" height="100%" frameborder="0"' +
                                    'scrolling="yes" marginheight="0"' +
                                    'marginwidth="0" id="iframeGMAPOAS" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=London,+United+Kingdom&amp;aq=0&amp;oq=lond&amp;sll=55.378051,-3.435973&amp;sspn=20.606847,39.506836&amp;ie=UTF8&amp;hq=&amp;hnear=London,+United+Kingdom&amp;t=m&amp;z=11&amp;iwloc=A&amp;ll=51.511214,-0.119824&amp;output=embed"></iframe></div>')

               )
            if (data != undefined) {
                if (data != "[]" && data != null) {
                    $.each(data, function (key, value) {
                        tableIndex = value.IndexNo;
                        $('#lblManagerNameActivityStatus').html(value.ManagerName);//.html('Officer`s Activity Status - ' + value.ManagerName);
                        $('#tdGray').html(value.Darkgray);
                        $('#tdGreen').html(value.Green);
                        $('#tdRed').html(value.Red);
                        $('#tdAmber').html(value.Amber);

                        if ($('#tbl' + $.trim(value.ManagerId)).length == 0) {
                            $('#tdCont').append('<div id="tbl' + $.trim(value.ManagerId) + '"></div>')
                        }

                        if ($('#tbl' + $.trim(value.ManagerId) + ' tr').length == 0) {
                            $('#tbl' + $.trim(value.ManagerId)).append($('<div class="col-md-4 col-sm-4">').append($('<div class="bubble ' + value.Status + 'Bub">')
                                    .html(value.OfficerName).attr('id', 'OAS' + value.OfficerID)))
                        }
                        else {
                            if ($('#tbl' + $.trim(value.ManagerId) + ' tr:last' + ' td').length >= 3) {
                                $('#tbl' + $.trim(value.ManagerId)).append($('<tr>'))
                            }
                            $('#tbl' + $.trim(value.ManagerId) + ' tr:last').append($('<td style="padding:10px !important;">')
                                .append($('<div class="bubble ' + value.Status + 'Bub">').html(value.OfficerName).attr('id', 'OAS' + value.OfficerID)));
                        }

                        $('#OAS' + value.OfficerID).click(function () {
                            $('#btnBacktoStatus').show();
                            $('#lblOfficerNameMapview').html(value.OfficerName);
                            $('#lblOfficerNameMapview').show();

                            clearInterval(IntervalvalueOfficerStatus);

                            $('#iframeGMAPOAS').attr('src', 'GMaps.html?mID=0' + "&fDate=" + $.datepicker.formatDate('yy/mm/dd', new Date()) + "&tDate="
                                + $.datepicker.formatDate('yy/mm/dd', new Date())
                                + "&oID=" + $(this).attr('id').split('OAS')[1] + "&Map=Large&gID=0&ShowAll=1");

                            $('#tdPrev').hide();
                            $('#tdNext').hide();

                            $('#divMapOAS').show();
                            $('#tdCont').hide();
                            $('#divOfficerActivityStatus').css('width', '87%').css('height', '90%');

                            $('#btnBacktoStatus').unbind().click(function () {
                                $('#tdCont').show();
                                $('#divMapOAS').hide();
                                $('#tdPrev').show();
                                $('#tdNext').show();

                                $('#lblOfficerNameMapview').hide();
                                clearInterval(IntervalvalueOfficerStatus);
                                $('#divOfficerActivityStatus').css('width', '').css('height', '');
                                $(this).hide();

                                tableIndex = tableIndex - 1;
                                GetOfficerActivityStatus();

                                IntervalvalueOfficerStatus = setInterval(function () {
                                    GetOfficerActivityStatus();
                                }, 180000);
                            });
                        });

                    });

                    $('#lblManagerNameActivityStatus').html($('#tdCont table:eq(' + tableIndex + ')').attr('mname'));
                    tableIndex = tableIndex + 1;
                }
            }

            $('#DivOfficerStatusContent').append($('<table style="width:100%">')
           .append($('<tr >').append($('<td style="width:50%">').attr('id', 'tdPrev'))
           .append($('<td style="width:50%;text-align:right;">').attr('id', 'tdNext'))))

            $('#tdPrev').append('<img src="images/Previous_W.png" style="cursor:pointer;" /><br><span style="cursor:pointer;">Previous</span>');
            $('#tdNext').append('<img src="images/Next_W.png" style="cursor:pointer;" /><br><span style="cursor:pointer;">Next</span>');

            $('#tdPrev').unbind().click(function () {

                tableIndex = tableIndex - 2;

                if (tableIndex <= -1) tableIndex = 0;
                GetOfficerActivityStatus();
            });

            $('#tdNext').unbind().click(function () {

                GetOfficerActivityStatus();

                clearInterval(IntervalvalueOfficerStatus);
                IntervalvalueOfficerStatus = setInterval(function () {
                    GetOfficerActivityStatus();
                }, 180000);
            });
        },
        complete: function () { },
        error: function (xhr, textStatus, errorThrown) { }
    });
}

function OfficerActivityStatusInit() {
    GetOfficerActivityStatus();
}

//==========================================================================================================================================
//TODO : Flash news function

function FlashOn() {
    $('#btnFlashOn').hide();
    $('#DMarquee').show();
    $('#btnFlashOff').show();
    $.session.set('FlashNews', 'On');
    $('#' + $('#popup2Trigger').attr('aria-describedby')).attr('class', 'popover fade bottom');
}

function FlashOff() {
    $('#DMarquee').hide();
    $('#btnFlashOff').hide();
    $('#btnFlashOn').show();
    $.session.set('FlashNews', 'Off');
    $('#' + $('#popup2Trigger').attr('aria-describedby')).attr('class', 'popover fade bottom');
}

function GetFlashNews() {
    $.ajax({
        type: 'GET',
        url: ServiceURL + 'api/v1/cases/' + $.session.get('CompanyID') + '/GetFlashNews',
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        dataType: 'json',
        beforeSend: function () { },
        success: function (data) {
            if (data != undefined) {
                if (data != "[]" && data != null && data != '[{"Result":"NoRecords"}]') {
                    $('#DMarquee').empty().append('<marquee behavior="scroll" direction="left" scrollamount="6">' + data + '</marquee>')
                }
            }
        },
        complete: function () { },
        error: function (xhr, textStatus, errorThrown) {
        }
    });
}

//==========================================================================================================================================
//TODO : Company function

function GetCompany() {
    $.ajax({
        url: ServiceURL + 'api/v1/companies',
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        type: 'GET',
        dataType: DataType,
        beforeSend: function () {
            $('#selCompanyList').empty();
        },
        success: function (data) {
            $.each(data, function (key, value) {
                $('#selCompanyList').append($('<option>').attr('value', $.trim(value.CompanyID)).html(value.CompanyName));
            });
            $('#selCompanyList').val($.session.get('CompanyID')).attr("selected", "selected");
        },
        complete: function () { },
        error: function (xhr, textStatus, errorThrown) { }
    });
}

function SubmitOfficerTotal() {
    var TTargetInfo = '';
    $('#tbodyOfficerList tr').each(function (key, value) {
        if ($(this).find('label').html() != '' && $(this).find('label').html() != undefined && parseInt($(this).find('input').val()) > 0) {
            if (TTargetInfo != '') TTargetInfo += ',';
            TTargetInfo += $(this).find('label').html() + '|' + parseInt($(this).find('input').val());
        }
    });

    $.ajax({
        url: ServiceURL + 'api/v1/officers/' + $('#selCurrentMonth').val() + '/' + $('#selCurrentMonth').val() +
            '/' + $('#txtMonthTotalTarget').val() + '/' + ManagerID + '/' + TTargetInfo + '/OfficerTarget',
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        type: 'PUT',
        dataType: DataType,
        beforeSend: function () {

        },
        success: function (data) {
            if (data == true) {
                $('#lblTargetInfo').html('Target has been updated successfully.');
                $('#ATargetInfo').click();
            }
        },
        complete: function () { },
        error: function (xhr, textStatus, errorThrown) { }
    });
}

//==========================================================================================================================================
//TODO : Case detail function

function FillCaseDetailsAction(ManagerID, FromDate, ToDate, OfficerID, GroupID) {
    Clamp = 0;
    Bailed = 0;
    Type = "GET";
    serviceTypeLogin = "getcaseactions";

    if (OfficerID == undefined) OfficerID = 0;
    if (GroupID == undefined) GroupID = 0;


    if (FromDate == '') {
        var date = new Date();
        var TDate = $.datepicker.formatDate('dd/mm/yy', date);
        FromDate = TDate;
    }
    if (ToDate == '') {
        var date1 = new Date();
        var T1Date = $.datepicker.formatDate('dd/mm/yy', date1);
        ToDate = T1Date;
    }

    var dateAr = FromDate.split('/');
    var newDate = dateAr[2] + '/' + dateAr[1] + '/' + dateAr[0];
    FromDate = newDate;

    var dateTr = ToDate.split('/');
    var newToDate = dateTr[2] + '/' + dateTr[1] + '/' + dateTr[0];
    ToDate = newToDate;

    var inputParams = "/GetCaseActionsDetail_Dashboard?ManagerID=" + $.trim(ManagerID) + "&FromDate=" + FromDate + "&ToDate=" + ToDate + "&OfficerID=" + OfficerID
        + "&GroupID=" + GroupID + "&" + $.session.get('BrowserId');

    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;
    //CallService();
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
            $('#imgLoader8').show();
        },
        complete: function () {
            $('#imgLoader8').fadeOut(1000);
        },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {

                    result = JSON.parse(result);
                    $('#tblbodyActionPaid').empty();
                    $('#tbodyActionPP').empty();
                    $('#tbodyActionReturned').empty();
                    $('#tbodyActionLeftLetter').empty();
                    $('#tbodyActionEnforcementStart').empty();
                    $('#tbodyActionRevisit').empty();
                    $('#tbodyActionEnforcementEnd').empty();

                    $('#tbodyActionClamped').empty();
                    $('#tbodyActionBailed').empty();
                    $('#tbodyActionArrested').empty();
                    $('#tbodyActionSurrenderDateAgreed').empty();


                    $('#tbodyActionTCG').empty();
                    $('#tbodyActionTCGPAID').empty();
                    $('#tbodyActionTCGPP').empty();
                    //$('#tbodyAction_PAID').empty();
                    //$('#tbodyAction_PARTPAID').empty();
                    $('#tbodyActionUTTC').empty();
                    $('#tbodyActionDROPPED').empty();
                    $('#tbodyActionOTHER').empty();

                    $('#tbodyRankOfficerPaid').empty();
                    $('#tbodyRankOfficerPartPaid').empty();
                    $('#tbodyRankOfficerReturn').empty();

                    $.each(result, function (key, value) {
                        switch (value.ActionText) {
                            case 'Paid':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tblbodyActionPaid').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))

                                        $('#tbodyRankOfficerPaid').append($('<tr style = "background:#66FF99">')
                                      .append($('<td>').html(''))
                                      .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                      .append($('<td>').html(value.ActionText))
                                      .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                      .append($('<td>').html(value.DateActioned))
                                      .append($('<td>').html("" + value.Fees))
                                      .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficerRank(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                      .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tblbodyActionPaid').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))

                                        $('#tbodyRankOfficerPaid').append($('<tr style = "background:#FFCCFF">')
                                     .append($('<td>').html(''))
                                     .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                     .append($('<td>').html(value.ActionText))
                                     .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                     .append($('<td>').html(value.DateActioned))
                                     .append($('<td>').html("" + value.Fees))
                                     .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficerRank(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                     .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tblbodyActionPaid').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))

                                    $('#tbodyRankOfficerPaid,#tbodyRankMorePaidCaseList').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionRank(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;
                            case 'Part Paid':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionPP').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 2 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))

                                        $('#tbodyRankOfficerPartPaid').append($('<tr style = "background:#66FF99">')
                                    .append($('<td>').html(''))
                                    .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                    .append($('<td>').html(value.ActionText))
                                    .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                    .append($('<td>').html(value.DateActioned))
                                    .append($('<td>').html("" + value.Fees))
                                    .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficerRank(" + key + "," + 2 + ")'> ...More</label>" : value.Notes)))
                                    .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tbodyActionPP').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 2 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))

                                        $('#tbodyRankOfficerPartPaid').append($('<tr style = "background:#FFCCFF">')
                                      .append($('<td>').html(''))
                                      .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                      .append($('<td>').html(value.ActionText))
                                      .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                      .append($('<td>').html(value.DateActioned))
                                      .append($('<td>').html("" + value.Fees))
                                      .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficerRank(" + key + "," + 2 + ")'> ...More</label>" : value.Notes)))
                                      .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tbodyActionPP').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 2 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))

                                    $('#tbodyRankOfficerPartPaid,#tbodyRankMorePPCaseList').append($('<tr>')
                                      .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                      .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                      .append($('<td>').html(value.ActionText))
                                      .append($('<td>').html(value.DoorColour))
                                      .append($('<td>').html(value.HouseType))
                                      .append($('<td>').html(value.DateActioned))
                                      .append($('<td>').html("" + value.Fees))
                                      .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionRank(" + key + "," + 2 + ")'> ...More</label>" : value.Notes)))
                                      .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;
                            case 'Returned':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionReturned').append($('<tr style = "background:#66FF99">')//,#tblbodyReturned
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 4 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))

                                        $('#tbodyRankOfficerReturn').append($('<tr style = "background:#66FF99">')//,#tblbodyReturned
                                      .append($('<td>').html(''))
                                      .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                      .append($('<td>').html(value.ActionText))
                                      .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                      .append($('<td>').html(value.DateActioned))
                                      .append($('<td>').html("" + value.Fees))
                                      .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficerRank(" + key + "," + 4 + ")'> ...More</label>" : value.Notes)))
                                      .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tbodyActionReturned').append($('<tr style = "background:#FFCCFF">')//,#tblbodyReturned
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 4 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))

                                        $('#tbodyRankOfficerReturn').append($('<tr style = "background:#FFCCFF">')//,#tblbodyReturned
                                      .append($('<td>').html(''))
                                      .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                      .append($('<td>').html(value.ActionText))
                                      .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                      .append($('<td>').html(value.DateActioned))
                                      .append($('<td>').html("" + value.Fees))
                                      .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficerRank(" + key + "," + 4 + ")'> ...More</label>" : value.Notes)))
                                      .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tbodyActionReturned').append($('<tr>')//,#tblbodyReturned
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 4 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))

                                    $('#tbodyRankOfficerReturn,#tbodyRankMoreReturnedCaseList').append($('<tr>')//,#tblbodyReturned
                                    .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                    .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                    .append($('<td>').html(value.ActionText))
                                    .append($('<td>').html(value.DoorColour))
                                    .append($('<td>').html(value.HouseType))
                                    .append($('<td>').html(value.DateActioned))
                                    .append($('<td>').html("" + value.Fees))
                                    .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionRank(" + key + "," + 4 + ")'> ...More</label>" : value.Notes)))
                                    .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;
                            case 'Left Letter':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionLeftLetter').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 5 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tbodyActionLeftLetter').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 5 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tbodyActionLeftLetter').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 5 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;
                            case 'Clamped':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionClamped')
                                         .append('<tr><td>' + value.CaseNumber + '</td>' +
                                                   '<td>' + value.Officer + '</td>' +
                                                   '<td>' + value.DateActioned + '</td>' +
                                                   '<td><a href="OptimiseActionImage.html" target="_blank"><img src="images/ExpandImage_24_24_2.png" alt="Expand" title="Click to Expand image" style="padding-right:10px; onclick="ViewActionClampedImage(' + key + ',' + value.Officer + ',' + "'" + value.CaseNumber + "'" + ')"/></a>' +
                                                   '<img id="imgActionClampImage' + key + '" src="images/ViewImage_24_24_2.png" class="ActionclampUnselect"  alt="View" title="Click to view image" style="cursor:pointer;" onclick="ViewActionClampedImage(' + key + ',' + value.Officer + ',' + "'" + value.CaseNumber + "'" + ')"/></td></tr>')
                                        if (Clamp == 0) {
                                            ClampKey = key;
                                            ViewActionClampedImage(key, value.Officer, value.CaseNumber);
                                            $('#imgActionClampImage' + key).attr('src', 'images/ViewImage_24_24_1.png').attr('class', 'ActionclampSelect');
                                            Clamp += 1;
                                        }
                                    }
                                    else {
                                        $('#tbodyActionClamped')
                                        .append('<tr><td>' + value.CaseNumber + '</td>' +
                                                   '<td>' + value.Officer + '</td>' +
                                                   '<td>' + value.DateActioned + '</td>' +
                                                   '<td><a href="OptimiseActionImage.html" target="_blank"><img src="images/ExpandImage_24_24_2.png" alt="Expand" title="Click to Expand image" style="padding-right:10px; onclick="ViewActionClampedImage(' + key + ',' + value.Officer + ',' + "'" + value.CaseNumber + "'" + ')"/></a>' +
                                                   '<img id="imgActionClampImage' + key + '" src="images/ViewImage_24_24_2.png" class="ActionclampUnselect"  alt="View" title="Click to view image" style="cursor:pointer;" onclick="ViewActionClampedImage(' + key + ',' + value.Officer + ',' + "'" + value.CaseNumber + "'" + ')"/></td></tr>')
                                        if (Clamp == 0) {
                                            ClampKey = key;
                                            ViewActionClampedImage(key, value.Officer, value.CaseNumber);
                                            $('#imgActionClampImage' + key).attr('src', 'images/ViewImage_24_24_1.png').attr('class', 'ActionclampSelect');
                                            Clamp += 1;
                                        }
                                    }
                                }
                                else {
                                    $('#tbodyActionClamped')
                                       .append('<tr><td>' + value.CaseNumber + '</td>' +
                                                   '<td>' + value.Officer + '</td>' +
                                                   '<td>' + value.DateActioned + '</td>' +
                                                   '<td><a href="OptimiseActionImage.html" target="_blank"><img src="images/ExpandImage_24_24_2.png" alt="Expand" title="Click to Expand image" style="padding-right:10px; onclick="ViewActionClampedImage(' + key + ',' + value.Officer + ',' + "'" + value.CaseNumber + "'" + ')"/></a>' +
                                                   '<img id="imgActionClampImage' + key + '" src="images/ViewImage_24_24_2.png" class="ActionclampUnselect"  alt="View" title="Click to view image" style="cursor:pointer;" onclick="ViewActionClampedImage(' + key + ',' + value.Officer + ',' + "'" + value.CaseNumber + "'" + ')"/></td></tr>')
                                    if (Clamp == 0) {
                                        ClampKey = key;
                                        ViewActionClampedImage(key, value.Officer, value.CaseNumber);
                                        $('#imgActionClampImage' + key).attr('src', 'images/ViewImage_24_24_1.png').attr('class', 'ActionclampSelect');
                                        Clamp += 1;
                                    }
                                }
                                break;
                            case 'Bailed':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionBailed')
                                     .append('<tr><td>' + value.CaseNumber + '</td>' +
                                                    '<td>' + value.Officer + '</td>' +
                                                    '<td>' + value.DateActioned + '</td>' +
                                                    '<td><a href="OptimiseActionImage.html" target="_blank"><img src="images/ExpandImage_24_24_2.png" alt="Expand" title="Click to Expand image" style="padding-right:10px; onclick="ViewActionBailedImage(' + key + ',' + "'" + value.ImageURL + "'" + ')"/></a>' +
                                                     '<img id="imgActionBailedImage' + key + '" src="images/ViewImage_24_24_2.png" class="ActionBailedUnselect"  alt="View" title="Click to view image" style="cursor:pointer;" onclick="ViewActionBailedImage(' + key + ',' + "'" + value.ImageURL + "'" + ')"/></td></tr>')
                                        if (Bailed == 0) {
                                            BailedKey = key;
                                            $('#imgActionBailedImage' + key).attr('src', 'images/ViewImage_24_24_1.png').attr('class', 'ActionBailedSelect');
                                            ViewActionBailedImage(key, "'" + value.ImageURL + "'");
                                            Bailed += 1;
                                        }
                                    }
                                    else {
                                        $('#tbodyActionBailed')
                                     .append('<tr><td>' + value.CaseNumber + '</td>' +
                                                    '<td>' + value.Officer + '</td>' +
                                                    '<td>' + value.DateActioned + '</td>' +
                                                    '<td><a href="OptimiseActionImage.html" target="_blank"><img src="images/ExpandImage_24_24_2.png" alt="Expand" title="Click to Expand image" style="padding-right:10px; onclick="ViewActionBailedImage(' + key + ',' + "'" + value.ImageURL + "'" + ')"/></a>' +
                                                     '<img id="imgActionBailedImage' + key + '" src="images/ViewImage_24_24_2.png" class="ActionBailedUnselect"  alt="View" title="Click to view image" style="cursor:pointer;" onclick="ViewActionBailedImage(' + key + ',' + "'" + value.ImageURL + "'" + ')"/></td></tr>')
                                        if (Bailed == 0) {
                                            BailedKey = key;
                                            $('#imgActionBailedImage' + key).attr('src', 'images/ViewImage_24_24_1.png').attr('class', 'ActionBailedSelect');
                                            ViewActionBailedImage(key, "'" + value.ImageURL + "'");
                                            Bailed += 1;
                                        }
                                    }
                                }
                                else {
                                    $('#tbodyActionBailed')
                                    .append('<tr><td>' + value.CaseNumber + '</td>' +
                                                   '<td>' + value.Officer + '</td>' +
                                                   '<td>' + value.DateActioned + '</td>' +
                                                   '<td><a href="OptimiseActionImage.html" target="_blank"><img src="images/ExpandImage_24_24_2.png" alt="Expand" title="Click to Expand image" style="padding-right:10px; onclick="ViewActionBailedImage(' + key + ',' + "'" + value.ImageURL + "'" + ')"/></a>' +
                                                    '<img id="imgActionBailedImage' + key + '" src="images/ViewImage_24_24_2.png" class="ActionBailedUnselect"  alt="View" title="Click to view image" style="cursor:pointer;" onclick="ViewActionBailedImage(' + key + ',' + "'" + value.ImageURL + "'" + ')"/></td></tr>')
                                    if (Bailed == 0) {
                                        BailedKey = key;
                                        if ($.session.get('TreeLevel') == '3') {
                                            $('#imgActionBailedImage' + key).attr('src', 'images/ViewImage_24_24_1.png').attr('class', 'ActionBailedSelect');
                                            ViewActionBailedImage(key, "'" + value.ImageURL + "'");
                                        }
                                        Bailed += 1;
                                    }
                                }
                                break;
                            case 'Arrested':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionArrested').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 5 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tbodyActionArrested').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 5 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tbodyActionArrested').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 5 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;
                            case 'Surrender date agreed':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionSurrenderDateAgreed').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 5 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tbodyActionSurrenderDateAgreed').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 5 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tbodyActionSurrenderDateAgreed').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 5 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;
                            case 'EnforcementStart':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionEnforcementStart').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tbodyActionEnforcementStart').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tbodyActionEnforcementStart').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;
                            case '£ Collected':
                            case 'Revisit':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionRevisit').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 3 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tbodyActionRevisit').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 3 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tbodyActionRevisit').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 3 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;
                            case 'EnforcementEnd':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionEnforcementEnd').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tbodyActionEnforcementEnd').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tbodyActionEnforcementEnd').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;

                            case 'TCG':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionTCG').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 3 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tbodyActionTCG').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 3 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tbodyActionTCG').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 3 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;
                            case 'TCG PAID':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionTCGPAID').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 4 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tbodyActionTCGPAID').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 4 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tbodyActionTCGPAID').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 4 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;
                            case 'TCG PP':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionTCGPP').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 5 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tbodyActionTCGPP').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 5 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tbodyActionTCGPP').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 5 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;
                            case 'PAID':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tblbodyActionPaid').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tblbodyActionPaid').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tblbodyActionPaid').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;
                            case 'PART PAID':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionPP').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 2 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tbodyActionPP').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 2 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tbodyActionPP').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 2 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;
                            case 'UTTC':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionUTTC').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 6 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tbodyActionUTTC').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 6 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tbodyActionUTTC').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 6 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;
                            case 'DROPPED':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionDROPPED').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 7 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tbodyActionDROPPED').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 7 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tbodyActionDROPPED').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 7 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;
                            case 'OTHER':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionOTHER').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 8 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tbodyActionOTHER').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 8 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tbodyActionOTHER').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 8 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;
                        }
                    });
                }
                else {
                    $('#tblbodyActionPaid').empty().html('No records found');
                    $('#tbodyActionPP').empty().html('No records found');
                    $('#tbodyActionReturned').empty().html('No records found');
                    $('#tbodyActionLeftLetter').empty().html('No records found');
                    $('#tbodyActionEnforcementStart').empty().html('No records found');
                    $('#tbodyActionRevisit').empty().html('No records found');
                    $('#tbodyActionEnforcementEnd').empty().html('No records found');

                    $('#tbodyActionClamped').empty().html('No records found');
                    $('#tbodyActionBailed').empty().html('No records found');
                    $('#tbodyActionArrested').empty().html('No records found');
                    $('#tbodyActionSurrenderDateAgreed').empty().html('No records found');
                    //$('#tbodyleftletterformanager').empty().html('No records found');
                    //$('#tbodyrevisitformanager').empty().html('No records found');

                    $('#tbodyActionTCG').empty().html('No records found');
                    $('#tbodyActionTCGPAID').empty().html('No records found');
                    $('#tbodyActionTCGPP').empty().html('No records found');
                    $('#tbodyActionUTTC').empty().html('No records found');
                    $('#tbodyActionDROPPED').empty().html('No records found');
                    $('#tbodyActionOTHER').empty().html('No records found');
                    $('#tbodyRankOfficerPaid').empty().html('No records found');
                    $('#tbodyRankOfficerPartPaid').empty().html('No records found');
                    $('#tbodyRankOfficerReturn').empty().html('No records found');
                }
            }
        },
        error: function () {
            //alert('error');
        } // When Service call fails
    });
}

//==========================================================================================================================================
//TODO: Page link

function TeamAdmin() {
    window.location.href = TeamAdminUrl;
}

function ReturnAction() {
    window.location.href = ReturnActionUrl;
}

function CaseSearchAction() {
    window.location.href = CaseSearchUrl;
}

function ApproveRequest() {
    window.location.href = ApproveRequestUrl;
}

function HPICheckRequest() {
    window.location.href = HPICheckRequestUrl;
}

function PaymentReport() {
    window.location.href = PaymentReportUrl;
}

function CaseUpload() {
    window.location.href = CaseuploadUrl;
}

function HRview() {
    window.location.href = HRUrl;
}

function SpecialCase() {
    window.location.href = SCUrl;
}

function OptimiseHome() {
    window.location.href = OptimiseHomeUrl;
}

function CaseGuidance() {
    window.location.href = CGAdminUrl;
}

function ClampedImageSearch() {
    window.location.href = ClampedImageUrl;
}

function BailedImageSearch() {
    window.location.href = BailedImageSearchUrl;
}

function GetManageCaseReturn() {
    window.location.href = CaseReturnUrl;
}

function SCAdmin() {
    window.location.href = SCAdminUrl;
}