
//==========================================================================================================================================
/* File Name: AssignRequest.js */
/* File Created: Sep 21, 2015 */
/* Created By  : R.Santhakumar */
//==========================================================================================================================================

$(document).ready(function () {
    $(':text').val('');
    $('#txtFromDate,#txtToDate').attr('readonly', 'readonly');
    $('#txtFromDate').datetimepicker({
        format: 'Y/m/d',
        onShow: function (ct) {
            this.setOptions({
                maxDate: $('#txtToDate').val() ? $('#txtToDate').val() : false
            })
        },
        timepicker: false
    });

    $('#txtToDate').datetimepicker({
        format: 'Y/m/d',
        onShow: function (ct) {
            this.setOptions({
                minDate: $('#txtFromDate').val() ? $('#txtFromDate').val() : false
            })
        },
        timepicker: false
    });


    $('#chkAll').prop('checked', false);
    GetUnAssignRequestForAdmin();
    $('input[name=AppUser][value=1]').prop('checked', true);
    $('input[type=radio][name=AppUser]').change(function () {
        if (this.value == '1') {
            $('#trDeclineNotes').fadeOut("slow");
            $('#txtDeclineNotes').val('');
        }
        else if (this.value == '2') {
            $('#trDeclineNotes').fadeIn(1000);
        }
    });

    $('#btnProcess').click(function () {
        var id = '';
        var ProcessType;//1:Approve 2:Decline

        if ($("input[type='radio'][name=AppUser]").is(':checked')) {
            ProcessType = $("input[type='radio'][name=AppUser]:checked").val();
            $('[name=PendingActionAll]:checked').each(function (key, value) {
                var Data = $(this).val();
                if ($.isEmptyObject(id)) {
                    id += Data;
                }
                else {
                    id += ',' + Data;
                }
            });
            if (!(id == '' || id == undefined || id == null)) {
                UpdateAssignRequestForAdmin(id, ProcessType, $('#txtDeclineNotes').val());
            }
            else {
                alert('Select any one un assign request!');
            }

        }
    });

    $('#btnFindAction').click(function () {
        GetUnAssignRequestForAdmin();
    });

});

function GetUnAssignRequestForAdmin() {
    $.ajax({
        url: ServiceURL + 'api/v1/cases/UnAssignRequest',
        //?OfficerId=' + $('#txtOfficerID').val() + '&CaseNo=' + $('#txtCaseNo').val() +
        //'&FromDate=' + $('#txtFromDate').val() + '&ToDate=' + $('#txtToDate').val(),
        type: 'GET',
        //headers: {
        //    "Authorization": $.session.get('TokenAuthorization')
        //},
        data: {//JSON.stringify(
            "OfficerId": $('#txtOfficerID').val(),
            "CaseNo": $('#txtCaseNo').val(),
            "FromDate": $('#txtFromDate').val(),
            "ToDate": $('#txtToDate').val()
            //"OfficerId": $('#txtOfficerID').val(),
            //"CaseNo": $('#txtCaseNo').val(),
            //"FromDate": $('#txtFromDate').val(),
            //"ToDate": $('#txtToDate').val()
        },
        dataType: 'json',
        contentType: "application/json",
        beforeSend: function (xhr, settings) {
            xhr.setRequestHeader('Authorization', $.session.get('TokenAuthorization'))
        },
        success: function (result) {
            alert(result);
            $('#PendingActionTBody').empty();
            if (!(result == '' || result == '[]' || result == null)) {
                $('#btnApprove').show();
                $.each(result, function (key, value) {
                    $('#PendingActionTBody').append('<tr>' +
                                          '<td id="tdCheckList' + key + '" class="Checklist">' +
                                               '<input type="checkbox" name="PendingActionAll" class="action" id="Chk' + key + '" value="' + value.ID + '" ></td>' +
                                          '<td >' + value.OfficerID + '</td>' +
                                          '<td>' + value.CaseNumber + '</td>' +
                                          '<td>' + value.RequestedDate + '</td>' +
                                          '</tr>');
                });
            }
            else {
                $('#btnApprove').hide();
                $('#PendingActionTBody').append('<tr><td colspan="4" align="center">No data found...</td></tr>');
            }
        },
        complete: function () { },
        error: function (xhr, textStatus, errorThrown) {
            alert('Error :' + errorThrown);
        }
    });
}

function UpdateAssignRequestForAdmin(AssignID, ProcessType, Notes) {
    $.ajax({
        type: 'PUT',
        url: ServiceURL + 'api/v1/cases/' + AssignID + '/' + $.session.get('OfficerID') + '/' + ProcessType + '/' + Notes + '/UpdateAssignRequest',
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        dataType: 'json',
        beforeSend: function () { },
        success: function (data) {
            GetUnAssignRequestForAdmin();
        },
        complete: function () {
            $('#txtDeclineNotes').val('');
            $('#trDeclineNotes').fadeOut("slow");
            $('input[name=AppUser][value=1]').prop('checked', true);
        },
        error: function (xhr, textStatus, errorThrown) { }
    });
}

function CheckboxSelectAll() {
    if (!$('#chkAll').is(':checked')) {
        $('.action').prop('checked', false);
    }
    else {
        $('.action').prop('checked', 'checked');
    }
}