﻿
var selectedGroupID;
var selectedManagerID;
var selectedOfficerID;
var returntype;
var mID;
var OfficerType;
var OfficerRole;
var Certificate;
var Role;
var selectedADM = '';
var TreeLevel = 3;

$(document).ready(function () {
    mID = getCookie("OfficerID");
    GetClientName();
    GetCaseType();
    GetCaseGuidanceList();

    $('#txtOfficerID,#txtOfficerName,#txtDateRegistered').val('');
    $('#btnCreatenewuser').click(function () {
        $(':text').val('');
        $('#txtMessage').val('');
        AlertMessage("Add Case Guidance");
        $('#SelCaseTypeForm').attr('style', 'width:65%');
        $('#SelClientNameForm').attr('style', 'width:65%');
    });

    $('#btnSearchCG').click(function () {
        GetCaseGuidanceList();
    });

    $('#btnClearCG').click(function () {
        $('#SelClientName').prop('selectedIndex', 0);
        $('#SelCaseType').prop('selectedIndex', 0);
        GetCaseGuidanceList();
    });

    $('#DOfficersList').show();
    $('.TeamAdmin').hide();
    $('#lblOffList').html('Team Admin');

    $('#txtDateRegistered').datetimepicker({
        timepicker: false,
        format: 'd/m/Y',
        maxDate: '0'
    });

    $('#txtEmail').change(function () {
        if (!(filter.test($('#txtEmail').val()))) {
            $('#txtEmail').attr('style', 'border:2px solid #7A4073');
            return false;
        }
        else {
            $('#txtEmail').css("border", "1px solid #D7D7D7");
        }
        //return ReturnVal;
    });

    $('#btnPasswordReset').click(function () {
        ResetPassword(selectedOfficerID, 2);
    });
});

function GetClientName() {
    var CName = '<option value="">Broxtowe Borough Council</option>' +
'<option value="">Amber Valley Borough Council</option>' +
'<option value="">HMCTS Greater Manchester</option>' +
'<option value="">Nottinghamshire County Council</option>' +
'<option value="">HMCTS Leicester (Warrant)</option>' +
'<option value="">Shepway Council</option>' +
'<option value="">Hammersmith & Fulham</option>' +
'<option value="">HMCTS Merseyside Confiscation Orders</option>' +
'<option value="">Sandwell Council</option>' +
'<option value="">Maidstone Council - new contract</option>' +
'<option value="">HMCTS Northumbria Central Enforcement (Warrant)</option>' +
'<option value="">Nottinghamshire County Council Bus Lane</option>' +
'<option value="">London Borough of Hackney</option>' +
'<option value="">Swindon Borough Council</option>' +
'<option value="">HMCTS Cambridgeshire</option>' +
'<option value="">HMCTS West Mercia (Warrant)</option>' +
'<option value="">Lancashire County Council 2nd Referral</option>' +
'<option value="">HMCTS London NW (Warrant)</option>' +
'<option value="">Gedling Borough Council</option>' +
'<option value="">HMCTS London SW (Warrant)</option>' +
'<option value="">Manchester City Council 2nd Ref</option>' +
'<option value="">Medway Council</option>' +
'<option value="">HMCTS South Yorkshire 035</option>' +
'<option value="">London Borough of Lambeth</option>' +
'<option value="">HMCTS Avon & Somerset</option>' +
'<option value="">Wiltshire Council</option>' +
'<option value="">HMCTS Derbyshire (Warrant)</option>' +
'<option value="">Ipswich Council</option>' +
'<option value="">HMCTS West Yorkshire (Warrant)</option>' +
'<option value="">HMCTS North West London 123</option>' +
'<option value="">Newcastle City Council - TMA</option>' +
'<option value="">Bournemouth Borough Council</option>' +
'<option value="">Kirklees Council Parking TMA</option>' +
'<option value="">HMCTS Sussex</option>' +
'<option value="">Basingstoke and Deane Borough Council</option>' +
'<option value="">Winchester City Council</option>' +
'<option value="">East Sussex County Council (Eastbourne)</option>' +
'<option value="">Basingstoke and Deane BC - 2nd</option>' +
'<option value="">Transport for London Traffic Enforcement</option>' +
'<option value="">Newark District Council</option>' +
'<option value="">Bournemouth Borough Council - 2nd</option>' +
'<option value="">HMCTS South West London 123</option>' +
'<option value="">Crawley Borough Council</option>' +
'<option value="">Chichester District Council - New</option>' +
'<option value="">Mid Sussex Council 2nd ref</option>' +
'<option value="">Liverpool Council</option>' +
'<option value="">HMCTS Kent</option>' +
'<option value="">Watford Borough Council - New</option>' +
'<option value="">Chesterfield Borough Council</option>' +
'<option value="">Southend-on-Sea Borough Council</option>' +
'<option value="">Essex County Council (B)</option>' +
'<option value="">HMCTS Gwent</option>' +
'<option value="">HMCTS Hampshire & I.O.W</option>' +
'<option value="">Havant Borough Council</option>' +
'<option value="">HMCTS Swindon</option>' +
'<option value="">London Borough of Barnet - 2</option>' +
'<option value="">Essex County Council (U)</option>' +
'<option value="">London Borough of Merton 2</option>' +
'<option value="">Luton Borough Council</option>' +
'<option value="">HMCTS Gloucester</option>' +
'<option value="">Lincolnshire County Council</option>' +
'<option value="">North Lincolnshire Council 2nd ref</option>' +
'<option value="">HMCTS Nottingham (Warrant)</option>' +
'<option value="">HMCTS Surrey</option>' +
'<option value="">Allerdale Borough Council</option>' +
'<option value="">New Forest District Council</option>' +
'<option value="">HMCTS Liverpool</option>' +
'<option value="">HMCTS Bedfordshire</option>' +
'<option value="">Highways England</option>' +
'<option value="">Eastleigh Council</option>' +
'<option value="">North Lincolnshire Council</option>' +
'<option value="">HMCTS North Yorkshire (Warrant)</option>' +
'<option value="">Test Valley Borough Council</option>' +
'<option value="">Reigate and Banstead Council SC</option>' +
'<option value="">HMCTS Cleveland Enforcement unit (Warrant)</option>' +
'<option value="">Mansfield Borough Council</option>' +
'<option value="">London Borough of Southwark</option>' +
'<option value="">Erewash Borough Council</option>' +
'<option value="">Newcastle City Council Bus Lane</option>' +
'<option value="">Ashford Borough Council</option>' +
'<option value="">Borough of Poole - New</option>' +
'<option value="">Swindon Bus Lanes</option>' +
'<option value="">HMCTS North West Region Confiscation Orders</option>' +
'<option value="">Manchester City Council</option>' +
'<option value="">HMCTS Southend</option>' +
'<option value="">Sheffield City Council</option>' +
'<option value="">HMCTS Dyfed Powys</option>' +
'<option value="">South Tyneside Council</option>' +
'<option value="">Bournemouth BC Bus Lane 2nd</option>' +
'<option value="">HMCTS Humber 035</option>' +
'<option value="">High Peak Borough Council</option>' +
'<option value="">Cornwall Council - New</option>' +
'<option value="">London Borough of Barking & Dagenham</option>' +
'<option value="">Scarborough Council</option>' +
'<option value="">HMCTS London SE & C (Warrant)</option>' +
'<option value="">Derbyshire County Council</option>' +
'<option value="">HMCTS Warwickshire (Warrant)</option>' +
'<option value="">HMCTS Durham Central Enforcement unit (Warrant)</option>' +
'<option value="">Leeds City Council TMA</option>' +
'<option value="">HMCTS Hertfordshire</option>' +
'<option value="">London Borough of Waltham Forest</option>' +
'<option value="">HMCTS Northumbria 555</option>' +
'<option value="">Westminster City Council</option>' +
'<option value="">Gravesham Borough Council</option>' +
'<option value="">London Borough of Enfield - New</option>' +
'<option value="">East Sussex County Council  (Lewes)</option>' +
'<option value="">HMCTS Devon & Cornwall</option>' +
'<option value="">HMCTS Lincolnshire (Warrant)</option>' +
'<option value="">East Sussex County Council (Hastings)</option>' +
'<option value="">Transport for London CC</option>' +
'<option value="">Reigate and Banstead Council</option>' +
'<option value="">London Borough of Hillingdon</option>' +
'<option value="">West Lindsey District Council</option>' +
'<option value="">Tandridge District Council</option>' +
'<option value="">HMCTS Cheshire Div 1 North</option>' +
'<option value="">London Borough of Merton</option>' +
'<option value="">Rushmoor Borough Council</option>' +
'<option value="">Bassetlaw District Council</option>' +
'<option value="">London Borough of Barnet</option>' +
'<option value="">Woking Borough Council</option>' +
'<option value="">HMCTS Cumbria</option>' +
'<option value="">Royal Borough of Greenwich</option>' +
'<option value="">HMCTS West Midlands (Warrant)</option>' +
'<option value="">HMCTS Poole</option>' +
'<option value="">London Borough of Southwark (SC)</option>' +
'<option value="">Royal Borough of Kensington and Chelsea</option>' +
'<option value="">Newcastle City Council  TMA 2nd</option>' +
'<option value="">HMCTS North Wales</option>' +
'<option value="">Transport for London LEZ</option>' +
'<option value="">Bournemouth BC Bus Lane</option>' +
'<option value="">Essex County Council (HL)</option>' +
'<option value="">Dacorum Borough Council - New</option>' +
'<option value="">Essex County Council (E)</option>' +
'<option value="">HMCTS Staffordshire (Warrant)</option>' +
'<option value="">HMCTS Thames Valley (O)</option>' +
'<option value="">City of London Corporation</option>' +
'<option value="">Tandridge District Council SC</option>' +
'<option value="">London Borough of Islington - New IZ Prefix</option>' +
'<option value="">Tonbridge & Malling Borough Council</option>' +
'<option value="">HMCTS Lancashire</option>' +
'<option value="">Swale Borough Council - new contract</option>' +
'<option value="">East Hampshire District Council</option>' +
'<option value="">Slough Borough Council TMA</option>' +
'<option value="">East Lindsey Council</option>' +
'<option value="">HMCTS Norfolk</option>' +
'<option value="">HMCTS Suffolk</option>' +
'<option value="">HMCTS South East & Central London 123</option>' +
'<option value="">HMCTS London NE (Warrant)</option>' +
'<option value="">Canterbury City Council</option>' +
'<option value="">Portsmouth City Council</option>' +
'<option value="">Kirklees Metropolitan Council Parking (Parking TCE)</option>' +
'<option value="">London Borough of Camden - CU New</option>' +
'<option value="">Dover District Council</option>' +
'<option value="">HMCTS Sheffield (Warrant)</option>' +
'<option value="">HMCTS North East London 123</option>' +
'<option value="">Brentwood Council NEW</option>' +
'<option value="">Essex County Council (C)</option>' +
'<option value="">HMCTS Kingston upon Hull (Warrant)</option>' +
'<option value="">Essex Council Council (T)</option>' +
'<option value="">Peterborough City Council</option>' +
'<option value="">Derbyshire Dales District Council</option>' +
'<option value="">HMCTS South Wales</option>' +
'<option value="">Three Rivers District Council - New</option>' +
'<option value="">London Borough of Islington - New</option>' +
'<option value="">Tendring District Council</option>' +
'<option value="">London Borough of Waltham Forest 2</option>' +
'<option value="">Leeds TMA Bus Lanes</option>' +
'<option value="">Milton Keynes Council</option>' +
'<option value="">Cardiff Council</option>' +
'<option value="">HMCTS Northamptonshire (Warrant)</option>' +
'<option value="">Mid Sussex Council</option>' +
'<option value="">Stockport Borough Council TMA</option>';

    $('#SelClientName,#SelClientNameForm').html(CName);
}

function GetCaseType() {
    var CType = '<option value="">Arrest Breach No Bail</option>' +
'<option value="">Magistrates Distress (inc. Inland Revenue)</option>' +
'<option value="">Arrest Section 136</option>' +
'<option value="">Arrest Bail</option>' +
'<option value="">RTA/TMA (RoadTraffic/Traffic Management)</option>' +
'<option value="">Arrest No Bail</option>' +
'<option value="">Arrest Breach Bail</option>' +
'<option value="">Arrest Committal</option>';
    $('#SelCaseType,#SelCaseTypeForm').html(CType);
}

function GetManagers() {
    $.ajax({
        type: "GET",
        url: serviceUrl + "/GetManagerDetailsAdmin?ManagerID=" + mID + "&" + getCookie('BrowserId'), // Location of the service
        contentType: "application/javascript", // content type sent to server
        dataType: "jsonp", //Expected data format from server       
        processdata: false, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
            $('#selManager').empty();
        },
        complete: function () { },
        success: function (msg) {//On Successfull service call  
            var result = JSON.parse(msg);
            var ManagerList = '<option value="0">Select</option>';
            $.each(result, function (key, value) {
                if (key == 0)
                    GetADMList(0);

                ManagerList = ManagerList + '<option value="' + $.trim(value.OfficerID) + '">' + value.OfficerName + '</option>';
            });
            $('#selManager').html(ManagerList);
            $('#selADM').html('<option value="0">Select</option>');
            $('#selManager').change(function () {
                GetADMList($('#selManager').val());
            });
        },
        error: function () { }
    });
}

function InsertManagerGroupOfficer() {
    $.ajax({
        type: "GET",
        url: serviceUrl + "/InsertManagerGroupOfficer?Name=" + $.trim($('#txtName').val() + '|0') + "&ManagerID=" + $('#selManager').val() + "&" + getCookie('BrowserId'), // Location of the service
        contentType: "application/javascript", // content type sent to server
        dataType: "jsonp", //Expected data format from server       
        processdata: false, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
        },
        complete: function () {
            GetOfficerListforGridView();
        },
        success: function (msg) {//On Successfull service call  
            var result = JSON.parse(msg);
            $.each(result, function (key, value) {
                if (value.Result == 'Success') {
                    $('#lblErrMsg').html('');
                    $('#DAddDialog').dialog("close");
                    if (TreeLevel == 2)
                        $('#DAlert').empty().attr('class', 'alert alert-success').append('<strong>Info!</strong> Case guidance added successfully').fadeIn(1).fadeOut(6000);
                    else
                        $('#DAlert').empty().attr('class', 'alert alert-success').append('<strong>Info!</strong> Officer added successfully').fadeIn(1).fadeOut(6000);
                }
                else if (value.Result == 'AlreadyExists') {
                    $('#lblErrMsg').html('Officer ID already exists.');
                    returntype = false;
                }
            });
        },
        error: function () {
        }
    });

}

function AlertMessage(Title) {
    $('#DAddDialog').dialog({
        modal: true,
        resizable: false,
        width: "65%",
        "title": Title,
        buttons: {
            "Add": function () {
                InsertManagerGroupOfficer();
            }
        }
    })
}

function GetCaseGuidanceList() {
    //$.ajax({
    //    type: "GET",
    //    url: serviceUrl + "/GetOfficerListforGridView?OfficerID=" + $.trim($('#SelClientName').val()) + "&OfficerName=" + $.trim($('#SelCaseType').val()) + "&" + getCookie('BrowserId'), // Location of the service
    //    contentType: "application/javascript", // content type sent to server
    //    dataType: "jsonp", //Expected data format from server       
    //    processdata: false, //True or False      
    //    async: false,
    //    timeout: 20000,
    //    beforeSend: function () {
    //        $.loader({
    //            className: "blue-with-image",
    //            content: ''
    //        });
    //    },
    //    complete: function () {
    //        $.loader('close');
    //    },
    //    success: function (msg) {//On Successfull service call  
    //        $('#tblGridView').dataTable().fnClearTable();
    //        var result = JSON.parse(msg);
    //        var Role;
    //        if (result != '') {
    //            $.each(result, function (key, value) {
    //                Role = (value.RoleType == null || value.RoleType == '' ? 'Select' : value.RoleType);
    //                $('#tbodyGridView').append('<tr><td>' + value.OfficerID + '</td>' +
    //                           '<td>' + value.OfficerName +
    //                           '<br/> IMEI # : ' + value.IMEINumber + '<br/>' +
    //                           'Device : ' + value.DeviceName + '</td>' +
    //                           '<td class="tdAdmName">' + value.GroupName + '</td>' +
    //                           '<td>' + value.ManagerName + '</td>' +
    //                           '<td>' + ($('input[name=AppUser]:checked').val() == 1 ? value.RegisteredDate : value.DisabledDate) + '</td>' +
    //                           '<td>' + value.Email + '</td>' +
    //                           '<td>' + value.Mobile + '</td>' +
    //                           '<td class="tdLastLoginDate">' + value.LastloginDate + '</td>' +
    //                           '<td>' +
    //                           '<img class="imgCaseAction" src="images/hd-CaseActions-ico.png" alt="Case" title="Case Action" style="cursor:pointer; width:24px;padding-right:5px;" onclick="GetCaseAction(' + $.trim(value.OfficerID) + ',' + "'" + value.LastActionDate + "'" + ',' + "'" + value.OfficerName + "'" + ')" />' +
    //                           '<img class="imgAll" src="images/Audit_32_32.png" alt="Audit" title="Audit Trail" style="cursor:pointer;padding-right:5px;" onclick="GetAuditTrail(' + $.trim(value.OfficerID) + ')" />' +
    //                           '<img class="imgPrevious" src="images/hd-LastLocation-ico.png" alt="Last Location" title="Last Location" style="cursor:pointer;width:24px;padding-right:5px;" onclick="MapFillLastKnownLocation(' + $.trim(value.OfficerID) + ',' + "'" + value.LastActionDate + "'" + ')" />' +
    //                           '<img class="imgView" src="images/View_32_32.png" alt="Edit" title="View" style="cursor:pointer;padding-right:5px;display:none;" onclick="UpdateUser(' + $.trim(value.OfficerID) + ',2' + ')"/> ' +
    //                           '<img class="imgEdit" src="images/Edit_32_32.png" alt="Edit" title="Edit" style="cursor:pointer;padding-right:5px;" onclick="UpdateUser(' + $.trim(value.OfficerID) + ',1' + ')"/> ' +
    //                           '<img class="imgUnRegister" src="images/Delete_32_32.png" alt="UnRegister" title="Un Register" style="cursor:pointer;padding-right:5px;" onclick="UnRegister(' + $.trim(value.OfficerID) + ',' + "'" + value.OfficerName + "'" + ',' + "'" + Role + "'" + ',' + (value.OfficerCount == undefined ? 0 : value.OfficerCount) + ')" /></td>' +
    //                           '<td style="display:none">' + value.EmpType + '</td>' +
    //                           '<td style="display:none">' + value.Teamcode + '</td>' +
    //                           '<td style="display:none">' + value.OfficerRole + '</td>' +
    //                           '<td style="display:none">' + value.Certificated + '</td>' +
    //                           '<td style="display:none">' + value.Postcode + '</td>' +
    //                           '<td style="display:none">' + value.Teamleader + '</td>' +
    //                           '<td style="display:none">' + value.IsValidatedActions + '</td>' +
    //                           '<td style="display:none">' + value.RoleType + '</td>' +
    //                           '<td style="display:none">' + value.Password + '</td>' +
    //                           '<td style="display:none">' + value.CompanyID + '</td>' +
    //                           '<td style="display:none">' + value.OfficerName + '</td>' +
    //                           '<td style="display:none">' + value.Note + '</td>' +
    //                           '<td style="display:none">' + value.ANPR + '</td>' +
    //                           '</tr>');
    //            });

    //            $('#tblGridView').dataTable({
    //                "sScrollY": "auto",
    //                "bPaginate": true,
    //                "bDestroy": true,
    //                "bSort": false,
    //                "sPaginationType": "full_numbers",
    //                "bLengthChange": false,
    //                "bFilter": false,
    //                "sInfo": true,
    //                "iDisplayLength": 5
    //            });

    //            $("#tblGridView").delegate("tbody tr", "click", function (key, event) {
    //                selectedOfficerID = this.cells[0].innerHTML;
    //                $('#txtID').val(this.cells[0].innerHTML).attr('disabled', true);
    //                $('#txtName').val(this.cells[19].innerHTML);
    //                $('#selManager option:contains(' + (this.cells[3].innerHTML == '' || this.cells[3].innerHTML == 'null' ? 'Select' : this.cells[3].innerHTML) + ')').prop("selected", "selected");
    //                GetADMList($('#selManager').val());

    //                $('#txtNote').val(this.cells[20].innerHTML);
    //                $('#txtEmail').val(this.cells[5].innerHTML);
    //                $('#txtMobile').val($.trim(this.cells[6].innerHTML));
    //                : 7 -image (Not a ID)
    //               OfficerType = (this.cells[9].innerHTML == '' || this.cells[9].innerHTML == 'null' || this.cells[9].innerHTML == null ? 'Select' : this.cells[9].innerHTML);
    //                OfficerRole = (this.cells[11].innerHTML == '' || this.cells[11].innerHTML == 'null' || this.cells[11].innerHTML == null ? 'Select' : this.cells[11].innerHTML);
    //                Certificate = (this.cells[12].innerHTML == '' || this.cells[12].innerHTML == 'null' || this.cells[12].innerHTML == null ? 'Select' : this.cells[12].innerHTML);
    //                Role = (this.cells[16].innerHTML == '' || this.cells[16].innerHTML == 'null' || this.cells[16].innerHTML == null ? 'Select' : this.cells[16].innerHTML);
    //                $('#selOffType option:contains(' + OfficerType + ')').prop("selected", "selected");//Employee type : Type
    //                $('#txtTeamcode').val(this.cells[10].innerHTML);// Team code
    //                $('#SelOfficerrole option:contains(' + OfficerRole + ')').prop("selected", "selected");//Officer role
    //                $('#SelCertificated option:contains(' + Certificate + ')').prop("selected", "selected");//Certificated
    //                $('#txtPostcode').val(this.cells[13].innerHTML);//Postcode
    //                $('#SelTeamleader option:contains(' + (this.cells[14].innerHTML == '' || this.cells[14].innerHTML == 'null' ? 'Select' : this.cells[14].innerHTML) + ')').prop("selected", "selected");
    //                $('#SelValidatedActions option:contains(' + (this.cells[15].innerHTML == '' || this.cells[15].innerHTML == 'null' ? 'Select' : this.cells[15].innerHTML == 'true' ? 'Yes' : 'No') + ')').prop("selected", "selected");//Accepted actions
    //                $('#selRole option:contains(' + Role + ')').prop("selected", "selected");
    //                $('#txtPwd').val(this.cells[17].innerHTML).attr('type', 'password');//Password
    //                $('#selCompany').val((this.cells[18].innerHTML == '' || this.cells[18].innerHTML == 'null' ? 0 : this.cells[18].innerHTML)).attr("selected", "selected");
    //                selectedADM = this.cells[2].innerHTML;
    //                if (this.cells[21].innerHTML == 'true')
    //                    $("#chkANPR").prop('checked', 'checked');//.is(':checked');
    //                else
    //                    $("#chkANPR").prop('checked', '');//.is(':not(:checked)');
    //                ANPR=" + ($("#chkANPR").is(':checked') == true ? 1 : 0)
    //                $('#txtName,#txtID,#txtPwd,#selCompany,#selOffType,#selRole,#SelTeamleader,#SelCertificated,#SelOfficerrole,#SelValidatedActions,#txtEmail').css("border", "1px solid #D7D7D7");
    //            });
    //        }
    //        else {
    //            $('#tblGridView').dataTable({
    //                "sScrollY": "auto",
    //                "bPaginate": false,
    //                "bDestroy": true,
    //                "bSort": false,
    //                "bLengthChange": false,
    //                "bFilter": false,
    //                "bInfo": false,
    //                "iDisplayLength": 1
    //            });
    //        }
    //    },
    //    error: function () {
    //    }
    //});


    //$('#tbodyGridView').empty();
    //$('#tbodyGridView').append(
    //    '<tr><td>1</td><td>Broxtowe Borough Council</td><td>Magistrates Distress (inc. Inland Revenue)</td><td>Sample guidance for Lorem ipsum dolor sit amet <a title="Sample guidance for Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at ante.Mauris eleifend, quam a vulputate dictum, massa quam dapibus leo, eget vulputate orci purus ut lorem. In fringilla mi in ligula. Pellentesque aliquam quam vel dolor. Nunc adipiscing. Sed quam odio, tempus ac, aliquam molestie, varius ac, tellus." style="cursor:pointer"><b>...</b></a>' +
    //    '</td><td><img src="images/Edit_32_32.png" alt="Edit" title="Edit" style="cursor:pointer; padding-right:10px;padding-left:15px;" onclick="UpdateUser()"/> ' +
    //'<img src="images/Delete_32_32.png" alt="UnRegister" title="Un Register" style="cursor:pointer;" onclick="UnRegister(1)" /></td></tr>' +
    //'<tr><td>2</td><td>HMCTS Greater Manchester</td><td>Arrest Breach No Bail</td><td>Vestibulum ut nulla aliquam risus rutrum interdum. Pellentesque lorem. </td> <td><img src="images/Edit_32_32.png" alt="Edit" title="Edit" style="cursor:pointer; padding-right:10px;padding-left:15px;" onclick="UpdateUser()"/> ' +
    //'<img src="images/Delete_32_32.png" alt="UnRegister" title="Un Register" style="cursor:pointer;" onclick="UnRegister(2)" /></td></tr>' +
    //'<tr><td>3</td><td>Nottinghamshire County Council</td><td>Arrest Section 136</td><td>Sed eget turpis a pede tempor malesuada. Vivamus <a title="Sed eget turpis a pede tempor malesuada. Vivamus quis mi at leo pulvinar hendrerit. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Pellentesque aliquet lacus vitae pede. Nullam mollis dolor ac nisi. Phasellus sit amet urna. Praesent pellentesque sapien sed lacus " style="cursor:pointer"><b>...</b></a></td><td><img src="images/Edit_32_32.png" alt="Edit" title="Edit" style="cursor:pointer; padding-right:10px;padding-left:15px; onclick="UpdateUser()""/> ' +
    //'<img src="images/Delete_32_32.png" alt="UnRegister" title="Un Register" style="cursor:pointer;" onclick="UnRegister(3)" /></td></tr>' +
    //'<tr><td>4</td><td>Shepway Council</td><td>Magistrates Distress (inc. Inland Revenue)</td><td>Proin nec nibh. Duis eu dui quis nunc sagittis lobortis.</td> <td><img src="images/Edit_32_32.png" alt="Edit" title="Edit" style="cursor:pointer; padding-right:10px;padding-left:15px;" onclick="UpdateUser()"/> ' +
    //'<img src="images/Delete_32_32.png" alt="UnRegister" title="Un Register" style="cursor:pointer;" onclick="UnRegister(4)" /></td></tr>' +
    //'<tr><td>5</td><td>HMCTS Merseyside Confiscation Orders</td><td>Arrest Breach No Bail</td><td>Donec lacinia odio in odio. In sit amet elit. <a title=" Donec lacinia odio in odio. In sit amet elit. Maecenas gravida interdum urna. Integer pretium, arcu vitae imperdiet facilisis, elit tellus tempor nisi, vel feugiat ante velit sit amet mauris. Vivamus arcu. Integer pharetra magna ac lacus. Aliquam vitae sapien in nibh vehicula auctor. Suspendisse leo mauris, pulvinar sed, tempor et, consequat ac, lacus. Proin velit. Nulla semper lobortis mauris. Duis urna erat, ornare et, imperdiet eu, suscipit sit amet, massa. Nulla nulla nisi, pellentesque at, egestas quis, fringilla eu, diam. " style="cursor:pointer"><b>...</b></a></td><td><img src="images/Edit_32_32.png" alt="Edit" title="Edit" style="cursor:pointer; padding-right:10px;padding-left:15px;" onclick="UpdateUser()"/> ' +
    //'<img src="images/Delete_32_32.png" alt="UnRegister" title="Un Register" style="cursor:pointer;" onclick="UnRegister(5)" /></td></tr>' +
    //'<tr><td>6</td><td>Maidstone Council - new contract</td><td>Arrest Committal</td><td>Proin nec nibh. Duis eu dui quis nunc sagittis lobortis.</td> <td><img src="images/Edit_32_32.png" alt="Edit" title="Edit" style="cursor:pointer; padding-right:10px;padding-left:15px;" onclick="UpdateUser()"/> ' +
    //'<img src="images/Delete_32_32.png" alt="UnRegister" title="Un Register" style="cursor:pointer;" onclick="UnRegister(6)" /></td></tr>' +
    //'<tr><td>7</td><td>London Borough of Hackney</td><td>Arrest Breach Bail</td><td>Curabitur sit amet erat quis risus feugiat viverra.</td><td><img src="images/Edit_32_32.png" alt="Edit" title="Edit" style="cursor:pointer; padding-right:10px;padding-left:15px;" onclick="UpdateUser()"/> ' +
    //'<img src="images/Delete_32_32.png" alt="UnRegister" title="Un Register" style="cursor:pointer;" onclick="UnRegister(7)" /></td></tr>' +
    //'<tr><td>8</td><td>HMCTS Cambridgeshire</td><td>Arrest No Bail</td><td> Donec lacinia odio in odio</td><td><img src="images/Edit_32_32.png" alt="Edit" title="Edit" style="cursor:pointer; padding-right:10px;padding-left:15px;" onclick="UpdateUser()"/> ' +
    //'<img src="images/Delete_32_32.png" alt="UnRegister" title="Un Register" style="cursor:pointer;" onclick="UnRegister(8)" /></td></tr>' +
    //'<tr><td>9</td><td>Lancashire County Council 2nd Referral</td><td>Arrest Breach No Bail</td><td> Proin nec nibh. Duis eu dui quis nunc sagittis lobortis.</td><td><img src="images/Edit_32_32.png" alt="Edit" title="Edit" style="cursor:pointer; padding-right:10px;padding-left:15px;" onclick="UpdateUser()"/> ' +
    //'<img src="images/Delete_32_32.png" alt="UnRegister" title="Un Register" style="cursor:pointer;" onclick="UnRegister(9)" /></td></tr>' +
    //'<tr><td>10</td><td>HMCTS London NW (Warrant)</td><td>Arrest No Bail</td><td>Aliquam suscipit, lacus a iaculis adipiscing <a title="  Aliquam suscipit, lacus a iaculis adipiscing, eros orci pellentesque nisl, non pharetra dolor urna nec dolor. Integer cursus dolor vel magna. Integer ultrices feugiat sem. Proin nec nibh. Duis eu dui quis nunc sagittis lobortis. Fusce pharetra, enim ut sodales luctus, lectus arcu rhoncus purus, in fringilla augue elit vel lacus." style="cursor:pointer"><b>...</b></a></td><td><img src="images/Edit_32_32.png" alt="Edit" title="Edit" style="cursor:pointer; padding-right:10px;padding-left:15px;" onclick="UpdateUser()"/> ' +
    //'<img src="images/Delete_32_32.png" alt="UnRegister" title="Un Register" style="cursor:pointer;" onclick="UnRegister(10)" /></td></tr>' +
    //'<tr><td>11</td><td>Gedling Borough Council</td><td>RTA/TMA (RoadTraffic/Traffic Management)</td><td>Curabitur sit amet erat quis risus feugiat viverra.</td><td><img src="images/Edit_32_32.png" alt="Edit" title="Edit" style="cursor:pointer; padding-right:10px;padding-left:15px;"/> ' +
    //'<img src="images/Delete_32_32.png" alt="UnRegister" title="Un Register" style="cursor:pointer;" onclick="UnRegister(11)" /></td></tr>' +
    //  '<tr><td>12</td><td>HMCTS London SW (Warrant)</td><td>RTA/TMA (RoadTraffic/Traffic Management)</td><td> Donec lacinia odio in odio</td><td><img src="images/Edit_32_32.png" alt="Edit" title="Edit" style="cursor:pointer; padding-right:10px;padding-left:15px;"/> ' +
    //'<img src="images/Delete_32_32.png" alt="UnRegister" title="Un Register" style="cursor:pointer;" /></td></tr>' +
    //'<tr><td>13</td><td>Manchester City Council 2nd Ref</td><td>Arrest Section 136</td><td> Nulla semper lobortis mauris</td>In hac habitasse platea dictumst. Aliquam erat <a title=" In hac habitasse platea dictumst. Aliquam erat volutpat. Fusce iaculis elit id tellus. Ut accumsan malesuada turpis. Suspendisse potenti. Vestibulum lacus augue, lobortis mattis, laoreet in, varius at, nisi. Nunc gravida. Phasellus faucibus. In hac habitasse platea dictumst. Integer tempor lacus eget lectus. Praesent fringilla augue fringilla dui. " style="cursor:pointer"><b>...</b></a><td><img src="images/Edit_32_32.png" alt="Edit" title="Edit" style="cursor:pointer; padding-right:10px;padding-left:15px;"/> ' +
    //'<img src="images/Delete_32_32.png" alt="UnRegister" title="Un Register" style="cursor:pointer;" /></td></tr>' +
    //  '<tr><td>14</td><td>Broxtowe Borough Council</td><td>RTA/TMA (RoadTraffic/Traffic Management)</td><td> Nulla semper lobortis mauris</td><td><img src="images/Edit_32_32.png" alt="Edit" title="Edit" style="cursor:pointer; padding-right:10px;padding-left:15px;"/> ' +
    //'<img src="images/Delete_32_32.png" alt="UnRegister" title="Un Register" style="cursor:pointer;" /></td></tr>' +
    //  '<tr><td>15</td><td>Manchester City Council 2nd Ref</td><td>Arrest Section 136</td><td> Donec lacinia odio in odio.</td><td><img src="images/Edit_32_32.png" alt="Edit" title="Edit" style="cursor:pointer; padding-right:10px;padding-left:15px;"/> ' +
    //'<img src="images/Delete_32_32.png" alt="UnRegister" title="Un Register" style="cursor:pointer;" /></td></tr>'
    //);

    //setTimeout(function () {
    //    $('#tblGridView').dataTable({
    //        "sScrollY": "auto",
    //        "bPaginate": true,
    //        "bDestroy": true,
    //        "bSort": false,
    //        "sPaginationType": "full_numbers",
    //        "aLengthMenu": [[5, 10, 50, 100, -1], [5, 10, 50, 100, "All"]],
    //        "bLengthChange": false,
    //        "bFilter": false,
    //        "sInfo": true,
    //        "iDisplayLength": 5
    //    });
    //}, 1500);


}

function UpdateUser() {
    $('#txtMessage').val('Sample guidance for Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at ante.Mauris eleifend, quam a vulputate dictum, massa quam dapibus leo, eget vulputate orci purus ut lorem. In fringilla mi in ligula. Pellentesque aliquam quam vel dolor. Nunc adipiscing. Sed quam odio, tempus ac, aliquam molestie, varius ac, tellus.');
    $('#DAddDialog').dialog({
        modal: true,
        resizable: false,
        width: "65%",
        "title": 'Update Case Guidance Details',
        buttons: {
            "Update": function () {
                $('#DAddDialog').dialog("close");
                $('#DAlert').empty().attr('class', 'alert alert-success').append('<strong>Info!</strong> Case guidance updated successfully').fadeIn(1).fadeOut(6000);
            }
        }
    });
}

function UnRegister(ClientID) {
    $(':text').val('');
    $('#DConfirmDialog').attr('title', 'Confirmation of blocking Case Guidance');
    var Client;
    switch (ClientID) {
        case 1:
            Client = "Broxtowe Borough Council";
            break;
        case 2:
            Client = "HMCTS Greater Manchester";
            break;
        case 3:
            Client = "Nottinghamshire County Council";
            break;
        case 4:
            Client = "Shepway Council";
            break;
        case 5:
            Client = "HMCTS Merseyside Confiscation Orders";
            break;
    }
    $('#bOfficerName').html(Client);
    $('#DConfirmDialog').dialog({
        modal: true,
        resizable: false,
        width: "30%",
        buttons: {
            "Yes": function () {
                $('#DConfirmDialog').dialog('close');
            },
            "No": function () {
                $('#DConfirmDialog').dialog('close');
            }
        }
    });
}
