﻿//==========================================================================================================================================
/* File Name: Highcourt_dataservice.js */
/* File Created: January 21, 2014 */
/* Created By  : R.Santhakumar */
//==========================================================================================================================================
//TODO : Sevice URL:

var RedirectUrl = "http://www.testoptimise.org:8196/testoam/Admin/admin-page.html";
var serviceUrl = "http://intranet.atom8itsolutions.com:8195/OAService/MarstonOfficeToolService.svc"; // FOR ATOM8 USE; IGNORE IT
//var serviceUrl = "https://test-optimise.marstongroup.co.uk:26443/OAServiceV2/MarstonOfficeToolService.svc"; // For Marston UAT
//var serviceUrl = "https://secure.marstongroup.co.uk/Optimise/OAService/MarstonOfficeToolService.svc"; // For Marston LIVE
var HrUrl = "http://www.testoptimise.org:8196/testoam/Admin/HRView.html";
var VideoUrl = "http://www.testoptimise.org:8196/testoam/Intro.html";
//==========================================================================================================================================

/*****  START SERVICE CALLS *************************/

var Type;
var Url;
var Data;
var ContentType = "application/javascript";
var DataType;
var ProcessData;
var response;

var MTV = '';

var serviceTypeLogin;
var GlobalUserName;
var GlobalPassword;

var serviceTypeCaseSearch;
var serviceTypeCaseSearchDetail;
var serviceTypeSearchVRM;
var serviceTypeSearchVRMDetail;

var CaseNumber;
var IsManager = 0;
var LinkForManager = 0;
var CompanyID = 1;
var OffID;
var tableIndex = 1; // for officer status tables
var IntervalvalueOfficerStatus;
var MapIntervalStatus;
var MID, FD, TD, OID, GID;
var datacontent, datacontentforDisplay;

var center;
var options;
var markers = [];
var map;
var DMap;
var latLng;
var marker;
var markerCluster;
var infowindow;
var mcOptions;
var Latitude = [];
var Longitude = [];
var Icon = [];
var Content = [];
var pLatitude = 0;
var pLongitude = 0;
var pContentkey;
var pLatMap = 0;
var pLongMap = 0;


//==========================================================================================================================================
//TODO : Login function

function Login(officerId, password) {
    Type = "GET";
    serviceTypeLogin = "login";
    if (officerId != "" && password != "") {
        var inputParams = "/LoginOfficer?officerID=" + officerId + "&password=" + password + "&" + getCookie('BrowserId');
        Url = serviceUrl + inputParams;
        DataType = "jsonp"; ProcessData = false;
        CallService();
    }
}

//Generic function to call WCF Service for GET
function CallService() {
    var isGridProcessing = (serviceTypeCaseSearchDetail == 'getcasedetails') ? true : false;
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () { },
        complete: function () { },
        success: function (msg) {//On Successfull service call 
            DataServiceSucceeded(msg);
        },
        error: function () {
        } // When Service call fails
    });
}

//Result handler.
function DataServiceSucceeded(result) {
    if (DataType == "jsonp") {
        if (result != undefined) {
            if (serviceTypeLogin != undefined && serviceTypeLogin == "login") {
                serviceTypeLogin = undefined;
                if (result != undefined) {
                    if (result != "[]" && result != null) {
                        result = JSON.parse(result);
                        $.each(result, function (key, value) {
                            //window.name = value.OfficerID + "|" + value.OfficerName;
                            setCookie('OfficerID', $.trim(value.OfficerID.toString()), 1);
                            setCookie('OfficerName', $.trim(value.OfficerName), 1);
                            setCookie('CompanyID', $.trim(value.CompanyID), 1);
                            setCookie('tgVal', $.trim(value.TargetNo), 1);
                            setCookie('aref', $.trim(value.AutoRefresh), 1);
                            setCookie('RoleType', $.trim(value.RoleType), 1);

                            if (value.RoleType == 6) {
                                setCookie('bc', 1, 1);
                                setCookie('IsVedio', 1, 1);
                                window.top.location.href = HrUrl;// "HRView.html";
                            }
                            else if (value.RoleType == 5) {
                                setCookie('bc', 1, 1);
                                setCookie('IsVedio', 1, 1);
                                window.top.location.href = RedirectUrl;
                            }
                            else if (value.RoleType == 7) {
                                setCookie('IsVedio', 1, 1);
                                if (value.CompanyID == 1)
                                    window.top.location = "index.html";
                                else
                                    window.top.location = "index.html";
                            }
                            else {
                                setCookie('bc', 1, 1);
                                setCookie('IsVedio', 1, 1);
                                if (value.CompanyID == 1)
                                    window.top.location = "index.html";
                                else
                                    window.top.location = "index.html";
                            }
                        });
                    }
                    else {
                        $('#lblUNPWDWarningInfo').html("Officer ID and Password didnot match.");
                    }
                }
            }
        }
    }
}

function LoginDetails4SuperUser(companyid, superuser) {
    if (companyid == 2) {
        $('#DClient').hide();
        setCookie('ClientId', 0, 1);
        setCookie('ClientName', '', 1);
        $('#pClientName').html('');
    }
    Type = "GET";
    serviceTypeLogin = "getcaseactions";
    var inputParams = "/LoginDetails4SuperUser?CompanyID=" + companyid + "&SuperUserName=" + superuser + "&" + getCookie('BrowserId');
    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
        },
        complete: function () {
            if (companyid == 1)
                $('#lblClientSearch').show();
            else
                $('#lblClientSearch').hide();
        },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {
                    result = JSON.parse(result);
                    $.each(result, function (key, value) {
                        setCookie('OfficerID', $.trim(value.OfficerID.toString()), 1);
                        setCookie('OfficerName', $.trim(value.OfficerName), 1);
                        setCookie('CompanyID', $.trim(value.CompanyID), 1);
                        setCookie('tgVal', $.trim(value.TargetNo), 1);
                        setCookie('aref', $.trim(value.AutoRefresh), 1);
                        setCookie('RoleType', $.trim(value.RoleType), 1);

                        if (value.RoleType == 6) {
                            setCookie('bc', 1, 1);
                            setCookie('IsVedio', 1, 1);
                            window.top.location.href = HrUrl;// "HRView.html";
                        }
                        else if (value.RoleType == 5) {
                            setCookie('bc', 1, 1);
                            setCookie('IsVedio', 1, 1);
                            window.top.location.href = RedirectUrl;
                        }
                        else if (value.RoleType == 7) {
                            if (value.CompanyID == 1)
                                window.top.location = "index.html";
                            else
                                window.top.location = "index.html";
                        }
                        else {
                            setCookie('bc', 1, 1);
                            setCookie('IsVedio', 1, 1);
                            if (value.CompanyID == 1)
                                window.top.location = "index.html";
                            else
                                window.top.location = "index.html";
                        }
                    });
                }
            }
        },
        error: function () {
        } // When Service call fails
    });
}

function introvideo() {
    var params = {};
    params.type = 'iframe';
    params.src = VideoUrl;
    params.height = 550;
    params.width = 800;
    params.overflow_hidden = false;
    ws_lightbox.open(params);
    setCookie('IsVedio', 0, 1);
}

function GetCompany() {
    $('#selCompanyList').empty();
    $.ajax({
        type: "GET",
        url: serviceUrl + "/GetCompany", // Location of the service
        contentType: "application/javascript", // content type sent to server
        dataType: "jsonp", //Expected data format from server       
        processdata: false, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () { },
        complete: function () { },
        success: function (msg) {//On Successfull service call  
            var result = JSON.parse(msg);
            $.each(result, function (key, value) {
                $('#selCompanyList').append($('<option>').attr('value', $.trim(value.CompanyID)).html(value.CompanyName));
            });
            $('#selCompanyList').val(getCookie('CompanyID')).attr("selected", "selected");
        },
        error: function () {
        }
    });
}

//==========================================================================================================================================
//TODO : Tree view list and availability show

function GetOfficerManager(ManagerID, SelectedOfficerID, multiselect) {
    Type = "GET";
    serviceTypeLogin = "getofficermanager";
    var inputParams = "/GetOfficersForManager?ManagerID=" + ManagerID + "&" + getCookie('BrowserId');
    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
            $.loader({
                className: "blue-with-image",
                content: ' '
            });
        },
        complete: function () {
            setTimeout(function () {
                $.loader('close');
            }, 1000);
        },
        success: function (result) {//On Successfull service call 
            if (result != undefined) {

                if (result != "[]" && result != null) {
                    result = JSON.parse(result);

                    $("#ulOfficers").empty();
                    if (multiselect) {
                        $('#selOfficer').empty();
                        $('#tbodyOfficerList').empty();
                    }
                    $.each(result, function (key, value) {
                        if (value.COO == undefined) {
                            if ($('#Group' + value.GroupID).length == 0) {
                                $("#ulOfficers").append('<li><a href="#" id="Group' + value.GroupID + '">' +
                                               '<i class="glyphicon glyphicon-plus-sign"></i> ' + value.GroupName + '</a>' +
                                               '<ul id="UL' + value.GroupID + '"></ul></li>');
                                GetCaseActionsOfficerCount(1, value.ManagerID, 0, value.Manager);
                                GetWorkingHours(value.ManagerID);
                                $('#Group' + value.GroupID).click(function () {
                                    $('.lastLeaf').removeClass('lastLeaf');
                                    $(this).addClass('lastLeaf');
                                    LinkForManager = 1;
                                    GetCaseActionsOfficerCount(1, value.ManagerID, value.GroupID, value.Manager);
                                    setCookie('Map', '', 1);
                                    pLatitude = 0;
                                    pLongitude = 0;
                                    $('#lblCAOfficerName').html('');
                                    if (getCookie('CompanyID') == 1) {
                                        $('#tblWMASearchType').hide();
                                        $('#tblWMA').show();
                                        FillCaseActions(getCookie('OfficerID'), $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), '0', value.GroupID);
                                        FillCaseDetailsAction('0', $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), '0', value.GroupID);
                                        FillWMA('0', $('#txtFromDate').val(), $('#txtFromDate').val(), '0', value.GroupID);
                                        FillWMAMore('0', $('#txtFromDate').val(), $('#txtFromDate').val(), '0', value.GroupID);
                                    }
                                    else {
                                        FillCaseActions(getCookie('OfficerID'), $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), '0', value.GroupID);
                                        FillCaseDetailsAction('0', $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), '0', value.GroupID);
                                    }
                                    GetStatistics(getCookie('OfficerID'), 2);
                                    FillLastKnownLocation('0', $('#txtFromDateLocation').val(), $('#txtFromDateLocation').val(), '0', value.GroupID);
                                    FillRankings('0', $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Paid', value.GroupID);
                                    FillRankings('0', $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Returned', value.GroupID);
                                    FillRankings('0', $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Part Paid', value.GroupID);

                                    FillRankingsMore('0', $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Paid', value.GroupID);
                                    FillRankingsMore('0', $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Returned', value.GroupID);
                                    FillRankingsMore('0', $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Part Paid', value.GroupID);
                                    $('#AShowAll,#AShowEnlarge').hide();//,#APageEnlarge
                                });

                                if (multiselect) {
                                    $('#selOfficer').append($('<optgroup>').attr('label', value.Manager).attr('id', 'og' + value.ManagerID))
                                }
                            }

                            if (multiselect) {
                                $('#og' + value.ManagerID)
                                    .append($('<option>').attr('value', $.trim(value.OfficerID)).html(value.OfficerName))
                                $('#lblCurrentMonth').html($('#selCurrentMonth option:selected').text());

                                $('#lblCurrentMonth').show();
                                $('#selCurrentMonth').hide();
                                $('#lblMonthTotalTarget').show();
                                $('#txtMonthTotalTarget').hide();

                                $('#tbodyOfficerList').append($('<tr>')
                                    .append($('<td>').append($('<a class="targetNew">').attr('id', 'AOfficerTarget').html(value.OfficerName + ' ' + value.OfficerID))
                                    .append($('<label>').attr('id', 'lblOfficerID' + key).html(value.OfficerID).hide()))
                                    .append($('<td>').append($('<input>').attr('type', 'text').attr('id', 'txtOfficerTarget' + key).attr('style', 'color:black'))))

                                $('#txtOfficerTarget' + key).keydown(function (event) {
                                    if (event.shiftKey == true) {
                                        $('#txtOfficerTarget' + key).attr('title', 'Enter numeric values');
                                        return false;
                                    }
                                    if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190 || ((event.keyCode == 65 || event.keyCode == 67 || event.keyCode == 86) && event.ctrlKey == true)) {
                                        return true;
                                    }
                                    else {
                                        $('#txtOfficerTarget' + key).attr('title', 'Enter numeric values');
                                        return false;
                                    }
                                });

                                $('#txtOfficerTarget' + key).keyup(function (event) {
                                    var TTarget = 0;
                                    $('#tbodyOfficerList tr').each(function (key, value) {
                                        if ($(this).find('input').val() > 0) {
                                            TTarget += parseInt($(this).find('input').val());
                                        }
                                    });
                                    $('#lblTotalTarget').html(TTarget);
                                    if ($('#lblTotalTarget').html() != $('#lblMonthTotalTarget').html()) {
                                        $('#btnSubmitOfficerTotal').attr('disabled', 'disabled');
                                        $('#lblInfo').html('Total Target should match.');
                                        $("#lblInfo").show().delay(3000).fadeOut();
                                    }
                                    else {
                                        $('#btnSubmitOfficerTotal').removeAttr('disabled');
                                    }
                                });
                            }

                            $('#UL' + value.GroupID)
                              .append('<li id="LI' + $.trim(value.OfficerID) + '">' +//style="display: list-block;" 
                                      '<a style="cursor:pointer;"  id="A' + $.trim(value.OfficerID) + '" rel="popover" data-original-title=""  class="a1" data-html="true" data-popover="true">' +//class="a1"
                                      '<i class="glyphicon glyphicon-circle-arrow-right"></i> ' + value.OfficerName + '<span class="pull-right">' + value.OfficerID + '</span>' +
                                      '<span style="padding-left:20px"><img src="images/circle-active.png" id="img' + $.trim(value.OfficerID) + '"></span></a> </li>');

                            $(function () {
                                //****************Setting up data content for the popover *******************************
                                Type = "GET";
                                var inputParams = "/GetOfficerAvailability?OfficerID=" + value.OfficerID + "&" + getCookie('BrowserId');
                                Url = serviceUrl + inputParams;
                                DataType = "jsonp"; ProcessData = false;

                                $.ajax({
                                    type: Type,
                                    url: Url, // Location of the service
                                    contentType: ContentType, // content type sent to server
                                    dataType: DataType, //Expected data format from server       
                                    processdata: ProcessData, //True or False      
                                    async: true,
                                    timeout: 20000,
                                    beforeSend: function () { },
                                    complete: function () { },
                                    success: function (result) {//On Successfull service call  
                                        if (result != undefined) {
                                            if (result != "[]" && result != null) {
                                                result = JSON.parse(result);
                                                $('#DivAvailability').empty();
                                                $.each(result, function (key, value1) {

                                                    datacontent = '<table width="100%" class="display-status-table"><tr><td>' +
                                                      '<div class="block" style="margin:2px!important;padding:2px!important">' +
                                                      '<p class="block-heading">Availability</p><table class="table">' +
                                                      '<tbody><tr><td>Week</td><td>Current Week</td><td>Visits</td><td>Next Week</td></tr>' +
                                                      '<tr><td>Tue</td><td><img src="' + ((value1.Tuesday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Tuesday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Tuesday == 1) ? 'AM' : ((value1.Tuesday == 2) ? 'PM' : ((value1.Tuesday == 0) ? '' : ((value1.Tuesday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RTuesday + '</td><td><img src="' + ((value1.Tuesday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Tuesday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Tuesday1 == 1) ? 'AM' : ((value1.Tuesday1 == 2) ? 'PM' : ((value1.Tuesday1 == 0) ? '' : ((value1.Tuesday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                                      '<tr><td>Wed</td><td><img src="' + ((value1.Wednesday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Wednesday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Wednesday == 1) ? 'AM' : ((value1.Wednesday == 2) ? 'PM' : ((value1.Wednesday == 0) ? '' : ((value1.Wednesday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RWednesday + '</td><td><img src="' + ((value1.Wednesday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Wednesday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Wednesday1 == 1) ? 'AM' : ((value1.Wednesday1 == 2) ? 'PM' : ((value1.Wednesday1 == 0) ? '' : ((value1.Wednesday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                                      '<tr><td>Thr</td><td><img src="' + ((value1.Thursday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Thursday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Thursday == 1) ? 'AM' : ((value1.Thursday == 2) ? 'PM' : ((value1.Thursday == 0) ? '' : ((value1.Thursday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RThursday + '</td><td><img src="' + ((value1.Thursday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Thursday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Thursday1 == 1) ? 'AM' : ((value1.Thursday1 == 2) ? 'PM' : ((value1.Thursday1 == 0) ? '' : ((value1.Thursday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                                      '<tr><td>Fri</td><td><img src="' + ((value1.Friday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Friday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Friday == 1) ? 'AM' : ((value1.Friday == 2) ? 'PM' : ((value1.Friday == 0) ? '' : ((value1.Friday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RFriday + '</td><td><img src="' + ((value1.Friday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Friday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Friday1 == 1) ? 'AM' : ((value1.Friday1 == 2) ? 'PM' : ((value1.Friday1 == 0) ? '' : ((value1.Friday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                                     '<tr><td>Sat</td><td><img src="' + ((value1.Saturday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Saturday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Saturday == 1) ? 'AM' : ((value1.Saturday == 2) ? 'PM' : ((value1.Saturday == 0) ? '' : ((value1.Saturday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RSaturday + '</td><td><img src="' + ((value1.Saturday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Saturday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Saturday1 == 1) ? 'AM' : ((value1.Saturday1 == 2) ? 'PM' : ((value1.Saturday1 == 0) ? '' : ((value1.Saturday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                                     '<tr><td>Mon</td><td><img src="' + ((value1.Monday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Monday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Monday == 1) ? 'AM' : ((value1.Monday == 2) ? 'PM' : ((value1.Monday == 0) ? '' : ((value1.Monday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RMonday + '</td><td><img src="' + ((value1.Monday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Monday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Monday1 == 1) ? 'AM' : ((value1.Monday1 == 2) ? 'PM' : ((value1.Monday1 == 0) ? '' : ((value1.Monday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                                     '</tbody></table></div></td></tr></table>';

                                                    datacontentforDisplay = '<table class="table table-striped table-condensed ">' +
                                                     '<tbody><tr><td>Week</td><td>Current Week</td><td>Visits</td><td>Next Week</td></tr>' +
                                                     '<tr><td>Tue</td><td><img src="' + ((value1.Tuesday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Tuesday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Tuesday == 1) ? 'AM' : ((value1.Tuesday == 2) ? 'PM' : ((value1.Tuesday == 0) ? '' : ((value1.Tuesday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RTuesday + '</td><td><img src="' + ((value1.Tuesday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Tuesday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Tuesday1 == 1) ? 'AM' : ((value1.Tuesday1 == 2) ? 'PM' : ((value1.Tuesday1 == 0) ? '' : ((value1.Tuesday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                                     '<tr><td>Wed</td><td><img src="' + ((value1.Wednesday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Wednesday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Wednesday == 1) ? 'AM' : ((value1.Wednesday == 2) ? 'PM' : ((value1.Wednesday == 0) ? '' : ((value1.Wednesday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RWednesday + '</td><td><img src="' + ((value1.Wednesday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Wednesday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Wednesday1 == 1) ? 'AM' : ((value1.Wednesday1 == 2) ? 'PM' : ((value1.Wednesday1 == 0) ? '' : ((value1.Wednesday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                                     '<tr><td>Thr</td><td><img src="' + ((value1.Thursday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Thursday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Thursday == 1) ? 'AM' : ((value1.Thursday == 2) ? 'PM' : ((value1.Thursday == 0) ? '' : ((value1.Thursday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RThursday + '</td><td><img src="' + ((value1.Thursday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Thursday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Thursday1 == 1) ? 'AM' : ((value1.Thursday1 == 2) ? 'PM' : ((value1.Thursday1 == 0) ? '' : ((value1.Thursday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                                     '<tr><td>Fri</td><td><img src="' + ((value1.Friday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Friday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Friday == 1) ? 'AM' : ((value1.Friday == 2) ? 'PM' : ((value1.Friday == 0) ? '' : ((value1.Friday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RFriday + '</td><td><img src="' + ((value1.Friday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Friday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Friday1 == 1) ? 'AM' : ((value1.Friday1 == 2) ? 'PM' : ((value1.Friday1 == 0) ? '' : ((value1.Friday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                                    '<tr><td>Sat</td><td><img src="' + ((value1.Saturday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Saturday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Saturday == 1) ? 'AM' : ((value1.Saturday == 2) ? 'PM' : ((value1.Saturday == 0) ? '' : ((value1.Saturday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RSaturday + '</td><td><img src="' + ((value1.Saturday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Saturday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Saturday1 == 1) ? 'AM' : ((value1.Saturday1 == 2) ? 'PM' : ((value1.Saturday1 == 0) ? '' : ((value1.Saturday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                                    '<tr><td>Mon</td><td><img src="' + ((value1.Monday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Monday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Monday == 1) ? 'AM' : ((value1.Monday == 2) ? 'PM' : ((value1.Monday == 0) ? '' : ((value1.Monday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RMonday + '</td><td><img src="' + ((value1.Monday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Monday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Monday1 == 1) ? 'AM' : ((value1.Monday1 == 2) ? 'PM' : ((value1.Monday1 == 0) ? '' : ((value1.Monday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                                    '</tbody></table>';

                                                    $('#A' + $.trim(value.OfficerID)).mouseover(function () {
                                                        $('#A' + $.trim(value.OfficerID)).attr('class', 'lastLeafMouse');
                                                        $('#A' + $.trim(value.OfficerID)).attr('data-content', datacontent);
                                                        $('#A' + $.trim(value.OfficerID)).attr('Targetvalue', value1.CurrentStatus);
                                                        $('#A' + $.trim(value.OfficerID)).attr('panelcontent', datacontentforDisplay);
                                                        $('#A' + $.trim(value.OfficerID)).attr('data-placement', 'right');
                                                        $('#A' + $.trim(value.OfficerID)).popover('show');
                                                    });
                                                    $('#A' + $.trim(value.OfficerID)).mouseout(function () {
                                                        $('#A' + $.trim(value.OfficerID)).removeClass('lastLeafMouse');
                                                        $('#A' + $.trim(value.OfficerID)).popover('hide');
                                                    });
                                                });
                                            }
                                        }
                                    },
                                    error: function () {
                                    }
                                });
                                //****************Setting up data content for the popover *******************************
                            });

                            if (SelectedOfficerID > 0) {
                                $('.activeListview').removeClass('activeListview');
                                $('#A' + SelectedOfficerID).addClass('activeListview');
                            }

                            if (value.IsLogged) {
                                $('#LI' + $.trim(value.OfficerID)).addClass('sidebar-nav-active');
                                $('#img' + $.trim(value.OfficerID)).show();
                            }
                            else {
                                $('#LI' + $.trim(value.OfficerID)).removeClass('sidebar-nav-active');
                                $('#img' + $.trim(value.OfficerID)).hide();
                            }

                            $('#A' + $.trim(value.OfficerID)).click(function () {
                                LinkForManager = 2;
                                $('.lastLeaf').removeClass('lastLeaf');
                                $(this).addClass('lastLeaf');

                                $('#A' + $.trim(value.OfficerID)).mouseout(function () {
                                    $(this).addClass('lastLeaf');
                                });
                                $('#A' + $.trim(value.OfficerID)).mouseover(function () {
                                    $('.lastLeaf').removeClass('lastLeaf');
                                });

                                $('#lblSelectedOfficerID').html(value.OfficerID);
                                $('#DivAvailability').empty().append($('#A' + $.trim(value.OfficerID)).attr('panelcontent'));
                                if ($(this).attr('data-content') != undefined) {
                                    $('#DAvailability').show();

                                }
                                else {
                                    $('#DAvailability').hide();
                                }

                                $('#showPanel2').hide();
                                $('#DTimeline').show();
                                setCookie('Map', '', 1);
                                pLatitude = 0;
                                pLongitude = 0;
                                setCookie('IsOfficerSelect', value.OfficerID, 1);
                                setCookie('IsManagerSelect', '', 1);
                                if (getCookie('CompanyID') == 1) {
                                    FillCaseActions('0', $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), $(this).attr('id').substr(1));
                                    FillCaseDetailsAction('0', $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), $(this).attr('id').substr(1));
                                    FillWMA('0', $('#txtFromDate').val(), $('#txtFromDate').val(), $(this).attr('id').substr(1));
                                    FillWMAMore('0', $('#txtFromDate').val(), $('#txtFromDate').val(), $(this).attr('id').substr(1));
                                }
                                else {
                                    $('#tblWMASearchType').hide();
                                    FillCaseActions(ManagerID, $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), $(this).attr('id').substr(1));
                                    FillCaseDetailsAction(ManagerID, $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), $(this).attr('id').substr(1));
                                }

                                FillLastKnownLocation(ManagerID, $('#txtFromDateLocation').val(), $('#txtFromDateLocation').val(), $(this).attr('id').substr(1), 0, 0);
                                GetStatistics($(this).attr('id').substr(1), 1);
                                $('#DivRanking').hide();
                                $('#AShowAll,#AShowEnlarge').show();//,#APageEnlarge

                                OffID = $(this).attr('id').substr(1);
                                $('#AHeartBeat,#AHeartBeatEnlarge').show();
                                $('#ALocationwithHB,#ALocationwithHBEnlarge').show();
                            });
                        }
                        else {
                            // COO tree section *********************************************
                            if ($('#Manager' + value.ManagerID).length == 0) {
                                ///////////////////////************************************************************************
                                if (multiselect) {
                                    $('#pListName').html('Manager list');
                                    $('#tbodyOfficerList').append($('<tr>')
                                       .append($('<td>').append($('<a class="targetNew">').attr('id', 'AOfficerTarget')
                                           .attr('data-toggle', 'modal').html(value.Manager + ' ' + value.ManagerID))
                                           .append($('<label>').attr('id', 'lblOfficerID' + key).html(value.ManagerID).hide()))
                                       .append($('<td>').append($('<input>').attr('type', 'text').attr('id', 'txtOfficerTarget' + key).attr('style', 'color:black'))))

                                    $('#txtOfficerTarget' + key).keydown(function (event) {
                                        if (event.shiftKey == true) {
                                            $('#txtOfficerTarget' + key).attr('title', 'Enter numeric values');
                                            return false;
                                        }
                                        if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190 || ((event.keyCode == 65 || event.keyCode == 67 || event.keyCode == 86) && event.ctrlKey == true)) {
                                            return true;
                                        }
                                        else {
                                            $('#txtOfficerTarget' + key).attr('title', 'Enter numeric values');
                                            return false;
                                        }
                                    });

                                    $('#txtOfficerTarget' + key).keyup(function (event) {
                                        var TTarget = 0;
                                        $('#tbodyOfficerList tr').each(function (key, value) {
                                            if ($(this).find('input').val() > 0) {
                                                TTarget += parseInt($(this).find('input').val());
                                            }
                                        });
                                        $('#lblTotalTarget').html(TTarget);

                                        if ($('#lblTotalTarget').html() != $('#txtMonthTotalTarget').val()) {
                                            $('#btnSubmitOfficerTotal').attr('disabled', 'disabled');
                                            $('#lblInfo').html('Total Target should match.');
                                            $("#lblInfo").show().delay(3000).fadeOut();
                                        }
                                        else {
                                            $('#btnSubmitOfficerTotal').removeAttr('disabled');
                                        }
                                    });
                                }
                                ///////////////////////************************************************************************

                                $("#ulOfficers").append('<li><a href="#" id="Manager' + value.ManagerID + '">' +   // class="parent_li"
                                                '<i class="glyphicon glyphicon-plus-sign"></i> ' + value.Manager + '</a>' +
                                                '<ul id="ULMan' + value.ManagerID + '"></ul></li>');

                                //TODO: Get Working hours for DH
                                if (key == 0) {
                                    GetWorkingHours(getCookie('OfficerID'));
                                    setCookie('Availability', 0, 1);
                                }
                                $('#Manager' + value.ManagerID).click(function () {
                                    $('.lastLeaf').removeClass('lastLeaf');
                                    $(this).addClass('lastLeaf');
                                    LinkForManager = 1;
                                    setCookie('IsManagerSelect', value.ManagerID, 1);
                                    setCookie('IsOfficerSelect', '', 1);

                                    setCookie('ManagerID', value.ManagerID, 1);
                                    setCookie('MgrName', value.Manager, 1);
                                    setCookie('Availability', 0, 1);
                                    setCookie('Map', '', 1);
                                    pLatitude = 0;
                                    pLongitude = 0;
                                    $('#lblSelectedOfficerID').html('0');
                                    $('#lblSelectedGroupID').html('0');
                                    $('#lblSelectedManagerID').html(value.ManagerID);
                                    $('#lblCAOfficerName').html('');
                                    IsManager = 1;
                                    GetCaseActionsOfficerCount(1, value.ManagerID, 0, value.Manager);
                                    GetWorkingHours(value.ManagerID);
                                    if (getCookie('CompanyID') == 1) {
                                        $('#tblWMASearchType').hide();
                                        $('#tblWMA').show();
                                        FillCaseActions(value.ManagerID, $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), '0', '0');
                                        FillDeviation(value.ManagerID, $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), '0', '0');
                                        FillCaseDetailsAction(value.ManagerID, $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), '0', '0');
                                        FillWMA(value.ManagerID, $('#txtFromDate').val(), $('#txtFromDate').val(), '0', '0');
                                        FillWMAMore(value.ManagerID, $('#txtFromDate').val(), $('#txtFromDate').val(), '0', '0');
                                    }
                                    else {
                                        FillCaseActions(value.ManagerID, $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), '0', '0');
                                        FillCaseDetailsAction(value.ManagerID, $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), '0', '0');
                                    }
                                    FillLastKnownLocation(value.ManagerID, $('#txtFromDateLocation').val(), $('#txtFromDateLocation').val(), '0', '0');

                                    GetStatistics(value.ManagerID, 2);
                                    FillRankings(value.ManagerID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Paid', '0');
                                    FillRankings(value.ManagerID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Returned', '0');
                                    FillRankings(value.ManagerID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Part Paid', '0');

                                    FillRankingsMore(value.ManagerID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Paid', '0');
                                    FillRankingsMore(value.ManagerID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Returned', '0');
                                    FillRankingsMore(value.ManagerID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Part Paid', '0');
                                    $('#AShowAll,#AShowEnlarge').hide();//,#APageEnlarge

                                    $('#DivRanking').show();
                                    $('#DTimeline').hide();

                                    $('#showPanel2').show();
                                    $('#DAvailability').hide();

                                    $('#AHeartBeat,#AHeartBeatEnlarge').hide();
                                    $('#ALocationwithHB,#ALocationwithHBEnlarge').hide();
                                    $('#imgRefreshLocation').show();
                                    $('#pMap').html('Last known location');
                                });

                            }
                            if ($('#Group' + value.GroupID).length == 0) {
                                $("#ULMan" + value.ManagerID).append('<li><a href="#" id="Group' + value.GroupID + '">' +// class="parent_li" style="display: list-block;"
                                               '<i class="glyphicon glyphicon-plus-sign"></i> ' + value.GroupName + '</a>' +
                                               '<ul id="UL' + value.GroupID + '"></ul></li>');

                                $('#Group' + value.GroupID).click(function () {
                                    LinkForManager = 1;
                                    $('#lblSelectedOfficerID').html('0');
                                    $('#lblSelectedGroupID').html(value.GroupID);
                                    $('#lblSelectedManagerID').html('0');
                                    $('#lblCAOfficerName').html('');
                                    $('.lastLeaf').removeClass('lastLeaf');
                                    $(this).addClass('lastLeaf');
                                    GetCaseActionsOfficerCount(1, value.ManagerID, value.GroupID, value.Manager);
                                    setCookie('Availability', 0, 1);
                                    setCookie('Map', '', 1);
                                    pLatitude = 0;
                                    pLongitude = 0;
                                    if (getCookie('CompanyID') == 1) {
                                        $('#tblWMASearchType').hide();
                                        $('#tblWMA').show();
                                        FillCaseActions(getCookie('OfficerID'), $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), '0', value.GroupID);
                                        FillDeviation(getCookie('OfficerID'), $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), '0', value.GroupID);
                                        FillCaseDetailsAction('0', $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), '0', value.GroupID);
                                        FillWMA('0', $('#txtFromDate').val(), $('#txtFromDate').val(), '0', value.GroupID);
                                        FillWMAMore('0', $('#txtFromDate').val(), $('#txtFromDate').val(), '0', value.GroupID);
                                    }
                                    else {
                                        FillCaseActions(getCookie('OfficerID'), $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), '0', value.GroupID);
                                        FillCaseDetailsAction('0', $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), '0', value.GroupID);
                                    }

                                    GetStatistics(getCookie('ManagerID'), 2);
                                    FillLastKnownLocation('0', $('#txtFromDateLocation').val(), $('#txtFromDateLocation').val(), '0', value.GroupID);
                                    FillRankings('0', $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Paid', value.GroupID);
                                    FillRankings('0', $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Returned', value.GroupID);
                                    FillRankings('0', $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Part Paid', value.GroupID);

                                    FillRankingsMore('0', $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Paid', value.GroupID);
                                    FillRankingsMore('0', $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Returned', value.GroupID);
                                    FillRankingsMore('0', $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Part Paid', value.GroupID);
                                    $('#AShowAll,#AShowEnlarge').hide();//,#APageEnlarge

                                    $('#DivRanking').show();
                                    $('#DTimeline').hide();
                                    $('#showPanel2').show();
                                    $('#DAvailability').hide();

                                    $('#AHeartBeat,#AHeartBeatEnlarge').hide();
                                    $('#ALocationwithHB,#ALocationwithHBEnlarge').hide();
                                    $('#imgRefreshLocation').show();
                                    $('#pMap').html('Last known location');
                                });

                                if (multiselect)
                                    $('#selOfficer').append($('<optgroup>').attr('label', value.Manager).attr('id', 'og' + value.ManagerID))
                            }
                            if (multiselect) {
                                $('#og' + value.ManagerID)
                                .append($('<option>').attr('value', $.trim(value.OfficerID)).html(value.OfficerName + ' - ' + value.OfficerID))

                                $('#lblCurrentMonth').hide();
                                $('#selCurrentMonth').show();
                                $('#lblMonthTotalTarget').hide();
                                $('#txtMonthTotalTarget').show();
                            }

                            $('#UL' + value.GroupID)
                                .append('<li id="LI' + $.trim(value.OfficerID) + '">' +//style="display: list-block;" 
                                        '<a style="cursor:pointer;"  id="A' + $.trim(value.OfficerID) + '" rel="popover" data-original-title=""  class="a1" data-html="true" data-popover="true">' +//class="a1"
                                        '<i class="glyphicon glyphicon-circle-arrow-right"></i> ' + value.OfficerName + '<span class="pull-right">' + value.OfficerID + '</span>' +
                                        '<span style="padding-left:20px"><img src="images/circle-active.png" id="img' + $.trim(value.OfficerID) + '"></span></a> </li>');

                            $('#A' + $.trim(value.OfficerID)).mouseover(function () {
                                $(function () {
                                    //****************Setting up data content for the popover *******************************
                                    Type = "GET";
                                    var inputParams = "/GetOfficerAvailability?OfficerID=" + value.OfficerID + "&" + getCookie('BrowserId');
                                    Url = serviceUrl + inputParams;
                                    DataType = "jsonp"; ProcessData = false;

                                    $.ajax({
                                        type: Type,
                                        url: Url, // Location of the service
                                        contentType: ContentType, // content type sent to server
                                        dataType: DataType, //Expected data format from server       
                                        processdata: ProcessData, //True or False      
                                        async: true,
                                        timeout: 20000,
                                        beforeSend: function () { },
                                        complete: function () { },
                                        success: function (result) {//On Successfull service call  
                                            if (result != undefined) {
                                                if (result != "[]" && result != null) {
                                                    result = JSON.parse(result);
                                                    //    $('#DivAvailability').empty();
                                                    $.each(result, function (key, value1) {
                                                        datacontent = '<table width="100%" class="display-status-table"><tr><td>' +
                                                 '<div class="block" style="margin:2px!important;padding:2px!important">' +
                                                 '<p class="block-heading">Availability</p><table class="table">' +
                                                 '<tbody><tr><td>Week</td><td>Current Week</td><td>Visits</td><td>Next Week</td></tr>' +
                                                 '<tr><td>Tue</td><td><img src="' + ((value1.Tuesday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Tuesday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Tuesday == 1) ? 'AM' : ((value1.Tuesday == 2) ? 'PM' : ((value1.Tuesday == 0) ? '' : ((value1.Tuesday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RTuesday + '</td><td><img src="' + ((value1.Tuesday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Tuesday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Tuesday1 == 1) ? 'AM' : ((value1.Tuesday1 == 2) ? 'PM' : ((value1.Tuesday1 == 0) ? '' : ((value1.Tuesday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                                 '<tr><td>Wed</td><td><img src="' + ((value1.Wednesday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Wednesday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Wednesday == 1) ? 'AM' : ((value1.Wednesday == 2) ? 'PM' : ((value1.Wednesday == 0) ? '' : ((value1.Wednesday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RWednesday + '</td><td><img src="' + ((value1.Wednesday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Wednesday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Wednesday1 == 1) ? 'AM' : ((value1.Wednesday1 == 2) ? 'PM' : ((value1.Wednesday1 == 0) ? '' : ((value1.Wednesday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                                 '<tr><td>Thr</td><td><img src="' + ((value1.Thursday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Thursday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Thursday == 1) ? 'AM' : ((value1.Thursday == 2) ? 'PM' : ((value1.Thursday == 0) ? '' : ((value1.Thursday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RThursday + '</td><td><img src="' + ((value1.Thursday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Thursday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Thursday1 == 1) ? 'AM' : ((value1.Thursday1 == 2) ? 'PM' : ((value1.Thursday1 == 0) ? '' : ((value1.Thursday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                                 '<tr><td>Fri</td><td><img src="' + ((value1.Friday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Friday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Friday == 1) ? 'AM' : ((value1.Friday == 2) ? 'PM' : ((value1.Friday == 0) ? '' : ((value1.Friday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RFriday + '</td><td><img src="' + ((value1.Friday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Friday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Friday1 == 1) ? 'AM' : ((value1.Friday1 == 2) ? 'PM' : ((value1.Friday1 == 0) ? '' : ((value1.Friday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                                '<tr><td>Sat</td><td><img src="' + ((value1.Saturday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Saturday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Saturday == 1) ? 'AM' : ((value1.Saturday == 2) ? 'PM' : ((value1.Saturday == 0) ? '' : ((value1.Saturday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RSaturday + '</td><td><img src="' + ((value1.Saturday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Saturday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Saturday1 == 1) ? 'AM' : ((value1.Saturday1 == 2) ? 'PM' : ((value1.Saturday1 == 0) ? '' : ((value1.Saturday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                                '<tr><td>Mon</td><td><img src="' + ((value1.Monday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Monday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Monday == 1) ? 'AM' : ((value1.Monday == 2) ? 'PM' : ((value1.Monday == 0) ? '' : ((value1.Monday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RMonday + '</td><td><img src="' + ((value1.Monday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Monday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Monday1 == 1) ? 'AM' : ((value1.Monday1 == 2) ? 'PM' : ((value1.Monday1 == 0) ? '' : ((value1.Monday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                                '</tbody></table></div></td></tr></table>';

                                                        datacontentforDisplay = '<table class="table table-striped table-condensed ">' +
                                                 '<tbody><tr><td>Week</td><td>Current Week</td><td>Visits</td><td>Next Week</td></tr>' +
                                                 '<tr><td>Tue</td><td><img src="' + ((value1.Tuesday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Tuesday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Tuesday == 1) ? 'AM' : ((value1.Tuesday == 2) ? 'PM' : ((value1.Tuesday == 0) ? '' : ((value1.Tuesday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RTuesday + '</td><td><img src="' + ((value1.Tuesday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Tuesday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Tuesday1 == 1) ? 'AM' : ((value1.Tuesday1 == 2) ? 'PM' : ((value1.Tuesday1 == 0) ? '' : ((value1.Tuesday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                                 '<tr><td>Wed</td><td><img src="' + ((value1.Wednesday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Wednesday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Wednesday == 1) ? 'AM' : ((value1.Wednesday == 2) ? 'PM' : ((value1.Wednesday == 0) ? '' : ((value1.Wednesday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RWednesday + '</td><td><img src="' + ((value1.Wednesday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Wednesday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Wednesday1 == 1) ? 'AM' : ((value1.Wednesday1 == 2) ? 'PM' : ((value1.Wednesday1 == 0) ? '' : ((value1.Wednesday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                                 '<tr><td>Thr</td><td><img src="' + ((value1.Thursday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Thursday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Thursday == 1) ? 'AM' : ((value1.Thursday == 2) ? 'PM' : ((value1.Thursday == 0) ? '' : ((value1.Thursday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RThursday + '</td><td><img src="' + ((value1.Thursday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Thursday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Thursday1 == 1) ? 'AM' : ((value1.Thursday1 == 2) ? 'PM' : ((value1.Thursday1 == 0) ? '' : ((value1.Thursday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                                 '<tr><td>Fri</td><td><img src="' + ((value1.Friday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Friday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Friday == 1) ? 'AM' : ((value1.Friday == 2) ? 'PM' : ((value1.Friday == 0) ? '' : ((value1.Friday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RFriday + '</td><td><img src="' + ((value1.Friday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Friday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Friday1 == 1) ? 'AM' : ((value1.Friday1 == 2) ? 'PM' : ((value1.Friday1 == 0) ? '' : ((value1.Friday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                                '<tr><td>Sat</td><td><img src="' + ((value1.Saturday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Saturday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Saturday == 1) ? 'AM' : ((value1.Saturday == 2) ? 'PM' : ((value1.Saturday == 0) ? '' : ((value1.Saturday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RSaturday + '</td><td><img src="' + ((value1.Saturday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Saturday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Saturday1 == 1) ? 'AM' : ((value1.Saturday1 == 2) ? 'PM' : ((value1.Saturday1 == 0) ? '' : ((value1.Saturday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                                '<tr><td>Mon</td><td><img src="' + ((value1.Monday == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Monday == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Monday == 1) ? 'AM' : ((value1.Monday == 2) ? 'PM' : ((value1.Monday == 0) ? '' : ((value1.Monday == 'N/A') ? 'N/A' : 'AM PM')))) + '</td><td>' + value1.RMonday + '</td><td><img src="' + ((value1.Monday1 == 0) ? 'images/cancel.png' : 'images/tick.png') + '" style="display:' + ((value1.Monday1 == 'N/A') ? 'none' : 'inline') + '"/>' + ((value1.Monday1 == 1) ? 'AM' : ((value1.Monday1 == 2) ? 'PM' : ((value1.Monday1 == 0) ? '' : ((value1.Monday1 == 'N/A') ? 'N/A' : 'AM PM')))) + '</td></tr>' +
                                                '</tbody></table>';
                                                        //$('#A' + $.trim(value.OfficerID)).mouseover(function () {
                                                        //    //$('#A' + $.trim(value.OfficerID)).attr('class', 'lastLeafMouse');
                                                        //    $('#A' + $.trim(value.OfficerID)).attr('data-content', datacontent);
                                                        //    $('#A' + $.trim(value.OfficerID)).attr('panelcontent', datacontentforDisplay);
                                                        //    // $('#A' + $.trim(value.OfficerID)).attr('data-placement', 'right');
                                                        //    // $('#A' + $.trim(value.OfficerID)).popover('show');
                                                        //});
                                                        //$('#A' + $.trim(value.OfficerID)).mouseout(function () {
                                                        //    $('#A' + $.trim(value.OfficerID)).removeClass('lastLeafMouse');
                                                        //    $('#A' + $.trim(value.OfficerID)).popover('hide');
                                                        //});
                                                    });
                                                }
                                            }
                                        },
                                        error: function () {
                                        }
                                    });
                                    //****************Setting up data content for the popover *******************************
                                });
                                $('#A' + $.trim(value.OfficerID)).attr('class', 'lastLeafMouse');
                                $('#A' + $.trim(value.OfficerID)).attr('data-placement', 'right');
                                $('#A' + $.trim(value.OfficerID)).attr('data-content', datacontent);
                                $('#A' + $.trim(value.OfficerID)).attr('panelcontent', datacontentforDisplay);
                                $('#A' + $.trim(value.OfficerID)).popover('show');
                            });
                            $('#A' + $.trim(value.OfficerID)).mouseout(function () {
                                $('#A' + $.trim(value.OfficerID)).removeClass('lastLeafMouse');
                                $('#A' + $.trim(value.OfficerID)).popover('hide');
                            });

                            if (SelectedOfficerID > 0) {
                                $('.activeListview').removeClass('activeListview');
                                $('#A' + SelectedOfficerID).addClass('activeListview');
                            }

                            if (value.IsLogged) {
                                $('#LI' + $.trim(value.OfficerID)).addClass('sidebar-nav-active');
                                $('#img' + $.trim(value.OfficerID)).show();
                            }
                            else {
                                $('#LI' + $.trim(value.OfficerID)).removeClass('sidebar-nav-active');
                                $('#img' + $.trim(value.OfficerID)).hide();
                            }

                            $('#A' + $.trim(value.OfficerID)).click(function () {
                                LinkForManager = 2;
                                $('.lastLeaf').removeClass('lastLeaf');
                                $(this).addClass('lastLeaf');
                                $('#A' + $.trim(value.OfficerID)).mouseout(function () {
                                    $(this).addClass('lastLeaf');
                                });
                                $('#A' + $.trim(value.OfficerID)).mouseover(function () {
                                    $('.lastLeaf').removeClass('lastLeaf');
                                });
                                GetTimelineResult($.trim(value.OfficerID));

                                $('#lblSelectedOfficerID').html(value.OfficerID);
                                $('#lblSelectedGroupID').html('0');
                                $('#lblSelectedManagerID').html('0');
                                $('#DivAvailability').empty();
                                $('#DivAvailability').append($('#A' + $.trim(value.OfficerID)).attr('panelcontent'));
                                $('#DTimeline').show();
                                $('#DAvailability').show();

                                //   $('#IDeviated').attr('href', '#deviated');
                                setCookie('Map', '', 1);
                                pLatitude = 0;
                                pLongitude = 0;
                                setCookie('IsOfficerSelect', value.OfficerID, 1);
                                setCookie('IsManagerSelect', '', 1);
                                setCookie('Availability', 1, 1);
                                setCookie('TimelineOfficer', value.OfficerID, 1);

                                if (getCookie('CompanyID') == 1) {
                                    FillCaseActions('0', $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), $(this).attr('id').substr(1));
                                    FillDeviation('0', $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), $(this).attr('id').substr(1));
                                    FillCaseDetailsAction('0', $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), $(this).attr('id').substr(1));
                                    FillDeviationDetail('0', $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), $(this).attr('id').substr(1));
                                    FillWMA('0', $('#txtFromDate').val(), $('#txtFromDate').val(), $(this).attr('id').substr(1));
                                    FillWMAMore('0', $('#txtFromDate').val(), $('#txtFromDate').val(), $(this).attr('id').substr(1));
                                }
                                else {
                                    FillCaseActions('0', $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), $(this).attr('id').substr(1));
                                    FillCaseDetailsAction('0', $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), $(this).attr('id').substr(1));
                                    FillDeviationDetail('0', $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), $(this).attr('id').substr(1));
                                }

                                FillLastKnownLocation('0', $('#txtFromDateLocation').val(), $('#txtFromDateLocation').val(), $(this).attr('id').substr(1));

                                $('#DivRanking').hide();
                                $('#showPanel2').hide();

                                GetStatistics($(this).attr('id').substr(1), 1);
                                $('#AShowAll,#AShowEnlarge').show();//,#APageEnlarge

                                OffID = $(this).attr('id').substr(1);
                                $('#AHeartBeat,#AHeartBeatEnlarge').show();
                                $('#ALocationwithHB,#ALocationwithHBEnlarge').show();
                                $('#imgRefreshLocation').hide();
                            });
                            //COO tree section **********************************************
                        }
                    });

                    if (multiselect) {
                        var divTarget = parseInt($('#lblMonthTotalTarget').html() / ($('#tbodyOfficerList tr').length));
                        var remTarget = parseInt($('#lblMonthTotalTarget').html() % ($('#tbodyOfficerList tr').length));

                        $('#tbodyOfficerList tr').each(function (key, value) {
                            if (key == ($('#tbodyOfficerList tr').length - 1))
                                $(this).find('input').val(divTarget + remTarget);
                            else
                                $(this).find('input').val(divTarget);
                        });

                        $('#lblTotalTarget').html(getCookie('tgVal'));

                        $('#tbodyOfficerList').append($('<tr>')
                            .append($('<td>').append($('<span>').attr('class', 'pull-right').html('Total')))
                            .append($('<td>').append($('<label>').attr('id', 'lblTotalTarget'))))
                        .append($('<tr>').append($('<td>'))
                            .append($('<td>').append($('<button>').attr('id', 'btnSubmitOfficerTotal').attr('class', 'btn-primary')
                                                                  .attr('onclick', 'SubmitOfficerTotal()').html('Submit'))))

                        $('#btnSubmitOfficerTotal').click(function () {
                            var TTargetInfo = '';
                            $('#tbodyOfficerList tr').each(function (key, value) {
                                if ($(this).find('label').html() != '' && $(this).find('label').html() != undefined && parseInt($(this).find('input').val()) > 0) {
                                    if (TTargetInfo != '') TTargetInfo += ',';
                                    TTargetInfo += $(this).find('label').html() + '|' + parseInt($(this).find('input').val());
                                }
                            });

                            // ****************************** submit functionality
                            Type = "GET";
                            var inputParams = "/UpdateOfficerTarget?TMonth=" + $('#selCurrentMonth').val() + "&TargetNo=" +
                                $('#txtMonthTotalTarget').val() + "&ManagerID=" + ManagerID + "&OfficerIDs=" + TTargetInfo;

                            Url = serviceUrl + inputParams;
                            DataType = "jsonp"; ProcessData = false;

                            $.ajax({
                                type: Type,
                                url: Url, // Location of the service
                                contentType: ContentType, // content type sent to server
                                dataType: DataType, //Expected data format from server       
                                processdata: ProcessData, //True or False      
                                async: true,
                                timeout: 20000,
                                beforeSend: function () { },
                                complete: function () { },
                                success: function (result) {//On Successfull service call  
                                    $('#lblTargetInfo').html('Target has been updated successfully.');
                                    $('#ATargetInfo').click();
                                },
                                error: function () {
                                } // When Service call fails
                            });

                        });

                    }
                    $('#AShowAll').click(function () {
                        FillAllLocation(1);
                    });
                    $('#AShowEnlarge').click(function () {
                        FillAllLocation(2);
                    });
                    $('#selOfficer').multiselect({
                        noneSelectedText: 'Select Officers',
                        selectedList: 5,
                        multiple: true
                    }).multiselectfilter();

                    $('#selOfficer').multiselect("uncheckAll");
                }
            }

            // Code moved from Common.js - onload fn
            $('.tree li:has(ul)').addClass('parent_li');
            $('.tree li.parent_li > a').on('click', function (e) {
                var children = $(this).parent('li.parent_li').find(' > ul > li');
                if (children.is(":visible")) {
                    children.hide('fast');
                    $(this).find(' > i').addClass('glyphicon-plus-sign').removeClass('glyphicon-minus-sign');
                } else {
                    children.show('fast');
                    $(this).find(' > i').addClass('glyphicon-minus-sign').removeClass('glyphicon-plus-sign');
                }
                e.stopPropagation();
            });
            //

        },
        error: function () {
        } // When Service call fails
    });

    $('#popup1Trigger').click(function () {
        $('#selCurrentMonth').val((new Date).getMonth() + 1);

        $('#txtMonthTotalTarget').keydown(function (event) {
            if (event.shiftKey == true) {
                $('#txtMonthTotalTarget' + key).attr('title', 'Enter numeric values');
                return false;
            }
            if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190 || ((event.keyCode == 65 || event.keyCode == 67 || event.keyCode == 86) && event.ctrlKey == true)) {
                return true;
            }
            else {
                $('#txtMonthTotalTarget' + key).attr('title', 'Enter numeric values');
                return false;
            }
        });

        $('#txtMonthTotalTarget').keyup(function () {
            var divTarget = parseInt($('#txtMonthTotalTarget').val() / (($('#tbodyOfficerList tr').length / 2) - 2));
            var remTarget = parseInt($('#txtMonthTotalTarget').val() % (($('#tbodyOfficerList tr').length / 2) - 2));
            $('#tbodyOfficerList tr').each(function (key, value) {
                if (key == (($('#tbodyOfficerList tr').length / 2) - 3))
                    $(this).find('input').val(divTarget + remTarget);
                else
                    $(this).find('input').val(divTarget);
            });

            $('#lblTotalTarget').html($('#txtMonthTotalTarget').val());
        });
    });
}

//==========================================================================================================================================
//TODO : Case action function

function FillCaseActions(ManagerID, FromDate, ToDate, OfficerID, GroupID) {
    Type = "GET";
    serviceTypeLogin = "getcaseactions";

    if (OfficerID == undefined) OfficerID = 0;

    if (GroupID == undefined) GroupID = 0;

    if (FromDate == '') {
        var date = new Date();
        var TDate = $.datepicker.formatDate('dd/mm/yy', date);
        FromDate = TDate;
    }
    if (ToDate == '') {
        var date1 = new Date();
        var T1Date = $.datepicker.formatDate('dd/mm/yy', date1);
        ToDate = T1Date;
    }

    var dateAr = FromDate.split('/');
    var newDate = dateAr[2] + '/' + dateAr[1] + '/' + dateAr[0];
    FromDate = newDate;

    var dateTr = ToDate.split('/');
    var newToDate = dateTr[2] + '/' + dateTr[1] + '/' + dateTr[0];
    ToDate = newToDate;

    var inputParams = "/GetCaseActions_Dashboard?ManagerID=" + ManagerID + "&FromDate=" + FromDate + "&ToDate=" +
        ToDate + "&OfficerID=" + OfficerID + "&GroupID=" + GroupID + "&" + getCookie('BrowserId');//"&ClientId=" + (getCookie('ClientId') == undefined ? 0 : getCookie('ClientId')) +
    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () { },
        complete: function () { },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {
                    result = JSON.parse(result);
                    $('#tblbodyCA').empty();//,
                    if (getCookie('CompanyID') == 2) {
                        $('#tbodyWMA').empty();
                    }

                    if ((LinkForManager == 0 || LinkForManager == 1) && (getCookie('RoleType') == 1)) {//($.trim($('#lblLoginOfficer').html()) == 'David Burton')
                        $.each(result, function (key, value) {
                            if (getCookie('CompanyID') == 1) {
                                $('#SpanAction1').html('Case Actions');
                                $('#tblbodyCA').append($('<tr>')
                                    .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                    .append($('<td>').append($('<a id="AActionModal' + key + '">').attr('data-toggle', 'modal')
                                                     .attr('href', '#divCaseActionModal').attr('onclick', 'caseaction(' + key + ')').html(value.ActionText)))
                                    .append($('<td>').html(value.Case))
                                    .append($('<td>').attr('title', 'Last four week average').html(value.Ave)))

                                if (value.ActionText == 'Defendant contact')
                                    $('#AActionModal' + key).attr('href', '#defendantcontact')
                            }
                            else// Company-2
                            {
                                if (!(value.ActionText == 'TCG' || value.ActionText == 'UTTC' || value.ActionText == 'DROPPED' || value.ActionText == 'OTHER')) {
                                    $('#SpanAction1').html('Paid Actions');
                                    $('#tblbodyCA').append($('<tr>')
                                        .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                        .append($('<td>').append($('<a>').attr('data-toggle', 'modal').attr('href', '#divCaseActionModal').attr('onclick', 'caseaction(' + key + ')').html(value.ActionText)))
                                        .append($('<td>').html(value.Case))
                                        .append($('<td>').attr('title', 'Last four week average').html(value.Ave)))
                                }
                                else {
                                    $('#SpanAction2').html('Other Actions');
                                    $('#tbodyWMA').append($('<tr>')
                                        .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                        .append($('<td>').append($('<a>').attr('data-toggle', 'modal').attr('href', '#divCaseActionModal').attr('onclick', 'caseaction(' + key + ')').html(value.ActionText)))
                                        .append($('<td>').html(value.Case))
                                        .append($('<td>').attr('title', 'Last four week average').html(value.Ave)))
                                }
                            }
                        });
                    }
                    else if ((LinkForManager == 0 || LinkForManager == 1) && (getCookie('RoleType') != 1)) {
                        IsManager = 2;
                        $.each(result, function (key, value) {
                            if (getCookie('CompanyID') == 1) {
                                $('#SpanAction1').html('Case Actions');
                                $('#tblbodyCA').append($('<tr>')
                                    .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                    .append($('<td>').append($('<a id="AActionModal' + key + '">').attr('data-toggle', 'modal').attr('href', '#divCaseActionModal').attr('onclick', 'caseaction(' + key + ')').html(value.ActionText)))
                                    .append($('<td>').html(value.Case))
                                    .append($('<td>').attr('title', 'Last four week average').html(value.Ave)))
                                if (value.ActionText == 'Defendant contact')
                                    $('#AActionModal' + key).attr('href', '#defendantcontact')
                            }
                            else// Company-2
                            {
                                if (!(value.ActionText == 'TCG' || value.ActionText == 'UTTC' || value.ActionText == 'DROPPED' || value.ActionText == 'OTHER')) {
                                    $('#SpanAction1').html('Paid Actions');
                                    $('#tblbodyCA').append($('<tr>')
                                        .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                        .append($('<td>').append($('<a>').attr('data-toggle', 'modal').attr('href', '#divCaseActionModal').attr('onclick', 'caseaction(' + key + ')').html(value.ActionText)))
                                        .append($('<td>').html(value.Case))
                                        .append($('<td>').attr('title', 'Last four week average').html(value.Ave)))
                                }
                                else {
                                    $('#SpanAction2').html('Other Actions');
                                    $('#tbodyWMA').append($('<tr>')
                                        .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                        .append($('<td>').append($('<a>').attr('data-toggle', 'modal').attr('href', '#divCaseActionModal').attr('onclick', 'caseaction(' + key + ')').html(value.ActionText)))
                                        .append($('<td>').html(value.Case))
                                        .append($('<td>').attr('title', 'Last four week average').html(value.Ave)))
                                }
                            }
                        });
                    }
                    else {
                        if (getCookie('CompanyID') != 1) {
                            $('#tbodyWMA').empty();
                            $.each(result, function (key, value) {
                                if (!(value.ActionText == 'TCG' || value.ActionText == 'UTTC' || value.ActionText == 'DROPPED' || value.ActionText == 'OTHER')) {
                                    $('#SpanAction1').html('Paid Actions');
                                    $('#tblbodyCA').append($('<tr>')
                                       .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                       .append($('<td>').append($('<a>').attr('data-toggle', 'modal').attr('href', '#' + value.ActionText.toString().toLowerCase().replace(" ", "")).attr('onclick', 'caseaction(' + key + ')').html(value.ActionText)))
                                       .append($('<td>').html(value.Case))
                                       .append($('<td>').attr('title', 'Last four week average').html(value.Ave)))
                                }
                                else {
                                    $('#SpanAction2').html('Other Actions');
                                    $('#tbodyWMA').append($('<tr>')
                                       .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                       .append($('<td>').append($('<a>').attr('data-toggle', 'modal').attr('href', '#' + value.ActionText.toString().toLowerCase().replace(" ", "")).attr('onclick', 'caseaction(' + key + ')').html(value.ActionText)))
                                       .append($('<td>').html(value.Case))
                                       .append($('<td>').attr('title', 'Last four week average').html(value.Ave)))
                                }
                            });
                        }
                        else {
                            $('#tblbodyCA').empty();
                            $('#SpanAction1').html('Case Actions');
                            $('#SpanAction2').html('Warrant Matching');
                            $.each(result, function (key, value) {
                                $('#tblbodyCA').append($('<tr>')
                                    .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                    .append($('<td>').append($('<a id="AActionModal' + key + '">').attr('data-toggle', 'modal').attr('href', '#' + value.ActionText.toString().toLowerCase().replace(" ", "")).attr('onclick', 'caseaction(' + key + ')').html(value.ActionText)))
                                    .append($('<td>').html(value.Case))
                                    .append($('<td>').attr('title', 'Last four week average').html(value.Ave)))
                                if (value.ActionText == 'Defendant contact')
                                    $('#AActionModal' + key).attr('href', '#defendantcontact')
                            });
                        }
                    }
                }
            }
        },
        error: function () {
        } // When Service call fails
    });

}

function FillCaseDetailsActionForManager(ManagerID, FromDate, ToDate, OfficerID, GroupID) {
    $('#tblbodyActionPaid').empty();
    $('#tbodyActionPP').empty();
    $('#tbodyActionReturned').empty();
    $('#tbodyActionLeftLetter').empty();
    $('#tbodyActionEnforcementStart').empty();
    $('#tbodyActionRevisit').empty();
    $('#tbodyActionEnforcementEnd').empty();

    $('#tbodypaidformanager').empty();
    $('#tbodypartpaidformanager').empty();
    $('#tbodyreturnedformanager').empty();
    $('#tbodyleftletterformanager').empty();
    $('#tbodyrevisitformanager').empty();

    $('#tbodyActionTCG').empty();
    $('#tbodyActionTCGPAID').empty();
    $('#tbodyActionTCGPP').empty();
    $('#tbodyActionUTTC').empty();
    $('#tbodyActionDROPPED').empty();
    $('#tbodyActionOTHER').empty();

    $('#tbodyotherformanager').empty();
    $('#tbodydroppedformanager').empty();
    $('#tbodyuttcformanager').empty();
    $('#tbodytcgformanager').empty();
    $('#tbodytcgppformanager').empty();
    $('#tbodytcgpaidformanager').empty();

    $('#tbodyRankOfficerPaid').empty();
    $('#tbodyRankOfficerPartPaid').empty();
    $('#tbodyRankOfficerReturn').empty();

    // TODO : Rank more
    $('#tbodyRankMorePaidCaseList').empty();
    $('#tbodyRankMoreReturnedCaseList').empty();
    $('#tbodyRankMorePPCaseList').empty();

    Type = "GET";
    serviceTypeLogin = "getcaseactions";

    if (OfficerID == undefined) OfficerID = 0;
    if (GroupID == undefined) GroupID = 0;

    var inputParams = "/GetCaseActionsDetail_Dashboard?ManagerID=" + $.trim(ManagerID) + "&FromDate=" + FromDate + "&ToDate=" + ToDate + "&OfficerID=" + OfficerID
        + "&GroupID=" + GroupID + "&" + getCookie('BrowserId');//+ "&ActionText=" + ActionText;//"&ClientId=" + (getCookie('ClientId') == undefined ? 0 : getCookie('ClientId')) +
    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
            $('#imgLoader1').show();
        },
        complete: function () {
            $('#imgLoader1').fadeOut(1000);//.hide();
        },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {
                    result = JSON.parse(result);
                    $.each(result, function (key, value) {
                        switch (value.ActionText) {
                            case 'Paid':
                            case 'PAID':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodypaidformanager,#tbodyRankOfficerPaid,#tbodyRankMorePaidCaseList').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
									   .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodypaidformanager,#tbodyRankOfficerPaid,#tbodyRankMorePaidCaseList').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodypaidformanager,#tbodyRankOfficerPaid,#tbodyRankMorePaidCaseList').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                            case 'Part Paid':
                            case 'PART PAID':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodypartpaidformanager,#tbodyRankOfficerPartPaid,#tbodyRankMorePPCaseList').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 2 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodypartpaidformanager,#tbodyRankOfficerPartPaid,#tbodyRankMorePPCaseList').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 2 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodypartpaidformanager,#tbodyRankOfficerPartPaid,#tbodyRankMorePPCaseList').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 2 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                            case 'Returned':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyreturnedformanager,#tbodyRankOfficerReturn,#tbodyRankMoreReturnedCaseList').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 4 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodyreturnedformanager,#tbodyRankOfficerReturn,#tbodyRankMoreReturnedCaseList').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 4 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodyreturnedformanager,#tbodyRankOfficerReturn,#tbodyRankMoreReturnedCaseList').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 4 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                            case 'Left Letter':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyleftletterformanager').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 5 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodyleftletterformanager').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 5 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodyleftletterformanager').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 5 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                            case '£ Collected':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyrevisitformanager').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 3 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodyrevisitformanager').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 3 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodyrevisitformanager').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 3 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;

                            case 'TCG':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionTCG,#tbodytcgformanager').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 3 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodyActionTCG,#tbodytcgformanager').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 3 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodyActionTCG,#tbodytcgformanager').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 3 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                            case 'TCG PAID':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionTCGPAID,#tbodytcgpaidformanager').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 4 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodyActionTCGPAID,#tbodytcgpaidformanager').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 4 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodyActionTCGPAID,#tbodytcgpaidformanager').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 4 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                            case 'TCG PP':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionTCGPP,#tbodytcgppformanager').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 5 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodyActionTCGPP,#tbodytcgppformanager').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 5 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodyActionTCGPP,#tbodytcgppformanager').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 5 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                            case 'UTTC':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionUTTC,#tbodyuttcformanager').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 6 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodyActionUTTC,#tbodyuttcformanager').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 6 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodyActionUTTC,#tbodyuttcformanager').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 6 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                            case 'DROPPED':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionDROPPED,#tbodydroppedformanager').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 7 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodyActionDROPPED,#tbodydroppedformanager').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 7 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodyActionDROPPED,#tbodydroppedformanager').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 7 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                            case 'OTHER':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionOTHER,#tbodyotherformanager').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 8 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                    else {
                                        $('#tbodyActionOTHER,#tbodyotherformanager').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 8 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                    }
                                }
                                else {
                                    $('#tbodyActionOTHER,#tbodyotherformanager').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
									   .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 8 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes))
                                       )
                                }
                                break;
                        }
                    });
                }
                else {
                    $('#tbodypaidformanager').empty().html('No records found');
                    $('#tbodypartpaidformanager').empty().html('No records found');
                    $('#tbodyreturnedformanager').empty().html('No records found');
                    $('#tbodyleftletterformanager').empty().html('No records found');
                    $('#tbodyrevisitformanager').empty().html('No records found');

                    $('#tblbodyActionPaid').empty().html('No records found');
                    $('#tbodyActionPP').empty().html('No records found');
                    $('#tbodyActionReturned').empty().html('No records found');
                    $('#tbodyActionLeftLetter').empty().html('No records found');
                    $('#tbodyActionEnforcementStart').empty().html('No records found');
                    $('#tbodyActionRevisit').empty().html('No records found');
                    $('#tbodyActionEnforcementEnd').empty().html('No records found');

                    $('#tbodyActionTCG').empty().html('No records found');
                    $('#tbodyActionTCGPAID').empty().html('No records found');
                    $('#tbodyActionTCGPP').empty().html('No records found');
                    $('#tbodyActionUTTC').empty().html('No records found');
                    $('#tbodyActionDROPPED').empty().html('No records found');
                    $('#tbodyActionOTHER').empty().html('No records found');

                    $('#tbodyotherformanager').empty().html('No records found');
                    $('#tbodydroppedformanager').empty().html('No records found');
                    $('#tbodyuttcformanager').empty().html('No records found');
                    $('#tbodytcgformanager').empty().html('No records found');
                    $('#tbodytcgppformanager').empty().html('No records found');
                    $('#tbodytcgpaidformanager').empty().html('No records found');

                    $('#tbodyRankOfficerPaid').empty().html('No records found');
                    $('#tbodyRankOfficerPartPaid').empty().html('No records found');
                    $('#tbodyRankOfficerReturn').empty().html('No records found');

                    $('#tbodyRankMorePaidCaseList').empty().html('No records found');
                    $('#tbodyRankMoreReturnedCaseList').empty().html('No records found');
                    $('#tbodyRankMorePPCaseList').empty().html('No records found');
                }
            }
        },
        error: function () {
            //alert('error');
        } // When Service call fails
    });
}

function FullDescription(key, val) {
    $('#btnCaseActionBack,#divCaseActionsManagerCount,#divCaseActionsOfficerCount').hide();
    //$('#btnCaseActionBack,#Notesformanager,#btnNotesBack').hide();
    if (getCookie('CompanyID') == 1) {
        switch (val) {
            case 1:
                $('#paidformanager,#paid').hide();
                break;
            case 2:
                $('#partpaidformanager,#partpaid').hide();
                break;
            case 4:
                $('#returnedformanager,#returned').hide();
                break;
            case 5:
                $('#leftletterformanager,#leftletter').hide();
                break;
            case 3:
                $('#revisitformanager,#revisit').hide();
                break;
            case 7:
                $('#deviatedformanager,#deviated').hide();
                break;
        }
    }
    else {
        switch (val) {
            case 1:
                //$('#tcgpaidformanager,#tcgppformanager,#tcgformanager,#uttcformanager,#droppedformanager,#otherformanager').hide();
                $('#paidformanager,#paid').hide();
                break;
            case 2:
                $('#partpaidformanager,#partpaid').hide();
                break;
            case 3:
                $('#tcgformanager,#tcg').hide();
                break;
            case 4:
                $('#tcgpaidformanager,#tcgpaid').hide();
                break;
            case 5:
                $('#tcgppformanager,#tcgpp').hide();
                break;
            case 6:
                $('#uttcformanager,#uttc').hide();
                break;
            case 7:
                $('#droppedformanager,#dropped').hide();
                break;
            case 8:
                $('#otherformanager,#other').hide();
                break;
        }
    }
    $('#divCaseActionModal').attr('class', 'modal fade in').show();
    $('#Notesformanager').empty().html($('#tdFullDesc' + key).html()).show();
    $('#HCaseNumber').empty().html(' :  ' + $.trim($('#tdCaseNo' + key).html()) == '' ? $('#tdCaseOfficer' + key).html() : $('#tdCaseNo' + key).html() + " - " + $('#tdCaseOfficer' + key).html()).show();
    $('#btnNotesBack').show().click(function () {
        if (getCookie('CompanyID') == 1) {
            $('#paidformanager,#partpaidformanager,#returnedformanager,#leftletterformanager,#revisitformanager,#deviated').hide();
            switch (val) {
                case 1:
                    $('#paidformanager').show();
                    //$('#paid').show();
                    break;
                case 2:
                    $('#partpaidformanager').show();
                    //$('#partpaid').show();
                    break;
                case 4:
                    $('#returnedformanager').show();
                    //$('#returned').show();
                    break;
                case 5:
                    $('#leftletterformanager').show();
                    //$('#leftletter').show();
                    break;
                case 3:
                    $('#revisitformanager').show();
                    // $('#revisit').show();
                    break;
                case 7:
                    $('#deviated').show();
                    break;
            }
        }
        else {
            $('#paidformanager,#partpaidformanager,#tcgformanager,#tcgpaidformanager,#tcgppformanager,#uttcformanager,#droppedformanager,#otherformanager').hide();
            switch (val) {
                case 1:
                    $('#paidformanager').show();
                    break;
                case 2:
                    $('#partpaidformanager').show();
                    break;
                case 3:
                    $('#tcgformanager').show();
                    break;
                case 4:
                    $('#tcgpaidformanager').show();
                    break;
                case 5:
                    $('#tcgppformanager').show();
                    break;
                case 6:
                    $('#uttcformanager').show();
                    break;
                case 7:
                    $('#droppedformanager').show();
                    break;
                case 8:
                    $('#otherformanager').show();
                    break;
            }
        }
        $('#divCaseActionModal').attr('class', 'modal fade in').show();
        $('#Notesformanager,#HCaseNumber,#btnNotesBack').hide();
        $('#btnCaseActionBack').show();
    });
}

function FullDescriptionOfficer(key, val) {
    $('#btnCaseActionBack,#divCaseActionsManagerCount,#divCaseActionsOfficerCount').hide();
    $('#btnCaseActionBack,#Notesformanager,#btnNotesBack').hide();
    if (getCookie('CompanyID') == 1) {
        switch (val) {
            case 1:
                $('#paidformanager,#paid').hide();
                break;
            case 2:
                $('#partpaidformanager,#partpaid').hide();
                break;
            case 4:
                $('#returnedformanager,#returned').hide();
                break;
            case 5:
                $('#leftletterformanager,#leftletter').hide();
                break;
            case 3:
                $('#revisitformanager,#revisit').hide();
                break;
            case 7:
                $('#deviatedformanager,#deviated').hide();
                break;
        }
    }
    else {
        switch (val) {
            case 1:
                //$('#tcgpaidformanager,#tcgppformanager,#tcgformanager,#uttcformanager,#droppedformanager,#otherformanager').hide();
                $('#paidformanager,#paid').hide();
                break;
            case 2:
                $('#partpaidformanager,#partpaid').hide();
                break;
            case 3:
                $('#tcgformanager,#tcg').hide();
                break;
            case 4:
                $('#tcgpaidformanager,#tcgpaid').hide();
                break;
            case 5:
                $('#tcgppformanager,#tcgpp').hide();
                break;
            case 6:
                $('#uttcformanager,#uttc').hide();
                break;
            case 7:
                $('#droppedformanager,#dropped').hide();
                break;
            case 8:
                $('#otherformanager,#other').hide();
                break;
        }
    }
    $('#divCaseActionModal').attr('class', 'modal fade in').show();
    $('#Notesformanager').empty().html($('#tdFullDesc' + key).html()).show();
    $('#HCaseNumber').empty().html(' :  ' + $.trim($('#tdCaseNo' + key).html()) == '' ? $('#tdCaseOfficer' + key).html() : $('#tdCaseNo' + key).html() + " - " + $('#tdCaseOfficer' + key).html()).show();
    $('#btnNotesBack').show().click(function () {
        if (getCookie('CompanyID') == 1) {
            $('#paid,#partpaid,#returned,#leftletter,#revisit,#deviated').hide();
            switch (val) {
                case 1:
                    //$('#paidformanager').show();
                    $('#paid').show();
                    break;
                case 2:
                    //$('#partpaidformanager').show();
                    $('#partpaid').show();
                    break;
                case 4:
                    //$('#returnedformanager').show();
                    $('#returned').show();
                    break;
                case 5:
                    //$('#leftletterformanager').show();
                    $('#leftletter').show();
                    break;
                case 3:
                    //$('#revisitformanager').show();
                    $('#revisit').show();
                    break;
                case 7:
                    $('#deviated').show();
                    break;
            }
        }
        else {
            $('#paid,#partpaid,#tcg,#tcgpaid,#tcgpp,#uttc,#dropped,#other').hide();
            switch (val) {
                case 1:
                    $('#paid').show();
                    break;
                case 2:
                    $('#partpaid').show();
                    break;
                case 3:
                    $('#tcg').show();
                    break;
                case 4:
                    $('#tcgpaid').show();
                    break;
                case 5:
                    $('#tcgpp').show();
                    break;
                case 6:
                    $('#uttc').show();
                    break;
                case 7:
                    $('#dropped').show();
                    break;
                case 8:
                    $('#other').show();
                    break;
            }
        }
        $('#divCaseActionModal').attr('class', 'modal fade').hide();
        $('#Notesformanager,#HCaseNumber,#btnNotesBack').hide();
        // $('#btnCaseActionBack').show();
    });
}

function FillCaseActionDetailsOnClick(Action, OfficerID, OfficerName, Check) {
    $('#HCaseActionNavigation').show();
    $('#btnCaseActionBack').show();
    $('#divCaseActionsManagerCount,#divCaseActionsOfficerCount,#btnCaseActionManager').hide();
    $('#DRankingOfficer,#DRankMorePaidOfficer,#btnPaidOfficerBack,#DRankMoreReturnedOfficer,#btnReturnedOfficerBack,#DRankMorePPOfficer,#btnPPOfficerBack').hide();
    $('#tblRankOfficerReturn,#tblRankOfficerPartPaid,#tblRankOfficerPaid').hide();
    $('#lblCAOfficerName,#lblRankOfficerName,#lblRankPaidOfficer,#lblRankReturnedOfficer,#lblRankPPOfficer').html(' - ' + OfficerName);

    if (Check == 0) {
        switch (Action) {
            case 1:
                $('#lblRankStatus').html('Ranking - Paid');
                $('#DRankingCase,#tblRankOfficerPaid,#btnRankOfficerBack').show();
                $('#HCaseActionTitle').html('Paid');
                FillCaseDetailsAction(0, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), OfficerID, 0);
                break;
            case 2:
                $('#lblRankStatus').html('Ranking - Returned');
                $('#DRankingCase,#tblRankOfficerReturn,#btnRankOfficerBack').show();
                $('#HCaseActionTitle').html('Returned');
                FillCaseDetailsAction(0, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), OfficerID, 0);
                break;
            case 3:
                $('#lblRankStatus').html('Ranking - Part Paid');
                $('#DRankingCase,#tblRankOfficerPartPaid,#btnRankOfficerBack').show();
                $('#HCaseActionTitle').html('Part Paid');
                FillCaseDetailsAction(0, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), OfficerID, 0);
                break;
            case 4:
                FillCaseDetailsAction(0, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), OfficerID, 0);
                break;
        }
    }
    else {
        switch (Action) {
            case 1:
                FillCaseDetailsAction(0, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), OfficerID, 0);
                break;
            case 2:
                FillCaseDetailsAction(0, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), OfficerID, 0);
                break;
            case 3:
                FillCaseDetailsAction(0, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), OfficerID, 0);
                break;
            case 4:
                FillCaseDetailsAction(0, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), OfficerID, 0);
                break;
        }
    }
}

function FillCaseDetailsAction(ManagerID, FromDate, ToDate, OfficerID, GroupID) {
    Type = "GET";
    serviceTypeLogin = "getcaseactions";

    if (OfficerID == undefined) OfficerID = 0;
    if (GroupID == undefined) GroupID = 0;


    if (FromDate == '') {
        var date = new Date();
        var TDate = $.datepicker.formatDate('dd/mm/yy', date);
        FromDate = TDate;
    }
    if (ToDate == '') {
        var date1 = new Date();
        var T1Date = $.datepicker.formatDate('dd/mm/yy', date1);
        ToDate = T1Date;
    }

    var dateAr = FromDate.split('/');
    var newDate = dateAr[2] + '/' + dateAr[1] + '/' + dateAr[0];
    FromDate = newDate;

    var dateTr = ToDate.split('/');
    var newToDate = dateTr[2] + '/' + dateTr[1] + '/' + dateTr[0];
    ToDate = newToDate;

    var inputParams = "/GetCaseActionsDetail_Dashboard?ManagerID=" + $.trim(ManagerID) + "&FromDate=" + FromDate + "&ToDate=" + ToDate + "&OfficerID=" + OfficerID
        + "&GroupID=" + GroupID + "&" + getCookie('BrowserId');//+ "&ClientId=" + (getCookie('ClientId') == undefined ? 0 : getCookie('ClientId'));//+ "&ActionText=" + ActionText;

    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;
    //CallService();
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
        },
        complete: function () {

        },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {

                    result = JSON.parse(result);
                    $('#tblbodyActionPaid').empty();
                    $('#tbodyActionPP').empty();
                    $('#tbodyActionReturned').empty();
                    $('#tbodyActionLeftLetter').empty();
                    $('#tbodyActionEnforcementStart').empty();
                    $('#tbodyActionRevisit').empty();
                    $('#tbodyActionEnforcementEnd').empty();

                    $('#tbodyActionTCG').empty();
                    $('#tbodyActionTCGPAID').empty();
                    $('#tbodyActionTCGPP').empty();
                    //$('#tbodyAction_PAID').empty();
                    //$('#tbodyAction_PARTPAID').empty();
                    $('#tbodyActionUTTC').empty();
                    $('#tbodyActionDROPPED').empty();
                    $('#tbodyActionOTHER').empty();

                    $('#tbodyRankOfficerPaid').empty();
                    $('#tbodyRankOfficerPartPaid').empty();
                    $('#tbodyRankOfficerReturn').empty();

                    $.each(result, function (key, value) {
                        switch (value.ActionText) {
                            case 'Paid':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tblbodyActionPaid,#tbodyRankOfficerPaid').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tblbodyActionPaid,#tbodyRankOfficerPaid').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tblbodyActionPaid,#tbodyRankOfficerPaid').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;
                            case 'Part Paid':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionPP,#tbodyRankOfficerPartPaid').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 2 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tbodyActionPP,#tbodyRankOfficerPartPaid').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 2 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tbodyActionPP,#tbodyRankOfficerPartPaid').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 2 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;
                            case 'Returned':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionReturned,#tbodyRankOfficerReturn').append($('<tr style = "background:#66FF99">')//,#tblbodyReturned
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 4 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tbodyActionReturned,#tbodyRankOfficerReturn').append($('<tr style = "background:#FFCCFF">')//,#tblbodyReturned
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 4 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tbodyActionReturned,#tbodyRankOfficerReturn').append($('<tr>')//,#tblbodyReturned
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 4 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;
                            case 'Left Letter':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionLeftLetter').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 5 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tbodyActionLeftLetter').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 5 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tbodyActionLeftLetter').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 5 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;
                            case 'EnforcementStart':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionEnforcementStart').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tbodyActionEnforcementStart').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tbodyActionEnforcementStart').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;
                            case '£ Collected':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionRevisit').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 3 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tbodyActionRevisit').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 3 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tbodyActionRevisit').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 3 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;
                            case 'EnforcementEnd':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionEnforcementEnd').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tbodyActionEnforcementEnd').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tbodyActionEnforcementEnd').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;

                            case 'TCG':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionTCG').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 3 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tbodyActionTCG').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 3 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tbodyActionTCG').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 3 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;
                            case 'TCG PAID':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionTCGPAID').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 4 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tbodyActionTCGPAID').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 4 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tbodyActionTCGPAID').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 4 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;
                            case 'TCG PP':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionTCGPP').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 5 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tbodyActionTCGPP').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 5 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tbodyActionTCGPP').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 5 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;
                            case 'PAID':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tblbodyActionPaid').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tblbodyActionPaid').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tblbodyActionPaid').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;
                            case 'PART PAID':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionPP').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 2 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tbodyActionPP').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 2 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tbodyActionPP').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 2 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;
                            case 'UTTC':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionUTTC').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 6 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tbodyActionUTTC').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 6 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tbodyActionUTTC').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 6 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;
                            case 'DROPPED':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionDROPPED').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 7 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tbodyActionDROPPED').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 7 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tbodyActionDROPPED').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 7 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;
                            case 'OTHER':
                                if (value.ResponseType != '0') {
                                    if (value.ResponseType == 'NoError') {
                                        $('#tbodyActionOTHER').append($('<tr style = "background:#66FF99">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 8 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                    else {
                                        $('#tbodyActionOTHER').append($('<tr style = "background:#FFCCFF">')
                                       .append($('<td>').html(''))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 8 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                    }
                                }
                                else {
                                    $('#tbodyActionOTHER').append($('<tr>')
                                       .append($('<td>').attr('id', 'tdCaseNo' + key).html(value.CaseNumber))
                                       .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                                       .append($('<td>').html(value.ActionText))
                                       .append($('<td>').html(value.DoorColour))
                                       .append($('<td>').html(value.HouseType))
                                       .append($('<td>').html(value.DateActioned))
                                       .append($('<td>').html("" + value.Fees))
                                       .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescriptionOfficer(" + key + "," + 8 + ")'> ...More</label>" : value.Notes)))
                                       .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                                }
                                break;
                        }
                    });
                }
                else {
                    $('#tblbodyActionPaid').empty().html('No records found');
                    $('#tbodyActionPP').empty().html('No records found');
                    $('#tbodyActionReturned').empty().html('No records found');
                    $('#tbodyActionLeftLetter').empty().html('No records found');
                    $('#tbodyActionEnforcementStart').empty().html('No records found');
                    $('#tbodyActionRevisit').empty().html('No records found');
                    $('#tbodyActionEnforcementEnd').empty().html('No records found');

                    //$('#tbodypaidformanager').empty().html('No records found');
                    //$('#tbodypartpaidformanager').empty().html('No records found');
                    //$('#tbodyreturnedformanager').empty().html('No records found');
                    //$('#tbodyleftletterformanager').empty().html('No records found');
                    //$('#tbodyrevisitformanager').empty().html('No records found');

                    $('#tbodyActionTCG').empty().html('No records found');
                    $('#tbodyActionTCGPAID').empty().html('No records found');
                    $('#tbodyActionTCGPP').empty().html('No records found');
                    $('#tbodyActionUTTC').empty().html('No records found');
                    $('#tbodyActionDROPPED').empty().html('No records found');
                    $('#tbodyActionOTHER').empty().html('No records found');
                }
            }
        },
        error: function () {
            //alert('error');
        } // When Service call fails
    });
}

function caseaction(Flag) {
    $('#btnCaseActionBack,#btnCaseActionManager,#Notesformanager,#btnNotesBack,#divCaseActionsOfficerCount').hide();
    caseactionshowhidetabs();
    $('#divCaseActionsManagerCount').show();
    var ID = getCookie('OfficerID');
    CompanyID = getCookie('CompanyID');
    $('#lblCAManagerName,#lblCAOfficerName').html('');

    Flag += 1;
    if (CompanyID == 1) {
        switch (Flag) {
            case 1:
                $('#HCaseActionTitle').html('Paid');
                break;
            case 2:
                $('#HCaseActionTitle').html('Part Paid');
                break;
            case 3:
                $('#HCaseActionTitle').html('£ Collected');
                break;
            case 4:
                $('#HCaseActionTitle').html('Returned');
                break;
            case 5:
                $('#HCaseActionTitle').html('Left Letter');
                break;
            case 6:
                $('#HCaseActionTitle').html('Defendant Contact');
                break;
            case 7:
                $('#HCaseActionTitle').html('Deviation');
                break;
        }
    }
    else {
        switch (Flag) {
            case 1:
                $('#HCaseActionTitle').html('TCG');
                break;
            case 2:
                $('#HCaseActionTitle').html('TCG PAID');
                break;
            case 3:
                $('#HCaseActionTitle').html('TCG PP');
                break;
            case 4:
                $('#HCaseActionTitle').html('PAID');
                break;
            case 5:
                $('#HCaseActionTitle').html('PART PAID');
                break;
            case 6:
                $('#HCaseActionTitle').html('UTTC');
                break;
            case 7:
                $('#HCaseActionTitle').html('DROPPED');
                break;
            case 8:
                $('#HCaseActionTitle').html('OTHER');
                break;
        }
    }
    Type = "GET";

    if ($('#txtFromDateCA').val() == '') {
        var date = new Date();
        var TDate = $.datepicker.formatDate('dd/mm/yy', date);
        $('#txtFromDateCA').val(TDate);
    }
    var dateAr = $('#txtFromDateCA').val().split('/');
    var newDate = dateAr[2] + '/' + dateAr[1] + '/' + dateAr[0];

    if (CompanyID == 1 && Flag == 7) {
        var inputParams = "/GetDeviationDashboardForManager?ActionText=" + $('#HCaseActionTitle').html() + "&FromDate="
              + newDate + "&ToDate=" + newDate + "&CompanyID=" + getCookie('CompanyID') + "&ManagerID=" + 0 + "&GroupID=0" + "&" + getCookie('BrowserId');//+ "&ClientId=" + (getCookie('ClientId') == undefined ? 0 : getCookie('ClientId'))

    }
    else {
        var inputParams = "/GetCaseActionsDashboardForManager?ActionText=" + $('#HCaseActionTitle').html() + "&FromDate="
                + newDate + "&ToDate=" + newDate + "&CompanyID=" + getCookie('CompanyID') + "&ManagerID=" + 0 + "&GroupID=0" + "&" + getCookie('BrowserId');//+ "&ClientId=" + (getCookie('ClientId') == undefined ? 0 : getCookie('ClientId'))
    }

    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;
    //CallService();
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
            $('#imgLoader1').show();
        },
        complete: function () {
            $('#imgLoader1').fadeOut(1000);//.hide();
        },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {
                    result = JSON.parse(result);
                    $('#tbodyCaseActionsManagerCount').empty();
                    $.each(result, function (key, value) {
                        $('#tbodyCaseActionsManagerCount')
                            .append($('<tr>').append($('<td>').append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.ManagerId).hide())
                            .append($('<a>').attr('onclick', 'GetCaseActionsOfficerCount(' + Flag + ',' + $.trim(value.ManagerId) + ',' + 0 + ',' + "'" + value.ManagerName + "'" + ')')
                                .attr('href', '#' + $('#HCaseActionTitle').html().toString().toLowerCase().replace(" ", "")).html(value.ManagerName)))
                            .append($('<td>').html(value.CaseCnt)))
                    });
                }
                else {
                    $('#tbodyCaseActionsManagerCount').empty().html('No records found');
                }
            }
            else {
                $('#tbodyCaseActionsManagerCount').empty().html('No records found');
            }
        },
        error: function () {
            //alert('error');
        }
    });
    if (IsManager == 0) {
        $('#divCaseActionsOfficerCount').hide();
        $('#divCaseActionsManagerCount').show();
    }
    else {
        // fill offier calle
        GetCaseActionsOfficerCount(Flag, getCookie('ManagerID'), 0, getCookie('MgrName'));
        $('#divCaseActionsOfficerCount').show();
        $('#divCaseActionsManagerCount').hide();
    }
}

function GetCaseActionsOfficerCount(Flag, MgrID, GrpID, MgrName) {
    //  alert('off' + MgrID + ' G : ' + GrpID);
    $('#btnCaseActionBack,#Notesformanager,#btnNotesBack,#divCaseActionsManagerCount').hide();
    caseactionshowhidetabs();
    $('#divCaseActionsOfficerCount').show();
    if (!(getCookie('IsManagerSelect') == '' || getCookie('IsManagerSelect') == undefined))
        $('#btnCaseActionManager').hide();
    else
        $('#btnCaseActionManager').show();
    var ID = getCookie('OfficerID');
    CompanyID = getCookie('CompanyID');
    $('#lblCAManagerName').html(' - ' + MgrName);

    //Flag += 1;
    if (CompanyID == 1) {
        switch (Flag) {
            case 1:
                $('#HCaseActionTitle').html('Paid');
                break;
            case 2:
                $('#HCaseActionTitle').html('Part Paid');
                break;
            case 3:
                $('#HCaseActionTitle').html('£ Collected');
                break;
            case 4:
                $('#HCaseActionTitle').html('Returned');
                break;
            case 5:
                $('#HCaseActionTitle').html('Left Letter');
                break;
            case 6:
                $('#HCaseActionTitle').html('Defendant Contact');
                break;
            case 7:
                $('#HCaseActionTitle').html('Deviation');
                break;
        }
    }
    else {
        switch (Flag) {
            case 1:
                $('#HCaseActionTitle').html('TCG');
                break;
            case 2:
                $('#HCaseActionTitle').html('TCG PAID');
                break;
            case 3:
                $('#HCaseActionTitle').html('TCG PP');
                break;
            case 4:
                $('#HCaseActionTitle').html('PAID');
                break;
            case 5:
                $('#HCaseActionTitle').html('PART PAID');
                break;
            case 6:
                $('#HCaseActionTitle').html('UTTC');
                break;
            case 7:
                $('#HCaseActionTitle').html('DROPPED');
                break;
            case 8:
                $('#HCaseActionTitle').html('OTHER');
                break;
        }
    }
    Type = "GET";

    if ($('#txtFromDateCA').val() == '') {
        var date = new Date();
        var TDate = $.datepicker.formatDate('dd/mm/yy', date);
        $('#txtFromDateCA').val(TDate);
    }
    var dateAr = $('#txtFromDateCA').val().split('/');
    var newDate = dateAr[2] + '/' + dateAr[1] + '/' + dateAr[0];


    if (CompanyID == 1 && Flag == 7) {
        FillDeviation(MgrID, newDate, newDate, 0, GrpID);
        $('#tblCA').hide();
        $('#tblDeviation').show();
        //var inputParams = "/GetDeviationDashboardForManager?ActionText=" + $('#HCaseActionTitle').html() + "&FromDate="
        //   + newDate + "&ToDate=" + newDate + "&CompanyID=" + getCookie('CompanyID') + "&ManagerID=" + MgrID + "&GroupID=" + GrpID + "&" + getCookie('BrowserId');//+ "&ClientId=" + (getCookie('ClientId') == undefined ? 0 : getCookie('ClientId'))
    }
    else {
        var inputParams = "/GetCaseActionsDashboardForManager?ActionText=" + $('#HCaseActionTitle').html() + "&FromDate="
            + newDate + "&ToDate=" + newDate + "&CompanyID=" + getCookie('CompanyID') + "&ManagerID=" + MgrID + "&GroupID=" + GrpID + "&" + getCookie('BrowserId');//+ "&ClientId=" + (getCookie('ClientId') == undefined ? 0 : getCookie('ClientId')) 
        Url = serviceUrl + inputParams;
        DataType = "jsonp"; ProcessData = false;
        //CallService();
        $.ajax({
            type: Type,
            url: Url, // Location of the service
            contentType: ContentType, // content type sent to server
            dataType: DataType, //Expected data format from server       
            processdata: ProcessData, //True or False      
            async: true,
            timeout: 20000,
            beforeSend: function () {
                $('#imgLoader1').show();
            },
            complete: function () {
                $('#imgLoader1').fadeOut(1000);//.hide();
            },
            success: function (result) {//On Successfull service call  
                if (result != undefined) {
                    if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {
                        result = JSON.parse(result);
                        $('#tbodyCaseActionsOfficerCount').empty();
                        $.each(result, function (key, value) {
                            $('#tbodyCaseActionsOfficerCount')
                                .append($('<tr>').append($('<td>').append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.OfficerID).hide())
                                .append($('<a>').attr('onclick', 'clickoncaseactionmanager(' + Flag + ',' + key + ',' + $.trim(value.OfficerID) + ",'" + value.OfficerName + "','" + newDate + "','" + newDate + "'" + ')')
                                    .attr('href', '#' + $('#HCaseActionTitle').html().toString().toLowerCase().replace(" ", "")).html(value.OfficerName)))
                                .append($('<td>').html(value.CaseCnt)))
                        });
                    }
                    else {
                        $('#tbodyCaseActionsOfficerCount').empty().html('No records found');
                    }
                }
                else {
                    $('#tbodyCaseActionsOfficerCount').empty().html('No records found');
                }
            },
            error: function () {
                //alert('error');
            }
        });
    }

}

function clickoncaseactionmanager(caseid, key, OfficerID, OfficerName, FromDate, ToDate) {
    caseactionshowhidetabs();
    if (getCookie('CompanyID') == 1 && caseid == 7) {
        FillDeviationDetailForManager($('#HCaseActionManagerID' + key).html(), FromDate, ToDate, OfficerID, '0');
    }
    else {
        FillCaseDetailsActionForManager($('#HCaseActionManagerID' + key).html(), FromDate, ToDate, OfficerID, '0');
    }
    $('#HCaseActionNavigation').show();
    $('#btnCaseActionBack').show();
    $('#divCaseActionsManagerCount,#divCaseActionsOfficerCount,#btnCaseActionManager').hide();
    $('#DRankingOfficer').hide();

    //TODO: Rank more
    $('#DRankMorePaid,#DRankMorePaidOfficer,#btnPaidOfficerBack').hide();
    $('#DRankMoreReturned,#DRankMoreReturnedOfficer,#btnReturnedOfficerBack').hide();
    $('#DRankMorePP,#DRankMorePPOfficer,#btnPPOfficerBack').hide();

    $('#lblCAOfficerName,#lblRankOfficerName,#lblRankPaidOfficer,#lblRankReturnedOfficer,#lblRankPPOfficer').html(' - ' + OfficerName);
    $('#tblRankOfficerReturn,#tblRankOfficerPartPaid,#tblRankOfficerPaid,#btnRankOfficerBack').hide();
    if (getCookie('CompanyID') == 1) {
        switch (caseid) {
            case 1:
                $('#lblRankStatus').html('Ranking - Paid');
                $('#paidformanager,#DRankingCase,#tblRankOfficerPaid,#DRankMorePaidCaseList,#btnPaidCaseBack').show();
                $('#HCaseActionTitle').html('Paid');
                break;
            case 2:
                $('#lblRankStatus').html('Ranking - Part Paid');
                $('#partpaidformanager,#DRankingCase,#tblRankOfficerPartPaid,#DRankMorePPCaseList,#btnPPCaseBack').show();
                $('#HCaseActionTitle').html('Part Paid');
                break;
            case 3:
                $('#revisitformanager').show();
                $('#HCaseActionTitle').html('£ Collected');
                break;
            case 4:
                $('#lblRankStatus').html('Ranking - Returned');
                $('#returnedformanager,#DRankingCase,#tblRankOfficerReturn,#DRankMoreReturnedCaseList,#btnReturnedCaseBack').show();
                $('#HCaseActionTitle').html('Returned');
                break;
            case 5:
                $('#leftletterformanager').show();
                $('#HCaseActionTitle').html('Left Letter');
                break;
            case 7:
                $('#deviatedformanager').show();
                $('#HCaseActionTitle').html('Deviation');
                break;
        }
    }
    else {
        switch (caseid) {
            case 1:
                $('#tcgformanager').show();
                $('#HCaseActionTitle').html('TCG');
                break;
            case 2:
                $('#tcgpaidformanager').show();
                $('#HCaseActionTitle').html('TCG PAID');
                break;
            case 3:
                $('#tcgppformanager').show();
                $('#HCaseActionTitle').html('TCG PP');
                break;
            case 4:
                $('#paidformanager').show();
                $('#HCaseActionTitle').html('PAID');
                break;
            case 5:
                $('#partpaidformanager').show();
                $('#HCaseActionTitle').html('PART PAID');
                break;
            case 6:
                $('#uttcformanager').show();
                $('#HCaseActionTitle').html('UTTU');
                break;
            case 7:
                $('#droppedformanager').show();
                $('#HCaseActionTitle').html('DROPPED');
                break;
            case 8:
                $('#otherformanager').show();
                $('#HCaseActionTitle').html('OTHER');
                break;
        }
    }
}

function clickoncaseactionofficer(caseid, key, OfficerID, OfficerName, FromDate, ToDate) {
    caseactionshowhidetabs();
    if (getCookie('CompanyID') == 1 && caseid == 7) {
        FillDeviationDetailForManager($('#HCaseActionManagerID' + key).html(), FromDate, ToDate, OfficerID, '0');
    }
    else {
        FillCaseDetailsActionForManager($('#HCaseActionManagerID' + key).html(), FromDate, ToDate, OfficerID, '0');
    }
    $('#HCaseActionNavigation').show();
    $('#btnCaseActionBack').show();
    $('#divCaseActionsManagerCount,#divCaseActionsOfficerCount,#btnCaseActionManager').hide();
    $('#DRankingOfficer').hide();

    //TODO: Rank more
    $('#DRankMorePaid,#DRankMorePaidOfficer,#btnPaidOfficerBack').hide();
    $('#DRankMoreReturned,#DRankMoreReturnedOfficer,#btnReturnedOfficerBack').hide();
    $('#DRankMorePP,#DRankMorePPOfficer,#btnPPOfficerBack').hide();

    $('#lblCAOfficerName,#lblRankOfficerName,#lblRankPaidOfficer,#lblRankReturnedOfficer,#lblRankPPOfficer').html(' - ' + OfficerName);
    $('#tblRankOfficerReturn,#tblRankOfficerPartPaid,#tblRankOfficerPaid,#btnRankOfficerBack').hide();
    if (getCookie('CompanyID') == 1) {
        switch (caseid) {
            case 1:
                $('#lblRankStatus').html('Ranking - Paid');
                $('#paidformanager,#DRankingCase,#tblRankOfficerPaid,#DRankMorePaidCaseList,#btnPaidOfficerBack').show();
                $('#HCaseActionTitle').html('Paid');
                break;
            case 2:
                $('#lblRankStatus').html('Ranking - Part Paid');
                $('#partpaidformanager,#DRankingCase,#tblRankOfficerPartPaid,#DRankMorePPCaseList,#btnPPOfficerBack').show();
                $('#HCaseActionTitle').html('Part Paid');
                break;
            case 3:
                $('#revisitformanager').show();
                $('#HCaseActionTitle').html('£ Collected');
                break;
            case 4:
                $('#lblRankStatus').html('Ranking - Returned');
                $('#returnedformanager,#DRankingCase,#tblRankOfficerReturn,#DRankMoreReturnedCaseList,#btnReturnedOfficerBack').show();
                $('#HCaseActionTitle').html('Returned');
                break;
            case 5:
                $('#leftletterformanager').show();
                $('#HCaseActionTitle').html('Left Letter');
                break;
            case 7:
                $('#deviatedformanager').show();
                $('#HCaseActionTitle').html('Deviation');
                break;
        }
    }
    else {
        switch (caseid) {
            case 1:
                $('#tcgformanager').show();
                $('#HCaseActionTitle').html('TCG');
                break;
            case 2:
                $('#tcgpaidformanager').show();
                $('#HCaseActionTitle').html('TCG PAID');
                break;
            case 3:
                $('#tcgppformanager').show();
                $('#HCaseActionTitle').html('TCG PP');
                break;
            case 4:
                $('#paidformanager').show();
                $('#HCaseActionTitle').html('PAID');
                break;
            case 5:
                $('#partpaidformanager').show();
                $('#HCaseActionTitle').html('PART PAID');
                break;
            case 6:
                $('#uttcformanager').show();
                $('#HCaseActionTitle').html('UTTU');
                break;
            case 7:
                $('#droppedformanager').show();
                $('#HCaseActionTitle').html('DROPPED');
                break;
            case 8:
                $('#otherformanager').show();
                $('#HCaseActionTitle').html('OTHER');
                break;
        }
    }
}

function ReturnAction() {
    // window.open(ReturnActionUrl, '_blank');
    window.location.href = ReturnActionUrl;
}

//==========================================================================================================================================
//TODO : Deviation detail function

function FillDeviation(ManagerID, FromDate, ToDate, OfficerID, GroupID) {
    Type = "GET";
    serviceTypeLogin = "getcaseactions";

    if (OfficerID == undefined) OfficerID = 0;

    if (GroupID == undefined) GroupID = 0;

    if (FromDate == '') {
        var date = new Date();
        var TDate = $.datepicker.formatDate('dd/mm/yy', date);
        FromDate = TDate;
    }
    if (ToDate == '') {
        var date1 = new Date();
        var T1Date = $.datepicker.formatDate('dd/mm/yy', date1);
        ToDate = T1Date;
    }

    var dateAr = FromDate.split('/');
    var newDate = dateAr[2] + '/' + dateAr[1] + '/' + dateAr[0];
    FromDate = newDate;

    var dateTr = ToDate.split('/');
    var newToDate = dateTr[2] + '/' + dateTr[1] + '/' + dateTr[0];
    ToDate = newToDate;

    var inputParams = "/GetDeviation_Dashboard?ManagerID=" + ManagerID + "&FromDate=" + FromDate + "&ToDate=" +
        ToDate + "&OfficerID=" + OfficerID + "&GroupID=" + GroupID + "&" + getCookie('BrowserId');// "&ClientId=" + (getCookie('ClientId') == undefined ? 0 : getCookie('ClientId')) +
    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () { },
        complete: function () { },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {
                    result = JSON.parse(result);
                    $('#tblbodyDeviation').empty();

                    $.each(result, function (key, value) {
                        $('#tblbodyDeviation').append($('<tr>')
                       .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                       .append($('<td>').append($('<a id="AActionModal' + key + '">').attr('data-toggle', 'modal')
                                        .attr('href', '#divCaseActionModal').attr('onclick', 'GetDeviationDbForManager(' + "'" + value.ActionText + "'" + ')').html(value.ActionText)))
                       .append($('<td>').html(value.Case)))
                    });
                }
            }
        },
        error: function () {
        } // When Service call fails
    });

}

function GetDeviationDbForManager(ActionText) {
    $('#btnCaseActionBack,#btnCaseActionManager,#Notesformanager,#btnNotesBack,#divCaseActionsOfficerCount,#deviatedformanager').hide();
    caseactionshowhidetabs();
    $('#divCaseActionsManagerCount').show();
    $('#lblCAManagerName,#lblCAOfficerName').html('');

    var Flag;
    switch (ActionText) {
        case 'Paid':
            Flag = 1;
            break;
        case 'Part Paid':
            Flag = 2;
            break;
        case 'Returned':
            Flag = 4;
            break;
        case 'Left Letter':
            Flag = 5;
            break;
    }
    $('#divCaseActionsOfficerCount,#DeviatedGPS').hide();
    $('#divCaseActionsManagerCount').show();
    $('#HCaseActionTitle').html(ActionText);

    Type = "GET";

    if ($('#txtFromDateCA').val() == '') {
        var date = new Date();
        var TDate = $.datepicker.formatDate('dd/mm/yy', date);
        $('#txtFromDateCA').val(TDate);
    }
    var dateAr = $('#txtFromDateCA').val().split('/');
    var newDate = dateAr[2] + '/' + dateAr[1] + '/' + dateAr[0];

    var inputParams = "/GetDeviationDashboardForManager?ActionText=" + ActionText + "&FromDate="
          + newDate + "&ToDate=" + newDate + "&CompanyID=" + getCookie('CompanyID') + "&ManagerID=" + 0 + "&GroupID=0" + "&" + getCookie('BrowserId');//+ "&ClientId=" + (getCookie('ClientId') == undefined ? 0 : getCookie('ClientId'))

    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;
    //CallService();
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
            $('#imgLoader1').show();
        },
        complete: function () {
            $('#imgLoader1').fadeOut(1000);//.hide();
        },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {
                    result = JSON.parse(result);
                    $('#tbodyCaseActionsManagerCount').empty();
                    $.each(result, function (key, value) {
                        $('#tbodyCaseActionsManagerCount')
                            .append($('<tr>').append($('<td>').append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.ManagerId).hide())
                            .append($('<a>').attr('onclick', 'GetDeviationDbForOfficer(' + Flag + ',' + $.trim(value.ManagerId) + ',' + 0 + ',' + "'" + value.ManagerName + "'" + ')')
                                .attr('href', '#' + $('#HCaseActionTitle').html().toString().toLowerCase().replace(" ", "")).html(value.ManagerName)))
                            .append($('<td>').html(value.CaseCnt)))
                    });
                }
                else {
                    $('#tbodyCaseActionsManagerCount').empty().html('No records found');
                }
            }
            else {
                $('#tbodyCaseActionsManagerCount').empty().html('No records found');
            }
        },
        error: function () {
            //alert('error');
        }
    });
}

function GetDeviationDbForOfficer(Flag, MgrID, GrpID, MgrName) {
    $('#btnCaseActionBack,#Notesformanager,#btnNotesBack,#divCaseActionsManagerCount,#btnDeviationMapBack').hide();
    caseactionshowhidetabs();
    $('#divCaseActionsOfficerCount').show();
    if (!(getCookie('IsManagerSelect') == '' || getCookie('IsManagerSelect') == undefined))
        $('#btnCaseActionManager').hide();
    else
        $('#btnCaseActionManager').show();
    $('#lblCAManagerName').html(' - ' + MgrName);

    switch (Flag) {
        case 1:
            $('#HCaseActionTitle').html('Paid');
            break;
        case 2:
            $('#HCaseActionTitle').html('Part Paid');
            break;
        case 3:
            $('#HCaseActionTitle').html('£ Collected');
            break;
        case 4:
            $('#HCaseActionTitle').html('Returned');
            break;
        case 5:
            $('#HCaseActionTitle').html('Left Letter');
            break;
        case 6:
            $('#HCaseActionTitle').html('Defendant Contact');
            break;
        case 7:
            $('#HCaseActionTitle').html('Deviation');
            break;
    }

    Type = "GET";

    if ($('#txtFromDateCA').val() == '') {
        var date = new Date();
        var TDate = $.datepicker.formatDate('dd/mm/yy', date);
        $('#txtFromDateCA').val(TDate);
    }
    var dateAr = $('#txtFromDateCA').val().split('/');
    var newDate = dateAr[2] + '/' + dateAr[1] + '/' + dateAr[0];

    var inputParams = "/GetDeviationDashboardForManager?ActionText=" + $('#HCaseActionTitle').html() + "&FromDate="
       + newDate + "&ToDate=" + newDate + "&CompanyID=" + getCookie('CompanyID') + "&ManagerID=" + MgrID + "&GroupID=" + GrpID + "&" + getCookie('BrowserId');//+ "&ClientId=" + (getCookie('ClientId') == undefined ? 0 : getCookie('ClientId'))

    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;
    //CallService();
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
            $('#imgLoader1').show();
        },
        complete: function () {
            $('#imgLoader1').fadeOut(1000);//.hide();
        },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {
                    result = JSON.parse(result);
                    $('#tbodyCaseActionsOfficerCount').empty();
                    $.each(result, function (key, value) {
                        $('#tbodyCaseActionsOfficerCount')
                            .append($('<tr>').append($('<td>').append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.OfficerID).hide())
                            //.append($('<a>').attr('onclick', 'clickoncaseactionmanager(' + Flag + ',' + key + ',' + $.trim(value.OfficerID) + ",'" + value.OfficerName + "','" + newDate + "','" + newDate + "'" + ')')
                            .append($('<a>').attr('onclick', 'FillDeviationDetailForManager(' + "'" + value.OfficerName + "'," + $('#HCaseActionManagerID' + key).html() + ",'" + newDate + "','" + newDate + "'," + $.trim(value.OfficerID) + ',' + 0 + ",'" + $('#HCaseActionTitle').html() + "'" + ')')
                                .attr('href', '#' + $('#HCaseActionTitle').html().toString().toLowerCase().replace(" ", "")).html(value.OfficerName)))
                            .append($('<td>').html(value.CaseCnt)))
                    });
                }
                else {
                    $('#tbodyCaseActionsOfficerCount').empty().html('No records found');
                }
            }
            else {
                $('#tbodyCaseActionsOfficerCount').empty().html('No records found');
            }
        },
        error: function () {
            //alert('error');
        }
    });
}

function FillDeviationDetailForManager(OfficerName, ManagerID, FromDate, ToDate, OfficerID, GroupID, ActionText) {
    $('#HCaseActionNavigation').show();
    $('#btnCaseActionBack').show();
    $('#divCaseActionsManagerCount,#divCaseActionsOfficerCount,#btnCaseActionManager').hide();
    $('#DRankingOfficer').hide();

    //TODO: Rank more
    $('#DRankMorePaid,#DRankMorePaidOfficer,#btnPaidOfficerBack').hide();
    $('#DRankMoreReturned,#DRankMoreReturnedOfficer,#btnReturnedOfficerBack').hide();
    $('#DRankMorePP,#DRankMorePPOfficer,#btnPPOfficerBack').hide();

    $('#lblCAOfficerName,#lblRankOfficerName,#lblRankPaidOfficer,#lblRankReturnedOfficer,#lblRankPPOfficer').html(' - ' + OfficerName);
    $('#tblRankOfficerReturn,#tblRankOfficerPartPaid,#tblRankOfficerPaid,#btnRankOfficerBack').hide();
    $('#deviatedformanager').show();

    Type = "GET";
    serviceTypeLogin = "getcaseactions";

    if (OfficerID == undefined) OfficerID = 0;
    if (GroupID == undefined) GroupID = 0;

    var inputParams = "/GetDeviationDetail_Dashboard?ManagerID=" + $.trim(ManagerID) + "&FromDate=" + FromDate + "&ToDate=" + ToDate + "&OfficerID=" + OfficerID
        + "&GroupID=" + GroupID + "&ActionText=" + ActionText + "&" + getCookie('BrowserId');//+ "&ActionText=" + ActionText;// + "&ClientId=" + (getCookie('ClientId') == undefined ? 0 : getCookie('ClientId'))

    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
            $('#imgLoader1').show();
        },
        complete: function () {
            $('#imgLoader1').fadeOut(1000);//.hide();
        },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {
                    result = JSON.parse(result);
                    $('#tbodydeviatedformanager').empty();
                    $.each(result, function (key, value) {
                        $('#tbodydeviatedformanager').append($('<tr style = "background:#66FF99">')
                           .append($('<td>').html(value.CaseNumber))
                           .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                           .append($('<td>').html(value.DateActioned))
                           //.append($('<td>').html(value.GPSLatitude + ',' + value.GPSLongitude))
                           .append($('<td>').html(value.DeviatedDistance))
                           .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                           .append($('<td>')
                                .append($('<img src="images/DBMap_24_24.png" style="cursor:pointer;" onclick="GetDeviationMap(' + value.DefenderGPSLatitude + ',' + value.DefenderGPSLongitude + ',' + "'" + value.Dhtml + "'," + value.GPSLatitude + ',' + value.GPSLongitude + ",'" + value.html + "'" + ')" >')))//.html(value.DefenderGPSLatitude + ',' + value.DefenderGPSLongitude)
                           .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                    });
                }
                else {
                    $('#tbodydeviatedformanager').empty().html('No records found');
                }
            }
        },
        error: function () {
            //alert('error');
        } // When Service call fails
    });
}

function GetDeviationMap(DefGPSLat, DefGPSLon, Dhtml, GPSLat, GPSLon, html) {
    $('#DeviatedGPS,#btnDeviationMapBack').show();
    $('#deviatedformanager,#btnCaseActionBack').hide();

    center = new google.maps.LatLng(DefGPSLat, DefGPSLon);
    options = {
        'zoom': 9,
        'center': center,
        'mapTypeId': google.maps.MapTypeId.ROADMAP
    };

    DMap = new google.maps.Map(document.getElementById("DMap"), options);
    infowindow = new google.maps.InfoWindow();

    latLng = new google.maps.LatLng(DefGPSLat, DefGPSLon);
    marker = new google.maps.Marker({
        'position': latLng,
        'map': DMap,
        'icon': 'images/map-icon-red.png'
    });
    markers.push(marker);
    google.maps.event.addListener(markers[0], 'click', function (e) {
        //     alert('click');
        infowindow.setContent(Dhtml);
        infowindow.open(DMap, this);
    });

    latLng = new google.maps.LatLng(GPSLat, GPSLon);
    marker = new google.maps.Marker({
        'position': latLng,
        'map': DMap,
        'icon': 'images/map-icon-green.png'
    });
    markers.push(marker);
    google.maps.event.addListener(markers[1], 'click', function (e) {
        infowindow.setContent(html);
        infowindow.open(DMap, this);
    });
}

function FillDeviationDetail(ManagerID, FromDate, ToDate, OfficerID, GroupID) {
    Type = "GET";
    serviceTypeLogin = "getcaseactions";

    if (OfficerID == undefined) OfficerID = 0;
    if (GroupID == undefined) GroupID = 0;


    if (FromDate == '') {
        var date = new Date();
        var TDate = $.datepicker.formatDate('dd/mm/yy', date);
        FromDate = TDate;
    }
    if (ToDate == '') {
        var date1 = new Date();
        var T1Date = $.datepicker.formatDate('dd/mm/yy', date1);
        ToDate = T1Date;
    }

    var dateAr = FromDate.split('/');
    var newDate = dateAr[2] + '/' + dateAr[1] + '/' + dateAr[0];
    FromDate = newDate;

    var dateTr = ToDate.split('/');
    var newToDate = dateTr[2] + '/' + dateTr[1] + '/' + dateTr[0];
    ToDate = newToDate;

    var inputParams = "/GetDeviationDetail_Dashboard?ManagerID=" + $.trim(ManagerID) + "&FromDate=" + FromDate + "&ToDate=" + ToDate + "&OfficerID=" + OfficerID
        + "&GroupID=" + GroupID + "&" + getCookie('BrowserId');//+ "&ActionText=" + ActionText;//+ "&ClientId=" + (getCookie('ClientId') == undefined ? 0 : getCookie('ClientId'))

    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;
    //CallService();
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
        },
        complete: function () {

        },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {

                    result = JSON.parse(result);
                    $('#tbodydeviated').empty();
                    $.each(result, function (key, value) {
                        $('#tbodydeviated').append($('<tr style = "background:#66FF99">')
                           .append($('<td>').html(value.CaseNumber))
                           .append($('<td>').attr('id', 'tdCaseOfficer' + key).html(value.Officer))
                          // .append($('<td colspan="2" style="text-align:center;">').html(value.DoorColour))
                           .append($('<td>').html(value.DateActioned))
                          // .append($('<td>').html("" + value.Fees))
                           .append($('<td>').html(value.DefenderGPSLatitude))
                           .append($('<td>').html(value.DefenderGPSLongitude))
                           .append($('<td>').html(value.DeviatedDistance))
                           .append($('<td>').html((value.Notes == null && value.Notes == '') ? '' : (value.Notes.length > 20 ? value.Notes.substr(0, 20) + "<label style='cursor:pointer; color:Blue' onclick='FullDescription(" + key + "," + 1 + ")'> ...More</label>" : value.Notes)))
                           .append($('<td style="display:none">').attr('id', 'tdFullDesc' + key).html(value.Notes)))
                    });
                }
                else {
                    $('#tbodydeviated').empty().html('No records found');
                }
            }
        },
        error: function () {
            //alert('error');
        } // When Service call fails
    });
}

//==========================================================================================================================================
//TODO : Ranking function

function FillRankings(ManagerID, FromDate, ToDate, ActionText, GroupID) {
    Type = "GET";
    serviceTypeLogin = "getrankings";

    if (GroupID == undefined) GroupID = 0;
    if (FromDate == '') {
        var date = new Date();
        var TDate = $.datepicker.formatDate('dd/mm/yy', date);
        FromDate = TDate;
    }
    if (ToDate == '') {
        var date1 = new Date();
        var T1Date = $.datepicker.formatDate('dd/mm/yy', date1);
        ToDate = T1Date;
    }

    var dateAr = FromDate.split('/');
    var newDate = dateAr[2] + '/' + dateAr[1] + '/' + dateAr[0];
    FromDate = newDate;

    var dateTr = ToDate.split('/');
    var newToDate = dateTr[2] + '/' + dateTr[1] + '/' + dateTr[0];
    ToDate = newToDate;

    var inputParams = "/GetTop3Rank_Dashboard?ManagerID=" + ManagerID + "&FromDate=" + FromDate + "&ToDate=" + ToDate
        + "&ActionText=" + ActionText + "&GroupID=" + GroupID + "&" + getCookie('BrowserId');//+ "&ClientId=" + (getCookie('ClientId') == undefined ? 0 : getCookie('ClientId'))

    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () { },
        complete: function () { },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                switch (ActionText) {
                    case 'Paid':
                        $('#ARAPaidMore').hide();
                        $('#tblbodyPaid').empty();
                        $('#tblbodyPaid').append($('<tr>').append($('<td colspan="2">').html('No rankings for paid actions.')));
                        break;
                    case 'Returned':
                        $('#ARankingPaidMore').hide();
                        $('#tblbodyReturned').empty();
                        $('#tblbodyReturned').append($('<tr>').append($('<td colspan="2">').html('No rankings for returned actions.')));
                        break;
                    case 'Part Paid':
                        $('#ARankingPartPaidMore').hide();
                        $('#tblbodyPP').empty();
                        $('#tblbodyPP').append($('<tr>').append($('<td colspan="2">').html('No rankings for part paid actions.')));
                        break;
                }

                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {
                    result = JSON.parse(result);
                    switch (ActionText) {
                        case 'Paid':
                            $('#ARAPaidMore').show();
                            $('#tblbodyPaid').empty();
                            if ((getCookie('RoleType') == 1) && (LinkForManager == 0)) {
                                $.each(result, function (key, value) {
                                    $('#tblbodyPaid').append($('<tr>')
                                            .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                            .append($('<td>').append($('<label>').attr('style', 'display:none;').attr('id', 'lblPaidOfficerID' + key).html(value.Officer))
                                            .append($('<a>').attr('data-toggle', 'modal').attr('href', '#divOfficerRank')
                                                .attr('onclick', 'FillOfficerRankOnClick(1,' + value.Officer + ',1,"' + value.OfficerName + '")').html(value.OfficerName)))
                                            .append($('<td>')
                                                .append($('<a>').attr('data-toggle', 'modal').attr('onclick', 'clickoncaseactionmanager(' + 1 + ',' + key + ',' + 0 + ",'" + value.OfficerName + "','" + FromDate + "','" + ToDate + "'" + ')')
                                                    .attr('href', '#divOfficerRank').html(value.Case))
                                                .append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.Officer).hide())))
                                });
                            }
                            else {
                                $.each(result, function (key, value) {
                                    $('#tblbodyPaid').append($('<tr>')
                                            .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                            .append($('<td>')
                                                .append($('<label>').attr('style', 'display:none;')
                                                .attr('id', 'lblPaidOfficerID' + key).html(value.Officer))
                                            .append($('<a>').attr('data-toggle', 'modal')
                                        .attr('onclick', 'FillCaseActionDetailsOnClick(1,' + value.Officer + ',' + "'" + value.OfficerName + "'" + ',0)')
                                                    .attr('href', '#paid').html(value.OfficerName)))
                                            .append($('<td>').html(value.Case)
                                                .append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.Officer).hide())))
                                });
                            }
                            break;
                        case 'Returned':
                            $('#ARankingPaidMore').show();
                            $('#tblbodyReturned').empty();
                            if ((getCookie('RoleType') == 1) && (LinkForManager == 0)) {
                                $.each(result, function (key, value) {
                                    $('#tblbodyReturned').append($('<tr>')
                                            .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                            .append($('<td>').append($('<label>').attr('style', 'display:none;').attr('id', 'lblReturnOfficerID' + key).html(value.Officer))
                                            .append($('<a>').attr('data-toggle', 'modal').attr('href', '#divOfficerRank')
                                                .attr('onclick', 'FillOfficerRankOnClick(2,' + value.Officer + ',1,"' + value.OfficerName + '")').html(value.OfficerName)))
                                            .append($('<td>')
                                                .append($('<a>').attr('data-toggle', 'modal').attr('onclick', 'clickoncaseactionmanager(' + 3 + ',' + key + ',' + 0 + ",'" + value.OfficerName + "','" + FromDate + "','" + ToDate + "'" + ')')
                                                    .attr('href', '#divOfficerRank').html(value.Case))
                                                .append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.Officer).hide())))
                                });
                            }
                            else {
                                $.each(result, function (key, value) {
                                    $('#tblbodyReturned').append($('<tr>')
                                          .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                          .append($('<td>')
                                              .append($('<label>').attr('style', 'display:none;')
                                              .attr('id', 'lblPaidOfficerID' + key).html(value.Officer))
                                          .append($('<a>').attr('data-toggle', 'modal')
                                             .attr('onclick', 'FillCaseActionDetailsOnClick(2,' + value.Officer + ',' + "'" + value.OfficerName + "'" + ',0)')
                                                  .attr('href', '#returned').html(value.OfficerName)))
                                          .append($('<td>').html(value.Case)
                                              .append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.Officer).hide())))
                                });
                            }
                            break;
                        case 'Part Paid':
                            $('#ARankingPartPaidMore').show();
                            $('#tblbodyPP').empty();
                            if ((getCookie('RoleType') == 1) && (LinkForManager == 0)) {
                                $.each(result, function (key, value) {
                                    $('#tblbodyPP').append($('<tr>')
                                            .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                            .append($('<td>').append($('<label>').attr('style', 'display:none;').attr('id', 'lblPartPaidOfficerID' + key).html(value.Officer))
                                            .append($('<a>').attr('data-toggle', 'modal').attr('href', '#divOfficerRank')
                                                .attr('onclick', 'FillOfficerRankOnClick(3,' + value.Officer + ',1,"' + value.OfficerName + '")').html(value.OfficerName)))
                                            .append($('<td>')
                                                .append($('<a>').attr('data-toggle', 'modal').attr('onclick', 'clickoncaseactionmanager(' + 2 + ',' + key + ',' + 0 + ",'" + value.OfficerName + "','" + FromDate + "','" + ToDate + "'" + ')')
                                                    .attr('href', '#divOfficerRank').html(value.Case))
                                                .append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.Officer).hide())))
                                });
                            }
                            else {
                                $.each(result, function (key, value) {
                                    $('#tblbodyPP').append($('<tr>')
                                           .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                           .append($('<td>')
                                               .append($('<label>').attr('style', 'display:none;')
                                               .attr('id', 'lblPaidOfficerID' + key).html(value.Officer))
                                           .append($('<a>').attr('data-toggle', 'modal')
                                        .attr('onclick', 'FillCaseActionDetailsOnClick(3,' + value.Officer + ',' + "'" + value.OfficerName + "'" + ',0)')
                                                   .attr('href', '#partpaid').html(value.OfficerName)))
                                           .append($('<td>').html(value.Case)
                                               .append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.Officer).hide())))
                                });
                            }
                            break;
                    }
                }
            }
        },
        error: function () {
            //alert('error');
        } // When Service call fails
    });
}

function FillOfficerRankOnClick(Action, key, Condition, MgrName) {
    //Action 1-Paid , 2-Returned, 3-Part Paid, 4-Paid(default)
    if (Condition == 1) {
        $('#DRankingCase,#btnRankOfficerBack').hide();
        $('#DRankingOfficer').show();
        $('#lblRankOfficerName').html('');
    }
    else {
        //TODO: Ranking more actions 
        switch (Action) {
            case 1://Paid
                $('#DRankMorePaid,#DRankMorePaidCaseList').hide();
                $('#DRankMorePaidOfficer,#btnPaidOfficerBack').show();
                $('#lblRankPaidManager').html(' - ' + MgrName);
                break;
            case 2://Returned
                $('#DRankMoreReturned,#DRankMoreReturnedCaseList').hide();
                $('#DRankMoreReturnedOfficer,#btnReturnedOfficerBack').show();
                $('#lblRankReturnedManager').html(' - ' + MgrName);
                break;
            case 3://Part Paid
                $('#DRankMorePP,#DRankMorePPCaseList').hide();
                $('#DRankMorePPOfficer,#btnPPOfficerBack').show();
                $('#lblRankPPManager').html(' - ' + MgrName);
                break;
        }
    }

    switch (Action) {
        case 1:
            FillRankingsForManagers(key, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Paid', 0);//$('#lblPaidOfficerID' + key).html()
            break;
        case 2:
            FillRankingsForManagers(key, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Returned', 0);//$('#lblReturnOfficerID' + key).html()
            break;
        case 3:
            FillRankingsForManagers(key, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Part Paid', 0);//$('#lblPartPaidOfficerID' + key).html()
            break;
        case 4:
            FillRankingsForManagers(key, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Paid', 0);//$('#lblDefaultPaidOfficerID' + key).html()
            break;
    }
}

function FillRankingsForManagers(ManagerID, FromDate, ToDate, ActionText, GroupID) {
    Type = "GET";
    serviceTypeLogin = "getrankings";
    if (GroupID == undefined) GroupID = 0;

    if (FromDate == '') {
        var date = new Date();
        var TDate = $.datepicker.formatDate('dd/mm/yy', date);
        FromDate = TDate;
    }
    if (ToDate == '') {
        var date1 = new Date();
        var T1Date = $.datepicker.formatDate('dd/mm/yy', date1);
        ToDate = T1Date;
    }

    var dateAr = FromDate.split('/');
    var newDate = dateAr[2] + '/' + dateAr[1] + '/' + dateAr[0];
    FromDate = newDate;

    var dateTr = ToDate.split('/');
    var newToDate = dateTr[2] + '/' + dateTr[1] + '/' + dateTr[0];
    ToDate = newToDate;

    var inputParams = "/GetTop3Rank_Dashboard?ManagerID=" + ManagerID + "&FromDate=" + FromDate + "&ToDate=" + ToDate
        + "&ActionText=" + ActionText + "&GroupID=" + GroupID + "&" + getCookie('BrowserId');//+ "&ClientId=" + (getCookie('ClientId') == undefined ? 0 : getCookie('ClientId'))

    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;
    //CallService();
    var Flag;
    switch (ActionText) {
        case 'Paid':
            $('#lblRankStatus').html('Ranking - Paid');
            $('#HCaseActionTitle').html('Paid');
            Flag = 1;
            break;
        case 'Returned':
            $('#lblRankStatus').html('Ranking - Returned');
            $('#HCaseActionTitle').html('Returned');
            Flag = 4;
            break;
        case 'Part Paid':
            $('#lblRankStatus').html('Ranking - Part Paid');
            $('#HCaseActionTitle').html('Part Paid');
            Flag = 2;
            break;
    }

    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () { },
        complete: function () { },
        success: function (result) {//On Successfull service call  

            if (result != undefined) {
                $('#tbodyOfficerRank').empty();
                //TODO: Rank more
                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {
                    result = JSON.parse(result);
                    $.each(result, function (key, value) {
                        switch (ActionText) {
                            case 'Paid':
                                $('#tbodyRankMorePaidOfficer').empty();
                                $('#tbodyRankMorePaidOfficer').append($('<tr>').append($('<td>')
                                             .append($('<a>').attr('onclick', 'clickoncaseactionmanager(' + Flag + ',' + key + ',' + $.trim(value.Officer) + ",'" + value.OfficerName + "','" + FromDate + "','" + ToDate + "'" + ')')
                                                .attr('href', '#' + $('#HCaseActionTitle').html().toString().toLowerCase().replace(" ", "")).html(value.OfficerName))
                                             .append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.Officer).hide()))
                                             .append($('<td>').html(value.Case))
                                             .append($('<td>').html(value.ActionText)))

                                $('#tbodyOfficerRank').append($('<tr>').append($('<td>')
                                       .append($('<a>').attr('onclick', 'FillCaseActionDetailsOnClick(1,' + value.Officer + ',' + "'" + value.OfficerName + "'" + ',0)').attr('href', '#paid').html(value.OfficerName))
                                       .append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.Officer).hide()))
                                       .append($('<td>').html(value.Case))
                                       .append($('<td>').html(value.ActionText)))
                                break;
                            case 'Returned':
                                $('#tbodyRankMoreReturnedOfficer').empty();
                                $('#tbodyRankMoreReturnedOfficer').append($('<tr>').append($('<td>')
                                             .append($('<a>').attr('onclick', 'clickoncaseactionmanager(' + Flag + ',' + key + ',' + $.trim(value.Officer) + ",'" + value.OfficerName + "','" + FromDate + "','" + ToDate + "'" + ')')
                                                .attr('href', '#' + $('#HCaseActionTitle').html().toString().toLowerCase().replace(" ", "")).html(value.OfficerName))
                                             .append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.Officer).hide()))
                                             .append($('<td>').html(value.Case))
                                             .append($('<td>').html(value.ActionText)))

                                $('#tbodyOfficerRank').append($('<tr>').append($('<td>')
                                      .append($('<a>').attr('onclick', 'FillCaseActionDetailsOnClick(2,' + value.Officer + ',' + "'" + value.OfficerName + "'" + ',0)').attr('href', '#paid').html(value.OfficerName))
                                      .append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.Officer).hide()))
                                      .append($('<td>').html(value.Case))
                                      .append($('<td>').html(value.ActionText)))
                                break;
                            case 'Part Paid':
                                $('#tbodyRankMorePPOfficer').empty();
                                $('#tbodyRankMorePPOfficer').append($('<tr>').append($('<td>')
                                            .append($('<a>').attr('onclick', 'clickoncaseactionmanager(' + Flag + ',' + key + ',' + $.trim(value.Officer) + ",'" + value.OfficerName + "','" + FromDate + "','" + ToDate + "'" + ')')
                                               .attr('href', '#' + $('#HCaseActionTitle').html().toString().toLowerCase().replace(" ", "")).html(value.OfficerName))
                                            .append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.Officer).hide()))
                                            .append($('<td>').html(value.Case))
                                            .append($('<td>').html(value.ActionText)))

                                $('#tbodyOfficerRank').append($('<tr>').append($('<td>')
                                      .append($('<a>').attr('onclick', 'FillCaseActionDetailsOnClick(3,' + value.Officer + ',' + "'" + value.OfficerName + "'" + ',0)').attr('href', '#paid').html(value.OfficerName))
                                      .append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.Officer).hide()))
                                      .append($('<td>').html(value.Case))
                                      .append($('<td>').html(value.ActionText)))
                                break;
                        }


                    });
                }
                else {
                    $('#tbodyOfficerRank').append($('<tr>').append($('<td colspan="3">').html('No Rankings for "' + ActionText + '" actions.')));
                    //TODO: Rank more
                    switch (ActionText) {
                        case 'Paid':
                            $('#tbodyRankMorePaidOfficer').append($('<tr>').append($('<td colspan="3">').html('No Rankings for "' + ActionText + '" actions.')));
                            break;
                        case 'Returned':
                            $('#tbodyRankMoreReturnedOfficer').append($('<tr>').append($('<td colspan="3">').html('No Rankings for "' + ActionText + '" actions.')));
                            break;
                        case 'Part Paid':
                            $('#tbodyRankMorePPOfficer').append($('<tr>').append($('<td colspan="3">').html('No Rankings for "' + ActionText + '" actions.')));
                            break;
                    }
                }
            }
        },
        error: function () {
            //alert('error');
        } // When Service call fails
    });
}

// TODO : Ranking more details ( Manager list)
function FillRankingsMore(ManagerID, FromDate, ToDate, ActionText, GroupID) {
    if (getCookie('IsManagerSelect') == 0 || getCookie('IsManagerSelect') == '' || getCookie('IsManagerSelect') == undefined) {
        switch (ActionText) {
            case 'Paid':
                $('#DRankMorePaidOfficer,#DRankMorePaidCaseList').hide();
                $('#DRankMorePaid').show();
                break;
            case 'Returned':
                $('#DRankMoreReturnedOfficer,#DRankMoreReturnedCaseList').hide();
                $('#DRankMoreReturned').show();
                break;
            case 'Part Paid':
                $('#DRankMorePPOfficer,#DRankMorePPCaseList').hide();
                $('#DRankMorePP').show();
                break;
        }
    }
    else {
        switch (ActionText) {
            case 'Paid':
                $('#DRankMorePaid,#DRankMorePaidCaseList').hide();
                $('#DRankMorePaidOfficer').show();
                break;
            case 'Returned':
                $('#DRankMoreReturned,#DRankMoreReturnedCaseList').hide();
                $('#DRankMoreReturnedOfficer').show();
                break;
            case 'Part Paid':
                $('#DRankMorePP,#DRankMorePPCaseList').hide();
                $('#DRankMorePPOfficer').show();
                break;
        }
    }

    Type = "GET";
    serviceTypeLogin = "getrankings";
    var ShowAll = true;
    if (GroupID == undefined) GroupID = 0;

    if (FromDate == '') {
        var date = new Date();
        var TDate = $.datepicker.formatDate('dd/mm/yy', date);
        FromDate = TDate;
    }
    if (ToDate == '') {
        var date1 = new Date();
        var T1Date = $.datepicker.formatDate('dd/mm/yy', date1);
        ToDate = T1Date;
    }

    var dateAr = FromDate.split('/');
    var newDate = dateAr[2] + '/' + dateAr[1] + '/' + dateAr[0];
    FromDate = newDate;

    var dateTr = ToDate.split('/');
    var newToDate = dateTr[2] + '/' + dateTr[1] + '/' + dateTr[0];
    ToDate = newToDate;

    var inputParams = "/GetTop3Rank_Dashboard?ManagerID=" + ManagerID + "&FromDate=" + FromDate + "&ToDate=" + ToDate
        + "&ActionText=" + ActionText + "&GroupID=" + GroupID + "&ShowAll=" + ShowAll + "&" + getCookie('BrowserId');//+ "&ClientId=" + (getCookie('ClientId') == undefined ? 0 : getCookie('ClientId')) 

    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;

    //CallService();

    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () { },
        complete: function () {
            //  FillRankingsForManagers(ManagerID, FromDate, ToDate, ActionText, GroupID);
        },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {
                    result = JSON.parse(result);
                    switch (ActionText) {
                        case 'Paid':
                            $('#tblbodyRAMore').empty();
                            $.each(result, function (key, value) {
                                $('#tblbodyRAMore')
                                        .append($('<tr>')
                                            .append($('<td>').append($('<label>').attr('style', 'display:none;').attr('id', 'lblPaidOfficerID' + key).html(value.Officer))
                                                .append($('<a>').attr('data-toggle', 'modal').attr('href', '#')
                                                .attr('onclick', 'FillOfficerRankOnClick(1,' + value.Officer + ',2,"' + value.OfficerName + '")').html(value.OfficerName)))
                                            .append($('<td>')
                                            .append($('<a>').attr('data-toggle', 'modal').attr('onclick', 'clickoncaseactionofficer(' + 1 + ',' + key + ',' + 0 + ",'" + value.OfficerName + "','" + FromDate + "','" + ToDate + "'" + ')')
                                                    .attr('href', '#').html(value.Case))
                                                .append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.Officer).hide())))
                            });
                            break;
                        case 'Returned':
                            $('#tblbodyReturnedMore').empty();
                            $.each(result, function (key, value) {
                                $('#tblbodyReturnedMore')
                                        .append($('<tr>')
                                            .append($('<td>').append($('<label>').attr('style', 'display:none;').attr('id', 'lblPaidOfficerID' + key).html(value.Officer))
                                                .append($('<a>').attr('data-toggle', 'modal').attr('href', '#')
                                                .attr('onclick', 'FillOfficerRankOnClick(2,' + value.Officer + ',2,"' + value.OfficerName + '")').html(value.OfficerName)))
                                            .append($('<td>')
                                                .append($('<a>').attr('data-toggle', 'modal').attr('onclick', 'clickoncaseactionofficer(' + 3 + ',' + key + ',' + 0 + ",'" + value.OfficerName + "','" + FromDate + "','" + ToDate + "'" + ')')
                                                    .attr('href', '#').html(value.Case))
                                                .append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.Officer).hide())))
                            });
                            break;
                        case 'Part Paid':
                            $('#tblbodyPPRanking').empty();
                            $.each(result, function (key, value) {
                                $('#tblbodyPPRanking')
                                         .append($('<tr>')
                                             .append($('<td>').append($('<label>').attr('style', 'display:none;').attr('id', 'lblPaidOfficerID' + key).html(value.Officer))
                                                 .append($('<a>').attr('data-toggle', 'modal').attr('href', '#')
                                                 .attr('onclick', 'FillOfficerRankOnClick(3,' + value.Officer + ',2,"' + value.OfficerName + '")').html(value.OfficerName)))
                                             .append($('<td>')
                                                .append($('<a>').attr('data-toggle', 'modal').attr('onclick', 'clickoncaseactionofficer(' + 2 + ',' + key + ',' + 0 + ",'" + value.OfficerName + "','" + FromDate + "','" + ToDate + "'" + ')')
                                                    .attr('href', '#').html(value.Case))
                                                .append($('<label>').attr('id', 'HCaseActionManagerID' + key).html(value.Officer).hide())))
                            });
                            break;
                    }
                }
                else {
                    switch (ActionText) {
                        case 'Paid':
                            $('#tblbodyRAMore').empty();
                            $('#tblbodyRAMore').append($('<tr>').append($('<td colspan="2">').html('No rankings for paid actions.')));
                            break;
                        case 'Returned':
                            $('#tblbodyReturnedMore').empty();
                            $('#tblbodyReturnedMore').append($('<tr>').append($('<td colspan="2">').html('No rankings for returned actions.')));
                            break;
                        case 'Part Paid':
                            $('#tblbodyPPRanking').empty();
                            $('#tblbodyPPRanking').append($('<tr>').append($('<td colspan="2">').html('No rankings for rart paid actions.')));
                            break;
                    }
                }
            }
        },
        error: function () {
            //alert('error');
        } // When Service call fails
    });


}

//==========================================================================================================================================
//TODO : Last location function

function FillLastKnownLocation(ManagerID, FromDate, ToDate, OfficerID, GroupID, ShowAll) {
    //  alert('call: Mgr -' + ManagerID + ' From -' + FromDate + ' | ' + ToDate + 'OFF -' + OfficerID + 'Group -' + GroupID + 'Show ' + ShowAll);
    if (ManagerID == getCookie('OfficerID') && OfficerID == undefined) ManagerID = getCookie('ManagerID');

    MID = ManagerID;

    if (OfficerID == undefined) {
        if ($('#lblSelectedOfficerID').html() == undefined || $.trim($('#lblSelectedOfficerID').html()) == '') {
            OfficerID = 0;
        }
        else {
            OfficerID = $('#lblSelectedOfficerID').html();
        }
    }
    OID = OfficerID;
    GID = GroupID;
    if (FromDate == '') {
        var date = new Date();
        var TDate = $.datepicker.formatDate('dd/mm/yy', date);
        FromDate = TDate;
    }

    var dateAr = FromDate.split('/');
    var newDate = dateAr[2] + '/' + dateAr[1] + '/' + dateAr[0];
    FromDate = newDate;

    if (OfficerID == undefined) OfficerID = 0;
    if (GroupID == undefined) GroupID = 0;
    if (ShowAll == undefined) ShowAll = 0;

    FD = FromDate;
    TD = FromDate;
    //$('#iframeGMAP').attr('src', 'GMaps.html?mID=' + ManagerID + "&fDate=" + FromDate + "&tDate=" + FromDate +
    //    "&oID=" + OfficerID + "&Map=Medium" + "&gID=" + GroupID + "&ShowAll=" + ShowAll);

    //$('#iframePopupMap').attr('src', 'GMaps.html?mID=' + ManagerID + "&fDate=" + FromDate + "&tDate="
    //    + FromDate + "&oID=" + OfficerID + "&Map=Large" + "&gID=" + GroupID + "&ShowAll=" + ShowAll);
    //MapHeartBeat(OfficerID, FromDate, FromDate);
    MapFillLastKnownLocation(MID, FD, TD, OID, GID, 0);
    clearInterval(MapIntervalStatus);
    MapIntervalStatus = setInterval(function () {
        MapFillLastKnownLocation(MID, FD, TD, OID, GID, 0);
    }, 180000);

}

function FillAllLocation(Condition) {
    setCookie('Map', 'All', 1);

    if (OID == undefined) OID = 0;
    if (GID == undefined) GID = 0;

    if (Condition == 1) {// Medium 
        //$('#iframeGMAP').attr('src', 'Cluster.html?mID=' + MID + "&fDate=" + FD + "&tDate=" + TD +
        //"&oID=" + OID + "&Map=Medium" + "&gID=" + GID + "&ShowAll=1");
        MapFillLastKnownLocation(MID, FD, FD, OID, GID, 1);

        clearInterval(MapIntervalStatus);
        MapIntervalStatus = setInterval(function () {
            MapFillLastKnownLocation(MID, FD, FD, OID, GID, 1);
        }, 180000);
    }
    else {//large map
        $('#iframePopupMap').attr('src', 'Cluster.html?mID=' + MID + "&fDate=" + FD + "&tDate="
            + TD + "&oID=" + OID + "&Map=Large" + "&gID=" + GID + "&ShowAll=1");
    }
}

//==========================================================================================================================================
//TODO : Map

function MapHeartBeat(oID, fDate, tDate) {
    $('#map_canvas,#DLocationHBMap').hide();
    $('#DHeartBeatMap').show();
    Type = "GET";
    DataType = "jsonp";
    ProcessData = false;
    Url = serviceUrl + "/GetHeartBeats?OfficerID=" + oID + "&FromDate=" + fDate + "&ToDate=" + fDate + "&" + getCookie('BrowserId');
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () { },
        complete: function () {
            //   setCookie('Map', '', 1);
        },
        success: function (result) {//On Successfull service call 
            $('#lblDisplay').html('').hide();
            markers = [];
            if (result != undefined && result != '[]' && result != '[{"Result":"NoRecords"}]') {
                result = JSON.parse(result);
                center = new google.maps.LatLng(51.699467, 0.109348);
                options = {
                    'zoom': 5,
                    'center': center
                    //  'mapTypeId': google.maps.MapTypeId.ROADMAP
                };

                map = new google.maps.Map(document.getElementById("DHeartBeatMap"), options);
                infowindow = new google.maps.InfoWindow();

                $.each(result, function (key, value) {
                    latLng = new google.maps.LatLng(value.latitude, value.longitude);
                    marker = new google.maps.Marker({
                        'position': latLng,
                        'map': map,
                        'icon': "images/map-icon-pink.png"
                    });

                    markers.push(marker);

                    google.maps.event.addListener(markers[key], 'click', function (e) {
                        infowindow.setContent(value.html);
                        infowindow.open(map, this);
                    });
                });
                //markerCluster = new MarkerClusterer(map, markers, {
                //    maxZoom: 20,
                //    gridSize: 100
                //});
            }
            else {
                $('#lblDisplay').html('No activity recorded for today!').show();
                center = new google.maps.LatLng(52.8849565, -1.9770329);
                options = {
                    'zoom': 5,
                    'center': center,
                    'mapTypeId': google.maps.MapTypeId.ROADMAP
                };
                map = new google.maps.Map(document.getElementById("DHeartBeatMap"), options);
            }
        },
        error: function () {
            //alert('error');
        }
    });
}

function GetLocationHB(fDate, tDate, oID) {
    $('#map_canvas,#DHeartBeatMap').hide();
    $('#DLocationHBMap').show();
    Type = "GET";
    DataType = "jsonp";
    ProcessData = false;
    Url = serviceUrl + "/GetLocationHB?OfficerID=" + oID + "&FromDate=" + fDate + "&ToDate=" + tDate + "&" + getCookie('BrowserId');//+ "&ClientId=" + (getCookie('ClientId') == undefined ? 0 : getCookie('ClientId'))
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () { },
        complete: function () {
            // setCookie('Map', '', 1);
        },
        success: function (result) {//On Successfull service call 
            $('#lblDisplay').html('').hide();
            markers = [];
            if (result != undefined && result != '[]' && result != '[{"Result":"NoRecords"}]') {
                result = JSON.parse(result);
                var iconImg1;
                center = new google.maps.LatLng(51.699467, 0.109348);
                options = {
                    'zoom': 5,
                    'center': center,
                    'mapTypeId': google.maps.MapTypeId.ROADMAP
                };

                map = new google.maps.Map(document.getElementById("DLocationHBMap"), options);
                infowindow = new google.maps.InfoWindow();

                $.each(result, function (key, value) {
                    switch (value.IconType) {
                        case 1: // Paid , Part paid
                            iconImg1 = "images/map-icon-yellow.png";
                            break;
                        case 2:  // Returned
                            iconImg1 = "images/map-icon-red.png";
                            break;
                        case 3:  // Revisit
                            iconImg1 = "images/map-icon-green.png";
                            break;
                        case 5: // HeartBeat
                            iconImg1 = "images/map-icon-pink.png";
                            break;
                        default:
                            iconImg1 = "images/map-icon-white.png";
                    }
                    latLng = new google.maps.LatLng(value.latitude, value.longitude);
                    marker = new google.maps.Marker({
                        'position': latLng,
                        'map': map,
                        'icon': iconImg1
                    });

                    markers.push(marker);

                    google.maps.event.addListener(markers[key], 'click', function (e) {
                        infowindow.setContent(value.html);
                        infowindow.open(map, this);
                    });
                });
                //markerCluster = new MarkerClusterer(map, markers, {
                //    maxZoom: 20,
                //    gridSize: 100
                //});
            }
            else {
                $('#lblDisplay').html('No activity recorded for today!').show();
                center = new google.maps.LatLng(52.8849565, -1.9770329);
                options = {
                    'zoom': 6,
                    'center': center,
                    'mapTypeId': google.maps.MapTypeId.ROADMAP
                };
                map = new google.maps.Map(document.getElementById("DLocationHBMap"), options);
            }
        },
        error: function () {
            //alert('error');
        }
    });
}

function MapFillLastKnownLocation(mID, fDate, tDate, oID, gID, ShowAll) {
    $('#DHeartBeatMap,#DLocationHBMap').hide();
    $('#map_canvas').show();
    Type = "GET";
    if (gID == undefined) gID = 0;
    DataType = "jsonp";
    ProcessData = false;
    Url = serviceUrl + "/GetLastLocation_Dashboard?ManagerID=" + mID + "&FromDate=" + fDate + "&ToDate=" + tDate
        + "&OfficerID=" + $.trim(oID) + "&GroupID=" + $.trim(gID) + "&ShowAll=" + ShowAll
         + "&" + getCookie('BrowserId');//+ "&ClientId=" + (getCookie('ClientId') == undefined ? 0 : getCookie('ClientId'))

    //if showall =1  /// Cluster for all click
    if (ShowAll == 1) {
        $.ajax({
            type: Type,
            url: Url, // Location of the service
            contentType: ContentType, // content type sent to server
            dataType: DataType, //Expected data format from server       
            processdata: ProcessData, //True or False      
            async: true,
            timeout: 20000,
            beforeSend: function () { },
            complete: function () {
                // setCookie('Map', '', 1);
            },
            success: function (result) {//On Successfull service call 
                markers = [];
                if (result != undefined && result != '[]' && result != '[{"Result":"NoRecords"}]') {
                    $('#lblDisplay').html('').hide();
                    result = JSON.parse(result);
                    var iconImg1;
                    //center = new google.maps.LatLng(51.699467, 0.109348);
                    //options = {
                    //    'zoom': 5,
                    center = new google.maps.LatLng(52.8849565, -1.9770329);
                    options = {
                        'zoom': 6,
                        'center': center,
                        'mapTypeId': google.maps.MapTypeId.ROADMAP
                    };

                    map = new google.maps.Map(document.getElementById("map_canvas"), options);
                    infowindow = new google.maps.InfoWindow();

                    $.each(result, function (key, value) {
                        switch (value.IconType) {
                            case 1: // UTTC
                                iconImg1 = "images/map-icon-yellow.png";
                                break;
                            case 2:  // Returned, DROPPED
                                iconImg1 = "images/map-icon-red.png";
                                break;
                            case 3:  // Revisit,TCG
                                iconImg1 = "images/map-icon-floresent.png";
                                break;
                            case 5:  //  Paid , Part paid , TCG PP,TCG Paid 
                                iconImg1 = "images/map-icon-orange.png";
                                break;
                            case 6:  // Login
                                iconImg1 = "images/map-icon-blue.png";
                                break;
                            case 7:  // ARR
                                iconImg1 = "images/map-icon-navyblue.png";
                                break;
                            case 8:  // DEP
                                iconImg1 = "images/map-icon-lightvio.png";
                                break;
                            case 9:  // Logout
                                iconImg1 = "images/map-icon-grey.png";
                                break;
                            default:
                                iconImg1 = "images/map-icon-white.png";
                        }
                        Latitude[key] = value.latitude;
                        Longitude[key] = value.longitude;
                        if (Latitude[key] != pLatMap && Longitude[key] != pLongMap) {
                            Icon[key] = iconImg1;
                            Content[key] = value.html;
                            pContentkey = key;
                        }
                        else {
                            Content[pContentkey] = Content[pContentkey] + '<br><hr>' + value.html;
                        }
                        pLatMap = value.latitude;
                        pLongMap = value.longitude;

                    });

                    for (var i = 0; i < result.length; i++) {
                        if (Latitude[i] != pLatitude && Longitude[i] != pLongitude) {
                            latLng = new google.maps.LatLng(Latitude[i], Longitude[i]);
                            marker = new google.maps.Marker({
                                'position': latLng,
                                'map': map,
                                'icon': Icon[i]
                            });
                            addInfoWindow(marker, Content[i]);
                            markers.push(marker);
                        }
                        pLatitude = Latitude[i];
                        pLongitude = Longitude[i];
                    }
                    markerCluster = new MarkerClusterer(map, markers);
                }
                else {
                    $('#lblDisplay').html('No activity recorded for today!').show();
                    center = new google.maps.LatLng(52.8849565, -1.9770329);
                    options = {
                        'zoom': 6,
                        'center': center,
                        'mapTypeId': google.maps.MapTypeId.ROADMAP
                    };
                    map = new google.maps.Map(document.getElementById("map_canvas"), options);
                }
            },
            error: function (ex) {
            }
        });
    }
    else { // Last location
        $.ajax({
            type: Type,
            url: Url, // Location of the service
            contentType: ContentType, // content type sent to server
            dataType: DataType, //Expected data format from server       
            processdata: ProcessData, //True or False      
            async: true,
            timeout: 20000,
            beforeSend: function () { },
            complete: function () {
                //   setCookie('Map', '', 1);
            },
            success: function (result) {//On Successfull service call 
                markers = [];
                if (result != undefined && result != '[]' && result != '[{"Result":"NoRecords"}]') {
                    $('#lblDisplay').html('').hide();
                    result = JSON.parse(result);
                    var iconImg1;
                    //center = new google.maps.LatLng(51.699467, 0.109348);
                    //options = {
                    //    'zoom': 5,
                    center = new google.maps.LatLng(52.8849565, -1.9770329);
                    options = {
                        'zoom': 6,
                        'center': center,
                        'mapTypeId': google.maps.MapTypeId.ROADMAP
                    };

                    map = new google.maps.Map(document.getElementById("map_canvas"), options);
                    infowindow = new google.maps.InfoWindow();

                    $.each(result, function (key, value) {
                        switch (value.IconType) {
                            case 1: // UTTC
                                iconImg1 = "images/map-icon-yellow.png";
                                break;
                            case 2:  // Returned, DROPPED
                                iconImg1 = "images/map-icon-red.png";
                                break;
                            case 3:  // Revisit,TCG
                                iconImg1 = "images/map-icon-floresent.png";
                                break;
                            case 5:  //  Paid , Part paid , TCG PP,TCG Paid 
                                iconImg1 = "images/map-icon-orange.png";
                                break;
                            case 6:  // Login
                                iconImg1 = "images/map-icon-blue.png";
                                break;
                            case 7:  // ARR
                                iconImg1 = "images/map-icon-navyblue.png";
                                break;
                            case 8:  // DEP
                                iconImg1 = "images/map-icon-lightvio.png";
                                break;
                            case 9:  // Logout
                                iconImg1 = "images/map-icon-grey.png";
                                break;
                            default:
                                iconImg1 = "images/map-icon-white.png";
                        }
                        Latitude[key] = value.latitude;
                        Longitude[key] = value.longitude;
                        if (Latitude[key] != pLatMap && Longitude[key] != pLongMap) {
                            Icon[key] = iconImg1;
                            Content[key] = value.html;
                            pContentkey = key;
                        }
                        else {
                            Content[pContentkey] = Content[pContentkey] + '<br><hr>' + value.html;
                        }
                        pLatMap = value.latitude;
                        pLongMap = value.longitude;

                    });

                    for (var i = 0; i < result.length; i++) {
                        if (Latitude[i] != pLatitude && Longitude[i] != pLongitude) {
                            latLng = new google.maps.LatLng(Latitude[i], Longitude[i]);
                            marker = new google.maps.Marker({
                                'position': latLng,
                                'map': map,
                                'icon': Icon[i]
                            });
                            addInfoWindow(marker, Content[i]);
                            markers.push(marker);
                        }
                        pLatitude = Latitude[i];
                        pLongitude = Longitude[i];
                    }
                    //   markerCluster = new MarkerClusterer(map, markers);
                }
                else {
                    $('#lblDisplay').html('No activity recorded for today!').show();
                    center = new google.maps.LatLng(52.8849565, -1.9770329);
                    options = {
                        'zoom': 6,
                        'center': center,
                        'mapTypeId': google.maps.MapTypeId.ROADMAP
                    };
                    map = new google.maps.Map(document.getElementById("map_canvas"), options);
                }
            },
            error: function (ex) {
            }
        });
    }
}

function addInfoWindow(marker, message) {
    var infoWindow = new google.maps.InfoWindow({
        content: message
    });
    google.maps.event.addListener(marker, 'click', function () {
        infoWindow.open(map, marker);
    });
}

//==========================================================================================================================================
//TODO : Warrent matching function

function FillWMA(ManagerID, FromDate, ToDate, OfficerID, GroupID) {
    if (OfficerID == undefined) OfficerID = 0;
    if (GroupID == undefined) GroupID = 0;
    Type = "GET";

    if (FromDate == '') {
        var date = new Date();
        var TDate = $.datepicker.formatDate('dd/mm/yy', date);
        FromDate = TDate;
    }
    var dateAr = FromDate.split('/');
    var newDate = dateAr[2] + '/' + dateAr[1] + '/' + dateAr[0];
    FromDate = newDate;

    var inputParams = "/GetWMA_Dashboard?ManagerID=" + ManagerID + "&FromDate=" + FromDate
        + "&ToDate=" + FromDate + "&OfficerID=" + OfficerID + "&GroupID=" + GroupID + "&" + getCookie('BrowserId');

    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () { },
        complete: function () { },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {
                    result = JSON.parse(result);
                    $('#tbodyWMA').empty();
                    $('#tbodyWMAForOfficer').empty();
                    $.each(result, function (key, value) {
                        $('#tbodyWMA').append($('<tr>')
                              .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                              .append($('<td>').append($('<a>').attr('id', value.OfficerId).attr('data-toggle', 'modal')
                              .attr('href', '#windowTitleDialog').html(value.OfficerName))).append($('<td>').html(value.Activities)))

                        $('#' + value.OfficerId).click(function () {
                            $('#btnWMAOfficerBack,#btnBack').hide();
                            $('#lblOfficerDisplay,#lblSearchType').html('');
                            $('#lblWMAManager').html($(this).html());
                            if (!(getCookie('IsManagerSelect') == '' || getCookie('IsManagerSelect') == undefined))
                                FillWMADetailL1($(this).attr('id'), FromDate, ToDate);
                            else
                                FillWMAOfficers(value.OfficerId, FromDate, ToDate, OfficerID, GroupID)
                        });
                    });
                    $('#AMoreWMA').show();
                    //************
                }
                else {
                    if (getCookie('CompanyID') == 1) {
                        $('#tbodyWMA').empty();
                        $('#tblbodyWMASearchType').empty();//-- not comment
                    }

                    $('#tbodyWMAForOfficer').empty();
                    $('#AMoreWMA').hide();

                }
            }
        },
        error: function () {
            //alert('error');
        } // When Service call fails
    });
}

function FillWMAOfficers(ManagerID, FromDate, ToDate, OfficerID, GroupID) {
    if (OfficerID == undefined) OfficerID = 0;
    if (GroupID == undefined) GroupID = 0;
    Type = "GET";

    var inputParams = "/GetWMA_Dashboard?ManagerID=" + ManagerID + "&FromDate=" + FromDate
        + "&ToDate=" + FromDate + "&OfficerID=" + OfficerID + "&GroupID=" + GroupID + "&" + getCookie('BrowserId');

    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
            $('#show-search-details').hide();
            $('#tblOfficer').hide();
            $('#imgLoader3,#imgLoader4').show();
        },
        complete: function () {
            $('#imgLoader3,#imgLoader4').fadeOut(1000);
        },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {
                    result = JSON.parse(result);
                    $('#tbodyWMAForOfficer').empty();
                    $('#tbodyWMAOfficers').empty();
                    $('#tbodyWMAMoreOfficer').empty();

                    showhidetabs('tblWMAOfficers');
                    $('#lblWMAManager').show();

                    $.each(result, function (key, value) {
                        $('#tbodyWMAOfficers').append($('<tr>').append($('<td>').append($('<a style="cursor:pointer">').attr('id', value.OfficerId).attr('data-toggle', 'modal')
                        //.attr('href', '#windowTitleDialog')
                            .html(value.OfficerName))).append($('<td>').html(value.Activities)))

                        $('#tbodyWMAForOfficer').append($('<tr>').append($('<td>').append($('<label>').html(value.OfficerName)))
                            .append($('<td>').html(value.Activities)))

                        $('#tbodyWMAMoreOfficer').append($('<tr>').append($('<td>').append($('<a style="cursor:pointer">').attr('href', '#').attr('id', 'WMAMoreOfficer' + value.OfficerId)//.attr('data-toggle', 'modal')
                            .html(value.OfficerName))).append($('<td>').html(value.Activities)))


                        $('#' + value.OfficerId).click(function () {
                            $('#lblWMAManager').show();
                            $('#tblOfficer').show();
                            $('#lblOfficerDisplay').html(' - ' + $(this).html());
                            //    FillWMADetailL1($(this).attr('id'), FromDate, ToDate);
                            if (OfficerID > 0) {
                                FillWMADetailL3(OfficerID, FromDate, ToDate);
                            }
                            else {
                                FillWMADetailL1(value.OfficerId, FromDate, ToDate);
                            }
                        });
                        $('#WMAMoreOfficer' + value.OfficerId).click(function () {
                            $('#DWMAMoreOfficerCaseCount,#btnWMAMoreOfficer').show();
                            $('#DWMAMoreOfficer,#btnWMAMoreManager').hide();
                            $('#lblWMAMoreOfficerName').html(' - ' + $(this).html());
                            if (OfficerID > 0) {
                                FillWMADetailL3(OfficerID, FromDate, ToDate);
                            }
                            else {
                                FillWMADetailL1(value.OfficerId, FromDate, ToDate);
                            }
                        });
                    });
                }
                else {
                    if (getCookie('CompanyID') == 1) {
                        $('#tbodyWMA').empty();
                        $('#tblbodyWMASearchType').empty();//-- not comment
                    }
                    $('#tbodyWMAMoreOfficer').empty();
                    $('#tbodyWMAForOfficer').empty();
                    $('#AMoreWMA').hide();
                }
            }
        },
        error: function () {
            //alert('error');
        } // When Service call fails
    });
}

function FillWMAMore(ManagerID, FromDate, ToDate, OfficerID, GroupID) {
    if (OfficerID == undefined) OfficerID = 0;
    if (GroupID == undefined) GroupID = 0;
    var ShowAll = true;
    Type = "GET";

    if (FromDate == '') {
        var date = new Date();
        var TDate = $.datepicker.formatDate('dd/mm/yy', date);
        FromDate = TDate;
    }
    var dateAr = FromDate.split('/');
    var newDate = dateAr[2] + '/' + dateAr[1] + '/' + dateAr[0];
    FromDate = newDate;

    var inputParams = "/GetWMA_Dashboard?ManagerID=" + ManagerID + "&FromDate=" + FromDate
        + "&ToDate=" + FromDate + "&OfficerID=" + OfficerID + "&GroupID=" + GroupID + "&ShowAll=" + ShowAll + "&" + getCookie('BrowserId');
    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;

    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
            $('#imgLoader3').show();
        },
        complete: function () {
            $('#imgLoader3').fadeOut(1000);
        },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {
                    result = JSON.parse(result);
                    $('#tbodyWMAMore').empty();
                    $.each(result, function (key, value) {
                        $('#tbodyWMAMore').append($('<tr>').append($('<td>').append($('<a>').attr('id', 'AMoreWMA' + value.OfficerId).attr('data-toggle', 'modal')
                              .attr('href', '#').html(value.OfficerName)))//windowTitleDialog
                            .append($('<td>').html(value.Activities)))

                        if (!(getCookie('IsManagerSelect') == '' || getCookie('IsManagerSelect') == undefined))
                            FillWMAOfficers(getCookie('IsManagerSelect'), FromDate, ToDate, OfficerID, GroupID)

                        $('#AMoreWMA' + value.OfficerId).click(function () {
                            $('#DWMAMoreOfficer,#btnWMAMoreManager').show();
                            $('#DWMAMoreManager').hide();
                            $('#lblWMAMoreManagerName').html(' - ' + $(this).html());
                            if (!(getCookie('IsManagerSelect') == '' || getCookie('IsManagerSelect') == undefined))
                                FillWMADetailL1($(this).attr('id'), FromDate, ToDate);
                            else
                                FillWMAOfficers(value.OfficerId, FromDate, ToDate, OfficerID, GroupID)
                        });
                    });
                }
            }
            else {
                $('#tblbodyWMASearchType').empty();
            }
        },
        error: function () {
            //alert('error');
        } // When Service call fails
    });
}

function FillWMADetailL1(OfficerID, FromDate, ToDate) {
    Type = "GET";

    var inputParams = "/GetWMADetailL1_Dashboard?OfficerID=" + OfficerID + "&FromDate=" + FromDate + "&ToDate=" + FromDate + "&" + getCookie('BrowserId');
    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
            $('#imgLoader3,#imgLoader4').show();
        },
        complete: function () {
            $('#imgLoader3,#imgLoader4').fadeOut(1000);
        },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {
                    result = JSON.parse(result);

                    $('#tblbodySearchType').empty();
                    $('#tbodyWMAMoreOfficerCaseCount').empty();

                    showhidetabs('tblbodySearchType');
                    $('#lblSearchType').html('');
                    $('#btnBack,#btnWMAOfficerBack').hide();
                    $('#tblbodyWMASearchType').empty();
                    if (!(getCookie('IsManagerSelect') == '' || getCookie('IsManagerSelect') == undefined))
                        $('#btnWMAOfficerBack').hide();
                    else
                        $('#btnWMAOfficerBack').show();


                    $.each(result, function (key, value) {
                        $('#tblbodySearchType').append($('<tr>').append($('<td>').append($('<a style="cursor:pointer">').attr('id', value.LogType).html(value.LogType)))
                            .append($('<td>').html(value.Activities)))

                        $('#tbodyWMAMoreOfficerCaseCount').append($('<tr>').append($('<td>').append($('<a style="cursor:pointer">').attr('id', 'WMAMoreOfficerCase' + value.LogType)
                            .attr('href', '#').html(value.LogType)))
                          .append($('<td>').html(value.Activities)))

                        $('#tblbodyWMASearchType').append($('<tr>').append($('<td>').append($('<a style="cursor:pointer">').attr('id', 'A' + value.LogType)
                            .attr('data-toggle', 'modal').attr('href', '#windowTitleDialog').html(value.LogType))).append($('<td>').html(value.Activities)))

                        $('#A' + value.LogType).click(function () {
                            $('#lblSearchType').html(' - ' + $(this).html());
                            FillWMADetailL2(OfficerID, FromDate, ToDate, value.LogType);
                            showhidetabs('show-search-details');
                            $('#btnBack').show();
                            $('#btnWMAOfficerBack').hide();
                        });


                        $('#WMAMoreOfficerCase' + value.LogType).click(function () {
                            $('#lblWMAMoreOfficerCaseName').html(' - ' + $(this).html());
                            $('#DWMAMoreOfficerCase').show();
                            $('#DWMAMoreOfficerCaseCount').hide();
                            FillWMADetailL2(OfficerID, FromDate, ToDate, value.LogType);
                            //  showhidetabs('show-search-details');
                            $('#btnWMAMoreOfficerCase').show();
                            $('#btnWMAMoreOfficer').hide();
                        });

                        $('#' + value.LogType).click(function () {
                            $('#lblSearchType').html(' - ' + $(this).html());
                            FillWMADetailL2(OfficerID, FromDate, ToDate, value.LogType);
                            // alert('click' + FromDate + ' : ' + ToDate);
                            showhidetabs('show-search-details');
                            $('#btnBack').show();
                            $('#btnWMAOfficerBack').hide();
                        })
                    });
                }
            }

        },
        error: function () {
            //alert('error');
        } // When Service call fails
    });
}

function FillWMADetailL2(OfficerID, FromDate, ToDate, LogType) {
    Type = "GET";
    var inputParams = "/GetWMADetailL2_Dashboard?OfficerID=" + OfficerID + "&FromDate=" + FromDate + "&ToDate=" +
        FromDate + "&LogType=" + LogType + "&" + getCookie('BrowserId');

    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;

    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
            $('#tbodysearchdetails,#tbodyWMAMoreOfficerCase').empty();
            $('#imgLoader3,#imgLoader4').show();
        },
        complete: function () {
            $('#imgLoader3,#imgLoader4').fadeOut(1000);
        },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {
                    result = JSON.parse(result);
                    $('#tbodysearchdetails,#tbodyWMAMoreOfficerCase').empty();
                    $.each(result, function (key, value) {
                        $('#tbodysearchdetails,#tbodyWMAMoreOfficerCase').append($('<tr>')
                            .append($('<td>').html(value.SearchKey))
                            .append($('<td>').html(value.ResultReturned))
                            .append($('<td>').html(value.ActionedDate)))
                    });
                }
                else {
                    $('#tbodysearchdetails,#tbodyWMAMoreOfficerCase').append($('<tr>').append($('<td colspan="3">').html('No warrant matching activity found for ' + LogType)));
                }
            }
        },
        error: function () {
            //alert('error');
        } // When Service call fails
    });

}

function FillWMADetailL3(OfficerID, FromDate, ToDate) {
    Type = "GET";
    var inputParams = "/GetWMADetailL1_Dashboard?OfficerID=" + OfficerID + "&FromDate=" + FromDate + "&ToDate=" +
        FromDate + "&" + getCookie('BrowserId');
    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;

    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () { },
        complete: function () { },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {
                    result = JSON.parse(result);

                    $('#tblbodySearchType').empty();
                    showhidetabs('tblbodySearchType');
                    $('#lblSearchType').html('');
                    $('#btnBack').hide();
                    $('#tblbodyWMASearchType').empty();
                    $('#tblWMASearchType').show();
                    $('#tblWMA').hide();

                    $.each(result, function (key, value) {
                        $('#tblbodySearchType').append($('<tr>').append($('<td>').append($('<a>').attr('id', value.LogType).html(value.LogType)))
                            .append($('<td>').html(value.Activities)))

                        $('#tblbodyWMASearchType').append($('<tr>').append($('<td>').append($('<a>').attr('id', 'A' + value.LogType)
                            .attr('data-toggle', 'modal').attr('href', '#windowTitleDialog').html(value.LogType))).append($('<td>').html(value.Activities)))

                        $('#A' + value.LogType).click(function () {
                            $('#lblSearchType').html(' - ' + $(this).html());
                            FillWMADetailL2(OfficerID, FromDate, ToDate, value.LogType);
                            showhidetabs('show-search-details');
                            $('#btnBack').show();
                        });

                        $('#' + value.LogType).click(function () {
                            $('#lblSearchType').html(' - ' + $(this).html());
                            FillWMADetailL2(OfficerID, FromDate, ToDate, value.LogType);
                            showhidetabs('show-search-details');
                            $('#btnBack').show();
                        })
                    });
                }
            }
        },
        error: function () {
            //alert('error');
        } // When Service call fails
    });
}

//==========================================================================================================================================
//TODO : Working Hours function

function GetWorkingHours(OfficerID) {
    Type = "GET";
    var inputParams = "/GetOfficersWorkingHours?OfficerID=" + OfficerID + "&" + getCookie('BrowserId');
    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () { },
        complete: function () { },
        success: function (result) {//On Successfull service call
            if (result != undefined) {
                if (result != "[]" && result != null) {
                    result = JSON.parse(result);
                    $('#tbodyWorkingHours').empty();
                    $.each(result, function (key, value) {
                        if (value.CallId == '9999' || value.CallId == '99991') {
                            $('#thWorkingName').html('Manager Name');
                            $('#tbodyWorkingHours').append($('<tr>').append($('<td>').append($('<a>').attr('data-toggle', 'modal').attr('href', '#DivWorkingHoursMore')
                                                                        .attr('onclick', 'GetWorkingHoursMore(' + value.OfficerID + ',0' + ')').html(value.OfficerName)))
                                                                    .append($('<td id="tdbodyWorkingHours' + key + '">').html(value.AvgHours))
                                                                    .append($('<td>').html(value.PredictedHours)))
                        }
                        else {
                            $('#thWorkingName').html('Officer Name');
                            $('#tbodyWorkingHours').append($('<tr>').append($('<td>').html(value.OfficerName))
                                                               .append($('<td id="tdbodyWorkingHours' + key + '">').html(value.AvgHours))
                                                               .append($('<td>').html(value.PredictedHours)))
                        }
                    });
                }
                else {
                    $('#tbodyWorkingHours').append('<tr><td colspan="3">No data found</td></tr>');
                }
            }
        },
        error: function () {
            //alert('error');
        }
    });
}

function GetWorkingHoursMore(OfficerID, Flag) {
    $('#DivWorkingHoursOfficersList').hide();
    $('#DivWorkingHoursManagersList').show();
    $('#btnWorkingHoursBack').hide();

    Type = "GET";
    var inputParams = "/GetOfficersWorkingHours?OfficerID=" + OfficerID + "&ShowAll=" + Flag + "&" + getCookie('BrowserId');
    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () { },
        complete: function () { },
        success: function (result) {//On Successfull service call 
            if (result != undefined) {
                $('#tbodyWorkingHoursManagersList').empty();
                if (result != "[]" && result != null) {
                    result = JSON.parse(result);
                    $.each(result, function (key, value) {
                        if (value.CallId == '9999' || value.CallId == '99991') {
                            $('#thWorkingName').html('Manager Name');
                            $('#tbodyWorkingHoursManagersList').append($('<tr>').append($('<td>').append($('<a>').attr('style', 'cursor:pointer;')
                                                                    .attr('onclick', 'GetWorkingHoursForOfficers(' + value.OfficerID + ')').html(value.OfficerName)))
                                                               .append($('<td id="tdbodyWorkingHoursManagerList' + key + '">').html(value.AvgHours))
                                                               .append($('<td>').html(value.PredictedHours)))
                        }
                        else {
                            $('#thWorkingName').html('Officer Name');
                            $('#tbodyWorkingHoursManagersList').append($('<tr>').append($('<td>').html(value.OfficerName))
                                                           .append($('<td id="tdbodyWorkingHoursManagerList' + key + '">').html(value.AvgHours))
                                                           .append($('<td>').html(value.PredictedHours)))
                        }
                    });
                }
                else {
                    $('#tbodyWorkingHoursManagersList').append('<tr><td colspan="3">No data found </td></tr>');
                }
            }
        },
        error: function () {
            //alert('error');
        }
    });

}

function GetWorkingHoursForOfficers(OfficerID) {
    $('#DivWorkingHoursOfficersList').show();
    $('#DivWorkingHoursManagersList').hide();
    $('#btnWorkingHoursBack').show();

    Type = "GET";
    var inputParams = "/GetOfficersWorkingHours?OfficerID=" + OfficerID + "&ShowAll=0" + "&" + getCookie('BrowserId');
    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;

    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () { },
        complete: function () { },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                if (result != "[]" && result != null) {
                    result = JSON.parse(result);
                    $('#tbodyWorkingHoursOfficersList').empty();
                    $.each(result, function (key, value) {
                        $('#tbodyWorkingHoursOfficersList').append($('<tr>').append($('<td>').html(value.OfficerName))
                                                                .append($('<td id="tdbodyWorkingHoursOfficerList' + key + '">').html(value.AvgHours))
                                                                .append($('<td>').html(value.PredictedHours)))
                    });
                }
            }
        },
        error: function () {
            //alert('error');
        }
    });
}
//==========================================================================================================================================
//TODO : Flash news function

function GetFlashNews() {
    Type = "GET";
    var inputParams = "/GetFlashNews?CompanyID=" + getCookie('CompanyID') + "&" + getCookie('BrowserId');
    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;

    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () { },
        complete: function () { },
        success: function (result) {//On Successfull service call
            if (result != undefined) {
                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {
                    result = JSON.parse(result);
                    $('#DMarquee').empty();
                    $.each(result, function (key, value) {
                        $('#DMarquee').append('<marquee behavior="scroll" direction="left" scrollamount="6">' + value.MessageText + '</marquee>')
                        //.append('<p id="PFlash" style="text-align:right">' + value.MessageText + '</p>');
                    });
                    //$('#PFlash').animate({ width: $(window).width() / 2 }, function () {
                    //    $('#PFlash').delay(2000).animate({ width: 0, height: "toggle" }, 6000)//.hide();
                    //});
                }
            }
        },
        error: function () {
            //alert('error');
        }
    });
}

//==========================================================================================================================================
//TODO : Statistics function

function GetStatistics(OfficerID, link) {
    if (getCookie('MgrName') == '' || getCookie('MgrName') == undefined || getCookie('MgrName') == 'undefined')
        $('#lblStatsMgrName').html('');
    else
        $('#lblStatsMgrName').html(' - ' + getCookie('MgrName'));

    $('.popover').remove();
    Type = "GET";
    var inputParams = "/GetStatsDashboard?OfficerID=" + OfficerID + "&" + getCookie('BrowserId');
    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;

    //CallService();
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () { },
        complete: function () { },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                if (result != "[]" && result != null) {
                    result = JSON.parse(result);
                    $('#tbodyStats').empty();
                    $.each(result, function (key, value) {
                        var StatFormula = '<table width="100%" class="display-status-table"><tr><td>' +
                                            '<div class="block" style="margin:2px!important;padding:2px!important">' +
                                            '<a class="close" data-dismiss="modal" href="#">X</a>' +
                                            '<p class="block-heading">Percentage calculation</p><table class="">' +
                                            '<tbody>' +
                                            '<tr><td>Formula for ' + value.Description + ' : ' + value.Formula + '</td></tr>' +
                                            '<tr><td>' + '</td></tr>' +
                                            '<tr><td>Calculation for ' + value.Description + ' : ' + value.Calculation + '</td></tr>' +
                                           '</tbody></table></div></td>' +
                                           '</td></tr></table>';

                        if (getCookie('CompanyID') == 1) {
                            $('#tbodyStats').append($('<tr>')
                                .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                .append($('<td>').append($('<a>').attr('id', 'AStat' + key).attr('style', 'cursor:pointer;')
                                                    .attr('onclick', 'GetStatsOnClick(' + key + ')').html(value.Description)
                                                    .attr('data-original-title', '').attr('rel', 'popover').attr('class', 'a1').attr('data-html', 'true')))
                                .append($('<td id="tdbodyStats' + key + '">').html(value.DisplayValue)))
                        }
                        else {
                            $('#tbodyStats').append($('<tr>')
                              .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                              .append($('<td>').append($('<a>').attr('id', 'AStat' + key).attr('style', 'cursor:pointer;')
                                                 .html(value.Description)
                                                  .attr('data-original-title', '').attr('rel', 'popover').attr('class', 'a1').attr('data-html', 'true')))
                              .append($('<td id="tdbodyStats' + key + '">').html(value.DisplayValue)))
                        }


                        $('#AStat' + key).mouseover(function () {
                            $('#AStat' + key).attr('data-content', StatFormula);
                            $('#AStat' + key).attr('data-placement', 'left');
                            $('#AStat' + key).popover('show');
                        });
                        $('#AStat' + key).mouseout(function () {
                            $('#AStat' + key).popover('hide');
                        });

                        if (link == 1) {
                            if ($.trim(value.Description) == "Case holdings") {
                                $('#tdbodyStats' + key).empty();//data-toggle="modal" href="#DivLogout"
                                $('#tdbodyStats' + key).append($('<a>').attr('data-toggle', 'modal').attr('href', '#divCaseSearchByOfficerID').attr('onclick', 'CaseSearchByOfficerID(' + OfficerID + ')').html(value.DisplayValue));
                            }
                        }
                    });
                    $('.close').click(function () { $('.popover').remove(); });
                }
            }
        },
        error: function () {
            //alert('error');
        }
    });
}

function GetStatsOnClick(key) {
    if (getCookie('MgrName') == '' || getCookie('MgrName') == undefined || getCookie('MgrName') == 'undefined')
        $('#lblStatsMgrName').html('');
    else
        $('#lblStatsMgrName').html(' - ' + getCookie('MgrName'));
    var ID;
    var Flag = key + 1;
    if (LinkForManager == 0) {
        ID = getCookie('OfficerID');
        $('#AStat' + key).attr('data-toggle', 'modal').attr('href', '#divStats');
        stats(ID, Flag);
        $('#tblStatsOfficerCount').hide();
        $('#tblStatsManagerCount').show();
    }
    else if (($('#lblSelectedOfficerID').html() != "0") || ($('#lblSelectedGroupID').html() != "0")) {
        $('#AStat' + key).removeAttr('data-toggle').removeAttr('href');
    }
    else {
        ID = getCookie('ManagerID');
        $('#AStat' + key).attr('data-toggle', 'modal').attr('href', '#divStats');
        $('#btnStatsBack').hide();
        $('#tblStatsOfficerCount').show();
        $('#tblStatsManagerCount').hide();
        StatsOfficer(ID, Flag);
    }
}

function stats(ID, Flag) {
    $('#lblStatsMgrName').html('');
    switch (Flag) {
        case 1:
            $('#HStatsTitle').html('Productivity');
            break;
        case 2:
            $('#HStatsTitle').html('Efficiency');
            break;
        case 3:
            $('#HStatsTitle').html('Conversion');
            break;
        case 4:
            $('#HStatsTitle').html('Case Holdings');
            break;
        case 5:
            $('#HStatsTitle').html('Paids Vs Targets');
            break;
        case 6:
            $('#HStatsTitle').html('Predicted Efficiency');
            break;
    }
    Type = "GET";
    var inputParams = "/GetStatsOfficerCount?OfficerID=" + ID + "&Flag=" + Flag + "&" + getCookie('BrowserId');
    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;
    //CallService();
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
            $('#imgLoader2').show();
        },
        complete: function () {
            $('#imgLoader2').fadeOut(1000);//.hide();
        },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                $('#tbodyStatsManagerCount').empty();
                if (result != "[]" && result != null) {
                    result = JSON.parse(result);
                    $.each(result, function (key, value) {
                        $('#tbodyStatsManagerCount').append('<tr><td><a style="cursor:pointer" onclick="StatsManager(' + value.OfficerID + ',' + Flag + ',' + "'" + value.OfficerName + "'" + ')" >' + value.OfficerName + '</a></td>' +
                                                            '<td>' + value.Cnt + '</td></tr>');

                    });
                }
                else {
                    $('#tbodyStatsManagerCount').append($('<tr>').append($('<td>').html('No data found.')))
                }
            }
        },
        error: function () {
            //alert('error');
        }
    });
}

function StatsOfficer(ID, Flag) {
    switch (Flag) {
        case 1:
            $('#HStatsTitle').html('Productivity');
            break;
        case 2:
            $('#HStatsTitle').html('Efficiency');
            break;
        case 3:
            $('#HStatsTitle').html('Conversion');
            break;
        case 4:
            $('#HStatsTitle').html('Case Holdings');
            break;
        case 5:
            $('#HStatsTitle').html('Paids Vs Targets');
            break;
        case 6:
            $('#HStatsTitle').html('Predicted Efficiency');
            break;
    }
    Type = "GET";
    var inputParams = "/GetStatsOfficerCount?OfficerID=" + ID + "&Flag=" + Flag + "&" + getCookie('BrowserId');
    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;
    //CallService();
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
            $('#imgLoader2').show();
        },
        complete: function () {
            $('#imgLoader2').fadeOut(1000);//.hide();
        },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                $('#tbodyStatsOfficerCount').empty();
                if (result != "[]" && result != null) {
                    result = JSON.parse(result);
                    $.each(result, function (key, value) {
                        $('#tbodyStatsOfficerCount').append($('<tr>').append($('<td>').html(value.OfficerName)).append($('<td>').html(value.Cnt)))
                    });
                }
                else {
                    $('#tbodyStatsOfficerCount').append($('<tr>').append($('<td>').html('No data found.')))
                }
            }
        },
        error: function () {
            //alert('error');
        }
    });
}

function StatsManager(OfficerID, Flag, OfficerName) {
    setCookie('MgrName', OfficerName, 1);
    $('#btnStatsBack').show();
    $('#tblStatsOfficerCount').show();
    $('#tblStatsManagerCount').hide();
    $('#lblStatsMgrName').html(' - ' + getCookie('MgrName')).show();
    StatsOfficer(OfficerID, Flag);
}

//==========================================================================================================================================
//TODO : Notification

function UpdateNotificationAlert(ManagerID, MessageText, OfficerID, IsPriority) {
    if (OfficerID == undefined) OfficerID = 0;
    Type = "GET";
    var inputParams = "/UpdateNotificationMessage?ManagerID=" + ManagerID + "&MessageText=" + MessageText.replace('&', 'ybcc')
        + "&Officer=" + $.trim(OfficerID) + "&IsPriority=" + IsPriority + "&" + getCookie('BrowserId');

    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;

    //CallService();
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
            $('.word_count').hide();
            $('#imgLoading').show();
            $('#btnSendNotification').hide();
        },
        complete: function () {
            //  $('#selOfficer').multiselect("uncheckAll");
            if ($("#chkIsPriority").is(':checked')) {
                $('.word_count span').text('200 characters left');
            }
            else {
                $('.word_count span').text('900 characters left');
            }
            $('#lblMsgReport').html('Message sent successfully').show().fadeOut(3000);
            $('#lblMsgReport').css('color', 'green');
            $('.word_count').fadeIn(4000);
            $('#imgLoading').hide();
            $('#btnSendNotification').show();

            $('#selOfficer').multiselect({
                noneSelectedText: 'Select Officers',
                selectedList: 5,
                multiple: true
            }).multiselectfilter();

            $('#selOfficer').multiselect("uncheckAll");
        },
        success: function (result) {//On Successfull service call  
        },
        error: function () {
            //alert('error');
        } // When Service call fails
    });


}

function UpdateAutoRefresh(OfficerID, AutoRefresh) {
    if (OfficerID == undefined) OfficerID = 0;
    Type = "GET";
    var inputParams = "/UpdateAutoRefresh?OfficerID=" + OfficerID + "&AutoRefresh=" + AutoRefresh + "&" + getCookie('BrowserId');
    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;

    //CallService();
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () { },
        complete: function () { },
        success: function (result) {//On Successfull service call
            $('#lblTargetInfo').html('Auto refresh value has been updated successfully.');
            $('#ATargetInfo').click();
            setCookie('aref', AutoRefresh, 1);
            $('#txtRefreshTime').val(getCookie('aref'));
        },
        error: function () {
            //alert('error');
        } // When Service call fails
    });

}

function showhidetabs(tabname) {
    $('#show-search-details,#tblbodySearchType,#tblWMAOfficers').hide();
    $('#' + tabname).show();
}

function caseactionshowhidetabs() {
    $('#leftletterformanager,#revisitformanager,#partpaidformanager,#returnedformanager,#paidformanager,#deviatedformanager,#DeviatedGPS').hide();
    $('#tcgpaidformanager,#tcgppformanager,#tcgformanager,#uttcformanager,#droppedformanager,#otherformanager').hide();
}

/************************************copied from officer web for showing case holdings**************************************/

function CaseSearchByOfficerID(OfficerID) {
    var Type = "GET";
    var DataType = "jsonp"; var ProcessData = false;
    var Url = serviceUrl + "/GetCaseSearchByOfficerID?OfficerID=" + OfficerID + "&" + getCookie('BrowserId');

    var ContentType = "application/javascript";
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () { },
        complete: function () { },
        success: function (result) {//On Successfull service call                      
            if (!(result == null || result == '[]')) {

                result = JSON.parse(result);
                $('#tbodyCaseSearchByOfficerID').empty();
                var HTMLString = '';
                $.each(result, function (key, value) {
                    HTMLString += '<tr><td>' + value.casenumber + '</td><td>' + value.DebtName + '</td><td>' + value.ClientName + '</td><td>' + value.ContactFirstLine + ', ' + value.ContactAddress + ', ' + value.PostCode + '</td>' +
                            '<td>' + value.CaseStatus + '</td><td>' + value.OfficerFirstName + ' ' + value.OfficerLastName + '</td><td>' + value.Ageing + '</td><td>' + value.IssueDate + '</td><td>' + value.DateActioned + '</td></tr>';
                });
                $('#tbodyCaseSearchByOfficerID').html(HTMLString);

                $('#tblCaseSearchByOfficerID').dataTable({
                    "sScrollY": "370px",
                    "bAutoWidth": false,
                    "bPaginate": true,
                    "bDestroy": true,
                    "bSort": false,
                    "sPaginationType": "full_numbers",
                    "bLengthChange": false,
                    "bScrollCollapse": true,
                    "sInfo": true,
                    "iDisplayLength": 5

                });
            }
            else {
                $('#tbodyCaseSearchByOfficerID').empty();
                $('#tbodyCaseSearchByOfficerID').append($('<tr><td colspan="9"> No records found </td></tr>'));
            }

        },
        error: function () {
            //alert('error');
        } // When Service call fails
    });
}

/***********************************************************HeartBeat********************************************************/

function HeartBeat(Condition) {
    setCookie('Map', 'HB', 1);
    if (Condition == 1) {
        MapHeartBeat(OffID, FD, FD);

        clearInterval(MapIntervalStatus);
        MapIntervalStatus = setInterval(function () {
            MapHeartBeat(OffID, FD, FD);
        }, 180000);
        // $('#iframeGMAP').attr('src', 'GMaps.html?oID=' + OffID + "&Map=Medium&fDate=" + FD + "&tDate=" + FD);// + "&gID=" + GroupID + "&ShowAll=" + ShowAll);
    }
    else
        $('#iframePopupMap').attr('src', 'GMaps.html?oID=' + OffID + "&Map=Large&fDate=" + FD + "&tDate=" + FD);// + "&gID=" + GroupID + "&ShowAll=" + ShowAll);
}

function LocationwithHB(Condition) {
    setCookie('Map', 'LocHB', 1);

    if (Condition == 1) {
        GetLocationHB($('#txtFromDateLocation').val(), $('#txtFromDateLocation').val(), OffID);

        clearInterval(MapIntervalStatus);
        MapIntervalStatus = setInterval(function () {
            GetLocationHB($('#txtFromDateLocation').val(), $('#txtFromDateLocation').val(), OffID);
        }, 180000);
    }
    else
        $('#iframePopupMap').attr('src', 'GMaps.html?oID=' + OffID + "&Map=Large&fDate=" + $('#txtFromDateLocation').val() + "&tDate=" + $('#txtFromDateLocation').val() + "&LocationwithHB=1");
}

function MapView() {
    if (getCookie('Map') == undefined) {
        setCookie('Map', '', 1);
    }
    var OfficerId, GroupId;
    if (OffID == undefined || OffID == 'undefined')
        OfficerId = 0;
    else
        OfficerId = OffID;

    if (GID == undefined || GID == 'undefined')
        GroupId = 0;
    else
        GroupId = GID;
    switch (getCookie('Map')) {
        case 'All':
            $('#iframePopupMap').attr('src', 'Cluster.html?mID=' + MID + "&fDate=" + FD + "&tDate="
            + TD + "&oID=" + OID + "&Map=Large" + "&gID=" + GroupId + "&ShowAll=1");
            break;
        case 'HB':
            $('#iframePopupMap').attr('src', 'GMaps.html?oID=' + OfficerId + "&Map=Large&fDate=" + FD + "&tDate=" + FD);
            break;
        case 'LocHB':
            $('#iframePopupMap').attr('src', 'GMaps.html?oID=' + OfficerId + "&Map=Large&fDate=" + $('#txtFromDateLocation').val() + "&tDate=" + $('#txtFromDateLocation').val() + "&LocationwithHB=1");
            break;
        default:
            $('#iframePopupMap').attr('src', 'Cluster.html?mID=' + MID + "&fDate=" + FD + "&tDate="
            + TD + "&oID=" + OID + "&Map=Large" + "&gID=" + GroupId + "&ShowAll=0");
    }
}

function MapExpandView() {
    if (getCookie('Map') == undefined) {
        setCookie('Map', '', 1);
    }
    var OfficerId, GroupId;
    if (OffID == undefined || OffID == 'undefined')
        OfficerId = 0;
    else
        OfficerId = OffID;

    if (GID == undefined || GID == 'undefined')
        GroupId = 0;
    else
        GroupId = GID;
    switch (getCookie('Map')) {
        case 'All':
            $('#AExpand').attr('href', 'Cluster.html?mID=' + MID + "&fDate=" + FD + "&tDate="
            + TD + "&oID=" + OID + "&Map=Large" + "&gID=" + GroupId + "&ShowAll=1" + "&Mapview=1");
            break;
        case 'HB':
            $('#AExpand').attr('href', 'GMaps.html?oID=' + OfficerId + "&Map=Large&fDate=" + FD + "&tDate=" + FD + "&Mapview=1");
            break;
        case 'LocHB':
            $('#AExpand').attr('href', 'GMaps.html?oID=' + OfficerId + "&Map=Large&fDate=" + $('#txtFromDateLocation').val() + "&tDate=" + $('#txtFromDateLocation').val() + "&LocationwithHB=1" + "&Mapview=1");
            break;
        default:
            $('#AExpand').attr('href', 'Cluster.html?mID=' + MID + "&fDate=" + FD + "&tDate="
            + TD + "&oID=" + OID + "&Map=Large" + "&gID=" + GroupId + "&ShowAll=0" + "&Mapview=1");
    }
}

/***********************************************************HeartBeat********************************************************/

function GetOfficerActivityStatus() {
    var id = getCookie('OfficerID');
    Type = "GET";
    var inputParams = "/GetOfficerActivityStatus?IndexValue=" + tableIndex + "&OfficerID=" + id + "&" + getCookie('BrowserId');
    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;

    //CallService();
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () { },
        complete: function () { },
        success: function (result) {//On Successfull service call  
            $('#' + $('#popup2Trigger').attr('aria-describedby')).attr('class', 'popover fade bottom');
            $('#DivOfficerStatusContent').empty();
            $('#DivOfficerStatusContent').append('<div id="tdCont"></div>');
            $('#DivOfficerStatusContent').append($('<div style="border: 1px solid #ededed; margin: -15px; margin-left:12px; width:1150px; height: 370px;display:none;" id="divMapOAS"><iframe  width="100%" height="100%" frameborder="0"' +
                                    'scrolling="yes" marginheight="0"' +
                                    'marginwidth="0" id="iframeGMAPOAS" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=London,+United+Kingdom&amp;aq=0&amp;oq=lond&amp;sll=55.378051,-3.435973&amp;sspn=20.606847,39.506836&amp;ie=UTF8&amp;hq=&amp;hnear=London,+United+Kingdom&amp;t=m&amp;z=11&amp;iwloc=A&amp;ll=51.511214,-0.119824&amp;output=embed"></iframe></div>')

               )

            if (result != undefined) {
                if (result != "[]" && result != null) {
                    result = JSON.parse(result);
                    $.each(result, function (key, value) {
                        tableIndex = value.IndexNo;
                        $('#lblManagerNameActivityStatus').html(value.ManagerName);//.html('Officer`s Activity Status - ' + value.ManagerName);
                        $('#tdGray').html(value.Darkgray);
                        $('#tdGreen').html(value.Green);
                        $('#tdRed').html(value.Red);
                        $('#tdAmber').html(value.Amber);

                        if ($('#tbl' + $.trim(value.ManagerId)).length == 0) {
                            $('#tdCont').append('<div id="tbl' + $.trim(value.ManagerId) + '"></div>')
                        }

                        if ($('#tbl' + $.trim(value.ManagerId) + ' tr').length == 0) {
                            $('#tbl' + $.trim(value.ManagerId)).append($('<div class="col-md-4 col-sm-4">').append($('<div class="bubble ' + value.Status + 'Bub">')
                                    .html(value.OfficerName).attr('id', 'OAS' + value.OfficerID)))
                        }
                        else {
                            if ($('#tbl' + $.trim(value.ManagerId) + ' tr:last' + ' td').length >= 3) {
                                $('#tbl' + $.trim(value.ManagerId)).append($('<tr>'))
                            }
                            $('#tbl' + $.trim(value.ManagerId) + ' tr:last').append($('<td style="padding:10px !important;">')
                                .append($('<div class="bubble ' + value.Status + 'Bub">').html(value.OfficerName).attr('id', 'OAS' + value.OfficerID)));
                        }

                        $('#OAS' + value.OfficerID).click(function () {
                            $('#btnBacktoStatus').show();
                            $('#lblOfficerNameMapview').html(value.OfficerName);
                            $('#lblOfficerNameMapview').show();

                            clearInterval(IntervalvalueOfficerStatus);

                            $('#iframeGMAPOAS').attr('src', 'GMaps.html?mID=0' + "&fDate=" + $.datepicker.formatDate('yy/mm/dd', new Date()) + "&tDate="
                                + $.datepicker.formatDate('yy/mm/dd', new Date())
                                + "&oID=" + $(this).attr('id').split('OAS')[1] + "&Map=Large&gID=0&ShowAll=1");

                            $('#tdPrev').hide();
                            $('#tdNext').hide();

                            $('#divMapOAS').show();
                            $('#tdCont').hide();
                            $('#divOfficerActivityStatus').css('width', '87%').css('height', '90%');

                            $('#btnBacktoStatus').unbind().click(function () {
                                $('#tdCont').show();
                                $('#divMapOAS').hide();
                                $('#tdPrev').show();
                                $('#tdNext').show();

                                $('#lblOfficerNameMapview').hide();
                                clearInterval(IntervalvalueOfficerStatus);
                                $('#divOfficerActivityStatus').css('width', '').css('height', '');
                                $(this).hide();

                                tableIndex = tableIndex - 1;
                                GetOfficerActivityStatus();

                                IntervalvalueOfficerStatus = setInterval(function () {
                                    GetOfficerActivityStatus();
                                }, 180000);
                            });
                        });

                    });

                    $('#lblManagerNameActivityStatus').html($('#tdCont table:eq(' + tableIndex + ')').attr('mname'));
                    tableIndex = tableIndex + 1;
                }
            }

            $('#DivOfficerStatusContent').append($('<table style="width:100%">')
           .append($('<tr >').append($('<td style="width:50%">').attr('id', 'tdPrev'))
           .append($('<td style="width:50%;text-align:right;">').attr('id', 'tdNext'))))

            $('#tdPrev').append('<img src="images/Previous_W.png" style="cursor:pointer;" /><br><span style="cursor:pointer;">Previous</span>');
            $('#tdNext').append('<img src="images/Next_W.png" style="cursor:pointer;" /><br><span style="cursor:pointer;">Next</span>');

            $('#tdPrev').unbind().click(function () {

                tableIndex = tableIndex - 2;

                if (tableIndex <= -1) tableIndex = 0;
                GetOfficerActivityStatus();
            });

            $('#tdNext').unbind().click(function () {

                GetOfficerActivityStatus();

                clearInterval(IntervalvalueOfficerStatus);
                IntervalvalueOfficerStatus = setInterval(function () {
                    GetOfficerActivityStatus();
                }, 180000);
            });
        },
        error: function () {
            //alert('error');
        } // When Service call fails
    });

}

function OfficerActivityStatusInit() {
    GetOfficerActivityStatus();
}

function SubmitOfficerTotal() {
    var TTargetInfo = '';
    $('#tbodyOfficerList tr').each(function (key, value) {
        if ($(this).find('label').html() != '' && $(this).find('label').html() != undefined && parseInt($(this).find('input').val()) > 0) {
            if (TTargetInfo != '') TTargetInfo += ',';
            TTargetInfo += $(this).find('label').html() + '|' + parseInt($(this).find('input').val());
        }
    });

    // ****************************** submit functionality
    Type = "GET";
    var inputParams = "/UpdateOfficerTarget?TMonth=" + $('#selCurrentMonth').val() + "&TargetNo=" +
        $('#txtMonthTotalTarget').val() + "&ManagerID=" + ManagerID + "&OfficerIDs=" + TTargetInfo + "&" + getCookie('BrowserId');
    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;

    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () { },
        complete: function () { },
        success: function (result) {//On Successfull service call  
            $('#lblTargetInfo').html('Target has been updated successfully.');
            $('#ATargetInfo').click();
        },
        error: function () {
        } // When Service call fails
    });
}

//==========================================================================================================================================
//TODO : Timeline

function GetTimelineResult(OfficerID) {
    var Fdate = $.trim($('#txtTLineFromDate').val()) == '' ? '1/1/1900' : $.trim($('#txtTLineFromDate').val());
    // var Tdate = $.trim($('#txtTLineToDate').val()) == '' ? '1/1/1900' : $.trim($('#txtTLineToDate').val());

    var dateT1 = Fdate.split('/');
    var newDateFrom = dateT1[2] + '/' + dateT1[1] + '/' + dateT1[0];

    //var dateT2 = Tdate.split('/');
    //var newDateTo = dateT2[2] + '/' + dateT2[1] + '/' + dateT2[0];

    Type = "GET";
    var inputParams = "/GetTimelineDetailsforOfficer?OfficerID=" + OfficerID + "&FromDate=" + newDateFrom + "&ToDate=" + newDateFrom + "&" + getCookie('BrowserId');// + "&OfficerID=" + id;
    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;
    //CallService();
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (result) {//On Successfull service call  
            $('#cd-timeline').empty();
            if (result != undefined) {
                if (result != "[]" && result != null) {
                    result = JSON.parse(result);
                    $.each(result, function (key, value) {
                        //   LOGIN, ARR, DEP

                        //DROPPED
                        //Returned
                        //UTTC

                        //OTHER
                        //TCG
                        //Left Letter

                        //PAID
                        //PART PAID
                        //TCG PAID
                        //TCG PP
                        //Paid
                        //Part Paid

                        $('#cd-timeline').append('<div class="cd-timeline-block">' +
                                                 '<div id="DTimeLinePicture' + key + '" ></div>' +
                                                 '<div class="cd-timeline-content"><h2>' + value.TTitle + '</h2> <p>' + value.TDescription + '</p>' +
                                                 '<span class="cd-date">' + value.TDate + '</span></div></div>');

                        switch (value.TTitle) {
                            case 'LOGIN':
                            case 'ARR':
                            case 'DEP':
                                $('#DTimeLinePicture' + key).attr('class', 'cd-timeline-img cd-picture');
                                break;
                            case 'DROPPED':
                            case 'Returned':
                            case 'UTTC':
                                $('#DTimeLinePicture' + key).attr('class', 'cd-timeline-img cd-movie');
                                break;
                            case 'OTHER':
                            case 'TCG':
                            case 'Left Letter':
                                $('#DTimeLinePicture' + key).attr('class', 'cd-timeline-img cd-location');
                                break;
                            case 'PAID':
                            case 'PART PAID':
                            case 'TCG PAID':
                            case 'TCG PP':
                            case 'Paid':
                            case 'Part Paid':
                                $('#DTimeLinePicture' + key).attr('class', 'cd-timeline-img cd-location');
                                break;
                        }
                    });
                }
                else {
                    $('#cd-timeline').append('<div class="cd-timeline-block">' +
                                               '<div class="cd-timeline-img cd-location" ></div>' +
                                               '<div class="cd-timeline-content"><h2>No data found...</h2></div></div>');
                }
            }
        },
        error: function () {
            //alert('error');
        } // When Service call fails
    });

}

//==========================================================================================================================================
//TODO : Cookie

function setCookie(c_name, value, exdays) {
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
    document.cookie = c_name + "=" + c_value;
}

function getCookie(c_name) {
    var i, x, y, ARRcookies = document.cookie.split(";");
    for (i = 0; i < ARRcookies.length; i++) {
        x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
        y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
        x = x.replace(/^\s+|\s+$/g, "");
        if (x == c_name) {
            return unescape(y);
        }
    }
}

function DeleteCookie(name) {
    document.cookie = name + '=; expires=Thu, 01-Jan-70 00:00:01 GMT;';
}

//==========================================================================================================================================
