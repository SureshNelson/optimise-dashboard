﻿/// <reference path="../../admin/js/jquery.session.js" />
//==========================================================================================================================================
/* File Name: Marston_dataservice.js */
/* File Created: January 21, 2014 */
/* Created By  : R.Santhakumar */
//==========================================================================================================================================
//TODO : Tree view list and availability show

//==========================================================================================================================================
//TODO : Map

function FillAllLocation(Condition) {
    return;
    $.session.set('Map', 'All');

    if (OID == undefined) OID = 0;
    if (GID == undefined) GID = 0;

    if (Condition == 1) {// Medium 
        //$('#iframeGMAP').attr('src', 'Cluster.html?mID=' + MID + "&fDate=" + FD + "&tDate=" + TD +
        //"&oID=" + OID + "&Map=Medium" + "&gID=" + GID + "&ShowAll=1");
        MapFillLastKnownLocation(MID, FD, FD, OID, GID, 1);
        //clearInterval(MapIntervalStatus);
        //MapIntervalStatus = setInterval(function () {
        //    MapFillLastKnownLocation(MID, FD, FD, OID, GID, 1);
        //}, 180000);
    }
    //else {//large map
    //    $('#iframePopupMap').attr('src', 'Cluster.html?mID=' + MID + "&fDate=" + FD + "&tDate="
    //        + TD + "&oID=" + OID + "&Map=Large" + "&gID=" + GID + "&ShowAll=1");
    //}
}

function MapHeartBeat(oID, fDate, tDate) {
    return;
    $('#map_canvas,#DLocationHBMap').hide();
    $('#DHeartBeatMap').show();
    Type = "GET";
    DataType = "jsonp";
    ProcessData = false;
    Url = serviceUrl + "/GetHeartBeats?OfficerID=" + oID + "&FromDate=" + fDate + "&ToDate=" + fDate + "&" + $.session.get('BrowserId');
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
            $('#imgLoader12').show();
            $('#changeclass4 div').css('background', '#F9F9F9');
        },
        complete: function () {
            $('#imgLoader12').fadeOut(1000);
            $('#changeclass4 div').css('background', '');

        },
        success: function (result) {//On Successfull service call 
            $('#lblDisplay').html('').hide();
            markers = [];
            if (result != undefined && result != '[]' && result != '[{"Result":"NoRecords"}]') {
                result = JSON.parse(result);
                center = new google.maps.LatLng(51.699467, 0.109348);
                options = {
                    'zoom': 5,
                    'center': center
                    //  'mapTypeId': google.maps.MapTypeId.ROADMAP
                };

                map = new google.maps.Map(document.getElementById("DHeartBeatMap"), options);
                infowindow = new google.maps.InfoWindow();

                $.each(result, function (key, value) {
                    latLng = new google.maps.LatLng(value.latitude, value.longitude);
                    marker = new google.maps.Marker({
                        'position': latLng,
                        'map': map,
                        'icon': "images/map-icon-pink.png"
                    });

                    markers.push(marker);

                    google.maps.event.addListener(markers[key], 'click', function (e) {
                        infowindow.setContent(value.html);
                        infowindow.open(map, this);
                    });
                });
                //markerCluster = new MarkerClusterer(map, markers, {
                //    maxZoom: 20,
                //    gridSize: 100
                //});
            }
            else {
                $('#lblDisplay').html('No activity recorded for today!').show();
                center = new google.maps.LatLng(52.8849565, -1.9770329);
                options = {
                    'zoom': 5,
                    'center': center,
                    'mapTypeId': google.maps.MapTypeId.ROADMAP
                };
                map = new google.maps.Map(document.getElementById("DHeartBeatMap"), options);
            }
        },
        error: function () {
            //alert('error');
        }
    });
}

function GetLocationHB(fDate, tDate, oID) {
    return;
    $('#map_canvas,#DHeartBeatMap').hide();
    $('#DLocationHBMap').show();
    Type = "GET";
    DataType = "jsonp";
    ProcessData = false;
    Url = serviceUrl + "/GetLocationHB?OfficerID=" + oID + "&FromDate=" + fDate + "&ToDate=" + tDate + "&" + $.session.get('BrowserId');
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
            $('#imgLoader12').show();
        },
        complete: function () {
            $('#imgLoader12').fadeOut(1000);
        },
        success: function (result) {//On Successfull service call 
            $('#lblDisplay').html('').hide();
            markers = [];
            if (result != undefined && result != '[]' && result != '[{"Result":"NoRecords"}]') {
                result = JSON.parse(result);
                var iconImg1;
                center = new google.maps.LatLng(51.699467, 0.109348);
                options = {
                    'zoom': 5,
                    'center': center,
                    'mapTypeId': google.maps.MapTypeId.ROADMAP
                };
                map = new google.maps.Map(document.getElementById("DLocationHBMap"), options);
                infowindow = new google.maps.InfoWindow();

                $.each(result, function (key, value) {
                    switch (value.IconType) {
                        case 1: // Paid , Part paid
                            iconImg1 = "images/map-icon-yellow.png";
                            break;
                        case 2:  // Returned
                            iconImg1 = "images/map-icon-red.png";
                            break;
                        case 3:  // Revisit
                            iconImg1 = "images/map-icon-green.png";
                            break;
                        case 5: // HeartBeat
                            iconImg1 = "images/map-icon-pink.png";
                            break;
                        default:
                            iconImg1 = "images/map-icon-white.png";
                    }
                    latLng = new google.maps.LatLng(value.latitude, value.longitude);
                    marker = new google.maps.Marker({
                        'position': latLng,
                        'map': map,
                        'icon': iconImg1
                    });
                    markers.push(marker);
                    google.maps.event.addListener(markers[key], 'click', function (e) {
                        infowindow.setContent(value.html);
                        infowindow.open(map, this);
                    });
                });
            }
            else {
                $('#lblDisplay').html('No activity recorded for today!').show();
                center = new google.maps.LatLng(52.8849565, -1.9770329);
                options = {
                    'zoom': 6,
                    'center': center,
                    'mapTypeId': google.maps.MapTypeId.ROADMAP
                };
                map = new google.maps.Map(document.getElementById("DLocationHBMap"), options);
            }
        },
        error: function () {
            //alert('error');
        }
    });
}


//==========================================================================================================================================
//TODO : Notification

function UpdateAutoRefresh(OfficerID, AutoRefresh) {
    if (OfficerID == undefined) OfficerID = 0;
    $.ajax({
        url: ServiceURL + 'api/v1/officers/' + OfficerID + '/' + AutoRefresh + '/UpdateAutoRefresh',
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        type: 'PUT',
        dataType: 'json',
        beforeSend: function () { },
        success: function (data) {
            AlertMessage("Info", "Auto refresh value has been updated successfully.");
            //$('#lblTargetInfo').html('Auto refresh value has been updated successfully.');
            $('#ATargetInfo').click();
            $.session.set('aref', AutoRefresh, 1);
            $('#txtRefreshTime').val($.session.get('aref'));
        },
        complete: function () { },
        error: function (xhr, textStatus, errorThrown) {
        }
    });
}

function showhidetabs(tabname) {
    $('#show-search-details,#tblbodySearchType').hide();//,#tblWMAADM
    $('#' + tabname).show();
}

function caseactionshowhidetabs() {
    $('#leftletterformanager,#revisitformanager,#partpaidformanager,#returnedformanager,#paidformanager,#deviatedformanager,#DeviatedGPS').hide();
    $('#bailedformanager,#arrestedformanager,#Surrenderdateagreedformanager').hide();
    $('#tcgpaidformanager,#tcgppformanager,#tcgformanager,#uttcformanager,#droppedformanager,#otherformanager,#Clampedformanager').hide();
}

/************************************copied from officer web for showing case holdings**************************************/

/***********************************************************HeartBeat********************************************************/
function HeartBeat(Condition) {
    $.session.set('Map', 'HB');
    if (Condition == 1) {
        //   MapHeartBeat(OffID, FD, FD);
        var inputParams = OffID + ',' + FD;
        GetHeartBeat(inputParams);
    }
    else
        $('#iframePopupMap').attr('src', 'GMaps.html?oID=' + OffID + "&Map=Large&fDate=" + FD + "&tDate=" + FD);// + "&gID=" + GroupID + "&ShowAll=" + ShowAll);
}

function LocationwithHB(Condition) {
    $.session.set('Map', 'LocHB');

    if (Condition == 1)
        GetLocationHB($('#txtFromDateLocation').val(), $('#txtFromDateLocation').val(), OffID);
    else
        $('#iframePopupMap').attr('src', 'GMaps.html?oID=' + OffID + "&Map=Large&fDate=" + $('#txtFromDateLocation').val() + "&tDate=" + $('#txtFromDateLocation').val() + "&LocationwithHB=1");
}

function MapView() {
    if ($.session.get('Map') == undefined) {
        $.session.set('Map', '');
    }
    var OfficerId, GroupId;
    if (OffID == undefined || OffID == 'undefined')
        OfficerId = 0;
    else
        OfficerId = OffID;

    if (GID == undefined || GID == 'undefined')
        GroupId = 0;
    else
        GroupId = GID;
    switch ($.session.get('Map')) {
        case 'All':
            $('#iframePopupMap').attr('src', 'Cluster.html?mID=' + MID + "&fDate=" + FD + "&tDate="
            + TD + "&oID=" + OID + "&Map=Large" + "&gID=" + GroupId + "&ShowAll=1");
            break;
        case 'HB':
            $('#iframePopupMap').attr('src', 'GMaps.html?oID=' + OfficerId + "&Map=Large&fDate=" + FD + "&tDate=" + FD);
            break;
        case 'LocHB':
            $('#iframePopupMap').attr('src', 'GMaps.html?oID=' + OfficerId + "&Map=Large&fDate=" + $('#txtFromDateLocation').val() + "&tDate=" + $('#txtFromDateLocation').val() + "&LocationwithHB=1");
            break;
        default:
            $('#iframePopupMap').attr('src', 'Cluster.html?mID=' + MID + "&fDate=" + FD + "&tDate="
            + TD + "&oID=" + OID + "&Map=Large" + "&gID=" + GroupId + "&ShowAll=0");
    }
}

function MapExpandView() {
    if ($.session.get('Map') == undefined) {
        $.session.set('Map', '');
    }
    var OfficerId, GroupId;
    if (OffID == undefined || OffID == 'undefined')
        OfficerId = 0;
    else
        OfficerId = OffID;

    if (GID == undefined || GID == 'undefined')
        GroupId = 0;
    else
        GroupId = GID;
    switch ($.session.get('Map')) {
        case 'All':
            $('#AExpand').attr('href', 'Cluster.html?mID=' + MID + "&fDate=" + FD + "&tDate="
            + TD + "&oID=" + OID + "&Map=Large" + "&gID=" + GroupId + "&ShowAll=1" + "&Mapview=1");
            break;
        case 'HB':
            $('#AExpand').attr('href', 'GMaps.html?oID=' + OfficerId + "&Map=Large&fDate=" + FD + "&tDate=" + FD + "&Mapview=1");
            break;
        case 'LocHB':
            $('#AExpand').attr('href', 'GMaps.html?oID=' + OfficerId + "&Map=Large&fDate=" + $('#txtFromDateLocation').val() + "&tDate=" + $('#txtFromDateLocation').val() + "&LocationwithHB=1" + "&Mapview=1");
            break;
        default:
            $('#AExpand').attr('href', 'Cluster.html?mID=' + MID + "&fDate=" + FD + "&tDate="
            + TD + "&oID=" + OID + "&Map=Large" + "&gID=" + GroupId + "&ShowAll=0" + "&Mapview=1");
    }
}

//==========================================================================================================================================
//Unwanted functions

function FillWMADetailL3(OfficerID, FromDate, ToDate) {
    return;
    Type = "GET";
    var inputParams = "/GetWMADetailL1_Dashboard?OfficerID=" + OfficerID + "&FromDate=" + FromDate + "&ToDate=" +
        FromDate + "&" + $.session.get('BrowserId');
    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;

    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () { },
        complete: function () { },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                if (result != "[]" && result != null && result != '[{"Result":"NoRecords"}]') {
                    result = JSON.parse(result);

                    $('#tblbodySearchType').empty();
                    showhidetabs('tblbodySearchType');
                    $('#lblSearchType').html('');
                    $('#btnBack').hide();
                    $('#tblbodyWMASearchType').empty();
                    $('#tblWMASearchType').show();
                    $('#tblWMA').hide();

                    $.each(result, function (key, value) {
                        $('#tblbodySearchType').append($('<tr>').append($('<td>').append($('<a>').attr('id', value.LogType).html(value.LogType)))
                            .append($('<td>').html(value.Activities)))

                        $('#tblbodyWMASearchType').append($('<tr>').append($('<td>').append($('<a>').attr('id', 'A' + value.LogType)
                            .attr('data-toggle', 'modal').attr('href', '#windowTitleDialog').html(value.LogType))).append($('<td>').html(value.Activities)))

                        $('#A' + value.LogType).click(function () {
                            $('#lblSearchType').html(' - ' + $(this).html());
                            FillWMADetailL2(OfficerID, FromDate, ToDate, value.LogType);
                            showhidetabs('show-search-details');
                            $('#btnBack').show();
                        });

                        $('#' + value.LogType).click(function () {
                            $('#lblSearchType').html(' - ' + $(this).html());
                            FillWMADetailL2(OfficerID, FromDate, ToDate, value.LogType);
                            showhidetabs('show-search-details');
                            $('#btnBack').show();
                        })
                    });
                }
            }
        },
        error: function () {
            //alert('error');
        } // When Service call fails
    });
}

//TODO : Statistics function

function GetStatistics() {//OfficerID, link
    return;
    $('.popover').remove();
    Type = "GET";
    var inputParams = "/GetStatsDashboard?OfficerID=" + OfficerID + "&" + $.session.get('BrowserId');
    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;
    //CallService();
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () { },
        complete: function () { },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                if (result != "[]" && result != null) {
                    result = JSON.parse(result);
                    $('#tbodyStats').empty();
                    $.each(result, function (key, value) {
                        var StatFormula = '<table width="100%" class="display-status-table"><tr><td>' +
                                            '<div class="block" style="margin:2px!important;padding:2px!important">' +
                                            '<a class="close" data-dismiss="modal" href="#">X</a>' +
                                            '<p class="block-heading">Percentage calculation</p><table class="">' +
                                            '<tbody>' +
                                            '<tr><td>Formula for ' + value.Description + ' : ' + value.Formula + '</td></tr>' +
                                            '<tr><td>' + '</td></tr>' +
                                            '<tr><td>Calculation for ' + value.Description + ' : ' + value.Calculation + '</td></tr>' +
                                           '</tbody></table></div></td>' +
                                           '</td></tr></table>';

                        if ($.session.get('CompanyID') == 1) {
                            $('#tbodyStats').append($('<tr>')
                                .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                                .append($('<td>').append($('<a>').attr('id', 'AStat' + key).attr('style', 'cursor:pointer;')
                                                    .attr('onclick', 'GetStatsOnClick(' + key + ')').html(value.Description)
                                                    .attr('data-original-title', '').attr('rel', 'popover').attr('class', 'a1').attr('data-html', 'true')))
                                .append($('<td id="tdbodyStats' + key + '">').html(value.DisplayValue)))
                        }
                        else {
                            $('#tbodyStats').append($('<tr>')
                              .append($('<td>').append($('<i class="icon24"><img src="images/Case-ico.png" alt="icon"></i>')))
                              .append($('<td>').append($('<a>').attr('id', 'AStat' + key).attr('style', 'cursor:pointer;')
                                                 .html(value.Description)
                                                  .attr('data-original-title', '').attr('rel', 'popover').attr('class', 'a1').attr('data-html', 'true')))
                              .append($('<td id="tdbodyStats' + key + '">').html(value.DisplayValue)))
                        }


                        $('#AStat' + key).mouseover(function () {
                            $('#AStat' + key).attr('data-content', StatFormula);
                            $('#AStat' + key).attr('data-placement', 'left');
                            $('#AStat' + key).popover('show');
                        });
                        $('#AStat' + key).mouseout(function () {
                            $('#AStat' + key).popover('hide');
                        });

                        if (link == 1) {
                            if ($.trim(value.Description) == "Case holdings") {
                                $('#tdbodyStats' + key).empty();//data-toggle="modal" href="#DivLogout"
                                $('#tdbodyStats' + key).append($('<a>').attr('data-toggle', 'modal').attr('href', '#divCaseSearchByOfficerID').attr('onclick', 'CaseSearchByOfficerID(' + OfficerID + ')').html(value.DisplayValue));
                            }
                        }
                    });
                    $('.close').click(function () { $('.popover').remove(); });
                }
            }
        },
        error: function () {
            //alert('error');
        }
    });
}

function GetStatsOnClick(key) {
    return;
    var Flag = key + 1;
    $('#tblStatsManagerCount,#tblStatsADMCount,#tblStatsOfficerCount').hide();
    $('#AStat' + key).attr('data-toggle', 'modal').attr('href', '#divStats');
    $('#lblStatsMgrName').html('');
    switch (Flag) {
        case 1:
            $('#HStatsTitle').html('Productivity');
            break;
        case 2:
            $('#HStatsTitle').html('Efficiency');
            break;
        case 3:
            $('#HStatsTitle').html('Conversion');
            break;
        case 4:
            $('#HStatsTitle').html('Case Holdings');
            break;
        case 5:
            $('#HStatsTitle').html('Paids Vs Targets');
            break;
        case 6:
            $('#HStatsTitle').html('Predicted Efficiency');
            break;
    }
    switch ($.session.get('TreeLevel')) {
        case '0':
            StatsManager($.session.get('ManagerID'), Flag);
            $('#tblStatsManagerCount').show();
            break;
        case '1':
            StatsADM($.session.get('ManagerID'), Flag);
            $('#tblStatsADMCount').show();
            $('#btnStatsADMBack').hide();
            break;
        case '2':
        case '3':
            $('#btnStatsBack').hide();
            $('#tblStatsOfficerCount').show();
            StatsOfficer($.session.get('ManagerID'), Flag);
            break;
    }
}

function StatsManager(ID, Flag) {
    return;
    Type = "GET";
    var inputParams = "/GetStatsOfficerCount?OfficerID=" + ID + "&Flag=" + Flag + "&" + $.session.get('BrowserId');
    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;
    //CallService();
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
            $('#imgLoader2').show();
        },
        complete: function () {
            $('#imgLoader2').fadeOut(1000);//.hide();
        },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                $('#tbodyStatsManagerCount').empty();
                if (result != "[]" && result != null) {
                    result = JSON.parse(result);
                    $.each(result, function (key, value) {
                        $('#tbodyStatsManagerCount').append('<tr><td><a style="cursor:pointer" onclick="StatsADM(' + value.OfficerID + ',' + Flag + ',' + "'" + value.OfficerName + "'" + ')" >' + value.OfficerName + '</a></td>' +
                                                            '<td>' + value.Cnt + '</td></tr>');
                    });
                }
                else {
                    $('#tbodyStatsManagerCount').append($('<tr>').append($('<td colspan="2">').html('No data found.')));
                }
            }
        },
        error: function () {
            //alert('error');
        }
    });
}

function StatsADM(OfficerID, Flag, StatsMgrName) {
    return;
    $('#tblStatsManagerCount,#tblStatsADMCount,#tblStatsOfficerCount,#btnStatsADMBack,#btnStatsBack').hide();
    $('#tblStatsADMCount').show();
    if ($.session.get('TreeLevel') == 0 || $.session.get('TreeLevel') == 1) {
        $('#btnStatsADMBack').show();
        $('#lblStatsMgrName').html(' - ' + StatsMgrName);
    }
    Type = "GET";
    var inputParams = "/GetStatsOfficerCount?OfficerID=" + OfficerID + "&Flag=" + Flag + "&" + $.session.get('BrowserId');
    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
            $('#imgLoader2').show();
        },
        complete: function () {
            $('#imgLoader2').fadeOut(1000);//.hide();
        },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                $('#tbodyStatsADMCount').empty();
                if (result != "[]" && result != null) {
                    result = JSON.parse(result);
                    $.each(result, function (key, value) {
                        $('#tbodyStatsADMCount').append('<tr><td><a style="cursor:pointer" onclick="StatsOfficer(' + value.OfficerID + ',' + Flag + ')" >' + value.OfficerName + '</a></td>' +
                                                           '<td>' + value.Cnt + '</td></tr>');
                    });
                }
                else {
                    $('#tbodyStatsADMCount').append($('<tr>').append($('<td>').html('No data found.')))
                }
            }
        },
        error: function () {
            //alert('error');
        }
    });
}

function StatsOfficer(ID, Flag) {
    return;
    $('#tblStatsManagerCount,#tblStatsADMCount,#tblStatsOfficerCount,#btnStatsADMBack,#btnStatsBack').hide();
    $('#tblStatsOfficerCount').show();
    if ($.session.get('TreeLevel') == 0 || $.session.get('TreeLevel') == 1) {
        $('#btnStatsBack').show();
    }
    switch (Flag) {
        case 1:
            $('#HStatsTitle').html('Productivity');
            break;
        case 2:
            $('#HStatsTitle').html('Efficiency');
            break;
        case 3:
            $('#HStatsTitle').html('Conversion');
            break;
        case 4:
            $('#HStatsTitle').html('Case Holdings');
            break;
        case 5:
            $('#HStatsTitle').html('Paids Vs Targets');
            break;
        case 6:
            $('#HStatsTitle').html('Predicted Efficiency');
            break;
    }
    Type = "GET";
    var inputParams = "/GetStatsOfficerCount?OfficerID=" + ID + "&Flag=" + Flag + "&" + $.session.get('BrowserId');
    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;
    //CallService();
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () {
            $('#imgLoader2').show();
        },
        complete: function () {
            $('#imgLoader2').fadeOut(1000);//.hide();
        },
        success: function (result) {//On Successfull service call  
            if (result != undefined) {
                $('#tbodyStatsOfficerCount').empty();
                if (result != "[]" && result != null) {
                    result = JSON.parse(result);
                    $.each(result, function (key, value) {
                        $('#tbodyStatsOfficerCount').append($('<tr>').append($('<td>').html(value.OfficerName)).append($('<td>').html(value.Cnt)))
                    });
                }
                else {
                    $('#tbodyStatsOfficerCount').append($('<tr>').append($('<td>').html('No data found.')))
                }
            }
        },
        error: function () {
            //alert('error');
        }
    });
}

function CaseSearchByOfficerID(OfficerID) {
    var Type = "GET";
    var DataType = "jsonp"; var ProcessData = false;
    var Url = serviceUrl + "/GetCaseSearchByOfficerID?OfficerID=" + OfficerID + "&" + $.session.get('BrowserId');

    var ContentType = "application/javascript";
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () { },
        complete: function () { },
        success: function (result) {//On Successfull service call                      
            if (!(result == null || result == '[]')) {

                result = JSON.parse(result);
                $('#tbodyCaseSearchByOfficerID').empty();
                var HTMLString = '';
                $.each(result, function (key, value) {
                    HTMLString += '<tr><td>' + value.casenumber + '</td><td>' + value.DebtName + '</td><td>' + value.ClientName + '</td><td>' + value.ContactFirstLine + ', ' + value.ContactAddress + ', ' + value.PostCode + '</td>' +
                            '<td>' + value.CaseStatus + '</td><td>' + value.OfficerFirstName + ' ' + value.OfficerLastName + '</td><td>' + value.Ageing + '</td><td>' + value.IssueDate + '</td><td>' + value.DateActioned + '</td></tr>';
                });
                $('#tbodyCaseSearchByOfficerID').html(HTMLString);

                $('#tblCaseSearchByOfficerID').dataTable({
                    "sScrollY": "370px",
                    "bAutoWidth": false,
                    "bPaginate": true,
                    "bDestroy": true,
                    "bSort": false,
                    "sPaginationType": "full_numbers",
                    "bLengthChange": false,
                    "bScrollCollapse": true,
                    "sInfo": true,
                    "iDisplayLength": 5

                });
            }
            else {
                $('#tbodyCaseSearchByOfficerID').empty();
                $('#tbodyCaseSearchByOfficerID').append($('<tr><td colspan="9"> No records found </td></tr>'));
            }

        },
        error: function () {
            //alert('error');
        } // When Service call fails
    });
}

function FillOfficerRankOnClick(Action, key, Condition, MgrName, ActionText) {
    var FnName = FillRankingsForADM;
    switch (Condition) {
        case 1: //Ranking ADM list
            //Tree level click function
            switch ($.session.get('TreeLevel')) {
                case '0':// Manager click: Shown ADM list
                    $('#DRankingOfficer,#DRankingCase,#btnRankADMBack,#btnRankOfficerBack').hide();
                    $('#DRankingADM').show();
                    $('#lblRankOfficerName,#lblRankMgrName,#lblRankADMName').html('');
                    $('#lblRankMgrName').html(' - ' + MgrName);
                    FnName = FillRankingsForADM;
                    break;
                case '1'://ADM click: Shown Officer list
                    $('#DRankingADM,#DRankingCase,#btnRankADMBack,#btnRankOfficerBack,#btnRankADMBack').hide();
                    $('#DRankingOfficer').show();
                    $('#lblRankOfficerName').html('');
                    $('#lblRankADMName').html(' - ' + MgrName);
                    FnName = FillRankingsForOfficer;
                    break;
                case '2'://Officer click: Shown case list
                    $('#DRankingADM,#DRankingOfficer,#DRankingCase,#btnRankADMBack,#btnRankOfficerBack').hide();
                    FillCaseActionDetailsOnClick(Action, key, MgrName, 0);
                    break;
            }
            break;
        case 2: //Ranking More ADM list
            switch (Action) {
                case 1://Paid
                    $('#DRankMorePaid,#DRankMorePaidCaseList').hide();
                    $('#DRankMorePaidADM,#btnPaidADMBack').show();
                    $('#lblRankPaidManager').html(' - ' + MgrName);
                    break;
                case 2://Returned
                    $('#DRankMoreReturned,#DRankMoreReturnedCaseList').hide();
                    $('#DRankMoreReturnedADM,#btnReturnedADMBack').show();
                    $('#lblRankReturnedManager').html(' - ' + MgrName);
                    break;
                case 3://Part Paid
                    $('#DRankMorePP,#DRankMorePPCaseList').hide();
                    $('#DRankMorePPADM,#btnPPADMBack').show();
                    $('#lblRankPPManager').html(' - ' + MgrName);
                    break;
            }
            FnName = FillRankingsForADM;
            break;
        case 3: // Ranking Officer list
            $('#DRankingADM,#DRankingCase,#btnRankOfficerBack').hide();
            if ($.session.get('TreeLevel') == '1')
                $('#btnRankADMBack').hide();
            else
                $('#btnRankADMBack').show();

            $('#DRankingOfficer').show();
            $('#lblRankOfficerName').html('');
            $('#lblRankADMName').html(' - ' + MgrName);
            FnName = FillRankingsForOfficer;
            break;
        case 4:// Ranking More Officer list
            switch (Action) {
                case 1://Paid
                    $('#DRankMorePaid,#DRankMorePaidCaseList,#DRankMorePaidADM,#btnPaidADMBack').hide();
                    $('#DRankMorePaidOfficer,#btnPaidOfficerBack').show();
                    $('#lblRankPaidADM').html(' - ' + MgrName);
                    break;
                case 2://Returned
                    $('#DRankMoreReturned,#DRankMoreReturnedCaseList,#DRankMoreReturnedADM,#btnReturnedADMBack').hide();
                    $('#DRankMoreReturnedOfficer,#btnReturnedOfficerBack').show();
                    $('#lblRankReturnedADM').html(' - ' + MgrName);
                    break;
                case 3://Part Paid
                    $('#DRankMorePP,#DRankMorePPCaseList,#DRankMorePPADM,#btnPPADMBack').hide();
                    $('#DRankMorePPOfficer,#btnPPOfficerBack').show();
                    $('#lblRankPPADM').html(' - ' + MgrName);
                    break;
            }
            FnName = FillRankingsForOfficer;
            break;
    }
    FnName(key, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), $.session.get('RankingType'), 0);
}
