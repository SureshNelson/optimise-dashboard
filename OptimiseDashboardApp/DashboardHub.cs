﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.RegularExpressions;
using System.Timers;
using System.Web;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Newtonsoft.Json;

namespace OptimiseDashboardApp
{
    public class DashboardHub : Hub
    {
        private static int clientCounter = 0;
        //     private static Dictionary<string, string> LoggedInUsers = new Dictionary<string, string>();
        private static DataTable dtCaseActions;
        private static DataTable dtWMA;
        private static DataTable dtOfficersTree;
        private static DataTable dtManagerTree;
        private static DataTable dtStats;
        private static DataTable dtLastLocation;
        private static DataTable dtHeartBeat;

        private static DataTable dtEnforcementService;
        private static DataTable dtESOfficer;

        private static string Token = "";

        public DashboardHub()
        {

            //if (dtOfficersTree == null)
            //    getofficerstree();

            //if (dtStats == null)
            //    getstats();

            //=========================================================================================================
            // Enforcement service : Signal-R
            //if (dtESOfficer == null)
            //{
            //    Timer myTimerOfficer = new Timer();
            //    myTimerOfficer.Elapsed += new ElapsedEventHandler(getofficersfores);
            //    myTimerOfficer.Interval = 60000;
            //    myTimerOfficer.Start();
            //}

            //if (dtEnforcementService == null)
            //{
            //    Timer myTimerCase = new Timer();
            //    myTimerCase.Elapsed += new ElapsedEventHandler(getallrequestdashbord);
            //    myTimerCase.Interval = 60000;
            //    myTimerCase.Start();
            //}

        }

        #region FirstCall

        public void getmanagertree(int managerid)
        {
            try
            {
                Token = Context.QueryString["Authorization"];
                string URL = ConfigurationManager.AppSettings["AppService"] + "api/v1/officers/" + managerid + "/OfficersManager";
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.BaseAddress = new Uri(URL);
                HttpResponseMessage response = client.GetAsync("").Result;  // Blocking call!
                var readAsStringAsync = response.Content.ReadAsStringAsync();
                string message = readAsStringAsync.Result;
                dtManagerTree = ConvertJSONToDataTable(CleanJson(message));
                Clients.Caller.getmanagertree(JSONHelper.FromDataTable(dtManagerTree));
                if (dtStats == null)
                    getstats();
            }
            catch (AggregateException ex)
            {
                throw ex;
            }
            catch (System.Net.WebException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void getofficerstree()
        {
            try
            {
                string URL = ConfigurationManager.AppSettings["AppService"] + "api/v1/officers/OfficersTree";// "GetOfficersTree";
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Context.QueryString["Authorization"]);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.BaseAddress = new Uri(URL);
                HttpResponseMessage response = client.GetAsync("").Result;  // Blocking call!
                var readAsStringAsync = response.Content.ReadAsStringAsync();
                string message = readAsStringAsync.Result;
                dtOfficersTree = ConvertJSONToDataTable(CleanJson(message));

                if (dtCaseActions == null)
                {
                    Timer myTimer = new Timer();
                    myTimer.Elapsed += new ElapsedEventHandler(getcaseactionfordate);
                    myTimer.Interval = 60000;
                    myTimer.Start();
                }

                if (dtWMA == null)
                {
                    Timer myTimerWMA = new Timer();
                    myTimerWMA.Elapsed += new ElapsedEventHandler(getwmafordate);
                    myTimerWMA.Interval = 60000;
                    myTimerWMA.Start();
                }

                if (dtLastLocation == null)
                {
                    Timer myTimerLL = new Timer();
                    myTimerLL.Elapsed += new ElapsedEventHandler(getlastlocationfordate);
                    myTimerLL.Interval = 60000;
                    myTimerLL.Start();
                }

                if (dtHeartBeat == null)
                {
                    Timer myTimerHB = new Timer();
                    myTimerHB.Elapsed += new ElapsedEventHandler(gethbfordate);
                    myTimerHB.Interval = 60000;
                    myTimerHB.Start();
                }
            }
            catch (AggregateException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void getcaseactionfordate(object source, ElapsedEventArgs e)
        {
            try
            {
                string URL = ConfigurationManager.AppSettings["AppService"] + "api/v1/cases/" + DateTime.Now.ToString("dd-mm-yyyy") + "/CaseActionForDate";// "GetCaseActionForDate_Dashboard";
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.BaseAddress = new Uri(URL);
                HttpResponseMessage response = client.GetAsync("").Result;  // Blocking call!
                var readAsStringAsync = response.Content.ReadAsStringAsync();
                string message = readAsStringAsync.Result;
                dtCaseActions = ConvertJSONToDataTable(CleanJson(message));
            }
            catch (AggregateException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void getwmafordate(object source, ElapsedEventArgs e)
        {
            try
            {
                string URL = ConfigurationManager.AppSettings["AppService"] + "api/v1/officers/" + DateTime.Now.ToString("dd-mm-yyyy") + "/WMAForDate";//"GetWMAForDate_Dashboard";
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.BaseAddress = new Uri(URL);
                HttpResponseMessage response = client.GetAsync("").Result;  // Blocking call!
                var readAsStringAsync = response.Content.ReadAsStringAsync();
                string message = readAsStringAsync.Result;
                dtWMA = ConvertJSONToDataTable(CleanJson(message));
            }
            catch (AggregateException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void getlastlocationfordate(object source, ElapsedEventArgs e)
        {
            try
            {
                string URL = ConfigurationManager.AppSettings["AppService"] + "api/v1/officers/" + DateTime.Now.ToString("dd-mm-yyyy") + "/GetLastLocationForDate";//"GetLastLocationForDate_Dashboard";
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.BaseAddress = new Uri(URL);
                HttpResponseMessage response = client.GetAsync("").Result;  // Blocking call!
                var readAsStringAsync = response.Content.ReadAsStringAsync();
                string message = readAsStringAsync.Result;
                dtLastLocation = ConvertJSONToDataTable(CleanJson(message));
            }
            catch (AggregateException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void gethbfordate(object source, ElapsedEventArgs e)
        {
            try
            {
                string URL = ConfigurationManager.AppSettings["AppService"] + "api/v1/officers/" + DateTime.Now.ToString("dd-mm-yyyy") + "/HBForDate";// "GetHBForDate_Dashboard";
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.BaseAddress = new Uri(URL);
                HttpResponseMessage response = client.GetAsync("").Result;  // Blocking call!
                var readAsStringAsync = response.Content.ReadAsStringAsync();
                string message = readAsStringAsync.Result;
                dtHeartBeat = ConvertJSONToDataTable(CleanJson(message));
            }
            catch (AggregateException ex)
            {
                throw ex;
            }
            catch (System.Net.WebException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void getstats()
        {
            try
            {
                string URL = ConfigurationManager.AppSettings["AppService"] + "api/v1/officers/StatsData";// "GetStatsData";
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.BaseAddress = new Uri(URL);
                HttpResponseMessage response = client.GetAsync("").Result;  // Blocking call!
                var readAsStringAsync = response.Content.ReadAsStringAsync();
                string message = readAsStringAsync.Result;
                dtStats = ConvertJSONToDataTable(CleanJson(message));
            }
            catch (AggregateException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void getofficersfores()
        {
            try
            {
                string URL = ConfigurationManager.AppSettings["AppService"] + "api/v1/OfficersforES";
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Context.QueryString["Authorization"]);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.BaseAddress = new Uri(URL);
                HttpResponseMessage response = client.GetAsync("").Result;  // Blocking call!
                var readAsStringAsync = response.Content.ReadAsStringAsync();
                string message = readAsStringAsync.Result;
                dtESOfficer = ConvertJSONToDataTable(CleanJson(message));
                Clients.Caller.getofficersfores(JSONHelper.FromDataTable(dtESOfficer));
            }
            catch (AggregateException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void getallrequestdashbord()
        {
            try
            {
                string URL = ConfigurationManager.AppSettings["AppService"] + "api/v1/AllRequest4Dashboard";// "GetAllRequest4Dashboard_ES";
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Context.QueryString["Authorization"]);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.BaseAddress = new Uri(URL);
                HttpResponseMessage response = client.GetAsync("").Result;  // Blocking call!
                var readAsStringAsync = response.Content.ReadAsStringAsync();
                string message = readAsStringAsync.Result;
                dtEnforcementService = ConvertJSONToDataTable(CleanJson(message));
            }
            catch (AggregateException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region CaseAction

        public void getcaseactions(string urlparam)
        {
            var strParam = urlparam.Split(new char[] { ',' });
            int ManagerID = int.Parse(strParam[0]);
            DateTime FromDate = DateTime.Parse(strParam[1]);
            DateTime ToDate = DateTime.Parse(strParam[2]);
            int OfficerID = int.Parse(strParam[3]);
            int GroupID = int.Parse(strParam[4]);
            DataTable dtFilteredOfficer = new DataTable();

            try
            {
                if (OfficerID > 0)
                {
                    dtFilteredOfficer = dtCaseActions.Select("Officer='" + OfficerID + "'").CopyToDataTable();
                    ManagerID = 0;
                }
                else if (ManagerID > 0)
                    dtFilteredOfficer = dtCaseActions.Select("ManagerID='" + ManagerID + "'").CopyToDataTable();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            Clients.Caller.getcaseactions(JSONHelper.FromDataTable(dtFilteredOfficer));
        }

        public void gethubcaseactions(string caParam)
        {
            var strParam = caParam.Split(new char[] { ',' });

            int ManagerID = int.Parse(strParam[0]);
            DateTime FromDate = DateTime.Parse(strParam[1]);
            int OfficerID = int.Parse(strParam[2]);
            int GroupID = int.Parse(strParam[3]);

            DataSet ds = new DataSet();
            ds.Tables.Add("CaseActionStats");
            ds.Tables["CaseActionStats"].Columns.Add("ActionText");
            ds.Tables["CaseActionStats"].Columns.Add("Case");
            ds.Tables["CaseActionStats"].Columns.Add("Ave");

            int Paid, PartPaid, Returned, LeftLetter, DefendentContact, Clamped;

            Paid = PartPaid = Returned = LeftLetter = DefendentContact = Clamped = 0;
            if (ManagerID != 9999 && ManagerID != 6999)
            {
                DataTable dtFilteredCaseActions = new DataTable();
                DataRow[] drFilteredCaseActions;
                if (dtCaseActions != null)
                {
                    drFilteredCaseActions = dtCaseActions.Select("ManagerID = '" + ManagerID + "'");
                    if (drFilteredCaseActions.Length > 0)
                        dtFilteredCaseActions = drFilteredCaseActions.CopyToDataTable();
                    else
                    {
                        drFilteredCaseActions = dtCaseActions.Select("ADMID = '" + ManagerID + "'");
                        if (drFilteredCaseActions.Length > 0)
                            dtFilteredCaseActions = drFilteredCaseActions.CopyToDataTable();
                        else
                        {
                            drFilteredCaseActions = dtCaseActions.Select("Officer = '" + ((ManagerID == 0) ? OfficerID : ManagerID) + "'");
                            if (drFilteredCaseActions.Length > 0)
                                dtFilteredCaseActions = drFilteredCaseActions.CopyToDataTable();
                        }
                    }
                }

                Paid = (dtFilteredCaseActions.Rows.Count > 0) ? dtFilteredCaseActions.Select("ActionText = 'Paid'").Length : 0;
                PartPaid = (dtFilteredCaseActions.Rows.Count > 0) ? dtFilteredCaseActions.Select("ActionText = 'Part Paid'").Length : 0;
                Returned = (dtFilteredCaseActions.Rows.Count > 0) ? dtFilteredCaseActions.Select("ActionText = 'Returned'").Length : 0;
                LeftLetter = (dtFilteredCaseActions.Rows.Count > 0) ? dtFilteredCaseActions.Select("ActionText = 'Left Letter'").Length : 0;
                DefendentContact = (dtFilteredCaseActions.Rows.Count > 0) ? dtFilteredCaseActions.Select("ActionText = 'Defendant contact'").Length : 0;
                Clamped = (dtFilteredCaseActions.Rows.Count > 0) ? dtFilteredCaseActions.Select("ActionText = 'Clamped'").Length : 0;
            }
            else
            {
                Paid = (dtCaseActions != null) ? dtCaseActions.Select("ActionText = 'Paid'").Length : 0;
                PartPaid = (dtCaseActions != null) ? dtCaseActions.Select("ActionText = 'Part Paid'").Length : 0;
                Returned = (dtCaseActions != null) ? dtCaseActions.Select("ActionText = 'Returned'").Length : 0;
                LeftLetter = (dtCaseActions != null) ? dtCaseActions.Select("ActionText = 'Left Letter'").Length : 0;
                DefendentContact = (dtCaseActions != null) ? dtCaseActions.Select("ActionText = 'Defendant contact'").Length : 0;
                Clamped = (dtCaseActions != null) ? dtCaseActions.Select("ActionText = 'Clamped'").Length : 0;
            }

            DataRow dr;
            dr = ds.Tables["CaseActionStats"].NewRow();
            dr["ActionText"] = "Paid";
            dr["Case"] = Paid;
            dr["Ave"] = "0";
            ds.Tables["CaseActionStats"].Rows.Add(dr);

            dr = ds.Tables["CaseActionStats"].NewRow();
            dr["ActionText"] = "Part Paid";
            dr["Case"] = PartPaid;
            dr["Ave"] = "0";
            ds.Tables["CaseActionStats"].Rows.Add(dr);

            dr = ds.Tables["CaseActionStats"].NewRow();
            dr["ActionText"] = "Returned";
            dr["Case"] = Returned;
            dr["Ave"] = "0";
            ds.Tables["CaseActionStats"].Rows.Add(dr);

            dr = ds.Tables["CaseActionStats"].NewRow();
            dr["ActionText"] = "Left Letter";
            dr["Case"] = LeftLetter;
            dr["Ave"] = "0";
            ds.Tables["CaseActionStats"].Rows.Add(dr);

            dr = ds.Tables["CaseActionStats"].NewRow();
            dr["ActionText"] = "Defendant contact";
            dr["Case"] = DefendentContact;
            dr["Ave"] = "0";
            ds.Tables["CaseActionStats"].Rows.Add(dr);

            dr = ds.Tables["CaseActionStats"].NewRow();
            dr["ActionText"] = "Clamped";
            dr["Case"] = Clamped;
            dr["Ave"] = "0";
            ds.Tables["CaseActionStats"].Rows.Add(dr);

            Clients.Caller.gethubcaseactions(JSONHelper.FromDataTable(ds.Tables["CaseActionStats"]));
        }

        public void getcaseactionmanager(string caParam, string flag, string type)
        {
            var strParam = caParam.Split(new char[] { ',' });
            string ActionText = strParam[0].ToString();
            DateTime FromDate = DateTime.Parse(strParam[1]);
            DateTime ToDate = DateTime.Parse(strParam[2]);
            int CompanyID = int.Parse(strParam[3]);
            int ManagerID = int.Parse(strParam[4]);
            int GroupID = int.Parse(strParam[4]);

            DataTable dt = new DataTable("CAManager");
            dt.Columns.Add("OfficerID");
            dt.Columns.Add("OfficerName");
            dt.Columns.Add("CaseCnt");
            if (ManagerID == 6999 || ManagerID == 9999)
            {
                var qOfficer = from r in dtOfficersTree.AsEnumerable()
                               let otArray = new object[]
                                {
                                    r.Field<string>("ManagerID"), r.Field<string>("ManagerName"), 0
                                }
                               select otArray;

                foreach (var dr in qOfficer)
                {
                    dt.Rows.Add(dr);
                }
            }
            if (type == "ADM")
            {
                DataRow[] drFilteredOfficer = dtOfficersTree.Select("ManagerID='" + ManagerID + "'");
                DataTable dtFilteredOfficer = (drFilteredOfficer.Length > 0 ? drFilteredOfficer.CopyToDataTable() : new DataTable());
                var qOfficer = (from r in dtFilteredOfficer.AsEnumerable()
                                let otArray = new object[]
                                {
                                    r.Field<string>("ADMID"), r.Field<string>("ADMName"), 0
                                }
                                select otArray);
                foreach (var dr in qOfficer)
                {
                    dt.Rows.Add(dr);
                }
            }
            else if (type == "Officer")
            {
                DataRow[] drFilteredOfficer = dtOfficersTree.Select("ADMID='" + ManagerID + "'");
                DataTable dtFilteredOfficer = (drFilteredOfficer.Length > 0 ? drFilteredOfficer.CopyToDataTable() : new DataTable());
                var qOfficer = (from r in dtFilteredOfficer.AsEnumerable()
                                let otArray = new object[]
                                {
                                    r.Field<string>("OfficerID"), r.Field<string>("OfficerName"), 0
                                }
                                select otArray);
                foreach (var dr in qOfficer)
                {
                    dt.Rows.Add(dr);
                }
            }

            DataView view = new DataView(dt);
            dt = view.ToTable(true, "OfficerID", "OfficerName", "CaseCnt");
            foreach (DataRow dr in dt.Rows)
            {
                if (type == "Manager")
                {
                    DataRow[] cRow = dtCaseActions != null ? dtCaseActions.Select("ActionText = '" + ActionText + "' AND ManagerID='" + dr[0].ToString() + "'") : null;
                    DataRow[] HRow = dt.Select("OfficerID = '" + dr[0].ToString() + "'");
                    HRow[0]["CaseCnt"] = (cRow == null) ? 0 : cRow.Length;
                }
                else if (type == "ADM")
                {
                    DataRow[] cRow = dtCaseActions != null ? dtCaseActions.Select("ActionText = '" + ActionText + "' AND ADMID='" + dr[0].ToString() + "'") : null;
                    DataRow[] HRow = dt.Select("OfficerID = '" + dr[0].ToString() + "'");
                    HRow[0]["CaseCnt"] = (cRow == null) ? 0 : cRow.Length;
                }
                else if (type == "Officer")
                {
                    DataRow[] cRow = dtCaseActions != null ? dtCaseActions.Select("ActionText = '" + ActionText + "' AND Officer='" + dr[0].ToString() + "'") : null;
                    DataRow[] HRow = dt.Select("OfficerID = '" + dr[0].ToString() + "'");
                    HRow[0]["CaseCnt"] = (cRow == null) ? 0 : cRow.Length;
                }
            }
            dt = dt.Select("", "CaseCnt DESC").CopyToDataTable();
            Clients.Caller.getcaseactionmanager(JSONHelper.FromDataTable(dt), flag, type);
        }

        #endregion

        #region WMA

        public void getwmadetail(string caParam, string type)
        {
            var strParam = caParam.Split(new char[] { ',' });
            int ManagerID = int.Parse(strParam[0]);
            string FromDate = strParam[1].ToString();

            DataTable dt = new DataTable("WMADetail");
            dt.Columns.Add("OfficerID");
            dt.Columns.Add("OfficerName");
            dt.Columns.Add("CaseCnt");
            dt.Columns.Add("FromDate");

            if (ManagerID == 6999 || ManagerID == 9999)
            {
                if (dtWMA != null)
                {
                    var qOfficer = from r in dtWMA.AsEnumerable()
                                   let otArray = new object[]
                                {
                                    r.Field<string>("ManagerID"), r.Field<string>("ManagerName"), 0,FromDate//,r.Field<string>("SearchText"),r.Field<string>("TotalRecordCount"),r.Field<string>("LogType"),r.Field<string>("ActionedDate")
                                }
                                   select otArray;

                    foreach (var dr in qOfficer)
                    {
                        dt.Rows.Add(dr);
                    }
                }
            }
            if (type == "ADM")
            {
                if (dtWMA != null || dtWMA.Rows.Count > 0)
                {
                    DataRow[] drFilteredOfficer = dtWMA.Select("ManagerID='" + ManagerID + "'");
                    DataTable dtFilteredOfficer = (drFilteredOfficer.Length > 0 ? drFilteredOfficer.CopyToDataTable() : new DataTable());

                    var qOfficer = (from r in dtFilteredOfficer.AsEnumerable()
                                    let otArray = new object[]
                                {
                                    r.Field<string>("ADMID"), r.Field<string>("ADMName"), 0,FromDate//,r.Field<string>("SearchText"),r.Field<string>("TotalRecordCount"),r.Field<string>("LogType"),r.Field<string>("ActionedDate")
                                }
                                    select otArray);

                    foreach (var dr in qOfficer)
                    {
                        dt.Rows.Add(dr);
                    }
                }
            }
            else if (type == "Officer")
            {
                if (dtWMA != null || dtWMA.Rows.Count > 0)
                {
                    DataRow[] drFilteredOfficer = dtWMA.Select("ADMID='" + ManagerID + "'");
                    DataTable dtFilteredOfficer = (drFilteredOfficer.Length > 0 ? drFilteredOfficer.CopyToDataTable() : new DataTable());

                    var qOfficer = (from r in dtFilteredOfficer.AsEnumerable()
                                    let otArray = new object[]
                                {
                                    r.Field<string>("OfficerId"), r.Field<string>("OfficerName"), 0, FromDate//,r.Field<string>("SearchText"),r.Field<string>("TotalRecordCount"),r.Field<string>("LogType"),r.Field<string>("ActionedDate")
                                }
                                    select otArray);

                    foreach (var dr in qOfficer)
                    {
                        dt.Rows.Add(dr);
                    }
                }
            }

            DataView view = new DataView(dt);
            dt = view.ToTable(true, "OfficerID", "OfficerName", "CaseCnt", "FromDate");//, "SearchText", "TotalRecordCount", "LogType", "ActionedDate"

            foreach (DataRow dr in dt.Rows)
            {
                if (type == "Manager")
                {
                    DataRow[] cRow = dtWMA != null ? dtWMA.Select("ManagerID='" + dr[0].ToString() + "'") : null;
                    DataRow[] HRow = dt.Select("OfficerID = '" + dr[0].ToString() + "'");
                    HRow[0]["CaseCnt"] = (cRow == null) ? 0 : cRow.Length;
                }
                else if (type == "ADM")
                {
                    DataRow[] cRow = dtWMA != null ? dtWMA.Select("ADMID='" + dr[0].ToString() + "'") : null;
                    DataRow[] HRow = dt.Select("OfficerID = '" + dr[0].ToString() + "'");
                    HRow[0]["CaseCnt"] = (cRow == null) ? 0 : cRow.Length;
                }
                else if (type == "Officer")
                {
                    DataRow[] cRow = dtWMA != null ? dtWMA.Select("OfficerId='" + dr[0].ToString() + "'") : null;
                    DataRow[] HRow = dt.Select("OfficerID = '" + dr[0].ToString() + "'");
                    HRow[0]["CaseCnt"] = (cRow == null) ? 0 : cRow.Length;
                }
            }
            if (dt.Rows.Count > 0)
                dt = dt.Select("", "CaseCnt DESC").CopyToDataTable();

            Clients.Caller.getwmadetail(JSONHelper.FromDataTable(dt), type);
        }

        public void getwmalogdetail(string caParam, string type)
        {
            var strParam = caParam.Split(new char[] { ',' });

            int OfficerID = int.Parse(strParam[0]);
            string FromDate = strParam[1].ToString();

            DataSet ds = new DataSet();
            ds.Tables.Add("WMALogType");
            ds.Tables["WMALogType"].Columns.Add("OfficerID");
            ds.Tables["WMALogType"].Columns.Add("FromDate");
            ds.Tables["WMALogType"].Columns.Add("LogType");
            ds.Tables["WMALogType"].Columns.Add("Case");

            int Postcode, Vrm, Caseno;
            Postcode = Vrm = Caseno = 0;

            if (OfficerID != 9999 && OfficerID != 6999)
            {
                DataTable dtFilteredWMA = new DataTable();
                DataRow[] drFilteredWMA;
                if (dtWMA != null)
                {
                    drFilteredWMA = dtWMA.Select("ManagerID = '" + OfficerID + "'");
                    if (drFilteredWMA.Length > 0)
                        dtFilteredWMA = drFilteredWMA.CopyToDataTable();
                    else
                    {
                        drFilteredWMA = dtWMA.Select("ADMID = '" + OfficerID + "'");
                        if (drFilteredWMA.Length > 0)
                            dtFilteredWMA = drFilteredWMA.CopyToDataTable();
                        else
                        {
                            drFilteredWMA = dtWMA.Select("OfficerId = '" + ((OfficerID == 0) ? OfficerID : OfficerID) + "'");
                            if (drFilteredWMA.Length > 0)
                                dtFilteredWMA = drFilteredWMA.CopyToDataTable();
                        }
                    }
                }
                Postcode = (dtFilteredWMA != null) ? dtFilteredWMA.Select("LogType = 'Postcode'").Length : 0;
                Vrm = (dtFilteredWMA != null) ? dtFilteredWMA.Select("LogType = 'VRM'").Length : 0;
                Caseno = (dtFilteredWMA != null) ? dtFilteredWMA.Select("LogType = 'CaseNo'").Length : 0;
            }
            else
            {
                Postcode = (dtWMA != null) ? dtWMA.Select("LogType = 'Postcode'").Length : 0;
                Vrm = (dtWMA != null) ? dtWMA.Select("LogType = 'VRM'").Length : 0;
                Caseno = (dtWMA != null) ? dtWMA.Select("LogType = 'CaseNo'").Length : 0;
            }

            DataRow dr;
            dr = ds.Tables["WMALogType"].NewRow();
            dr["OfficerID"] = type;
            dr["FromDate"] = FromDate;
            dr["LogType"] = "Postcode";
            dr["Case"] = Postcode;
            ds.Tables["WMALogType"].Rows.Add(dr);

            dr = ds.Tables["WMALogType"].NewRow();
            dr["OfficerID"] = type;
            dr["FromDate"] = FromDate;
            dr["LogType"] = "VRM";
            dr["Case"] = Vrm;
            ds.Tables["WMALogType"].Rows.Add(dr);

            dr = ds.Tables["WMALogType"].NewRow();
            dr["OfficerID"] = type;
            dr["FromDate"] = FromDate;
            dr["LogType"] = "CaseNo";
            dr["Case"] = Caseno;
            ds.Tables["WMALogType"].Rows.Add(dr);

            Clients.Caller.getwmalogdetail(JSONHelper.FromDataTable(ds.Tables["WMALogType"]));
        }

        public void getwmacasedetail(string caParam)
        {
            var strParam = caParam.Split(new char[] { ',' });
            int OfficerID = int.Parse(strParam[0]);
            string FromDate = strParam[1].ToString();
            string LogType = strParam[2].ToString();

            DataTable dtFilteredWMACase = new DataTable();
            if (dtWMA.Select("LogType = '" + LogType + "' AND OfficerId='" + OfficerID + "'").Count() > 0)
                dtFilteredWMACase = dtWMA.Select("LogType = '" + LogType + "' AND OfficerId='" + OfficerID + "'").CopyToDataTable();

            Clients.Caller.getwmacasedetail(JSONHelper.FromDataTable(dtFilteredWMACase) + "||" + LogType);
        }

        #endregion

        #region Lastlocation

        public void getlastlocation(string lcParam)
        {
            var strParam = lcParam.Split(new char[] { ',' });

            int ManagerID = int.Parse(strParam[0]);
            DateTime FromDate = DateTime.Parse(strParam[1]);
            int OfficerID = int.Parse(strParam[2]);
            int GroupID = int.Parse(strParam[3]);

            DataSet ds = new DataSet();
            ds.Tables.Add("LastLocationStatus");
            ds.Tables["LastLocationStatus"].Columns.Add("latitude");
            ds.Tables["LastLocationStatus"].Columns.Add("longitude");
            ds.Tables["LastLocationStatus"].Columns.Add("ActionText");
            ds.Tables["LastLocationStatus"].Columns.Add("html");
            ds.Tables["LastLocationStatus"].Columns.Add("IconType");

            string GPSLatitude = string.Empty;
            string GPSLongitude = string.Empty;
            DataTable dtFilteredLastLocation = new DataTable();
            DataRow[] drFilteredLastLocation;

            if (ManagerID != 9999 && ManagerID != 6999)
            {
                if (dtLastLocation != null)
                {
                    drFilteredLastLocation = dtLastLocation.Select("ManagerID = '" + ManagerID + "'");
                    if (drFilteredLastLocation.Length > 0)
                        dtFilteredLastLocation = drFilteredLastLocation.CopyToDataTable();
                    else
                    {
                        drFilteredLastLocation = dtLastLocation.Select("ADMID = '" + ManagerID + "'");
                        if (drFilteredLastLocation.Length > 0)
                            dtFilteredLastLocation = drFilteredLastLocation.CopyToDataTable();
                        else
                        {
                            drFilteredLastLocation = dtLastLocation.Select("OfficerID = '" + ((ManagerID == 0) ? OfficerID : ManagerID) + "'");
                            if (drFilteredLastLocation.Length > 0)
                                dtFilteredLastLocation = drFilteredLastLocation.CopyToDataTable();
                        }
                    }
                }
            }
            else
            {
                dtFilteredLastLocation = dtLastLocation;
            }

            DataRowView drvOutput;
            DataView dvOutput;
            dvOutput = ds.Tables["LastLocationStatus"].DefaultView;

            if (dtFilteredLastLocation != null)
            {
                foreach (DataRow dr in dtFilteredLastLocation.Rows)
                {
                    drvOutput = dvOutput.AddNew();
                    drvOutput["latitude"] = dr["GPSLatitude"];
                    drvOutput["longitude"] = dr["GPSLongitude"];
                    drvOutput["ActionText"] = dr["ActionText"];
                    drvOutput["html"] = "<b>" + dr["OfficerName"] + "[" + dr["OfficerID"] + "]" + "</b><br>" + " " + dr["CaseNumber"] + " " + " - <b>" + dr["ActionText"] + "</b><br>" + " " + dr["DateActioned"] + "<br> Accuracy: " + dr["GPSHDOP"] + " metres <br>";
                    drvOutput["IconType"] = dr["IconType"];
                    drvOutput.EndEdit();
                }
            }
            Clients.Caller.getlastlocation(JSONHelper.FromDataTable(ds.Tables["LastLocationStatus"]));
        }

        #endregion

        #region HeartBeat

        public void getheartbeat(string lcParam)
        {
            var strParam = lcParam.Split(new char[] { ',' });
            int OfficerID = int.Parse(strParam[0]);
            DateTime FromDate = DateTime.Parse(strParam[1]);
            DataSet ds = new DataSet();
            ds.Tables.Add("HeartBeatStatus");
            ds.Tables["HeartBeatStatus"].Columns.Add("latitude");
            ds.Tables["HeartBeatStatus"].Columns.Add("longitude");
            ds.Tables["HeartBeatStatus"].Columns.Add("html");

            string GPSLatitude = string.Empty;
            string GPSLongitude = string.Empty;
            DataTable dtFilteredHB = new DataTable();
            DataRow[] drFilteredHB;

            if (dtHeartBeat != null)
            {
                drFilteredHB = dtHeartBeat.Select("OfficerID = '" + OfficerID + "'");
                if (drFilteredHB.Length > 0)
                    dtFilteredHB = drFilteredHB.CopyToDataTable();
            }

            DataRowView drvOutput;
            DataView dvOutput;
            dvOutput = ds.Tables["HeartBeatStatus"].DefaultView;

            foreach (DataRow dr in dtFilteredHB.Rows)
            {
                drvOutput = dvOutput.AddNew();
                drvOutput["latitude"] = dr["latitude"];
                drvOutput["longitude"] = dr["longitude"];
                drvOutput["html"] = dr["html"];
                drvOutput.EndEdit();
            }
            Clients.Caller.getheartbeat(JSONHelper.FromDataTable(ds.Tables["HeartBeatStatus"]));
        }
        #endregion

        #region Ranking

        public void getrankingmanager(string caParam, string flag, string type)
        {
            var strParam = caParam.Split(new char[] { ',' });
            string ActionText = strParam[0].ToString();
            string FromDate = strParam[1].ToString();
            string ToDate = strParam[2].ToString();
            int CompanyID = int.Parse(strParam[3]);
            int ManagerID = int.Parse(strParam[4]);

            DataTable dt = new DataTable("RankingManager");
            dt.Columns.Add("OfficerID");
            dt.Columns.Add("OfficerName");
            dt.Columns.Add("CaseCnt");
            dt.Columns.Add("ActionText");
            dt.Columns.Add("FromDate");
            dt.Columns.Add("ToDate");
            if (ManagerID == 6999 || ManagerID == 9999)
            {
                var qOfficer = from r in dtOfficersTree.AsEnumerable()
                               let otArray = new object[]
                                {
                                    r.Field<string>("ManagerID"), r.Field<string>("ManagerName"), 0,ActionText,FromDate,ToDate
                                }
                               select otArray;

                foreach (var dr in qOfficer)
                {
                    dt.Rows.Add(dr);
                }
            }
            if (type == "ADM")
            {
                DataRow[] drFilteredOfficer = dtOfficersTree.Select("ManagerID='" + ManagerID + "'");
                DataTable dtFilteredOfficer = (drFilteredOfficer.Length > 0 ? drFilteredOfficer.CopyToDataTable() : new DataTable());

                var qOfficer = (from r in dtFilteredOfficer.AsEnumerable()
                                let otArray = new object[]
                                {
                                    r.Field<string>("ADMID"), r.Field<string>("ADMName"), 0,ActionText,FromDate,ToDate
                                }
                                select otArray);

                foreach (var dr in qOfficer)
                {
                    dt.Rows.Add(dr);
                }
            }
            else if (type == "Officer")
            {
                DataRow[] drFilteredOfficer = dtOfficersTree.Select("ADMID='" + ManagerID + "'");
                DataTable dtFilteredOfficer = (drFilteredOfficer.Length > 0 ? drFilteredOfficer.CopyToDataTable() : new DataTable());

                var qOfficer = (from r in dtFilteredOfficer.AsEnumerable()
                                let otArray = new object[]
                                {
                                    r.Field<string>("OfficerID"), r.Field<string>("OfficerName"), 0,ActionText,FromDate,ToDate//.ToString("yyyy/MM/dd")
                                }
                                select otArray);

                foreach (var dr in qOfficer)
                {
                    dt.Rows.Add(dr);
                }
            }

            DataView view = new DataView(dt);
            dt = view.ToTable(true, "OfficerID", "OfficerName", "CaseCnt", "ActionText", "FromDate", "ToDate");
            foreach (DataRow dr in dt.Rows)
            {
                if (type == "Manager")
                {

                    DataRow[] cRow = dtCaseActions != null ? dtCaseActions.Select("ActionText = '" + ActionText + "' AND ManagerID='" + dr[0].ToString() + "'") : null;
                    DataRow[] HRow = dt.Select("OfficerID = '" + dr[0].ToString() + "'");
                    HRow[0]["CaseCnt"] = (cRow == null) ? 0 : cRow.Length;
                }
                else if (type == "ADM")
                {
                    DataRow[] cRow = dtCaseActions != null ? dtCaseActions.Select("ActionText = '" + ActionText + "' AND ADMID='" + dr[0].ToString() + "'") : null;
                    DataRow[] HRow = dt.Select("OfficerID = '" + dr[0].ToString() + "'");
                    HRow[0]["CaseCnt"] = (cRow == null) ? 0 : cRow.Length;
                }
                else if (type == "Officer")
                {
                    DataRow[] cRow = dtCaseActions != null ? dtCaseActions.Select("ActionText = '" + ActionText + "' AND Officer='" + dr[0].ToString() + "'") : null;
                    DataRow[] HRow = dt.Select("OfficerID = '" + dr[0].ToString() + "'");
                    HRow[0]["CaseCnt"] = (cRow == null) ? 0 : cRow.Length;
                }
            }
            if (dt.Select("", "CaseCnt DESC").Count() > 0)
                dt = dt.Select("", "CaseCnt DESC").CopyToDataTable();
            Clients.Caller.getrankingmanager(JSONHelper.FromDataTable(dt), flag, type);
        }

        #endregion

        #region Stats

        public void gethubstats(string caParam, int link)
        {
            var strParam = caParam.Split(new char[] { ',' });
            int ManagerID = int.Parse(strParam[0]);

            DataSet ds = new DataSet();
            ds.Tables.Add("StatsAction");
            ds.Tables["StatsAction"].Columns.Add("Description");
            ds.Tables["StatsAction"].Columns.Add("DisplayValue");
            ds.Tables["StatsAction"].Columns.Add("Formula");
            ds.Tables["StatsAction"].Columns.Add("Calculation");

            double Productivity, Efficiency, Conversion, TotalPaidCaseCnt, Target, TotalVisitedCnt, TotalCaseCnt, Availability, Caseholdings;
            int PaidsVsTarget, PredictedEfficiency;
            Productivity = Efficiency = Conversion = Caseholdings = PaidsVsTarget = PredictedEfficiency = 0;
            TotalPaidCaseCnt = Target = TotalVisitedCnt = TotalCaseCnt = Availability = 0;

            double TotalVisitCntSum, TotalCaseCntSum, TotalPaidCaseCntSum;
            TotalVisitCntSum = TotalCaseCntSum = TotalPaidCaseCntSum = 0;
            DataTable dtFilteredStats = new DataTable();
            DataRow[] drFilteredStats;

            if (!(ManagerID == 9999 || ManagerID == 6999))
            {
                if (dtStats != null)
                {
                    drFilteredStats = dtStats.Select("ManagerID='" + ManagerID + "'");
                    if (drFilteredStats.Length > 0)
                        dtFilteredStats = drFilteredStats.CopyToDataTable();
                    else
                    {
                        drFilteredStats = dtStats.Select("ADMID = '" + ManagerID + "'");
                        if (drFilteredStats.Length > 0)
                            dtFilteredStats = drFilteredStats.CopyToDataTable();
                        else
                        {
                            drFilteredStats = dtStats.Select("OfficerID = '" + ((ManagerID == 0) ? ManagerID : ManagerID) + "'");
                            if (drFilteredStats.Length > 0)
                                dtFilteredStats = drFilteredStats.CopyToDataTable();
                        }
                    }
                }

                DataTable dtClonedFilteredStats = dtFilteredStats.Clone();
                dtClonedFilteredStats.Columns[0].DataType = typeof(double);
                dtClonedFilteredStats.Columns[1].DataType = typeof(double);
                dtClonedFilteredStats.Columns[2].DataType = typeof(double);
                dtClonedFilteredStats.Columns[3].DataType = typeof(double);
                dtClonedFilteredStats.Columns[4].DataType = typeof(double);//totalcasecnt
                dtClonedFilteredStats.Columns[5].DataType = typeof(double);//totalvisitcnt
                dtClonedFilteredStats.Columns[6].DataType = typeof(double);//totalpaidcnt
                dtClonedFilteredStats.Columns[7].DataType = typeof(double);//target
                dtClonedFilteredStats.Columns[8].DataType = typeof(double);//availability
                foreach (DataRow row in dtFilteredStats.Rows)
                {
                    dtClonedFilteredStats.ImportRow(row);
                }

                Productivity = double.Parse(dtClonedFilteredStats.Compute("Avg(Productivity)", "").ToString());
                Efficiency = double.Parse(dtClonedFilteredStats.Compute("Avg(Efficiency)", "").ToString());
                Conversion = double.Parse(dtClonedFilteredStats.Compute("Avg(Conversion)", "").ToString());
                Caseholdings = double.Parse(dtClonedFilteredStats.Compute("Sum(Holdings)", "").ToString());
                TotalPaidCaseCnt = double.Parse(dtClonedFilteredStats.Compute("Avg(TotalPaidsCnt)", "").ToString());
                Target = double.Parse(dtClonedFilteredStats.Compute("Avg(Target)", "").ToString());
                PaidsVsTarget = Convert.ToInt32(TotalPaidCaseCnt) / Convert.ToInt32(Target);
                Availability = double.Parse(dtClonedFilteredStats.Compute("Avg(Availability)", "").ToString());
                TotalVisitedCnt = double.Parse(dtClonedFilteredStats.Compute("Avg(TotalVisitCnt)", "").ToString());
                TotalCaseCnt = double.Parse(dtClonedFilteredStats.Compute("Avg(TotalCaseCnt)", "").ToString());
                PredictedEfficiency = Convert.ToInt32(Availability) * Convert.ToInt32(TotalPaidCaseCnt) / Convert.ToInt32(TotalVisitedCnt);

                TotalVisitCntSum = double.Parse(dtClonedFilteredStats.Compute("Sum(TotalVisitCnt)", "").ToString());
                TotalCaseCntSum = double.Parse(dtClonedFilteredStats.Compute("Sum(TotalCaseCnt)", "").ToString());
                TotalPaidCaseCntSum = double.Parse(dtClonedFilteredStats.Compute("Sum(TotalPaidsCnt)", "").ToString());
            }
            else
            {
                if (dtStats != null)
                {
                    drFilteredStats = dtStats.Select();
                    dtFilteredStats = drFilteredStats.CopyToDataTable();

                    DataTable dtClonedFilteredStats = dtFilteredStats.Clone();
                    dtClonedFilteredStats.Columns[0].DataType = typeof(double);
                    dtClonedFilteredStats.Columns[1].DataType = typeof(double);
                    dtClonedFilteredStats.Columns[2].DataType = typeof(double);
                    dtClonedFilteredStats.Columns[3].DataType = typeof(double);
                    dtClonedFilteredStats.Columns[4].DataType = typeof(double);//totalcasecnt
                    dtClonedFilteredStats.Columns[5].DataType = typeof(double);//totalvisitcnt
                    dtClonedFilteredStats.Columns[6].DataType = typeof(double);//totalpaidcnt
                    dtClonedFilteredStats.Columns[7].DataType = typeof(double);//target
                    dtClonedFilteredStats.Columns[8].DataType = typeof(double);//availability
                    foreach (DataRow row in dtFilteredStats.Rows)
                    {
                        dtClonedFilteredStats.ImportRow(row);
                    }

                    Productivity = double.Parse(dtClonedFilteredStats.Compute("Avg(Productivity)", "").ToString());
                    Efficiency = double.Parse(dtClonedFilteredStats.Compute("Avg(Efficiency)", "").ToString());
                    Conversion = double.Parse(dtClonedFilteredStats.Compute("Avg(Conversion)", "").ToString());
                    Caseholdings = double.Parse(dtClonedFilteredStats.Compute("Sum(Holdings)", "").ToString());
                    TotalPaidCaseCnt = double.Parse(dtClonedFilteredStats.Compute("Avg(TotalPaidsCnt)", "").ToString());
                    Target = double.Parse(dtClonedFilteredStats.Compute("Avg(Target)", "").ToString());
                    PaidsVsTarget = Convert.ToInt32(TotalPaidCaseCnt) / Convert.ToInt32(Target);
                    Availability = double.Parse(dtClonedFilteredStats.Compute("Avg(Availability)", "").ToString());
                    TotalVisitedCnt = double.Parse(dtClonedFilteredStats.Compute("Avg(TotalVisitCnt)", "").ToString());
                    TotalCaseCnt = double.Parse(dtClonedFilteredStats.Compute("Avg(TotalCaseCnt)", "").ToString());
                    PredictedEfficiency = Convert.ToInt32(Availability) * Convert.ToInt32(TotalPaidCaseCnt) / Convert.ToInt32(TotalVisitedCnt);

                    TotalVisitCntSum = double.Parse(dtClonedFilteredStats.Compute("Sum(TotalVisitCnt)", "").ToString());
                    TotalCaseCntSum = double.Parse(dtClonedFilteredStats.Compute("Sum(TotalCaseCnt)", "").ToString());
                    TotalPaidCaseCntSum = double.Parse(dtClonedFilteredStats.Compute("Sum(TotalPaidsCnt)", "").ToString());
                }
            }

            DataRow dr;
            dr = ds.Tables["StatsAction"].NewRow();
            dr["Description"] = "Productivity %";
            dr["DisplayValue"] = Math.Round(Productivity);
            dr["Formula"] = "( Total visit / Total case ) * 100";
            dr["Calculation"] = "( " + TotalVisitCntSum + " / " + TotalCaseCntSum + " ) " + " * 100";
            ds.Tables["StatsAction"].Rows.Add(dr);

            dr = ds.Tables["StatsAction"].NewRow();
            dr["Description"] = "Efficiency %";
            dr["DisplayValue"] = Math.Round(Efficiency);
            dr["Formula"] = "( Total Paid Case / Total Visited ) * 100";
            dr["Calculation"] = "( " + TotalPaidCaseCntSum + " / " + TotalVisitCntSum + " ) " + " * 100";
            ds.Tables["StatsAction"].Rows.Add(dr);

            dr = ds.Tables["StatsAction"].NewRow();
            dr["Description"] = "Conversion %";
            dr["DisplayValue"] = Math.Round(Conversion);
            dr["Formula"] = "( Total Paid Case / Total Case ) * 100";
            dr["Calculation"] = "( " + TotalPaidCaseCntSum + " / " + TotalCaseCntSum + " ) " + " * 100";
            ds.Tables["StatsAction"].Rows.Add(dr);

            dr = ds.Tables["StatsAction"].NewRow();
            dr["Description"] = "Case holdings";
            dr["DisplayValue"] = Caseholdings;
            dr["Formula"] = "Case holdings";
            dr["Calculation"] = "( " + Caseholdings + " ) ";
            ds.Tables["StatsAction"].Rows.Add(dr);

            dr = ds.Tables["StatsAction"].NewRow();
            dr["Description"] = "Paids Vs Target %";
            dr["DisplayValue"] = PaidsVsTarget;
            dr["Formula"] = "( Total Paid Case / Target )";
            dr["Calculation"] = "( " + PaidsVsTarget + " ) ";
            ds.Tables["StatsAction"].Rows.Add(dr);

            dr = ds.Tables["StatsAction"].NewRow();
            dr["Description"] = "Predicted Efficiency %";
            dr["DisplayValue"] = PredictedEfficiency;
            dr["Formula"] = "( Total Paid Case / Total Visited ) * 100";
            dr["Calculation"] = "( " + TotalPaidCaseCntSum + " / " + TotalVisitCntSum + " ) " + " * 100";
            ds.Tables["StatsAction"].Rows.Add(dr);

            Clients.Caller.gethubstats(JSONHelper.FromDataTable(ds.Tables["StatsAction"]), link);
        }

        public void getstatscaseactions(string urlparam)
        {
            var strParam = urlparam.Split(new char[] { ',' });
            int OfficerID = int.Parse(strParam[3]);
            DataTable dtFilteredStatsOfficer = new DataTable();
            try
            {
                if (OfficerID > 0)
                {
                    dtFilteredStatsOfficer = dtCaseActions.Select("Officer='" + OfficerID + "'").CopyToDataTable();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            Clients.Caller.getstatscaseactions(JSONHelper.FromDataTable(dtFilteredStatsOfficer));

        }

        #endregion

        #region ES_Dashboard

        public void getallrequest(string caParam)
        {
            var strParam = caParam.Split(new char[] { ',' });
            int TreeLevel = int.Parse(strParam[0]);
            int OfficerID = int.Parse(strParam[1]);
            int ReqDateType = int.Parse(strParam[2]);

            DataSet ds = new DataSet();
            ds.Tables.Add("AssignRequest");
            ds.Tables["AssignRequest"].Columns.Add("RequestType");
            ds.Tables["AssignRequest"].Columns.Add("Count");
            ds.Tables["AssignRequest"].Columns.Add("Ave");
            ds.Tables["AssignRequest"].Columns.Add("TAT");

            ds.Tables.Add("HPICheck");
            ds.Tables["HPICheck"].Columns.Add("RequestType");
            ds.Tables["HPICheck"].Columns.Add("Count");
            ds.Tables["HPICheck"].Columns.Add("Ave");
            ds.Tables["HPICheck"].Columns.Add("TAT");

            ds.Tables.Add("PermissionToRemove");
            ds.Tables["PermissionToRemove"].Columns.Add("RequestType");
            ds.Tables["PermissionToRemove"].Columns.Add("Count");
            ds.Tables["PermissionToRemove"].Columns.Add("Ave");
            ds.Tables["PermissionToRemove"].Columns.Add("TAT");

            ds.Tables.Add("CaseReturn");
            ds.Tables["CaseReturn"].Columns.Add("RequestType");
            ds.Tables["CaseReturn"].Columns.Add("Count");
            ds.Tables["CaseReturn"].Columns.Add("Ave");
            ds.Tables["CaseReturn"].Columns.Add("TAT");

            DataTable dtFilteredRequest = new DataTable();
            DataRow[] drFilteredRequest;

            if (dtEnforcementService != null)
            {
                //DateTime baseDate = DateTime.Today;
                //var thisWeekStart = baseDate.AddDays(-(int)baseDate.DayOfWeek);
                //var thisWeekEnd = thisWeekStart.AddDays(7).AddSeconds(-1);
                //var thisMonthStart = baseDate.AddDays(1 - baseDate.Day);
                //var thisMonthEnd = thisMonthStart.AddMonths(1).AddSeconds(-1);
                //TreeLevel
                if (TreeLevel == 1)
                {
                    //Manager
                    switch (ReqDateType)
                    {
                        case 1: // Today
                            drFilteredRequest = dtEnforcementService.Select("TodayFilter = 1");
                            if (drFilteredRequest.Length > 0)
                                dtFilteredRequest = drFilteredRequest.CopyToDataTable();
                            break;
                        case 2://Last 24 Hours
                            drFilteredRequest = dtEnforcementService.Select("Last24hrs = 1");
                            if (drFilteredRequest.Length > 0)
                                dtFilteredRequest = drFilteredRequest.CopyToDataTable();
                            break;
                        case 3:// This Week
                            drFilteredRequest = dtEnforcementService.Select("Thisweek = 1");
                            if (drFilteredRequest.Length > 0)
                                dtFilteredRequest = drFilteredRequest.CopyToDataTable();
                            break;
                        case 4:// This Month
                            drFilteredRequest = dtEnforcementService.Select("ThisMonth = 1");
                            if (drFilteredRequest.Length > 0)
                                dtFilteredRequest = drFilteredRequest.CopyToDataTable();
                            break;
                    }
                }
                else
                {
                    //Agent 
                    switch (ReqDateType)
                    {
                        case 1: // Today
                            drFilteredRequest = dtEnforcementService.Select("ActionBy = '" + OfficerID + "' AND TodayFilter = 1");
                            if (drFilteredRequest.Length > 0)
                                dtFilteredRequest = drFilteredRequest.CopyToDataTable();
                            break;
                        case 2://Last 24 Hours
                            drFilteredRequest = dtEnforcementService.Select("ActionBy = '" + OfficerID + "' AND Last24hrs = 1");
                            if (drFilteredRequest.Length > 0)
                                dtFilteredRequest = drFilteredRequest.CopyToDataTable();
                            break;
                        case 3:// This Week
                            drFilteredRequest = dtEnforcementService.Select("ActionBy = '" + OfficerID + "' AND Thisweek = 1");
                            if (drFilteredRequest.Length > 0)
                                dtFilteredRequest = drFilteredRequest.CopyToDataTable();
                            break;
                        case 4:// This Month
                            drFilteredRequest = dtEnforcementService.Select("ActionBy = '" + OfficerID + "' AND ThisMonth = 1");
                            if (drFilteredRequest.Length > 0)
                                dtFilteredRequest = drFilteredRequest.CopyToDataTable();
                            break;
                    }
                }
                DataRow dr;
                // On all tables' rows
                //AssignRequest
                dr = ds.Tables["AssignRequest"].NewRow();
                dr["RequestType"] = "Open";
                DataRow[] aRow = (dtEnforcementService.Rows.Count > 0) ? dtEnforcementService.Select("ActionStatus = '0' AND Requesttype='AssignRequest'") : null;
                dr["Count"] = (aRow == null) ? 0 : aRow.Length;
                if (dtEnforcementService.Rows.Count > 0)
                {
                    if (dtEnforcementService.Select("ActionStatus = '0' AND Requesttype='AssignRequest'").Length > 0)
                    {
                        dr["Ave"] = dtEnforcementService.Select("ActionStatus = '0' AND Requesttype='AssignRequest'")[0]["Lastweekavg"].ToString();
                        dr["TAT"] = dtEnforcementService.Select("ActionStatus = '0' AND Requesttype='AssignRequest'")[0]["TAT"].ToString();
                    }
                    else
                    {
                        dr["Ave"] = "0";
                        dr["TAT"] = "0";
                    }
                }
                else
                {
                    dr["Ave"] = "0";
                    dr["TAT"] = "0";
                }
                // dr["TAT"] = dtFilteredRequest.AsEnumerable().Where(row => (row["ActionStatus"].ToString().Equals("0")) && (row["Requesttype"].ToString().Equals("AssignRequest"))).Sum(row => Convert.ToDecimal(row["TAT"]));
                ds.Tables["AssignRequest"].Rows.Add(dr);

                dr = ds.Tables["AssignRequest"].NewRow();
                dr["RequestType"] = "Approved";
                DataRow[] aRow1 = (dtFilteredRequest.Rows.Count > 0) ? dtFilteredRequest.Select("ActionStatus = '1' AND Requesttype='AssignRequest'") : null;
                dr["Count"] = (aRow1 == null) ? 0 : aRow1.Length;
                if (dtFilteredRequest.Rows.Count > 0)
                {
                    if (dtFilteredRequest.Select("ActionStatus = '1' AND Requesttype='AssignRequest'").Length > 0)
                    {
                        dr["Ave"] = dtFilteredRequest.Select("ActionStatus = '1' AND Requesttype='AssignRequest'")[0]["Lastweekavg"].ToString();
                        dr["TAT"] = dtFilteredRequest.Select("ActionStatus = '1' AND Requesttype='AssignRequest'")[0]["TAT"].ToString();
                    }
                    else
                    {
                        dr["Ave"] = "0";
                        dr["TAT"] = "0";
                    }
                }
                else
                {
                    dr["Ave"] = "0";
                    dr["TAT"] = "0";
                }

                ds.Tables["AssignRequest"].Rows.Add(dr);

                dr = ds.Tables["AssignRequest"].NewRow();
                dr["RequestType"] = "Declined";
                DataRow[] aRow2 = (dtFilteredRequest.Rows.Count > 0) ? dtFilteredRequest.Select("ActionStatus = '2' AND Requesttype='AssignRequest'") : null;
                dr["Count"] = (aRow2 == null) ? 0 : aRow2.Length;
                if (dtFilteredRequest.Rows.Count > 0)
                {
                    if (dtFilteredRequest.Select("ActionStatus = '2' AND Requesttype='AssignRequest'").Length > 0)
                    {
                        dr["Ave"] = dtFilteredRequest.Select("ActionStatus = '2' AND Requesttype='AssignRequest'")[0]["Lastweekavg"].ToString();
                        dr["TAT"] = dtFilteredRequest.Select("ActionStatus = '2' AND Requesttype='AssignRequest'")[0]["TAT"].ToString();
                    }
                    else
                    {
                        dr["Ave"] = "0";
                        dr["TAT"] = "0";
                    }
                }
                else
                {
                    dr["Ave"] = "0";
                    dr["TAT"] = "0";
                }

                ds.Tables["AssignRequest"].Rows.Add(dr);

                ////HPI Check
                dr = ds.Tables["HPICheck"].NewRow();
                dr["RequestType"] = "Open";
                DataRow[] hRow = (dtEnforcementService.Rows.Count > 0) ? dtEnforcementService.Select("ActionStatus = '0' AND Requesttype='HPI Check'") : null;
                dr["Count"] = (hRow == null) ? 0 : hRow.Length;
                if (dtEnforcementService.Rows.Count > 0)
                {
                    if (dtEnforcementService.Select("ActionStatus = '0' AND Requesttype='HPI Check'").Length > 0)
                    {
                        dr["Ave"] = dtEnforcementService.Select("ActionStatus = '0' AND Requesttype='HPI Check'")[0]["Lastweekavg"].ToString();
                        dr["TAT"] = dtEnforcementService.Select("ActionStatus = '0' AND Requesttype='HPI Check'")[0]["TAT"].ToString();
                    }
                    else
                    {
                        dr["Ave"] = "0";
                        dr["TAT"] = "0";
                    }
                }
                else
                {
                    dr["Ave"] = "0";
                    dr["TAT"] = "0";
                }
                ds.Tables["HPICheck"].Rows.Add(dr);

                dr = ds.Tables["HPICheck"].NewRow();
                dr["RequestType"] = "Approved";
                DataRow[] hRow1 = (dtFilteredRequest.Rows.Count > 0) ? dtFilteredRequest.Select("ActionStatus = '1' AND Requesttype='HPI Check'") : null;
                dr["Count"] = (hRow1 == null) ? 0 : hRow1.Length;
                if (dtFilteredRequest.Rows.Count > 0)
                {
                    if (dtFilteredRequest.Select("ActionStatus = '1' AND Requesttype='HPI Check'").Length > 0)
                    {
                        dr["Ave"] = dtFilteredRequest.Select("ActionStatus = '1' AND Requesttype='HPI Check'")[0]["Lastweekavg"].ToString();
                        dr["TAT"] = dtFilteredRequest.Select("ActionStatus = '1' AND Requesttype='HPI Check'")[0]["TAT"].ToString();
                    }
                    else
                    {
                        dr["Ave"] = "0";
                        dr["TAT"] = "0";
                    }
                }
                else
                {
                    dr["Ave"] = "0";
                    dr["TAT"] = "0";
                }
                ds.Tables["HPICheck"].Rows.Add(dr);

                dr = ds.Tables["HPICheck"].NewRow();
                dr["RequestType"] = "Declined";
                DataRow[] hRow2 = (dtFilteredRequest.Rows.Count > 0) ? dtFilteredRequest.Select("ActionStatus = '2' AND Requesttype='HPI Check'") : null;
                dr["Count"] = (hRow2 == null) ? 0 : hRow2.Length;
                if (dtFilteredRequest.Rows.Count > 0)
                {
                    if (dtFilteredRequest.Select("ActionStatus = '2' AND Requesttype='HPI Check'").Length > 0)
                    {
                        dr["Ave"] = dtFilteredRequest.Select("ActionStatus = '2' AND Requesttype='HPI Check'")[0]["Lastweekavg"].ToString();
                        dr["TAT"] = dtFilteredRequest.Select("ActionStatus = '2' AND Requesttype='HPI Check'")[0]["TAT"].ToString();
                    }
                    else
                    {
                        dr["Ave"] = "0";
                        dr["TAT"] = "0";
                    }
                }
                else
                {
                    dr["Ave"] = "0";
                    dr["TAT"] = "0";
                }
                ds.Tables["HPICheck"].Rows.Add(dr);

                ////Permission to remove
                dr = ds.Tables["PermissionToRemove"].NewRow();
                dr["RequestType"] = "Open";
                DataRow[] pRow = (dtEnforcementService.Rows.Count > 0) ? dtEnforcementService.Select("ActionStatus = '0' AND Requesttype='Permission to remove'") : null;
                dr["Count"] = (pRow == null) ? 0 : pRow.Length;
                if (dtEnforcementService.Rows.Count > 0)
                {
                    if (dtEnforcementService.Select("ActionStatus = '0' AND Requesttype='Permission to remove'").Length > 0)
                    {
                        dr["Ave"] = dtEnforcementService.Select("ActionStatus = '0' AND Requesttype='Permission to remove'")[0]["Lastweekavg"].ToString();
                        dr["TAT"] = dtEnforcementService.Select("ActionStatus = '0' AND Requesttype='Permission to remove'")[0]["TAT"].ToString();
                    }
                    else
                    {
                        dr["Ave"] = "0";
                        dr["TAT"] = "0";
                    }
                }
                else
                {
                    dr["Ave"] = "0";
                    dr["TAT"] = "0";
                }
                ds.Tables["PermissionToRemove"].Rows.Add(dr);

                dr = ds.Tables["PermissionToRemove"].NewRow();
                dr["RequestType"] = "Approved";
                DataRow[] pRow1 = (dtFilteredRequest.Rows.Count > 0) ? dtFilteredRequest.Select("ActionStatus = '1' AND Requesttype='Permission to remove'") : null;
                dr["Count"] = (pRow1 == null) ? 0 : pRow1.Length;
                if (dtFilteredRequest.Rows.Count > 0)
                {
                    if (dtFilteredRequest.Select("ActionStatus = '1' AND Requesttype='Permission to remove'").Length > 0)
                    {
                        dr["Ave"] = dtFilteredRequest.Select("ActionStatus = '1' AND Requesttype='Permission to remove'")[0]["Lastweekavg"].ToString();
                        dr["TAT"] = dtFilteredRequest.Select("ActionStatus = '1' AND Requesttype='Permission to remove'")[0]["TAT"].ToString();
                    }
                    else
                    {
                        dr["Ave"] = "0";
                        dr["TAT"] = "0";
                    }
                }
                else
                {
                    dr["Ave"] = "0";
                    dr["TAT"] = "0";
                }
                ds.Tables["PermissionToRemove"].Rows.Add(dr);

                dr = ds.Tables["PermissionToRemove"].NewRow();
                dr["RequestType"] = "Declined";
                DataRow[] pRow2 = (dtFilteredRequest.Rows.Count > 0) ? dtFilteredRequest.Select("ActionStatus = '2' AND Requesttype='Permission to remove'") : null;
                dr["Count"] = (pRow2 == null) ? 0 : pRow2.Length;
                if (dtFilteredRequest.Rows.Count > 0)
                {
                    if (dtFilteredRequest.Select("ActionStatus = '2' AND Requesttype='Permission to remove'").Length > 0)
                    {
                        dr["Ave"] = dtFilteredRequest.Select("ActionStatus = '2' AND Requesttype='Permission to remove'")[0]["Lastweekavg"].ToString();
                        dr["TAT"] = dtFilteredRequest.Select("ActionStatus = '2' AND Requesttype='Permission to remove'")[0]["TAT"].ToString();
                    }
                    else
                    {
                        dr["Ave"] = "0";
                        dr["TAT"] = "0";
                    }
                }
                else
                {
                    dr["Ave"] = "0";
                    dr["TAT"] = "0";
                }
                ds.Tables["PermissionToRemove"].Rows.Add(dr);

                ////CaseReturn
                dr = ds.Tables["CaseReturn"].NewRow();
                dr["RequestType"] = "Open";
                DataRow[] cRow = (dtEnforcementService.Rows.Count > 0) ? dtEnforcementService.Select("ActionStatus = '0' AND Requesttype='PendingReturn'") : null;
                dr["Count"] = (cRow == null) ? 0 : cRow.Length;
                if (dtEnforcementService.Rows.Count > 0)
                {
                    if (dtEnforcementService.Select("ActionStatus = '0' AND Requesttype='PendingReturn'").Length > 0)
                    {
                        dr["Ave"] = dtEnforcementService.Select("ActionStatus = '0' AND Requesttype='PendingReturn'")[0]["Lastweekavg"].ToString();
                        dr["TAT"] = dtEnforcementService.Select("ActionStatus = '0' AND Requesttype='PendingReturn'")[0]["TAT"].ToString();
                    }
                    else
                    {
                        dr["Ave"] = "0";
                        dr["TAT"] = "0";
                    }
                }
                else
                {
                    dr["Ave"] = "0";
                    dr["TAT"] = "0";
                }
                ds.Tables["CaseReturn"].Rows.Add(dr);

                dr = ds.Tables["CaseReturn"].NewRow();
                dr["RequestType"] = "Approved";
                DataRow[] cRow1 = (dtFilteredRequest.Rows.Count > 0) ? dtFilteredRequest.Select("ActionStatus = '1' AND Requesttype='PendingReturn'") : null;
                dr["Count"] = (cRow1 == null) ? 0 : cRow1.Length;
                if (dtFilteredRequest.Rows.Count > 0)
                {
                    if (dtFilteredRequest.Select("ActionStatus = '1' AND Requesttype='PendingReturn'").Length > 0)
                    {
                        dr["Ave"] = dtFilteredRequest.Select("ActionStatus = '1' AND Requesttype='PendingReturn'")[0]["Lastweekavg"].ToString();
                        dr["TAT"] = dtFilteredRequest.Select("ActionStatus = '1' AND Requesttype='PendingReturn'")[0]["TAT"].ToString();
                    }
                    else
                    {
                        dr["Ave"] = "0";
                        dr["TAT"] = "0";
                    }
                }
                else
                {
                    dr["Ave"] = "0";
                    dr["TAT"] = "0";
                }
                ds.Tables["CaseReturn"].Rows.Add(dr);

                dr = ds.Tables["CaseReturn"].NewRow();
                dr["RequestType"] = "Declined";
                DataRow[] cRow2 = (dtFilteredRequest.Rows.Count > 0) ? dtFilteredRequest.Select("ActionStatus = '2' AND Requesttype='PendingReturn'") : null;
                dr["Count"] = (cRow2 == null) ? 0 : cRow2.Length;
                if (dtFilteredRequest.Rows.Count > 0)
                {
                    if (dtFilteredRequest.Select("ActionStatus = '2' AND Requesttype='PendingReturn'").Length > 0)
                    {
                        dr["Ave"] = dtFilteredRequest.Select("ActionStatus = '2' AND Requesttype='PendingReturn'")[0]["Lastweekavg"].ToString();
                        dr["TAT"] = dtFilteredRequest.Select("ActionStatus = '2' AND Requesttype='PendingReturn'")[0]["TAT"].ToString();
                    }
                    else
                    {
                        dr["Ave"] = "0";
                        dr["TAT"] = "0";
                    }
                }
                else
                {
                    dr["Ave"] = "0";
                    dr["TAT"] = "0";
                }
                ds.Tables["CaseReturn"].Rows.Add(dr);
            }
            Clients.Caller.getallrequest(JSONHelper.FromDataTable(ds.Tables["AssignRequest"]) + "|" + JSONHelper.FromDataTable(ds.Tables["HPICheck"]) + "|" + JSONHelper.FromDataTable(ds.Tables["PermissionToRemove"]) + "|" + JSONHelper.FromDataTable(ds.Tables["CaseReturn"]));
        }

        public void getesofficerloggedon()
        {
            Clients.Caller.getesofficerloggedon(JSONHelper.FromDataTable(dtESOfficer));
        }

        #endregion

        #region HubConnection

        protected string CleanJson(string jsonString)
        {
            return jsonString.Replace("{\"d\":\"", "").Replace("]\"}", "]");
        }

        protected DataTable ConvertJSONToDataTable(string jsonString)
        {
            if (jsonString == "[]")
                return null;
            DataTable dt = new DataTable();
            //strip out bad characters
            string[] jsonParts = Regex.Split(jsonString.Replace("[", "").Replace("]", "").Replace("\\", ""), "},{");

            //hold column names
            List<string> dtColumns = new List<string>();

            //get columns
            foreach (string jp in jsonParts)
            {
                //only loop thru once to get column names
                string[] propData = Regex.Split(jp.Replace("{", "").Replace("}", ""), ",");
                foreach (string rowData in propData)
                {
                    try
                    {
                        int idx = rowData.IndexOf(":");
                        string n = rowData.Substring(0, idx - 1);
                        string v = rowData.Substring(idx + 1);
                        if (!dtColumns.Contains(n))
                        {
                            dtColumns.Add(n.Replace("\"", ""));
                        }
                    }
                    catch (Exception ex)
                    {
                        //throw new Exception(string.Format("Error Parsing Column Name : {0}", rowData));
                        throw ex;
                    }

                }
                break; // TODO: might not be correct. Was : Exit For
            }

            //build dt
            foreach (string c in dtColumns)
            {
                dt.Columns.Add(c);
            }
            //get table data
            foreach (string jp in jsonParts)
            {
                string[] propData = Regex.Split(jp.Replace("{", "").Replace("}", ""), ",");
                DataRow nr = dt.NewRow();
                foreach (string rowData in propData)
                {
                    try
                    {
                        int idx = rowData.IndexOf(":");
                        string n = rowData.Substring(0, idx - 1).Replace("\"", "");
                        string v = rowData.Substring(idx + 1).Replace("\"", "");
                        nr[n] = v;
                    }
                    catch (Exception ex)
                    {
                        continue;
                    }

                }
                dt.Rows.Add(nr);
            }
            return dt;
        }

        /// <summary>
        /// register online user
        /// </summary>
        /// <returns></returns>
        public override System.Threading.Tasks.Task OnConnected()
        {
            clientCounter++;
            return base.OnConnected();
        }

        /// <summary>
        /// unregister disconected user
        /// </summary>
        /// <returns></returns>
        public override System.Threading.Tasks.Task OnDisconnected()
        {
            clientCounter--;
            return base.OnDisconnected();
        }

        #endregion
    }
}