﻿
$(document).ready(function () {
    $(':text').val('');
    $('select').prop('selectedIndex', 0);

    $('#txtToDate,#txtFromDate').attr('readonly', 'readonly');

    //format: 'Y/m/d',
    $('#txtFromDate').datetimepicker({
        format: 'Y/m/d',
        onShow: function (ct) {
            this.setOptions({
                maxDate: $('#txtToDate').val() ? $('#txtToDate').val() : false
            })
        },
        timepicker: false
    });

    $('#txtToDate').datetimepicker({
        format: 'Y/m/d',
        onShow: function (ct) {
            this.setOptions({
                minDate: $('#txtFromDate').val() ? $('#txtFromDate').val() : false
            })
        },
        timepicker: false
    });

    $('#btnSearch').click(function () {
        var pass = 0;
        $('#selReport').each(function () {
            var $this = $(this);
            if ($this.is('select')) {
                if ($this.val() == '0') {
                    pass = 1;
                    $('#' + $this.attr('id')).attr('style', 'border:2px solid #7A4073;');
                }
            }
        });
        if (pass == 0) {
            GetPaymentReport();
        }
    });

    $('#btnPending').click(function () {
        GetPaymentReport();
    });

    $('#btnClearOfficer').click(function () {
        $(':text').val('');
        $('select').prop('selectedIndex', 0);
        $('#lblReport').html('');
        $('#selReport').css("border", "1px solid #D7D7D7");
        $('#DDownload').hide();
        $('#DPaymentReport,#DOfficerIMEI,#DSurrenderLog,#DePaying').hide();
    });

    $('#selReport').change(function () {
        $('#btnPending').hide();
        $('#selReport').css("border", "1px solid #D7D7D7");
        switch ($(this).val()) {
            case '1':
                $('#lblReport').html('Payment Report');
                $('#imgExcelDownload').attr('title', 'Download payment report');
                $('#btnPending').show();
                break;
            case '2':
                $('#imgExcelDownload').attr('title', 'Download officer IMEI report');
                $('#lblReport').html('Officer IMEI Report');
                break;
            case '3':
                $('#imgExcelDownload').attr('title', 'Download surrender log report');
                $('#lblReport').html('Surrender Log Report');
                break;
            case '4':
                $('#imgExcelDownload').attr('title', 'Download epaying report');
                $('#lblReport').html('ePaying Report');
                break;
        }
    });
});

function GetPaymentReport() {
    var FromDate = "", ToDate = "";
    if ($.trim($('#txtFromDate').val()) != "") {
        var dateF = $('#txtFromDate').val().split('/');
        FromDate = dateF[2] + '-' + dateF[1] + '-' + dateF[0];
    }
    if ($.trim($('#txtToDate').val()) != "") {
        var dateT = $('#txtToDate').val().split('/');
        ToDate = dateT[2] + '-' + dateT[1] + '-' + dateT[0];
    }
    $('#DDownload,#DPaymentReport,#DOfficerIMEI,#DSurrenderLog,#DePaying').hide();
    switch ($('#selReport').val()) {
        case '0':
            $.ajax({
                type: 'GET',
                url: ServiceURL + 'api/v1/cases/' + (FromDate == '' ? '1-1-1900' : FromDate) + '/' + (ToDate == '' ? '1-1-1900' : ToDate) + '/PaymentReport',
                headers: {
                    "Authorization": $.session.get('TokenAuthorization'),
                    'Content-Type': 'application/json'
                },
                dataType: 'json',
                beforeSend: function () {
                    $.loader({
                        className: "blue-with-image",
                        content: ''
                    });
                },
                success: function (data) {
                    if (!(data == '' || data == null || data == '[]')) {
                        $('#tblPaymentReport').dataTable().fnClearTable();
                        $('#tbodyPaymentReport').empty();
                        $('#DPaymentReport').show();
                        $.each(data, function (key, value) {
                            $('#tbodyPaymentReport').append('<tr><td>' + value.Officer + '</td><td>' + value.OfficerName + '</td>' +
                                                                '<td>' + value.CaseNumber + '</td><td>' + value.DateActioned + '</td>' +
                                                                '<td>' + value.Cash + '</td><td>' + value.Card + '</td><td>' + value.ADMname + '</td>' +
                                                                '<td>' + value.CashupStatus + '</td><td>' + value.CashierStatus + '</td><td>' + value.PType + '</td></tr>');
                        });
                        $('#tblPaymentReport').dataTable({
                            "sScrollY": "auto",
                            "bPaginate": true,
                            "bDestroy": true,
                            "bSort": false,
                            "sPaginationType": "full_numbers",
                            "bFilter": true,
                            "sInfo": true,
                            "bLengthChange": true,
                            "iDisplayLength": 10
                        });
                    }
                    else {
                        $('#DPaymentReport').show();
                        $('#tblPaymentReport').dataTable({
                            "sScrollY": "auto",
                            "bPaginate": false,
                            "bDestroy": true,
                            "bSort": false,
                            "bLengthChange": false,
                            "bFilter": false,
                            "bInfo": false,
                            "iDisplayLength": 1
                        });
                    }
                },
                complete: function () {
                    $.loader('close');
                },
                error: function () { }
            });
            break;
        case '1':
            $.ajax({
                type: 'GET',
                url: ServiceURL + 'api/v1/cases/' + (FromDate == '' ? '1-1-1900' : FromDate) + '/' + (ToDate == '' ? '1-1-1900' : ToDate) + '/PendingPaymentReport',
                headers: {
                    "Authorization": $.session.get('TokenAuthorization'),
                    'Content-Type': 'application/json'
                },
                dataType: 'json',
                beforeSend: function () {
                    $.loader({
                        className: "blue-with-image",
                        content: ''
                    });
                },
                success: function (data) {
                    if (!(data == '' || data == null || data == '[]')) {
                        $('#tblPaymentReport').dataTable().fnClearTable();
                        $('#tbodyPaymentReport').empty();
                        $('#DPaymentReport').show();
                        $.each(data, function (key, value) {
                            $('#tbodyPaymentReport').append('<tr><td>' + value.Officer + '</td><td>' + value.OfficerName + '</td>' +
                                                                '<td>' + value.CaseNumber + '</td><td>' + value.DateActioned + '</td>' +
                                                                '<td>' + value.Cash + '</td><td>' + value.Card + '</td><td>' + value.ADMname + '</td>' +
                                                                '<td>' + value.CashupStatus + '</td><td>' + value.CashierStatus + '</td><td>' + value.PType + '</td></tr>');
                        });
                        $('#tblPaymentReport').dataTable({
                            "sScrollY": "auto",
                            "bPaginate": true,
                            "bDestroy": true,
                            "bSort": false,
                            "sPaginationType": "full_numbers",
                            "bFilter": true,
                            "sInfo": true,
                            "bLengthChange": true,
                            "iDisplayLength": 10
                        });
                    }
                    else {
                        $('#DPaymentReport').show();
                        $('#tblPaymentReport').dataTable({
                            "sScrollY": "auto",
                            "bPaginate": false,
                            "bDestroy": true,
                            "bSort": false,
                            "bLengthChange": false,
                            "bFilter": false,
                            "bInfo": false,
                            "iDisplayLength": 1
                        });
                    }
                },
                complete: function () {
                    $.loader('close');
                },
                error: function () {  }
            });
            break;
        case '2':
            $.ajax({
                type: 'GET',
                url: ServiceURL + 'api/v1/cases/' + (FromDate == "" ? '1-1-1900' : FromDate) + '/' + (ToDate == "" ? '1-1-1900' : ToDate) + '/OfficerIMEIReport',
                headers: {
                    "Authorization": $.session.get('TokenAuthorization'),
                    'Content-Type': 'application/json'
                },
                dataType: 'json',
                beforeSend: function () {
                    $.loader({
                        className: "blue-with-image",
                        content: ''
                    });
                },
                success: function (data) {
                    if (!(data == '' || data == null || data == '[]')) {
                        $('#tblOfficerIMEI').dataTable().fnClearTable();
                        $('#DOfficerIMEI').show();
                        $.each(data, function (key, value) {
                            $('#tbodyOfficerIMEI').append('<tr><td>' + value.OfficerID + '</td><td>' + value.OfficerName + '</td>' +
                                                                '<td>' + value.IMEInumber + '</td><td>' + value.ManagerID + '</td>' +
                                                                '<td>' + value.ManagerName + '</td><td>' + value.LastActionedDate + '</td></tr>');
                        });
                        $('#tblOfficerIMEI').dataTable({
                            "sScrollY": "auto",
                            "bPaginate": true,
                            "bDestroy": true,
                            "bSort": false,
                            "sPaginationType": "full_numbers",
                            "bFilter": true,
                            "sInfo": true,
                            "bLengthChange": true,
                            "iDisplayLength": 10
                        });
                    }
                    else {
                        $('#DOfficerIMEI').show();
                        $('#tblOfficerIMEI').dataTable({
                            "sScrollY": "auto",
                            "bPaginate": true,
                            "bDestroy": true,
                            "bSort": false,
                            "sPaginationType": "full_numbers",
                            "bFilter": true,
                            "sInfo": true,
                            "bLengthChange": true,
                            "iDisplayLength": 10
                        });
                    }
                },
                complete: function () {
                    $.loader('close');
                },
                error: function () { }
            });
            break;
        case '3':
            $.ajax({
                type: 'GET',
                url: ServiceURL + 'api/v1/cases/' + (FromDate == "" ? '1-1-1900' : FromDate) + '/' + (ToDate == "" ? '1-1-1900' : ToDate) + '/SurrenderLogReport',
                headers: {
                    "Authorization": $.session.get('TokenAuthorization'),
                    'Content-Type': 'application/json'
                },
                dataType: 'json',
                beforeSend: function () {
                    $.loader({
                        className: "blue-with-image",
                        content: ''
                    });
                },
                success: function (data) {
                    if (!(data == '' || data == null || data == '[]')) {
                        $('#tblSurrenderLog').dataTable().fnClearTable();
                        $('#DSurrenderLog').show();
                        $.each(data, function (key, value) {
                            $('#tbodySurrenderLog').append('<tr><td>' + value.OfficerId + '</td><td>' + value.caseNumber + '</td>' +
                                                                '<td>' + value.SDate + '</td><td>' + value.STime + '</td>' +
                                                                '<td>' + value.Location + '</td><td>' + value.ContactNo + '</td><td>' + value.ActionDate + '</td>' +
                                                                '<td>' + value.DefendantName + '</td><td>' + value.ClientReference + '</td><td>' + value.Warranttype + '</td></tr>');
                        });
                        $('#tblSurrenderLog').dataTable({
                            "sScrollY": "auto",
                            "bPaginate": true,
                            "bDestroy": true,
                            "bSort": false,
                            "sPaginationType": "full_numbers",
                            "bFilter": true,
                            "sInfo": true,
                            "bLengthChange": true,
                            "iDisplayLength": 10
                        });
                    }
                    else {
                        $('#DSurrenderLog').show();
                        $('#tblSurrenderLog').dataTable({
                            "sScrollY": "auto",
                            "bPaginate": true,
                            "bDestroy": true,
                            "bSort": false,
                            "sPaginationType": "full_numbers",
                            "bFilter": true,
                            "sInfo": true,
                            "bLengthChange": true,
                            "iDisplayLength": 10
                        });
                    }
                },
                complete: function () {
                    $.loader('close');
                },
                error: function () { }
            });
            break;
        case '4':
            $.ajax({
                type: 'GET',
                url: ServiceURL + 'api/v1/cases/' + (FromDate == "" ? '1-1-1900' : FromDate) + '/' + (ToDate == "" ? '1-1-1900' : ToDate) + '/EpayingReport',
                headers: {
                    "Authorization": $.session.get('TokenAuthorization'),
                    'Content-Type': 'application/json'
                },
                dataType: 'json',
                beforeSend: function () {
                    $.loader({
                        className: "blue-with-image",
                        content: ''
                    });
                },
                success: function (data) {
                    if (!(data == '' || data == null || data == '[]')) {

                        //var resPayment = data.split('||');
                        ////data = JSON.parse(resPayment[0]);
                        //$('#AExcelDownload').attr('href', PaymentReportPath + resPayment[1]);

                        //$('#DDownload').show();
                        //$('#DPaymentReport,#DOfficerIMEI,#DSurrenderLog,#DePaying').hide();

                        $('#tblePaying').dataTable().fnClearTable();
                        $('#DePaying').show();
                        $.each(data, function (key, value) {
                            $('#tbodyePaying').append('<tr><td>' + value.OfficerID + '</td><td>' + value.GiroNo + '</td>' +
                                                                '<td>' + value.Paying_Date + '</td><td>' + value.Processed_On + '</td>' +
                                                                '<td>' + value.WarrantNo + '</td><td>' + value.ScreenPrint + '</td>' +
                                                                '<td>' + value.Total_Cash + '</td><td>' + value.ReceiptNo + '</td></tr>');
                        });
                        $('#tblePaying').dataTable({
                            "sScrollY": "auto",
                            "bPaginate": true,
                            "bDestroy": true,
                            "bSort": false,
                            "sPaginationType": "full_numbers",
                            "bFilter": true,
                            "sInfo": true,
                            "bLengthChange": true,
                            "iDisplayLength": 10
                        });
                    }
                    else {
                        $('#DePaying').show();
                        $('#tblePaying').dataTable({
                            "sScrollY": "auto",
                            "bPaginate": true,
                            "bDestroy": true,
                            "bSort": false,
                            "sPaginationType": "full_numbers",
                            "bFilter": true,
                            "sInfo": true,
                            "bLengthChange": true,
                            "iDisplayLength": 10
                        });
                    }
                },
                complete: function () {
                    $.loader('close');
                },
                error: function () { }
            });
            break;
    }
}


