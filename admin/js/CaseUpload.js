﻿//==========================================================================================================================================
/* File Name: CaseUpload.js */
/* File Created: Aug 16, 2016 */
/* Created By  : R.Santhakumar */
//==========================================================================================================================================

$(document).ready(function () {
    $(':text').val('');
    $('#lblLoginOfficer').html($.session.get('OfficerName'));

    $('#btnUpload').click(function () {
        GetCaseUpload();
    });

    $('#btnClear').click(function () {
        $('#fileInput').val('');
        $('#DErrorMsg,#btnClear').hide();
    });

    $('#btnDownloadSCcase').click(function () {
        GetCaseDownloadSC();
    });
});

function GetCaseTemplateDownload() {
    window.location.href = CaseTemplateDownload;
}

function GetCaseUpload() {
    var formData = new FormData();
    formData.append('userfile', $('#fileInput')[0].files[0]);
    if (!($('#fileInput').val() == "" || $('#fileInput').val() == null)) {
        $.ajax({
            type: 'POST',
            url: ServiceURL + 'api/v1/cases/CaseUpload',
            headers: {
                "Authorization": $.session.get('TokenAuthorization')
            },
            data: formData,
            processData: false,
            contentType: false,
            beforeSend: function () {
                $('#tbodyGridView').empty();
                $.loader({
                    className: "blue-with-image",
                    content: ''
                });
            },
            success: function (data) {
                if (!(data == '' || data == null || data == '[]')) {
                    $('#DErrorMsg,#btnClear').show();
                    $.each(data, function (key, value) {
                        $('#tbodyGridView').append('<tr><td><label id="lblErrorType' + key + '" style="font-weight:normal; color:red;">' + value.ErrorType + '</label></td>' +
                                                        '<td>' + value.RowNo + '</td>' +
                                                        '<td>' + value.ErrorMessage + '</td>' +
                                                        '<td>' + value.SheetName + '</td></tr>');
                    });

                    $('#tblGridView').DataTable({
                        "sScrollY": "auto",
                        "bFilter": false,
                        "bPaginate": true,
                        "bDestroy": true,
                        "bSort": false,
                        "sPaginationType": "full_numbers",
                        "bLengthChange": false,
                        "sInfo": true,
                        "iDisplayLength": 5,
                        dom: 'Bfrtip',
                        buttons: [
                            //'copyHtml5', 'excelHtml5', 'csvHtml5', 'pdfHtml5'
                            'excelHtml5'
                        ]
                    });
                }
                else {
                    $('#tblGridView').DataTable({
                        "sScrollY": "auto",
                        "bPaginate": true,
                        "bDestroy": true,
                        "bSort": false,
                        "sPaginationType": "full_numbers",
                        "bLengthChange": false,
                        "bFilter": false,
                        "sInfo": true,
                        "iDisplayLength": 5
                    });
                    $('#DErrorMsg,#btnClear').hide();
                    $('#DAlert').empty().attr('class', 'alert alert-success').append('<strong>Info!</strong> Case uploaded successfully').fadeIn(1).fadeOut(6000);
                }
            },
            complete: function () {
                $.loader('close');
            },
            error: function (xhr, textStatus, errorThrown) {
                // alert('Error :' + errorThrown);
            }
        });
    }
    else {
        alert('please upload case');
    }
}

function GetCaseDownloadSC() {
    var resultSC = '';
    $.ajax({
        type: 'GET',
        url: ServiceURL + 'api/v1/GetCaseDownloadSC/Excel',
        dataType: 'text',
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/vnd.ms-excel',
            'Content-disposition': 'attachment',
            'X-Requested-With': 'XMLHttpRequest'
        },
        // dataType: 'json',
        // headers: { 'Content-Type': 'image/png',  },
        processData: false,
        beforeSend: function () {
            $.loader({
                className: "blue-with-image",
                content: ''
            });
        },
        success: function (data) {
            //alert(JSON.stringify(data));
            var uri = 'data:application/vnd.ms-excel,' + data; //encodeURIComponent(data);
            window.open(uri, 'tiketi.xls');
            //if (data != '') {
            //    resultSC = data.split('||');
            //    $('#ADownloadSC').attr('href', SCDownloadPath + resultSC[1]);
            //    window.location.href = $('#ADownloadSC').attr('href');
            //}
        },
        complete: function () {
            $.loader('close');
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('Error: ' + errorThrown);
        }
    });
}

function getCookie(c_name) {
    var i, x, y, ARRcookies = document.cookie.split(";");
    for (i = 0; i < ARRcookies.length; i++) {
        x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
        y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
        x = x.replace(/^\s+|\s+$/g, "");
        if (x == c_name) {
            return unescape(y);
        }
    }
}


