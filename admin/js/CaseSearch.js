﻿//==========================================================================================================================================
/* File Name: PendingAction.js */
/* File Created: March 19, 2014 */
/* Created By  : R.Santhakumar */
//==========================================================================================================================================
var mID;
var MgrID;
var DNotes;
var Tabledata = '';

$(document).ready(function () {
    mID = $.session.get('OfficerID');
    $(':text').val('');

    if (mID == 9999 || mID == 8998) {
        $('#ManagerSelect').show();
        $('#ManagerNameLabel').hide();
        GetManagers();
    }
    else {
        $('#ManagerNameLabel').html($.session.get('OfficerName')).show();
        $('#ManagerSelect').hide();
        GetOfficerManager(mID);
    }

    $('#ActionTypeSelect').prop("selectedIndex", -1);

    $('#ToDateText,#FromDateText').attr('readonly', 'readonly');
    //format: 'Y/m/d',
    $('#FromDateText').datetimepicker({
        //format: 'd/m/Y',
        format: 'Y/m/d',
        onShow: function (ct) {
            this.setOptions({
                maxDate: $('#ToDateText').val() ? $('#ToDateText').val() : false
            })
        },
        timepicker: false
    });

    $('#ToDateText').datetimepicker({
        format: 'Y/m/d',
        onShow: function (ct) {
            this.setOptions({
                minDate: $('#FromDateText').val() ? $('#FromDateText').val() : false
            })
        },
        timepicker: false
    });

    $('#ClearButton').click(function () {
        $(':text').val('');
        if (mID == 9999 || mID == 8998) {
            $("#OfficerSelect").empty();
        }
        $("#chkColumbusAction").prop("checked", "checked");
        $('input[name=Type][value=0]').prop('checked', true);

        $('#page-content-wrapper select').prop('selectedIndex', 0);
        $('#ActionTypeSelect').prop("selectedIndex", -1);
        $('input[name=rdBW][value=2]').prop('checked', true);
        $('#selNotFilming').hide();
        $('#selRecordingNotSuccess').show();
        $('#selNotFilming').val(0).attr("selected", "selected");
        $('#lblReason').html('Reason for recoding was not successful');
    });

    $('input[name=Type][value=0]').prop('checked', true);

    $('#FindButton').click(function () {
        if (mID == 9999 || mID == 8998)
            MgrID = $('#ManagerSelect').val();
        else
            MgrID = mID;

        GetCaseActionsSearchForAdmin(MgrID,
            $("#OfficerSelect option:selected").map(function () { return this.value; }).get().join(','),
            $('#FromDateText').val(), $('#ToDateText').val(),
            (($('#ManagerSelect').val() == '0' && (mID == 9999 || mID == 8998)) ? $('#CaseNoText').val() + '|' + mID : $('#CaseNoText').val()),
            $("#ActionTypeSelect option:selected").map(function () { return this.value; }).get().join(','),
            ($("#chkColumbusAction").is(':checked') == true ? 1 : 0),
            $('input[name=rdBW]:checked').val(),
            ($('input[name=rdBW]:checked').val() == 0 ? $('#selNotFilming option:selected').text() : ''),
            ($('input[name=rdBW]:checked').val() == 1 ? $('#selRecordingNotSuccess option:selected').text() : ''),
            $('input[name=Type]:checked').val());
    });

    $('input[type=radio][name=rdBW]').change(function () {
        if (this.value == 0) {
            $('#selNotFilming').hide();
            $('#selRecordingNotSuccess').show();
            $('#selNotFilming').val(0).attr("selected", "selected");
            $('#lblReason').html('Reason for recoding was not successful');
        }
        else if (this.value == 1) {
            $('#selRecordingNotSuccess').hide();
            $('#selNotFilming').show();
            $('#selRecordingNotSuccess').val(0).attr("selected", "selected");
            $('#lblReason').html('Reason for not filming');
        }
        else {
            $('#selRecordingNotSuccess,#selNotFilming').hide();
            $('#lblReason').html('');
        }
    });
    $('input[name=rdBW][value=2]').prop('checked', true);
    $('#selRecordingNotSuccess').show();
    $('#lblReason').html('Reason for recoding was not successful');

    //----------------------------------------------------------------------------------------------------------------------
    // Keyup function

    $('#OfficerFindText').keyup(function () {
        if ($.trim($('#OfficerFindText').val()) != '') {
            GetOfficersOnSearch($('#OfficerFindText').val());
        }
        else {
            GetOfficersOnSearch($('#OfficerFindText').val());
        }
    });

    //----------------------------------------------------------------------------------------------------------------------
});


//----------------------------------------------------------------------------------------------------------------------
// TOP Panel list box

//TODO: Manager list
function GetManagers() {
    $.ajax({
        type: 'GET',
        url: ServiceURL + 'api/v1/managers/' + mID,
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        dataType: 'json',
        beforeSend: function () { },
        success: function (data) {
            $('#ManagerSelect').empty();
            var ManagerList = '<option value="0">Select all managers</option>';
            $.each(data, function (key, value) {
                ManagerList = ManagerList + '<option value="' + value.OfficerID + '">' + value.OfficerName + '</option>';
            });
            $('#ManagerSelect').html(ManagerList);
            $('#ManagerSelect').change(function () {
                GetOfficerManager($('#ManagerSelect').val());
                if ($('#ManagerSelect').val() == 0)
                    $('#OfficerFindText').show();
                else
                    $('#OfficerFindText').hide();
            });
        },
        complete: function () { },
        error: function (xhr, textStatus, errorThrown) { }
    });
}

//TODO: officer list for selected manager
function GetOfficerManager(ManagerID) {
    $.ajax({
        type: 'GET',
        url: ServiceURL + 'api/v1/officers/' + $.trim(ManagerID) + '/OfficersManager',
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        dataType: 'json',
        beforeSend: function () { },
        success: function (data) {
            $("#OfficerSelect").empty();
            if (data != undefined) {
                if (data != "[]" && data != null) {
                    var OfficerList = '<option value="">Select all officers</option>';
                    $.each(data, function (key, value) {
                        OfficerList = OfficerList + '<option value="' + value.OfficerID + '">' + value.OfficerName + '</option>';
                    });
                    $('#OfficerSelect').html(OfficerList);
                }
            }
        },
        complete: function () { },
        error: function (xhr, textStatus, errorThrown) {
        }
    });
}

//TODO: Officers On Search list
function GetOfficersOnSearch(SearchText) {
    $.ajax({
        type: 'GET',
        url: ServiceURL + 'api/v1/officers/' + SearchText + '/SearchOfficers',
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        dataType: 'json',
        beforeSend: function () { },
        success: function (data) {
            $("#OfficerSelect").empty();
            if (data != undefined) {
                if (data != "[]" && data != null) {
                    var OfficerList = '<option value="">Select all officers</option>';
                    $.each(data, function (key, value) {
                        OfficerList = OfficerList + '<option value="' + value.OfficerID + '">' + value.OfficerName + '</option>';
                    });
                    $('#OfficerSelect').html(OfficerList);
                }
            }
        },
        complete: function () { },
        error: function (xhr, textStatus, errorThrown) { }
    });
}

//----------------------------------------------------------------------------------------------------------------------
//TODO
function GetCaseActionsSearchForAdmin(ManagerID, OfficerID, FromDatep, ToDatep, CaseNo, Type, IsApprove, IsBWV, ReasonNotFilming, ReasonUnsuccessful, Category) {
    var Parameter = "";
    var FromDate = "", ToDate = "";
    if ($.trim(FromDatep) != "") {
        var dateF = FromDatep.split('/');
        FromDate = dateF[2] + '-' + dateF[1] + '-' + dateF[0];
    }
    if ($.trim(ToDatep) != "") {
        var dateT = ToDatep.split('/');
        ToDate = dateT[2] + '-' + dateT[1] + '-' + dateT[0];
    }

    if ($.trim(ManagerID) != "") {
        Parameter = 'ManagerId=' + ManagerID;
    }

    if ($.trim(OfficerID) != "") {
        if (Parameter != "") {
            Parameter += "&";
            Parameter += 'OfficerId=' + OfficerID;
        }
        else
            Parameter = 'OfficerId=' + OfficerID;
    }

    if ($.trim(Type) != "") {
        if (Parameter != "") {
            Parameter += "&";
            Parameter += 'ActionType=' + Type;
        }
        else
            Parameter = 'ActionType=' + Type;
    }

    if ($.trim(FromDate) != "") {
        if (Parameter != "") {
            Parameter += "&";
            Parameter += 'FromDate=' + FromDate;
        }
        else
            Parameter = 'FromDate=' + FromDate;
    }

    if ($.trim(ToDate) != "") {
        if (Parameter != "") {
            Parameter += "&";
            Parameter += 'ToDate=' + ToDate;
        }
        else
            Parameter = 'ToDate=' + ToDate;
    }

    if ($.trim(IsApprove) != "") {
        if (Parameter != "") {
            Parameter += "&";
            Parameter += 'IsApproved=' + IsApprove;
        }
        else
            Parameter = 'IsApproved=' + IsApprove;
    }

    if ($.trim(IsBWV) != "") {
        if (Parameter != "") {
            Parameter += "&";
            Parameter += 'IsBWV=' + IsBWV;
        }
        else
            Parameter = 'IsBWV=' + IsBWV;
    }

    if ($.trim(ReasonNotFilming) != "") {
        if (Parameter != "") {
            Parameter += "&";
            Parameter += 'ReasonNotFilming=' + ReasonNotFilming;
        }
        else
            Parameter = 'ReasonNotFilming=' + ReasonNotFilming;
    }

    if ($.trim(ReasonUnsuccessful) != "") {
        if (Parameter != "") {
            Parameter += "&";
            Parameter += 'ReasonUnsuccessful=' + ReasonUnsuccessful;
        }
        else
            Parameter = 'ReasonUnsuccessful=' + ReasonUnsuccessful;
    }

    if ($.trim(CaseNo) != "") {
        if (Parameter != "") {
            Parameter += "&";
            Parameter += 'CaseNo=' + CaseNo;
        }
        else
            Parameter = 'CaseNo=' + CaseNo;
    }



    //var parameter = {};
    //parameter = parameterPush(parameter, "ManagerId", ManagerID)
    //parameter = parameterPush(parameter, "OfficerId", OfficerID)
    //parameter = parameterPush(parameter, "FromDate", FromDate)
    //parameter = parameterPush(parameter, "ToDate", ToDate)
    //parameter = parameterPush(parameter, "ActionType", Type)
    //parameter = parameterPush(parameter, "IsApproved", IsApprove)
    //parameter = parameterPush(parameter, "IsBWV", IsBWV)
    //parameter = parameterPush(parameter, "ReasonNotFilming", ReasonNotFilming)
    //parameter = parameterPush(parameter, "ReasonUnsuccessful", ReasonUnsuccessful)
    //// parameter = parameterPush(parameter, "CaseNo", CaseNo)

    $.ajax({
        type: 'GET',
        url: ServiceURL + 'api/v1/cases/CaseActionsSearchForDataExport?' + Parameter,
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        //data: parameter + '&CaseNo=' + CaseNo,
        dataType: 'json',
        beforeSend: function () {
            $.loader({
                className: "blue-with-image",
                content: ''
            });
        },
        success: function (msg) {
            alert(JSON.stringify(msg));
            alert('sucess :' + msg);
            if (msg == 'row limit exceeded') {
                $('#DAlert').empty().attr('class', 'alert alert-danger').append('<strong>Opps!</strong> The result has been exceeded the excel sheet row limit. Please refine your search').fadeIn(1).fadeOut(6000);
                //   alert('The result has been exceeded the excel sheet row limit. Please refine your search');
            }
            else if (msg == 'No data') {
                //  alert('No data found.');
                $('#DAlert').empty().attr('class', 'alert alert-danger').append('<strong>No data found! </strong>').fadeIn(1).fadeOut(6000);
            }
            else {
                Tabledata = msg;
                window.open(Tabledata, "_blank");
            }
        },
        complete: function () {
            $.loader('close');
        },
        error: function (xhr, textStatus, errorThrown) {
            $('#DAlert').empty().attr('class', 'alert alert-danger').append('<strong>Opps!</strong> The result has been exceeded the excel sheet row limit. Please refine your search').fadeIn(1).fadeOut(6000);
            $.loader('close');
        }
    });



    //$.ajax({
    //    type: "GET",
    //    url: serviceUrl + "/GetCaseActionsSearchForDataExport?ManagerId=" + ManagerID + "&OfficerId=" + OfficerID + "&FromDate="
    //        + FromDate + "&ToDate=" + ToDate + "&CaseNo=" + CaseNo + "&ActionType=" + Type + "&IsApproved=" + IsApprove +
    //        "&IsBWV=" + IsBWV + "&ReasonNotFilming=" + ReasonNotFilming + "&ReasonUnsuccessful=" + ReasonUnsuccessful +
    //        "&Category=" + Category + "&" + getCookie('BrowserId'), // Location of the service
    //    contentType: "application/javascript", // content type sent to server
    //    dataType: "jsonp", //Expected data format from server       
    //    processdata: false, //True or False      
    //    async: true,
    //    timeout: 60000,
    //    beforeSend: function () {
    //        $.loader({
    //            className: "blue-with-image",
    //            content: ''
    //        });
    //    },
    //    complete: function () {
    //        $.loader('close');
    //    },
    //    success: function (msg) {//On Successfull service call  
    //        if (msg == 'row limit exceeded') {
    //            $('#DAlert').empty().attr('class', 'alert alert-danger').append('<strong>Opps!</strong> The result has been exceeded the excel sheet row limit. Please refine your search').fadeIn(1).fadeOut(6000);
    //            //   alert('The result has been exceeded the excel sheet row limit. Please refine your search');
    //        }
    //        else if (msg == 'No data') {
    //            //  alert('No data found.');
    //            $('#DAlert').empty().attr('class', 'alert alert-danger').append('<strong>No data found! </strong>').fadeIn(1).fadeOut(6000);
    //        }
    //        else {
    //            Tabledata = msg;
    //            window.open(Tabledata, "_blank");
    //        }
    //    },
    //    error: function (jqXHR, textStatus, errorThrown) {
    //        //alert('error' + errorThrown);
    //        $('#DAlert').empty().attr('class', 'alert alert-danger').append('<strong>Opps!</strong> The result has been exceeded the excel sheet row limit. Please refine your search').fadeIn(1).fadeOut(6000);
    //        $.loader('close');
    //    }
    //});
}