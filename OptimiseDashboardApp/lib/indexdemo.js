﻿//==========================================================================================================================================
/* File Name: index.js */
/* File Created: January 21, 2014 */
/* Created By  : R.Santhakumar */
//==========================================================================================================================================

var NotificationHostURL = 'http://www.testoptimise.org:8196/testofficeractivitymonitor/TestNotificationHost/SendNotification.aspx';
//var RedirectUrl = "http://localhost:8065/Admin/admin-page.html";
var RedirectUrl = "http://intranet.atom8itsolutions.com:8195/testoam/Admin/admin-page.html";

var max;// Notification message character
var OfficerID;// = window.name.toString().split('|');
var OfficerName;
var Tab, CntTab1, CntTab2, CntTab3;
var date;
var TodayDate;
var TodayDateOnly;
var RefreshIntervalValue;

$(document).ready(function () {
    $('#AHeartBeat').click(function () {
        HeartBeat(1);
    });

    $('#AHeartBeatEnlarge').click(function () {
        HeartBeat(2);
    });

    $('#ALocationwithHB').click(function () {
        LocationwithHB(1);
    });

    $('#ALocationwithHBEnlarge').click(function () {
        LocationwithHB(2);
    });

    $('#APageEnlarge').click(function () {
        MapExpandView();
    });
    if (getCookie('OfficerID') != '' && getCookie('OfficerID') != undefined) {
        OfficerID = getCookie('OfficerID');
        OfficerName = getCookie('OfficerName');
        setCookie('RankingType', 'Paid', 1);
        setCookie('ManagerID', OfficerID);
        setCookie('Map', '', 1);
        // Vedio
        if (getCookie('IsVedio') == 1) {
            introvideo();
        }
        //CompanyID Check
        if (getCookie('CompanyID') == 1)
            $('#lblClientSearch,#DClient,#lblPendingAction,#lblCaseActionSearch,#IDeviated').show();
        else
            $('#lblClientSearch,#DClient,#lblPendingAction,#lblCaseActionSearch,#IDeviated').hide();

        //TODO: ClientID check
        if (getCookie('ClientId') == undefined || getCookie('ClientId') == 0)
            $('#DClient').hide();
        else
            $('#DClient').show();

        $('#pClientName').html(getCookie('ClientName'));

        // getting month target for manager when login
        if (getCookie('tgVal') != '') {
            $('#lblMonthTotalTarget').html(getCookie('tgVal'));
            $('#txtMonthTotalTarget').html(getCookie('tgVal'));
        }

        // TODO: Flash news
        if (getCookie('FlashNews') == undefined || getCookie('FlashNews') == 'Off') {
            $('#DMarquee').hide();
            $('#btnFlashOff').hide();
            $('#btnFlashOn').show();
        }
        else {
            $('#btnFlashOn').hide();
            $('#DMarquee').show();
            $('#btnFlashOff').show();
        }

        //Super user role
        if (getCookie('RoleType') == 7) {
            $('#ACompanyList').show();
            GetCompany();
        }
        else {
            $('#ACompanyList').hide();
        }
        //Refresh time
        if (getCookie('aref') != undefined) {
            $('#txtRefreshTime').val(getCookie('aref'));
        }
    }
    else {
        window.location.href = "sign-in.html";
    }

    $('#txtRefreshTime').keydown(function (event) {
        if (event.shiftKey == true) {
            $('#txtRefreshTime').attr('title', 'Enter numeric values');
            return false;
        }
        if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190 || ((event.keyCode == 65 || event.keyCode == 67 || event.keyCode == 86) && event.ctrlKey == true)) {
            return true;
        }
        else {
            $('#txtRefreshTime').attr('title', 'Enter numeric values');
            return false;
        }
    });

    date = new Date();
    TodayDate = $.datepicker.formatDate('yy/mm/dd', date) + ' ' + date.getHours() + ':' + date.getMinutes();
    TodayDateOnly = $.datepicker.formatDate('dd/mm/yy', date);

    $('#txtFromDate,#txtToDate,#txtFromDateCA,#txtToDateCA,#txtFromDateLocation,#txtToDateLocation').val(TodayDateOnly);
    $('#txtFromDateRanking,#txtToDateRanking,#txtTLineFromDate,#txtTLineToDate').val(TodayDateOnly);

    $('#btnTodayWMADate').click(function () {
        $('#txtFromDate').val(TodayDateOnly);
        $('#txtToDate').val(TodayDateOnly);

        $('#btnWMADateOk').trigger('click');
        $('#AWMADate').attr('data-original-title', 'Today');
    });

    $('#btnCAToday').click(function () {
        $('#txtFromDateCA').val(TodayDateOnly);
        $('#txtToDateCA').val(TodayDateOnly);

        $('#btnCADateOk').trigger('click');
        $('#ACADate').attr('data-original-title', 'Today');
    });

    $('#btnRAToday').click(function () {
        $('#txtFromDateRanking').val(TodayDateOnly);
        $('#txtToDateRanking').val(TodayDateOnly);

        $('#btnRankingDateOK').trigger('click');
        $('#ARankingDate').attr('data-original-title', 'Today');
    });

    $('#btnLocationToday').click(function () {
        $('#txtFromDateLocation').val(TodayDateOnly);
        $('#btnLocationDateOk').trigger('click');
        $('#ALocationDate').attr('data-original-title', 'Today');
    });

    $('#ALastLocation').click(function () {
        MapView();
        clearInterval(RefreshIntervalValue);
    });

    $('#btnTLineSubmit').click(function () {
        GetTimelineResult($.trim(getCookie('TimelineOfficer')));
    });

    $('#ACloseLocation').click(function () {
        RefreshIntervalValue = setInterval(function () {
            if ($('#lblSelectedOfficerID').html() != '0') {

                FillWMA('0', $('#txtFromDate').val(), $('#txtFromDate').val(), $('#lblSelectedOfficerID').html());

                FillWMAMore('0', $('#txtFromDate').val(), $('#txtFromDate').val(), $('#lblSelectedOfficerID').html());

                FillCaseActions('0', $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), $('#lblSelectedOfficerID').html());

                FillDeviation('0', $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), $('#lblSelectedOfficerID').html());

                FillCaseDetailsAction('0', $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), $('#lblSelectedOfficerID').html());

                GetStatistics($('#lblSelectedOfficerID').html());

            }
            else if ($('#lblSelectedGroupID').html() != '0') {

                FillWMA('0', $('#txtFromDate').val(), $('#txtFromDate').val(), '0', $('#lblSelectedGroupID').html());

                FillWMAMore('0', $('#txtFromDate').val(), $('#txtFromDate').val(), '0', $('#lblSelectedGroupID').html());

                FillCaseActions('0', $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), '0', $('#lblSelectedGroupID').html());

                FillDeviation('0', $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), '0', $('#lblSelectedGroupID').html());

                FillCaseDetailsAction('0', $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), '0', $('#lblSelectedGroupID').html());

                FillRankings('0', $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Paid', $('#lblSelectedGroupID').html());
                //FillRankings('0', $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Returned', $('#lblSelectedGroupID').html());
                //FillRankings('0', $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Part Paid', $('#lblSelectedGroupID').html());

                FillRankingsMore('0', $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Paid', $('#lblSelectedGroupID').html());
                //FillRankingsMore('0', $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Returned', $('#lblSelectedGroupID').html());
                //FillRankingsMore('0', $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Part Paid', $('#lblSelectedGroupID').html());


            }
            else if ($('#lblSelectedManagerID').html() != '0') {
                FillWMA($('#lblSelectedManagerID').html(), $('#txtFromDate').val(), $('#txtFromDate').val(), '0', '0');

                FillWMAMore($('#lblSelectedManagerID').html(), $('#txtFromDate').val(), $('#txtFromDate').val(), '0', '0');

                FillCaseActions($('#lblSelectedManagerID').html(), $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), '0', '0');

                FillDeviation($('#lblSelectedManagerID').html(), $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), '0', '0');

                FillCaseDetailsAction($('#lblSelectedManagerID').html(), $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), '0', '0');

                FillRankings($('#lblSelectedManagerID').html(), $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Paid', '0');
                //FillRankings($('#lblSelectedManagerID').html(), $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Returned', '0');
                //FillRankings($('#lblSelectedManagerID').html(), $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Part Paid', '0');

                FillRankingsMore($('#lblSelectedManagerID').html(), $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Paid', '0');
                //FillRankingsMore($('#lblSelectedManagerID').html(), $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Returned', '0');
                //FillRankingsMore($('#lblSelectedManagerID').html(), $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Part Paid', '0');

                GetStatistics($('#lblSelectedManagerID').html());
            }
            else {
                FillWMA(OfficerID, $('#txtFromDate').val(), $('#txtFromDate').val());

                FillWMAMore(OfficerID, $('#txtFromDate').val(), $('#txtFromDate').val());

                FillCaseActions(OfficerID, $('#txtFromDateCA').val(), $('#txtFromDateCA').val());

                FillDeviation(OfficerID, $('#txtFromDateCA').val(), $('#txtFromDateCA').val());

                FillCaseDetailsAction(OfficerID, $('#txtFromDateCA').val(), $('#txtFromDateCA').val());

                FillRankings(OfficerID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Paid');
                //FillRankings(OfficerID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Returned');
                //FillRankings(OfficerID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Part Paid');

                FillRankingsMore(OfficerID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Paid');
                //FillRankingsMore(OfficerID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Returned');
                //FillRankingsMore(OfficerID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Part Paid');

                GetStatistics(OfficerID);
            }

        }, $('#txtRefreshTime').val() * 1000 * 60);
    });

    $('#txtareacontent').val('');

    //: Default checkbox is checked or not
    if ($("#chkIsPriority").is(':checked')) {
        max = 200;
        $('.word_count span').text(max - $.trim($('#txtareacontent').val()).length + ' characters left');
    }
    else {
        max = 900;
        $('.word_count span').text(max - $.trim($('#txtareacontent').val()).length + ' characters left');
    }

    // Checkbox change function
    $("#chkIsPriority").change(function () {
        if ($("#chkIsPriority").is(':checked')) {
            max = 200;
            $('.word_count span').text(max - $.trim($('#txtareacontent').val()).length + ' characters left');
        }
        else {
            max = 900;
            $('.word_count span').text(max - $.trim($('#txtareacontent').val()).length + ' characters left');
        }
    });

    $("#txtareacontent").bind('paste', function (e) {
        if ($("#chkIsPriority").is(':checked')) {
            max = 200;
            $('#txtareacontent').attr('maxlength', '200');
            setTimeout(function () {
                if ($('#txtareacontent').val().length >= max) {
                    $('.word_count span').text(' Character limit exceeded').attr('style', 'font-weight: bold');
                }
                else {
                    $('.word_count span').text(max - $('#txtareacontent').val().length + ' characters left');
                }
            }, 100);
        }
        else {
            max = 900;
            $('#txtareacontent').attr('maxlength', '900');
            setTimeout(function () {
                if ($('#txtareacontent').val().length >= max) {
                    $('.word_count span').text(' Character limit exceeded').attr('style', 'font-weight: bold');
                }
                else {
                    $('.word_count span').text(max - $('#txtareacontent').val().length + ' characters left');
                }
            }, 100);
        }
    });

    // Textarea keydown function
    $('#txtareacontent').keydown(function () {
        var len = $.trim($('#txtareacontent').val()).length;// $(this).val().length;
        if ($("#chkIsPriority").is(':checked')) {
            max = 200;
            $('.word_count span').text(max - len + ' characters left');
        }
        else {
            max = 900;
            $('.word_count span').text(max - len + ' characters left');
        }
        if (len >= max) {
            $(this).val($(this).val().substring(0, len - 1));
            $('.word_count span').text(' Reached the limit');
        }
        else {
            var ch = max - len;
            $('.word_count span').text(ch + ' characters left');
        }

        if ($.trim($('#txtareacontent').val()).length > 0) {
            $('#btnSendNotification').removeAttr('disabled');
        }
        else {
            $('#btnSendNotification').attr('disabled', true);
        }
    });

    $('#btnSendNotification').click(function () {
        var array_of_checked_values = $("#selOfficer").multiselect("getChecked").map(function () {
            return this.value;
        }).get();
        var notes = $("#txtareacontent").val().replace(/[^a-z0-9,£:'`&.\s]/gi, '').replace(/[_\s]/g, ' ');
        var OIDs = array_of_checked_values.toString().split(',');
        if (!(OIDs == '' || OIDs == null)) {
            if ($("#chkIsPriority").is(':checked')) {
                max = 200;
                if ($.trim($('#txtareacontent').val()).length >= max) {
                    $('.word_count span').text(' Character limit exceeded').attr('style', 'font-weight: bold');
                }
                else {
                    $.each(OIDs, function (key, value) {
                        UpdateNotificationAlert(getCookie('OfficerID'), notes, value, $('#chkIsPriority:checked').val() != undefined ? 1 : 0);
                    });
                    $('#txtareacontent').val('');
                    $("#chkIsPriority").prop("checked", false);
                }
            }
            else {
                max = 900;
                if ($.trim($('#txtareacontent').val()).length >= max) {
                    $('.word_count span').text(' Character limit exceeded').attr('style', 'font-weight: bold');
                }
                else {
                    $.each(OIDs, function (key, value) {
                        UpdateNotificationAlert(getCookie('OfficerID'), notes, value, $('#chkIsPriority:checked').val() != undefined ? 1 : 0);
                    });
                    $('#txtareacontent').val('');
                    $("#chkIsPriority").prop("checked", false);
                }
            }
        }
        else {
            $('.word_count').hide();
            $('#lblMsgReport').html('Please select officer(s)').show().fadeOut(4000);
            $('#lblMsgReport').css('color', 'red');
            $('.word_count').fadeIn(4000);
        }
    });

    //==========================================================================================================================================
    //TODO : Datepicker 
    var cDate = new Date()
    $('#txtFromDate').datetimepicker({
        timepicker: false,
        format: 'd/m/Y'
    }).datepicker("setDate", TodayDate);

    $('#txtToDate').datetimepicker({
        timepicker: false,
        format: 'd/m/Y'
    }).datepicker("setDate", TodayDate);

    $('#txtFromDateCA').datetimepicker({
        timepicker: false,
        format: 'd/m/Y'
    }).datepicker("setDate", TodayDate);

    $('#txtToDateCA').datetimepicker({
        timepicker: false,
        format: 'd/m/Y'
    }).datepicker("setDate", TodayDate);

    $('#txtFromDateRanking').datetimepicker({
        timepicker: false,
        format: 'd/m/Y'
    }).datepicker("setDate", TodayDate);

    $('#txtToDateRanking').datetimepicker({
        timepicker: false,
        format: 'd/m/Y'
    }).datepicker("setDate", TodayDate);

    $('#txtFromDateLocation').datetimepicker({
        timepicker: false,
        format: 'd/m/Y'
    }).datepicker("setDate", TodayDate);

    $('#txtToDateLocation').datetimepicker({
        timepicker: false,
        format: 'd/m/Y'
    }).datepicker("setDate", TodayDate);

    $('#txtTLineFromDate').datetimepicker({
        timepicker: false,
        format: 'd/m/Y'
    }).datepicker("setDate", TodayDate);

    $('#txtTLineToDate').datetimepicker({
        timepicker: false,
        format: 'd/m/Y'
    }).datepicker("setDate", TodayDate);

    //==========================================================================================================================================

    // Target tab click
    $('#popup1Trigger').click(function () {
        $('#' + $('#popup1Trigger').attr('aria-describedby')).attr('class', 'popover fade bottom in');
        $('#' + $('#popup2Trigger').attr('aria-describedby')).attr('class', 'popover fade bottom');
        $('#' + $('#popup3Trigger').attr('aria-describedby')).attr('class', 'popover fade bottom');

        CntTab1 = 0;
        Tab = 1;
    });

    //Quick Menu tab click
    $('#popup2Trigger').click(function () {
        $('#' + $('#popup2Trigger').attr('aria-describedby')).attr('class', 'popover fade bottom in');
        $('#' + $('#popup1Trigger').attr('aria-describedby')).attr('class', 'popover fade bottom');
        $('#' + $('#popup3Trigger').attr('aria-describedby')).attr('class', 'popover fade bottom');
        if (getCookie('FlashNews') == undefined || getCookie('FlashNews') == 'Off') {
            $('#DMarquee').hide();
            $('#btnFlashOff').hide();
            $('#btnFlashOn').show();
        }
        else {
            $('#btnFlashOn').hide();
            $('#DMarquee').show();
            $('#btnFlashOff').show();
        }

        CntTab2 = 0;
        Tab = 2;
        $('#txtRefreshTime').val(getCookie('aref'));
    });

    //Logout tab click
    $('#popup3Trigger').click(function () {
        $('#' + $('#popup3Trigger').attr('aria-describedby')).attr('class', 'popover fade bottom in');
        $('#' + $('#popup1Trigger').attr('aria-describedby')).attr('class', 'popover fade bottom');
        $('#' + $('#popup2Trigger').attr('aria-describedby')).attr('class', 'popover fade bottom');

        CntTab3 = 0;
        Tab = 3;
    });

    $('#btnBack').click(function () {
        $('#lblSearchType').html('');
        showhidetabs('tblbodySearchType');
        $('#btnBack').hide();
        if (getCookie('TreeLevel') == 0 || getCookie('TreeLevel') == 1)
            $('#btnWMAOfficerBack').show();
        else
            $('#btnWMAOfficerBack').hide();
    });

    $('#btnWMAOfficerBack').click(function () {
        $('#lblSearchType,#lblOfficerDisplay').html('');
        showhidetabs('tblWMAOfficers');
        $('#btnWMAOfficerBack').hide();
        $('#lblWMAManager').show();
        if (getCookie('TreeLevel') == 0)
            $('#btnWMAADMBack').show();
        else
            $('#btnWMAADMBack').hide();


    });

    $('#btnWMAADMBack').click(function () {
        $('#lblSearchType,#lblOfficerDisplay,#lblWMAADM').html('');
        showhidetabs('tblWMAADM');
        $('#tblWMAOfficers,#btnWMAADMBack').hide();
    });

    $('#btnStatsBack').click(function () {
        $('#tblStatsManagerCount,#tblStatsOfficerCount,#btnStatsADMBack,#btnStatsBack').hide();
        $('#tblStatsADMCount').show();
        $('#lblStatsMgrName').html('').hide();
        if (getCookie('TreeLevel') == 0 || getCookie('TreeLevel') == 2)
            $('#btnStatsADMBack').show();

    });

    $('#btnStatsADMBack').click(function () {
        $('#tblStatsADMCount,#tblStatsOfficerCount,#btnStatsADMBack').hide();
        $('#tblStatsManagerCount').show();
        //if (getCookie('TreeLevel') == 0 || getCookie('TreeLevel') == 1)
        //    $('#btnStatsADMBack').show();
    });

    $('#btnCaseActionBack').click(function () {
        caseactionshowhidetabs();
        $('#HCaseActionNavigation,#btnCaseActionBack').hide();
        $('#divCaseActionsOfficerCount').show();
        $('#btnCaseActionADM').show();
        $('#lblCAOfficerName').html('');
        if (getCookie('TreeLevel') == 2) {
            $('#btnCaseActionADM').hide();
        }
    });

    $('#btnDeviationMapBack').click(function () {
        caseactionshowhidetabs();
        $('#HCaseActionNavigation,#btnDeviationMapBack').hide();
        $('#deviatedformanager').show();
        if (getCookie('TreeLevel') == 0 || getCookie('TreeLevel') == 1)
            $('#btnCaseActionBack').hide();
        else
            $('#btnCaseActionBack').show();

        $('#lblCAOfficerName').html('');
    });

    $('#btnCaseActionManager').click(function () {
        caseactionshowhidetabs();
        $('#HCaseActionNavigation,#divCaseActionsOfficerCount,#btnCaseActionManager,#divCaseActionsADMCount').hide();
        $('#divCaseActionsManagerCount').show();
        $('#lblCAManagerName').html('');
    });

    $('#btnCaseActionADM').click(function () {
        caseactionshowhidetabs();
        $('#HCaseActionNavigation,#divCaseActionsOfficerCount,#btnCaseActionADM,#divCaseActionsManagerCount').hide();
        $('#divCaseActionsADMCount,#btnCaseActionManager').show();
        $('#lblCAADMName').html('');
        if (getCookie('TreeLevel') == 1) {
            $('#btnCaseActionManager').hide();
        }
    });

    $('#btnRankADMBack').click(function () {
        $('#DRankingOfficer,#DRankingCase,#btnRankADMBack').hide();
        $('#DRankingADM').show();
        $('#lblRankADMName').html('');
    });

    $('#btnRankOfficerBack').click(function () {
        if (getCookie('TreeLevel') == 1) {
            $('#DRankingCase,#btnRankOfficerBack,#btnRankADMBack').hide();
            $('#DRankingOfficer').show();
            $('#lblRankOfficerName').html('');
        }
        else {
            $('#DRankingCase,#btnRankOfficerBack').hide();
            $('#DRankingOfficer,#btnRankADMBack').show();
            $('#lblRankOfficerName').html('');
        }
    });

    $('#IDeviated').click(function () {
        $('#SpanAction1').html('Deviation Actions');
        $('#tblCA,#IDeviated').hide();
        $('#tblDeviation,#ICaseAction').show();
    });

    $('#ICaseAction').click(function () {
        $('#SpanAction1').html('Case Actions');
        $('#tblCA,#IDeviated').show();
        $('#tblDeviation,#ICaseAction').hide();
    });

    // TODO : Rank more
    //==========================================================================================================================================
    // paid more click
    $('#ARankingPaidMore').click(function () {
        $('#DRankMorePaid,#DRankMorePaidADM,#DRankMorePaidOfficer,#DRankMorePaidCaseList,#btnPaidADMBack,#btnPaidOfficerBack,#btnPaidCaseBack').hide();
        switch (getCookie('TreeLevel')) {
            case '0':
                $('#DRankMorePaid').show();
                break;
            case '1':
                $('#DRankMorePaidADM').show();
                break;
            case '2':
                $('#DRankMorePaidOfficer').show();
                break;
        }
        $('#lblRankPaidManager,#lblRankPaidADM,#lblRankPaidOfficer').html('');
    });

    // return more click
    $('#ARankingReturnedMore').click(function () {
        $('#DRankMoreReturned,#DRankMoreReturnedADM,#DRankMoreReturnedOfficer,#DRankMoreReturnedCaseList,#btnReturnedADMBack,#btnReturnedOfficerBack,#btnReturnedCaseBack').hide();
        switch (getCookie('TreeLevel')) {
            case '0':
                $('#DRankMoreReturned').show();
                break;
            case '1':
                $('#DRankMoreReturnedADM').show();
                break;
            case '2':
                $('#DRankMoreReturnedOfficer').show();
                break;
        }

        $('#lblRankReturnedManager,#lblRankReturnedADM,#lblRankReturnedOfficer').html('');
    });

    // part paid more click
    $('#ARankingPartPaidMore').click(function () {
        $('#DRankMorePP,#DRankMorePPOfficer,#DRankMorePPCaseList,#btnPPOfficerBack').hide();
        if (getCookie('TreeLevel') == 0 || getCookie('TreeLevel') == 1)
            $('#DRankMorePP').show();
        else
            $('#DRankMorePPOfficer').show();

        $('#lblRankPPManager,#lblRankPPOfficer').html('');
    });

    //==========================================================================================================================================
    // Paid ranking
    $('#btnPaidADMBack').click(function () {
        $('#DRankMorePaidADM,#DRankMorePaidOfficer,#DRankMorePaidCaseList,#btnPaidADMBack').hide();
        $('#DRankMorePaid').show();
        $('#lblRankPaidManager,#lblRankPaidOfficer').html('');
    });

    $('#btnPaidOfficerBack').click(function () {
        $('#DRankMorePaidOfficer,#DRankMorePaidCaseList,#btnPaidOfficerBack').hide();
        $('#DRankMorePaidADM').show();
        if (getCookie('TreeLevel') == 0)
            $('#btnPaidADMBack').show();
        else
            $('#btnPaidADMBack').hide();
        $('#lblRankPaidADM,#lblRankPaidOfficer').html('');
    });

    $('#btnPaidCaseBack').click(function () {
        $('#DRankMorePaidCaseList,#DRankMorePaid,#btnPaidCaseBack,#btnPaidOfficerBack').hide();
        if (getCookie('TreeLevel') == 0 || getCookie('TreeLevel') == 1)
            $('#DRankMorePaidOfficer,#btnPaidOfficerBack').show();
        else
            $('#DRankMorePaidOfficer').show();
        $('#lblRankPaidOfficer').html('');
    });

    // Returned ranking DRankMoreReturnedADM
    $('#btnReturnedADMBack').click(function () {
        $('#DRankMoreReturnedADM,#DRankMoreReturnedOfficer,#DRankMoreReturnedCaseList,#btnReturnedADMBack').hide();
        $('#DRankMoreReturned').show();
        $('#lblRankReturnedManager,#lblRankReturnedOfficer').html('');
    });

    $('#btnReturnedOfficerBack').click(function () {
        $('#DRankMoreReturnedOfficer,#DRankMoreReturnedCaseList,#btnReturnedOfficerBack').hide();
        $('#DRankMoreReturnedADM').show();
        if (getCookie('TreeLevel') == 0)
            $('#btnReturnedADMBack').show();
        else
            $('#btnReturnedADMBack').hide();
        $('#lblRankReturnedADM,#lblRankReturnedOfficer').html('');
    });

    $('#btnReturnedCaseBack').click(function () {
        $('#DRankMoreReturnedCaseList,#DRankMoreReturned,#btnReturnedCaseBack,#btnReturnedOfficerBack').hide();
        if (getCookie('TreeLevel') == 0 || getCookie('TreeLevel') == 1)
            $('#DRankMoreReturnedOfficer,#btnReturnedOfficerBack').show();
        else
            $('#DRankMoreReturnedOfficer').show();
        $('#lblRankReturnedOfficer').html('');
    });

    // Part paid ranking
    $('#btnPPADMBack').click(function () {
        $('#DRankMorePPADM,#DRankMorePPOfficer,#DRankMorePPCaseList,#btnPPOfficerBack,#btnPPCaseBack').hide();
        $('#DRankMorePP').show();
        $('#lblRankPPManager,#lblRankPPOfficer').html('');
    });

    $('#btnPPOfficerBack').click(function () {
        $('#DRankMorePPOfficer,#DRankMorePPCaseList,#btnPPOfficerBack,#btnPPCaseBack').hide();
        $('#DRankMorePPADM,#btnPPADMBack').show();
        $('#lblRankPPADM,#lblRankPPOfficer').html('');
    });

    $('#btnPPCaseBack').click(function () {
        $('#DRankMorePPCaseList,#DRankMorePP,#btnPPCaseBack,#btnPPOfficerBack').hide();

        if (getCookie('TreeLevel') == 0 || getCookie('TreeLevel') == 1)
            $('#DRankMorePPOfficer,#btnPPOfficerBack').show();
        else
            $('#DRankMorePPOfficer').show();
        $('#lblRankPPOfficer').html('');
    });

    // WMA More
    $('#AMoreWMA').click(function () {
        $('#lblWMAMoreManagerName,#lblWMAMoreOfficerName,#lblWMAMoreOfficerCaseName').html('');
        $('#DWMAMoreManager,#DWMAMoreADM,#DWMAMoreOfficer,#DWMAMoreOfficerCaseCount,#DWMAMoreOfficerCase').hide();
        $('#btnWMAMoreManager,#btnWMAMoreADM,#btnWMAMoreOfficer,#btnWMAMoreOfficerCase').hide();
        switch (getCookie('TreeLevel')) {
            case '0':
                $('#DWMAMoreManager').show();
                break;
            case '1':
                $('#DWMAMoreADM').show();
                break;
            case '2':
                $('#DWMAMoreOfficer').show();
                break;
        }
    });

    $('#btnWMAMoreManager').click(function () {
        $('#lblWMAMoreManagerName').html('');
        $('#btnWMAMoreManager,#DWMAMoreADM,#DWMAMoreOfficer').hide();
        $('#DWMAMoreManager').show();
    });

    $('#btnWMAMoreADM').click(function () {
        $('#lblWMAMoreADMName').html('');
        $('#btnWMAMoreADM,#DWMAMoreOfficer').hide();
        $('#DWMAMoreADM').show();
        if (getCookie('TreeLevel') == 0)
            $('#btnWMAMoreManager').show();
        else
            $('#btnWMAMoreManager').hide();
    });

    $('#btnWMAMoreOfficer').click(function () {
        $('#lblWMAMoreOfficerName').html('');
        $('#DWMAMoreOfficerCaseCount,#btnWMAMoreOfficer,#btnWMAMoreManager').hide();
        if (getCookie('TreeLevel') == 0 || getCookie('TreeLevel') == 1)
            $('#DWMAMoreOfficer,#btnWMAMoreADM').show();
        else
            $('#DWMAMoreOfficer').show();
    });

    $('#btnWMAMoreOfficerCase').click(function () {
        $('#lblWMAMoreOfficerCaseName').html('');
        $('#DWMAMoreOfficerCase,#btnWMAMoreOfficerCase,#btnWMAMoreOfficer').hide();
        if (getCookie('TreeLevel') == 0 || getCookie('TreeLevel') == 1 || getCookie('TreeLevel') == 2)
            $('#DWMAMoreOfficerCaseCount,#btnWMAMoreOfficer').show();
        else
            $('#DWMAMoreOfficerCaseCount').show();
    });

    $('#btnWMADateOk').click(function () {
        OfficerID = getCookie('ManagerID');
        OfficerName = getCookie('MgrName');

        $('#AWMADate').attr('data-original-title', $('#txtFromDate').val());
        FillWMA(OfficerID, $('#txtFromDate').val(), $('#txtFromDate').val());
        FillWMAMore(OfficerID, $('#txtFromDate').val(), $('#txtFromDate').val());
        $('#AWMACloseDate').trigger('click');
    });

    $('#btnCADateOk').click(function () {
        OfficerID = getCookie('ManagerID');
        OfficerName = getCookie('MgrName');

        $('#ACADate').attr('data-original-title', $('#txtFromDateCA').val());
        FillCaseActions(OfficerID, $('#txtFromDateCA').val(), $('#txtFromDateCA').val());
        FillDeviation(OfficerID, $('#txtFromDateCA').val(), $('#txtFromDateCA').val());
        FillCaseDetailsAction(OfficerID, $('#txtFromDateCA').val(), $('#txtFromDateCA').val());
        $('#btnCACloseDate').trigger('click');
    });

    $('#btnLocationDateOk').click(function () {
        OfficerID = getCookie('ManagerID');
        OfficerName = getCookie('MgrName');

        FillLastKnownLocation(OfficerID, $('#txtFromDateLocation').val(), $('#txtFromDateLocation').val());
        $('#ALocationDate').attr('data-original-title', $('#txtFromDateLocation').val());
        $('#btnLocationDateClose').trigger('click');
    });

    $('#btnRankingDateOK').click(function () {
        OfficerID = getCookie('ManagerID');
        OfficerName = getCookie('MgrName');

        FillRankings(OfficerID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Paid');
        FillRankingsMore(OfficerID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Paid');
        $('#ARankingDate').attr('data-original-title', $('#txtFromDateRanking').val());
        $('#ARAClose').trigger('click');
    });

    $('#ACATab').click(function () {
        $('#ACADate').show();
        $('#AWMADate').hide();
    });

    $('#AWMATab').click(function () {
        $('#ACADate').hide();
        $('#AWMADate').show();
    });

    $('#AReturned').click(function () {
        setCookie('RankingType', 'Returned', 1);
    });

    $('#APaid').click(function () {
        setCookie('RankingType', 'Paid', 1);
    });

    $('#APartPaid').click(function () {
        setCookie('RankingType', 'Part Paid', 1);
    });

    $('#imgRefreshLocation').click(function () {
        if ($('#lblSelectedOfficerID').html() != '0') {
            FillLastKnownLocation('0', $('#txtFromDateLocation').val(), $('#txtFromDateLocation').val(), $('#lblSelectedOfficerID').html());
        }
        else if ($('#lblSelectedGroupID').html() != '0') {
            FillLastKnownLocation('0', $('#txtFromDateLocation').val(), $('#txtFromDateLocation').val(), '0', $('#lblSelectedGroupID').html());
        }
        else if ($('#lblSelectedManagerID').html() != '0') {
            FillLastKnownLocation($('#lblSelectedManagerID').html(), $('#txtFromDateLocation').val(), $('#txtFromDateLocation').val(), '0', '0');
        }
        else {
            FillLastKnownLocation(OfficerID, $('#txtFromDateLocation').val(), $('#txtFromDateLocation').val());
        }
    });

    $('.tree-toggle').click(function () {
        $(this).parent().children('ul.tree').toggle(200);
    });

    $('#lblTeamAdmin').click(function () {
        window.location.href = RedirectUrl;
    });

    $('#AMoreWorkingHours').click(function () {
        GetWorkingHoursMore(getCookie('OfficerID'), 0);
    });

    $('#btnWorkingHoursBack').click(function () {
        $('#DivWorkingHoursOfficersList').hide();
        $('#DivWorkingHoursManagersList').show();
        $('#btnWorkingHoursBack').hide();
    });

    //==========================================================================================================================================

    $('#lblLoginOfficer').html(OfficerName);
    FillCaseActions(OfficerID, $('#txtFromDateCA').val(), $('#txtFromDateCA').val());
    FillDeviation(OfficerID, $('#txtFromDateCA').val(), $('#txtFromDateCA').val());
    FillCaseDetailsAction(OfficerID, $('#txtFromDateCA').val(), $('#txtFromDateCA').val());

    FillWMA(OfficerID, $('#txtFromDate').val(), $('#txtFromDate').val());
    FillWMAMore(OfficerID, $('#txtFromDate').val(), $('#txtFromDate').val());
    GetStatistics(OfficerID);

    FillRankings(OfficerID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Paid');
    FillRankingsMore(OfficerID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Paid');

    FillLastKnownLocation(OfficerID, $('#txtFromDateLocation').val(), $('#txtFromDateLocation').val());
    GetOfficerManager(OfficerID, '0', true);

    RefreshIntervalValue = setInterval(function () {
        if ($('#lblSelectedOfficerID').html() != '0') {
            FillWMA('0', $('#txtFromDate').val(), $('#txtFromDate').val(), $('#lblSelectedOfficerID').html());
            FillWMAMore('0', $('#txtFromDate').val(), $('#txtFromDate').val(), $('#lblSelectedOfficerID').html());
            FillCaseActions('0', $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), $('#lblSelectedOfficerID').html());
            FillDeviation('0', $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), $('#lblSelectedOfficerID').html());
            FillCaseDetailsAction('0', $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), $('#lblSelectedOfficerID').html());

            GetStatistics($('#lblSelectedOfficerID').html());
        }
        else if ($('#lblSelectedGroupID').html() != '0') {
            FillWMA('0', $('#txtFromDate').val(), $('#txtFromDate').val(), '0', $('#lblSelectedGroupID').html());
            FillWMAMore('0', $('#txtFromDate').val(), $('#txtFromDate').val(), '0', $('#lblSelectedGroupID').html());
            FillCaseActions('0', $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), '0', $('#lblSelectedGroupID').html());
            FillDeviation('0', $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), '0', $('#lblSelectedGroupID').html());
            FillCaseDetailsAction('0', $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), '0', $('#lblSelectedGroupID').html());

            FillRankings('0', $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Paid', $('#lblSelectedGroupID').html());
            FillRankingsMore('0', $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Paid', $('#lblSelectedGroupID').html());
        }
        else if ($('#lblSelectedManagerID').html() != '0') {
            FillWMA($('#lblSelectedManagerID').html(), $('#txtFromDate').val(), $('#txtFromDate').val(), '0', '0');
            FillWMAMore($('#lblSelectedManagerID').html(), $('#txtFromDate').val(), $('#txtFromDate').val(), '0', '0');
            FillCaseActions($('#lblSelectedManagerID').html(), $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), '0', '0');
            FillDeviation($('#lblSelectedManagerID').html(), $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), '0', '0');
            FillCaseDetailsAction($('#lblSelectedManagerID').html(), $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), '0', '0');

            FillRankings($('#lblSelectedManagerID').html(), $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Paid', '0');
            FillRankingsMore($('#lblSelectedManagerID').html(), $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Paid', '0');
            GetStatistics($('#lblSelectedManagerID').html());
        }
        else {
            FillWMA(OfficerID, $('#txtFromDate').val(), $('#txtFromDate').val());
            FillWMAMore(OfficerID, $('#txtFromDate').val(), $('#txtFromDate').val());
            FillCaseActions(OfficerID, $('#txtFromDateCA').val(), $('#txtFromDateCA').val());
            FillDeviation(OfficerID, $('#txtFromDateCA').val(), $('#txtFromDateCA').val());
            FillCaseDetailsAction(OfficerID, $('#txtFromDateCA').val(), $('#txtFromDateCA').val());

            FillRankings(OfficerID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Paid');
            FillRankingsMore(OfficerID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Paid');
            GetStatistics(OfficerID);
        }
    }, (getCookie('aref') != undefined ? parseInt(getCookie('aref')) * 1000 * 60 : 10000));

    $('#txtSearchOfficer').keyup(function () {
        //***********************************
        var isAtleastOneMatch = false;
        var isManagerMatch = false;

        if ($('#txtSearchOfficer').val().length > 0) {
            $('#ulOfficers li').each(function (key, value) {
                if ($(this).find('a').attr('id').indexOf('Manager') >= 0) {
                    if ($(this).find('a').html().toLowerCase().indexOf($('#txtSearchOfficer').val().toLowerCase()) >= 0) {
                        $(this).find('a').show();
                        isManagerMatch = true;
                    }
                    else {
                        $(this).find('a').hide();
                    }
                }

            });

            if (!isManagerMatch) {
                $('#ulOfficers li').find('ul').find('li').each(function (key, value) {
                    if ($(this).find('a').html().toLowerCase().indexOf($('#txtSearchOfficer').val().toLowerCase()) >= 0) {
                        isAtleastOneMatch = true;
                        $(this).find('a').show();
                        if ($(this).parent().attr('id').toString().indexOf('UL') >= 0) {
                            $('#Group' + $(this).parent().attr('id').toString().substr(2)).show();
                            // $('#Group' + $(this).parent().attr('id').toString().substr(2)).find('i').attr('class', 'glyphicon glyphicon-minus-sign');
                            $('#Group' + $(this).parent().attr('id').toString().substr(2)).parent().show();
                            if ($('#Group' + $(this).parent().attr('id').toString().substr(2)).parent().parent().attr('id').indexOf('ULMan') >= 0) {
                                $('#Manager' + $('#Group' + $(this).parent().attr('id').toString().substr(2)).parent().parent().attr('id').substr(5)).show();
                                $('#Manager' + $('#Group' + $(this).parent().attr('id').toString().substr(2)).parent().parent().attr('id').substr(5)).find('i').attr('class', 'glyphicon glyphicon-minus-sign');
                            }
                        }
                    }
                    else {
                        $(this).hide();
                    }
                });
            }
            if (!isAtleastOneMatch && !isManagerMatch) {
                $('#lblSearchOfficerInfo').html('No Match found');
            }
            else {
                $('#lblSearchOfficerInfo').html('');
            }
        }
        else {
            GetOfficerManager(OfficerID, '0', true);
            $('#lblSearchOfficerInfo').html('');
        }
    });

    GetFlashNews();

    $('#selCompanyList').change(function () {
        LoginDetails4SuperUser($('#selCompanyList').val(), getCookie('OfficerName'));
    });


});

//==========================================================================================================================================
//TODO : Officer Activity status
// To check the officer status

function OfficerActivityStatusClick() {
    OfficerActivityStatusInit();
    $('#AOfficerActivityStatus').click();
}

//==========================================================================================================================================
//TODO : Notification function
// Nofication message pass to manager / officer via tablet / phone

function Notification() {
    $('#lblSendNotification').attr('data-toggle', 'modal').attr('href', '#Notification');
    $('#' + $('#popup2Trigger').attr('aria-describedby')).attr('class', 'popover fade bottom');
}

//==========================================================================================================================================
//TODO : Refresh duration function
// Manager set a refresh duration of case actions records auto refresh.

function RefreshDuration() {
    $('#' + $('#popup2Trigger').attr('aria-describedby')).attr('class', 'popover fade bottom');

    UpdateAutoRefresh(OfficerID, $('#txtRefreshTime').val());
    clearInterval(RefreshIntervalValue);
    RefreshIntervalValue = setInterval(function () {

        if ($('#lblSelectedOfficerID').html() != '0') {

            FillWMA('0', $('#txtFromDate').val(), $('#txtFromDate').val(), $('#lblSelectedOfficerID').html());
            FillWMAMore('0', $('#txtFromDate').val(), $('#txtFromDate').val(), $('#lblSelectedOfficerID').html());
            FillCaseActions('0', $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), $('#lblSelectedOfficerID').html());
            FillDeviation('0', $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), $('#lblSelectedOfficerID').html());
            FillCaseDetailsAction('0', $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), $('#lblSelectedOfficerID').html());

            GetStatistics($('#lblSelectedOfficerID').html());
        }
        else if ($('#lblSelectedGroupID').html() != '0') {

            FillWMA('0', $('#txtFromDate').val(), $('#txtFromDate').val(), '0', $('#lblSelectedGroupID').html());
            FillWMAMore('0', $('#txtFromDate').val(), $('#txtFromDate').val(), '0', $('#lblSelectedGroupID').html());
            FillCaseActions('0', $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), '0', $('#lblSelectedGroupID').html());
            FillDeviation('0', $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), '0', $('#lblSelectedGroupID').html());
            FillCaseDetailsAction('0', $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), '0', $('#lblSelectedGroupID').html());

            FillRankings('0', $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Paid', $('#lblSelectedGroupID').html());
            FillRankingsMore('0', $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Paid', $('#lblSelectedGroupID').html());
        }
        else if ($('#lblSelectedManagerID').html() != '0') {

            FillWMA($('#lblSelectedManagerID').html(), $('#txtFromDate').val(), $('#txtFromDate').val(), '0', '0');
            FillWMAMore($('#lblSelectedManagerID').html(), $('#txtFromDate').val(), $('#txtFromDate').val(), '0', '0');
            FillCaseActions($('#lblSelectedManagerID').html(), $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), '0', '0');
            FillDeviation($('#lblSelectedManagerID').html(), $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), '0', '0');
            FillCaseDetailsAction($('#lblSelectedManagerID').html(), $('#txtFromDateCA').val(), $('#txtFromDateCA').val(), '0', '0');

            FillRankings($('#lblSelectedManagerID').html(), $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Paid', '0');
            FillRankingsMore($('#lblSelectedManagerID').html(), $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Paid', '0');
            GetStatistics($('#lblSelectedManagerID').html());
        }
        else {
            FillWMA(OfficerID, $('#txtFromDate').val(), $('#txtFromDate').val());
            FillWMAMore(OfficerID, $('#txtFromDate').val(), $('#txtFromDate').val());
            FillCaseActions(OfficerID, $('#txtFromDateCA').val(), $('#txtFromDateCA').val());
            FillDeviation(OfficerID, $('#txtFromDateCA').val(), $('#txtFromDateCA').val());
            FillCaseDetailsAction(OfficerID, $('#txtFromDateCA').val(), $('#txtFromDateCA').val());

            FillRankings(OfficerID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Paid');
            FillRankingsMore(OfficerID, $('#txtFromDateRanking').val(), $('#txtFromDateRanking').val(), 'Paid');
            GetStatistics(OfficerID);
        }

    }, $('#txtRefreshTime').val() * 1000 * 60);
}

//==========================================================================================================================================
//TODO : Team admin function
// Redirect to admin page for use this function .

function TeamAdmin() {
    window.location.href = RedirectUrl;
}

//==========================================================================================================================================
//TODO : Client search function
// Selected client case action list

function ClientSearch() {
    $('#lblClientSearch').attr('data-toggle', 'modal').attr('href', '#ClientSearch');
    $('#' + $('#popup2Trigger').attr('aria-describedby')).attr('class', 'popover fade bottom');
    GetClientList();
}

function GetClientList() {
    Type = "GET";
    var inputParams = "/GetClientList?ClientName=";
    Url = serviceUrl + inputParams;
    DataType = "jsonp"; ProcessData = false;
    $.ajax({
        type: Type,
        url: Url, // Location of the service
        contentType: ContentType, // content type sent to server
        dataType: DataType, //Expected data format from server       
        processdata: ProcessData, //True or False      
        async: true,
        timeout: 20000,
        beforeSend: function () { },
        complete: function () {
            if (getCookie('ClientId') == undefined || getCookie('ClientId') == 0) {
                setCookie('ClientId', 0, 1);
            }
        },
        success: function (result) {//On Successfull service call
            if (result != undefined) {
                $('#tblClientList').dataTable().fnClearTable();
                if (result != "[]" && result != null) {
                    result = JSON.parse(result);
                    $.each(result, function (key, value) {
                        $('#tbodyClientList').append('<tr style="border-bottom: 1px solid;"><td>' + value.ClientName + '</td>' +
                            '<td><img src="images/Disactive.png" class="Unselect" style="width: 24px; cursor: pointer;" title="Select client" onclick="ClientSelect(' + key + ',' + value.ClientId + ',' + "'" + value.ClientName + "'" + ')" id="imgClientview' + key + '" /></td></tr>');
                    });
                    $('#tblClientList').dataTable({
                        "sScrollY": "auto",
                        "bPaginate": true,
                        "bDestroy": true,
                        "bSort": false,
                        "sPaginationType": "full_numbers",
                        "bLengthChange": false,
                        "sPageButton": "paginate_button",
                        "sPageButtonActive": "paginate_active",
                        "sPageButtonStaticDisabled": "paginate_button",
                        "bFilter": true,
                        "bInfo": false,
                        "iDisplayLength": 5
                    });
                    if (!(getCookie('ClientKey') == undefined || getCookie('ClientKey') == '')) {
                        $('#imgClientview' + getCookie('ClientKey')).attr('src', 'images/Active.png').attr('class', 'Select');
                    }
                }
                else {
                    $('#tblClientList').dataTable({
                        "sScrollY": "auto",
                        "bPaginate": false,
                        "bDestroy": true,
                        "bSort": false,
                        "bLengthChange": false,
                        "bFilter": false,
                        "bInfo": false,
                        "iDisplayLength": 1
                    });
                }
            }
        },
        error: function () {
            //alert('error');
        }
    });
}

function ClientSelect(key, ClientId, Name) {
    setCookie('ClientKey', key, 1);
    setCookie('ClientId', ClientId, 1);
    setCookie('ClientName', Name, 1);
    $('#pClientName').html(Name);
    $('#DClient').show();

    $('.Select').removeClass('Select').addClass('Unselect').attr('src', 'images/Disactive.png');
    if ($('#imgClientview' + key).attr('src') == 'images/Disactive.png')
        $('#imgClientview' + key).attr('src', 'images/Active.png').attr('class', 'Select');
    else
        $('#imgClientview' + key).attr('src', 'images/Disactive.png').attr('class', 'Unselect');
}

function ShowAllClient() {
    setCookie('ClientId', 0, 1);
    setCookie('ClientName', '', 1);
    setCookie('ClientKey', '', 1);
    $('#pClientName').html('');
    $('#DClient').hide();
}

//==========================================================================================================================================
//TODO : Flash news function
// Flash news on / off events.

function FlashOn() {
    $('#btnFlashOn').hide();
    $('#DMarquee').show();
    $('#btnFlashOff').show();
    setCookie('FlashNews', 'On', 1);
    $('#' + $('#popup2Trigger').attr('aria-describedby')).attr('class', 'popover fade bottom');
}

function FlashOff() {
    $('#DMarquee').hide();
    $('#btnFlashOff').hide();
    $('#btnFlashOn').show();
    setCookie('FlashNews', 'Off', 1);
    $('#' + $('#popup2Trigger').attr('aria-describedby')).attr('class', 'popover fade bottom');
}

//==========================================================================================================================================
//TODO : Logout function
// Clear cookie for logout function.

function Logout() {
    DeleteCookie('BrowserId');
    DeleteCookie('OfficerID');
    DeleteCookie('OfficerName');
    DeleteCookie('CompanyID');
    DeleteCookie('tgVal');
    DeleteCookie('aref');
    DeleteCookie('RoleType');
    DeleteCookie('bc');
    DeleteCookie('RankingType');
    DeleteCookie('ManagerID');
    DeleteCookie('Map');
    DeleteCookie('IsVedio');
    DeleteCookie('TreeLevel');
    DeleteCookie('Availability');
    DeleteCookie('ClampCaseNo');
    DeleteCookie('ClampOfficerID');
    DeleteCookie('ClientId');
    DeleteCookie('ClientName');
    DeleteCookie('ClientKey');
    DeleteCookie('TimelineOfficer');
    DeleteCookie('Page');
    DeleteCookie('FlashNews');
    DeleteCookie('MgrName');
    window.name = "";
    window.top.location.href = "sign-in.html";
}

function LogoutCancel() {
    $('#' + $('#popup3Trigger').attr('aria-describedby')).attr('class', 'popover fade bottom');
}

//==========================================================================================================================================
//TODO : Cookie function

function setCookie(c_name, value, exdays) {
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
    document.cookie = c_name + "=" + c_value;
}

function getCookie(c_name) {
    var i, x, y, ARRcookies = document.cookie.split(";");
    for (i = 0; i < ARRcookies.length; i++) {
        x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
        y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
        x = x.replace(/^\s+|\s+$/g, "");
        if (x == c_name) {
            return unescape(y);
        }
    }
}

function DeleteCookie(name) {
    document.cookie = name + '=; expires=Thu, 01-Jan-70 00:00:01 GMT;';
}
//==========================================================================================================================================
