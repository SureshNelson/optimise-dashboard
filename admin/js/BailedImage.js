﻿//==========================================================================================================================================
/* File Name: BailedImage.js */
/* File Created: oct 5, 2016 */
/* Created By  : R.Santhakumar */
//==========================================================================================================================================
var mID;

$(document).ready(function () {
    mID = $.session.get("OfficerID");

    $('#txtFromDate,#txtToDate').attr('readonly', 'readonly');

    $('#txtFromDate').datetimepicker({
        format: 'Y/m/d',
        onShow: function (ct) {
            this.setOptions({
                maxDate: $('#txtToDate').val() ? $('#txtToDate').val() : false
            })
        },
        timepicker: false
    });
    $('#txtToDate').datetimepicker({
        format: 'Y/m/d',
        onShow: function (ct) {
            this.setOptions({
                minDate: $('#txtFromDate').val() ? $('#txtFromDate').val() : false
            })
        },
        timepicker: false
    });

    $('#btnSearchBailed').click(function () {
        GetBailedImageSearchForAdmin();
    });

    $('#btnClearBailed').click(function () {
        $(':text').val('');
        $('#DBailedImage').hide();
    });
});

function InsertManagerGroupOfficer() {
    $('#DAddDialog').dialog("close");
    $('#DAlert').empty().attr('class', 'alert alert-success').append('<strong>Info!</strong> Case guidance added successfully').fadeIn(1).fadeOut(6000);
}

function AlertMessage(Title) {
    $('#DAddDialog').dialog({
        modal: true,
        resizable: false,
        width: "65%",
        "title": Title,
        buttons: {
            "Add": function () {
                InsertManagerGroupOfficer();
            }
        }
    })
}

function GetBailedImageSearchForAdmin() {
    var parameter = {};
    var FromDate = "", ToDate = "";
    if ($.trim($('#txtFromDate').val()) != "") {
        var dateF = $('#txtFromDate').val().split('/');
        FromDate = dateF[2] + '-' + dateF[1] + '-' + dateF[0];
    }
    if ($.trim($('#txtToDate').val()) != "") {
        var dateT = $('#txtToDate').val().split('/');
        ToDate = dateT[2] + '-' + dateT[1] + '-' + dateT[0];
    }

    parameter = parameterPush(parameter, "CaseNumber", $('#txtCaseNumber').val())
    parameter = parameterPush(parameter, "OfficerID", $('#txtOfficerID').val())
    parameter = parameterPush(parameter, "FromDate", FromDate)
    parameter = parameterPush(parameter, "ToDate", ToDate)
    $.ajax({
        type: 'GET',
        url: ServiceURL + 'api/v1/cases/BailedImageSearch',
        //?CaseNumber=' + $('#txtCaseNumber').val() +
        //    '&OfficerID=' + ($('#txtOfficerID').val() == '' ? 0 : $('#txtOfficerID').val()) +
        //    '&FromDate=' + $('#txtFromDate').val() + '&ToDate=' + $('#txtToDate').val(),
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        dataType: 'json',
        data: parameter,
        //data: {
        //    "CaseNumber": $('#txtCaseNumber').val(),
        //    "OfficerID": ($('#txtOfficerID').val() == '' ? 0 : $('#txtOfficerID').val()),
        //    "FromDate": $('#txtFromDate').val(),
        //    "ToDate": $('#txtToDate').val()
        //},
        beforeSend: function () {
            $('#tbodyBailed').empty();
            $('#DBailedImage').show();
        },
        success: function (data) {
            if (!(data == '' || data == null || data == '[]')) {
                $('#tblBailed').dataTable().fnClearTable();
                $('#tbBailedImage').show();
                $.each(data, function (key, value) {
                    $('#tbodyBailed').append('<tr><td>' + value.CaseNumber + '</td><td>' + value.Officer + '</td>' +
                                   '<td>' + value.DateActioned + '</td>' +
                                   '<td>' +
                                   '<a href="' + OptimiseImageUrl + '" target="_blank" ><img style="padding-right:10px;" src="images/ExpandImage_24_24_2.png" alt="Expand" title="Click to Expand image" onclick="ViewBailedImage(' + key + ',' + "'" + value.ImageURL + "'" + ')"/></a>' +
                                   '<img id="imgBailedImage' + key + '" src="images/ViewImage_24_24_2.png" class="BailedUnselect" alt="View" title="View bailed image" style="cursor:pointer;padding-right:10px;" onclick="ViewBailedImage(' + key + ',' + "'" + value.ImageURL + "'" + ')" />' +
                                   '</td></tr>');
                    if (key == 0) {
                        ViewBailedImage(key, "'" + value.ImageURL + "'");
                        $('#imgBailedImage' + key).attr('src', 'images/ViewImage_24_24_1.png').attr('class', 'BailedSelect');
                    }

                });
                $('#tblBailed').dataTable({
                    "sScrollY": "auto",
                    "bPaginate": true,
                    "bDestroy": true,
                    "bSort": false,
                    "sPaginationType": "full_numbers",
                    "aLengthMenu": [[5, 10, 50, 100, -1], [5, 10, 50, 100, "All"]],
                    "bLengthChange": false,
                    "bFilter": false,
                    "sInfo": true,
                    "iDisplayLength": 5
                });
            }
            else {
                $('#tbBailedImage').hide();
                $('#tblBailed').dataTable({
                    "sScrollY": "auto",
                    "bPaginate": false,
                    "bDestroy": true,
                    "bSort": false,
                    "sPaginationType": "full_numbers",
                    "aLengthMenu": [[5, 10, 50, 100, -1], [5, 10, 50, 100, "All"]],
                    "bLengthChange": false,
                    "bFilter": false,
                    "sInfo": true,
                    "iDisplayLength": 5
                });
            }
        },
        complete: function () {
        },
        error: function (xhr, textStatus, errorThrown) {
            alert('error');
        }
    });
}

function ViewBailedImage(key, BailedImageURL) {
    $.session.set('AdminImageType', 'B');
    $.session.set('AdminBailedImageURL', BailedImageURL);
    $('#iframeBailedImage').attr('src', OptimiseImageUrl);

    $('.BailedSelect').removeClass('BailedSelect').addClass('BailedUnselect').attr('src', 'images/ViewImage_24_24_2.png');

    if ($('#imgBailedImage' + key).attr('src') == 'images/ViewImage_24_24_2.png')
        $('#imgBailedImage' + key).attr('src', 'images/ViewImage_24_24_1.png').attr('class', 'BailedSelect');
    else
        $('#imgBailedImage' + key).attr('src', 'images/ViewImage_24_24_2.png').attr('class', 'BailedUnselect');
}

function DownloadBailedImage(ImageURL) {
    //window.location.href = ImageURL;
    window.open(ImageURL, '_blank');
}
